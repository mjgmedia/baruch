<?php

return [
    'recaptcha' => [
        'site' => env('RECAPTCHA_SITE_KEY'),
        'secret' => env('RECAPTCHA_SECRET_KEY'),
    ],

    'sentry' => [
        'enable' => env('SENTRY_ENABLED'),
    ],

    'admin' => [
        'name' => env('ADMIN_NAME'),
        'username' => env('ADMIN_USERNAME'),
        'email' => env('ADMIN_EMAIL'),
        'password' => env('ADMIN_PASSWORD'),
    ],

    'uploads' => [
        'avatar_dir' => env('UPLOAD_AVATAR_DIR', 'avatar/'),
        'avatar_disk' => env('UPLOAD_AVATAR_DISK', 'public'),
        'simple_pages_dir' => env('UPLOAD_SIMPLE_PAGES_DIR', 'simple-pages/'),
        'simple_pages_disk' => env('UPLOAD_SIMPLE_PAGES_DISK', 'public'),
        'file_dir' => env('UPLOAD_FILES_DIR', 'files'),
        'file_disk' => env('UPLOAD_FILES_DISK', 'public'),
    ],

    'settings' => [
        'push_notifications' => true,
        'test' => [
            'test1' => true,
        ],
        'team' => [
            'team_notify_about_new_members' => true,
            'team_notify_about_deleted_members' => true,
            'team_notify_member_profile_changed' => true,
            'team_notify_about_tasks_done' => true,
        ],
        'file' => [
            'file_notify_about_new_comments' => true,
            'file_notify_about_responses_to_comments' => true,
            'file_team_notify_about_open' => true,
            'file_happening_notify_about_open' => true,
        ],
        'happening' => [
            'happening_notify_about_new_members' => true,
            'happening_notify_about_deleted_members' => true,
            'happening_notify_about_new_teams' => true,
            'happening_notify_about_deleted_teams' => true,
            'happening_notify_member_profile_changed' => true,
        ],
    ],

    'settings_permissions' => [
        'test1' => 'edit_profile',
        'team_notify_about_new_members' => 'team_managers_notifications',
        'team_notify_about_deleted_members' => 'team_managers_notifications',
        'team_notify_member_profile_changed' => 'team_managers_notifications',
        'team_notify_about_tasks_done' => 'team_managers_notifications',
        'file_team_notify_about_open' => 'file_crud_team',
        'file_happening_notify_about_open' => 'file_crud_happening',
        'happening_notify_about_new_teams' => 'happening_crud',
        'happening_notify_about_deleted_teams' => 'happening_crud',
        'happening_notify_member_profile_changed' => 'happening_user_crud',
    ],

    'chat' => [
        'push_unread_delay' => env('CHAT_PUSH_UNREAD_DELAY', 30),
    ],

    'websockets' => [
        'super_admin_channel' => env('WEBSOCKETS_SUPERADMIN_CHANNEL', 'superadmins'),
    ],

    'mail_template' => [
        'fb_on' => env('MAIL_TEMPLATE_FB_TOGGLE', false),
        'fb_url' => env('MAIL_TEMPLATE_FB_URL'),
        'twitter_on' => env('MAIL_TEMPLATE_TWITTER_TOGGLE', false),
        'twitter_url' => env('MAIL_TEMPLATE_TWITTER_URL'),
        'logo' => env('MAIL_TEMPLATE_LOGO', ''),
    ],

    'activation_url' => env('ACTIVATION_URL'),
    'activation_enabled' => env('ACTIVATION_ENABLED', true),
];