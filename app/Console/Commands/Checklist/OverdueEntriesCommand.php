<?php

namespace App\Console\Commands\Checklist;

use App\Jobs\Checklist\OverdueEntriesJob;
use App\Models\Checklist\Checklist;
use Illuminate\Console\Command;

class OverdueEntriesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checklist:overdue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending notifications to users & admin of those checklist entries which are overdue.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $checklists = Checklist::whereHas(
            'entries',
            function ($query) {
                return $query->where(
                    [
                        ['notification_overdue', '=', false]
                    ]
                );
            }
        )->get();

        foreach ($checklists as $checklist) {
            OverdueEntriesJob::dispatch($checklist);
        }

        return 0;
    }
}
