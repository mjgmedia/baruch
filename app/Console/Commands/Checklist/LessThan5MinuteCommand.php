<?php

namespace App\Console\Commands\Checklist;

use App\Jobs\Checklist\LessThan5MinutesJob;
use App\Models\Checklist\Checklist;
use Illuminate\Console\Command;

class LessThan5MinuteCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checklist:five';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending notifications to admin & users which checklist entries are less then 5 minutes to time at task should be done';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $checklists = Checklist::whereHas(
            'entries',
            function ($query) {
                return $query->where(
                    [
                        ['notification_five', '=', false],
                    ]
                );
            }
        )->get();

        foreach ($checklists as $checklist) {
            LessThan5MinutesJob::dispatch($checklist);
        }

        return 0;
    }
}
