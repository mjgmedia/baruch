<?php

namespace App\Jobs\Chat;

use App\Models\Chat\ChatMessage;
use App\Notifications\Chat\UnreadMessagePushNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;

/**
 * Class PushUnreadMessageJob
 * @package App\Jobs\Chat
 */
class PushUnreadMessageJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var ChatMessage
     */
    public ChatMessage $message;

    /**
     * Create a new job instance.
     *
     * @param ChatMessage $message
     */
    public function __construct(ChatMessage $message)
    {
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->message->refresh();
        $users = $this->message->users()->wherePivot('read', null)->get();
        Notification::send($users, new UnreadMessagePushNotification($this->message));
    }
}
