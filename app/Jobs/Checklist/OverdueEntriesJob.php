<?php

namespace App\Jobs\Checklist;

use App\Models\Checklist\Checklist;
use App\Models\User\User;
use App\Notifications\Checklist\OverdueEntryAdminNotification;
use App\Notifications\Checklist\OverdueEntryUserNotification;
use App\Traits\Checklist\ManagersQueryTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;

/**
 * Class OverdueEntriesJob
 * @package App\Jobs\Checklist
 */
class OverdueEntriesJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;
    use ManagersQueryTrait;

    private Checklist $checklist;

    /**
     * Create a new job instance.
     *
     * @param Checklist $checklist
     */
    public function __construct(Checklist $checklist)
    {
        $this->checklist = $checklist;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = $this->queryManagers($this->checklist->happening_id, $this->checklist->team_id);

        $query = $this->checklist
            ->entries()
            ->where('notification_overdue', false)
            ->whereDate('should_done_at', '<=', now());

        foreach ($query->get() as $entry) {
            Notification::send($users, new OverdueEntryAdminNotification($entry));
            Notification::send($entry->users, new OverdueEntryUserNotification($entry));
        }

        $query->update(
            [
                'notification_overdue' => true
            ]
        );
    }
}
