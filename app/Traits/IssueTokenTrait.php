<?php

namespace App\Traits;

use App\Events\Auth\LoginFailedEvent;
use App\Events\Auth\OAuthFatalErrorEvent;
use App\Exceptions\Auth\AccountNotActivatedException;
use App\Exceptions\Auth\AccountNotFoundException;
use App\Exceptions\Auth\OAuthConnectException;
use App\Exceptions\Auth\OAuthFatalException;
use App\Exceptions\Auth\OAuthLoginException;
use App\Exceptions\Auth\OAuthRequestException;
use App\Models\User\User;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

/**
 * Trait IssueTokenTrait
 * @package App\Traits
 */
trait IssueTokenTrait
{
    /**
     * @param Request $request
     * @param $grantType
     * @param $scope
     * @return \Illuminate\Http\JsonResponse
     * @throws OAuthLoginException
     * @throws OAuthFatalException
     * @throws AccountNotActivatedException
     * @throws AccountNotFoundException
     * @throws OAuthRequestException
     * @throws OAuthConnectException
     */
    public function issueToken(Request $request, $grantType, $scope = '')
    {
        $user = null;


        $params = [
            'grant_type' => $grantType,
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'scope' => $scope
        ];

        switch ($grantType) {
            case 'password':
                $params['username'] = $request->username ?? $request->email;
                $params['password'] = $request->password;
                // early verification for non-existing or not activated accounts
                try {
                    $user = User::when(
                        $request->username,
                        function ($query) use ($request) {
                            $query->where('username', $request->username);
                        },
                        function ($query) use ($request) {
                            $query->where('email', $request->email);
                        }
                    )->firstOrFail();
                } catch (\Throwable $e) {
                    throw new AccountNotFoundException();
                }

                if ($user->email_verified_at == null) {
                    throw new AccountNotActivatedException();
                }

                break;
            case 'refresh_token':
                $params['refresh_token'] = $request->refresh_token;
                break;
        }

        $client = new \GuzzleHttp\Client();

        try {
            $response = $client->post(
                route('passport.token'),
                [
                    'form_params' => $params,
                    'timeout' => 10,
                ]
            );
        } catch (ClientException $e) {
            $response = $e->getResponse();
            if ($response->getStatusCode() == 400) {
                // Fire Event - saving this to separate logs
                event(new LoginFailedEvent($params['username'], $request->ip()));
                throw new OAuthLoginException();
            }
            // Fire Event 
            event(new OAuthFatalErrorEvent($response->getStatusCode(), $e->getMessage()));
            throw new OAuthFatalException($response->getStatusCode() . ': ' . $e->getMessage());
        } catch (ConnectException $e) {
            throw new OAuthConnectException($e);
        } catch (RequestException $e) {
            throw new OAuthRequestException($e);
        }

        $arrBody = json_decode((string)$response->getBody(), true);
        return response()->json($arrBody);
    }
}
