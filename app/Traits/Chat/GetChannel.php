<?php


namespace App\Traits\Chat;


use App\Exceptions\Chat\ChannelNotFoundException;
use App\Models\Chat\ChatChannel;

/**
 * Trait GetChannelTrait
 * @package App\Traits\Chat
 */
trait GetChannel
{
    /**
     * @param int $id
     * @param string|null $field
     * @return ChatChannel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     * @throws ChannelNotFoundException
     */
    private function _getChannel(int $id, ?string $field=null)
    {
        try {
            if ($field) {
                return ChatChannel::query()
                    ->where($field, $id)
                    ->firstOrFail();
            } else {
                return ChatChannel::findOrFail($id);
            }

        } catch (\Throwable $exception) {
            throw new ChannelNotFoundException($exception);
        }
    }
}