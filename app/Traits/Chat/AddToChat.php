<?php

namespace App\Traits\Chat;

use App\Exceptions\Chat\ChannelNotFoundException;
use App\Exceptions\Chat\ChannelUserAttachingException;
use App\Exceptions\Chat\MessagesUsersAttachingException;
use App\Models\Chat\ChatMessage;

/**
 * Trait AddToChatTrait
 * @package App\Traits\Chat
 */
trait AddToChat
{
    use GetChannel;

    /**
     * @param array $usersIds
     * @param int $id
     * @param string|null $field
     * @return array
     * @throws ChannelNotFoundException
     * @throws ChannelUserAttachingException
     */
    private function _addToChannel(array $usersIds, int $id, string $field=null): array
    {

        $channel = $this->_getChannel($id, $field);

        $status = $channel->users()->syncWithoutDetaching($usersIds);

        if ($status['attached'] <= 0) {
            throw new ChannelUserAttachingException($usersIds, $id, $field);
        }

        return $status;
    }

    /**
     * @param array $usersIds
     * @param int $id
     * @param string|null $field
     * @return array
     * @throws ChannelNotFoundException
     * @throws MessagesUsersAttachingException
     */
    private function _addToMessages(array $usersIds, int $id, string $field=null): array
    {
        $channel = $this->_getChannel($id, $field);
        $statuses = collect();

        ChatMessage::query()
            ->where('chat_channel_id', $channel->id)
            ->get()
            ->each(function ($message) use ($usersIds, $statuses) {
                $status = $message->users()->syncWithoutDetaching($usersIds);

                if ($status['attached'] <= 0) {
                    throw new MessagesUsersAttachingException($usersIds, $message);
                }

                /*
                 * setting all messages which users get access to as read
                 * otherwise there will be so much call to API from app
                 * that it could be kill server ro generate enormous of traffic
                 */
                foreach ($usersIds as $id) {
                    $message->users()->updateExistingPivot($id, ['read' => now()]);
                }

                $statuses->push($status);
            });

        return $statuses->toArray();
    }
}