<?php


namespace App\Traits\Chat;


use App\Exceptions\Chat\ChannelDeleteInServiceException;
use App\Exceptions\Chat\ChannelUserDetachException;
use App\Models\Chat\ChatMessage;
use Illuminate\Database\Eloquent\Builder;

/**
 * Trait DeleteTrait
 * @package App\Traits\Chat
 */
trait DeleteFromChat
{
    use GetChannel;

    /**
     * @param int $id
     * @param string|null $field
     * @return int
     * @throws ChannelDeleteInServiceException
     * @throws \App\Exceptions\Chat\ChannelNotFoundException
     */
    private function _deleteChatChannel(int $id, string $field=null): int
    {
        $channel = $this->_getChannel($id, $field);

        try {
            return $channel->delete();
        } catch (\Throwable $exception) {
            throw new ChannelDeleteInServiceException($exception);
        }
    }

    /**
     * @param int $id
     * @param array $userIds
     * @param string|null $field
     * @return array
     * @throws ChannelUserDetachException
     * @throws \App\Exceptions\Chat\ChannelNotFoundException
     */
    private function _deleteFromChat(int $id, array $userIds, string $field=null): array
    {
        $channel = $this->_getChannel($id, $field);

        $statuses = collect();

        // Detach from channel
        $status = $channel->users()->detach($userIds);

        if ($status <= 0) {
            throw new ChannelUserDetachException($channel, 'channel');
        }
        $statuses->push($status);

        // Detach from messages
        $messages = ChatMessage::query()
            ->where('chat_channel_id', $channel->id)
            ->whereHas(
                'users',
                function (Builder $query) use ($userIds) {
                    return $query->whereIn('user_id', $userIds);
                }
            )
            ->get();

        if ($messages->count() > 0) {
            $messages->each(
                function ($item) use ($userIds, $statuses) {
                    $status = $item->users()->detach($userIds);

                    if ($status < 0) {
                        throw new ChannelUserDetachException($item, 'message-users');
                    }
                    $statuses->push($status);
                }
            );
        }


        $userMessages = ChatMessage::query()
            ->where('chat_channel_id', '=', $channel->id)
            ->whereIn('author_id', $userIds);
        if ($userMessages->count() > 0) {
            // Delete user's messages
            $status = $userMessages->delete();

            if ($status <= 0) {
                throw new ChannelUserDetachException($channel, 'messages');
            }
            $statuses->push($status);
        }

        return $statuses->toArray();
    }
}