<?php

namespace App\Traits\Checklist;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Collection;

/**
 * Trait ManagersQueryTrait
 * @package App\Traits\Checklist
 */
trait ManagersQueryTrait
{
    /**
     * @param int $happeningId
     * @param int|null $teamId
     * @return Collection
     */
    private function queryManagers(int $happeningId, int $teamId = null):Collection
    {
        return User::query()
            ->when(
                !empty($teamId),
                function ($query) use ($teamId) {
                    return $query->orWhereHas(
                        'teamManager',
                        function ($query) use ($teamId) {
                            return $query->where('team_id', $teamId);
                        }
                    );
                }
            )->whereHas(
                'happeningManager',
                function ($query) use ($happeningId) {
                    return $query->where('happening_id', $happeningId);
                }
            )->get();
    }
}