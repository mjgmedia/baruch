<?php

namespace App\Traits\Checklist;

/**
 * Trait UsersToStringTrait
 * @package App\Traits\Checklist
 */
trait UsersToStringTrait
{
    /**
     * @return string
     */
    private function users(): string
    {
        $strUsers = '';
        $this->entry->users->each(function ($item) use ($strUsers) {
            $strUsers = $strUsers.$item->name.', ';
        });
        return $strUsers;
    }
}