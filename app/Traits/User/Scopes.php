<?php

namespace App\Traits\User;

use Illuminate\Database\Eloquent\Builder;

/**
 * Trait Scopes
 * @package App\Traits\User
 */
trait Scopes
{
    /**
     * @param $query
     * @param $ability
     */
    public function scopeWhereCan($query, $ability)
    {
        $query->where(
            function ($query) use ($ability) {
                // direct
                $query->whereHas(
                    'abilities',
                    function ($query) use ($ability) {
                        $query->byName($ability);
                    }
                );
                // through roles
                $query->orWhereHas(
                    'roles',
                    function ($query) use ($ability) {
                        $query->whereHas(
                            'abilities',
                            function ($query) use ($ability) {
                                $query->byName($ability);
                            }
                        );
                    }
                );
            }
        );
    }

    /**
     * @param Builder $query
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function scopeOnline(Builder $query)
    {
        return $query->whereTime('last_activity', '>=', now()->subMinute());
    }

    /**
     * @param Builder $query
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function scopeOffline(Builder $query)
    {
        return $query->whereTime('last_activity', '<=', now()->subMinute());
    }

    /**
     * @param Builder $query
     * @param string $settingKey
     * @return Builder
     */
    public function scopeOnSetting(Builder $query, string $settingKey)
    {
        return $query->whereHas('settings', function ($query) use ($settingKey) {
            return $query->whereHas('setting', function ($innerQuery) use ($settingKey) {
                return $innerQuery->where('key', '=', $settingKey);
            })
                ->where('value', true);
        });
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeWebpush(Builder $query)
    {
        return $query->where('webpush', true);
    }

    /**
     * @param Builder $query
     * @param int $teamId
     * @return Builder
     */
    public function scopeIsTeamMember(Builder $query, int $teamId)
    {
        return $query->whereHas('teams', function ($query) use ($teamId) {
            return $query->where('team_id', $teamId);
        });
    }

    /**
     * @param Builder $query
     * @param int $teamId
     * @return Builder
     */
    public function scopeIsNotTeamMember(Builder $query, int $teamId)
    {
        return $query->whereDoesntHave('teams', function ($query) use ($teamId) {
            return $query->where('team_id', $teamId);
        });
    }

    /**
     * @param Builder $query
     * @param int $teamId
     * @return Builder
     */
    public function scopeIsTeamManager(Builder $query, int $teamId)
    {
        return $query->whereHas('teamManager', function ($query) use ($teamId) {
            return $query->where('team_id', $teamId);
        });
    }

    /**
     * @param Builder $query
     * @param int $happeningId
     * @return Builder
     */
    public function scopeIsHappeningMember(Builder $query, int $happeningId)
    {
        return $query->whereHas('happenings', function ($query) use ($happeningId) {
            return $query->where('happening_id', $happeningId);
        });
    }

    /**
     * @param Builder $query
     * @param int $happeningId
     * @return Builder
     */
    public function scopeIsNotHappeningMember(Builder $query, int $happeningId)
    {
        return $query->whereDoesntHave('happenings', function ($query) use ($happeningId) {
            return $query->where('happening_id', $happeningId);
        });
    }

    /**
     * @param Builder $query
     * @param int $happeningId
     * @return Builder
     */
    public function scopeIsHappeningManager(Builder $query, int $happeningId)
    {
        return $query->whereHas('happenings', function ($query) use ($happeningId) {
            return $query->where('happening_id', $happeningId);
        });
    }
}
