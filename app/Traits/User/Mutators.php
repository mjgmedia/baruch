<?php

namespace App\Traits\User;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;

/**
 * Trait Mutators
 * @package App\Traits\User
 */
trait Mutators
{
    /**
     * Get All Permissions Attribute
     *
     */
    public function getAllPermissionsAttribute()
    {
        return $this->getAllPermissions()->pluck('name')->toArray();
    }

    /**
     * Get Can Attribute
     *
     */
    public function getAllRolesAttribute()
    {
        return $this->getRoleNames();
    }

    /**
     * Get Can Attribute
     *
     */
    public function getCanAttribute()
    {
        $permissions = [];

        foreach (Permission::all() as $permission) {
            if ($this->can($permission->name)) {
                $permissions[$permission->name] = true;
            } else {
                $permissions[$permission->name] = false;
            }
        }

        return $permissions;
    }

    /**
     * Add Default Avatar
     * @param $value
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getPhotoUrlAttribute($value)
    {
        return empty($value) ? sprintf(
            "https://www.gravatar.com/avatar/%s.jpg?s=200&d=mm",
            md5(Str::lower($this->email))
        ) : url($value);
    }

    /**
     * Set Email To Correct Format
     * @param $email
     */
    public function setEmailAttribute($email)
    {
        $this->attributes['email'] = $email;
    }

    /**
     * Set Password Hash
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    /**
     * Set Username To Correct Format
     * @param $username
     */
    public function setUsernameAttribute($username)
    {
        $this->attributes['username'] = strtolower(str_replace(' ', '_', $username));
    }
}
