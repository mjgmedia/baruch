<?php

namespace App\Traits\User;

use App\Models\User\Profile;
use App\Models\User\SocialAccount;

/**
 * Trait Relationships
 * @package App\Traits\User
 */
trait Relationships
{
    /**
     * Profile Relationship
     *
     */
    public function profile()
    {
        return $this->hasOne(Profile::class)->withDefault();
    }

    /* Chat module relationships */

    /**
     * @return  \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function chatChannels()
    {
        return $this->belongsToMany(
            '\App\Models\Chat\ChatChannel',
            'chat_channel_user',
            'user_id',
            'chat_channel_id'
        )
            ->withTimestamps()
            ->using('\App\Models\Chat\ChatChannelUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sentChatMessages()
    {
        return $this->hasMany(
            '\App\Models\Chat\ChatMessage',
            'user_id',
            'id'
        );
    }

    /**
     * @return  \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function receivedChatMessages()
    {
        return $this->belongsToMany(
            '\App\Models\Chat\ChatMessage',
            'chat_channel_messages',
            'user_id',
            'chat_messages_id'
        )
            ->withTimestamps()
            ->withPivot('read')
            ->using('\App\Models\Chat\ChatChannelUser');
    }

    /**
     *  Settings
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function settings()
    {
        return $this->hasMany(
            '\App\Models\User\SettingUser',
            'user_id',
            'id'
        );
    }

    /**
     * Social Account Relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function socialAccounts()
    {
        return $this->hasMany(SocialAccount::class);
    }

    /* Teams module relationships */

    /**
     * @return  \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teams()
    {
        return $this->belongsToMany(
            '\App\Models\Team\Team',
            'team_user',
            'user_id',
            'team_id'
        )
            ->withTimestamps()
            ->using('\App\Models\Team\TeamUser');
    }

    /**
     * @return  \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teamManager()
    {
        return $this->belongsToMany(
            '\App\Models\Team\Team',
            'team_manager',
            'user_id',
            'team_id'
        )
            ->withTimestamps()
            ->using('\App\Models\Team\TeamManager');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function authoredTeamPosts()
    {
        return $this->hasMany(
            '\App\Models\Team\TeamPost',
            'author_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function authoredTeamPostComments()
    {
        return $this->hasMany(
            '\App\Models\Team\TeamPostComment',
            'author_id',
            'id'
        );
    }

    /* Happening relationships */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function happenings()
    {
        return $this->belongsToMany(
            '\App\Models\Happening\Happening',
            'happening_user',
            'user_id',
            'happening_id'
        )
            ->withTimestamps()
            ->using('\App\Models\Happening\HappeningUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function happeingManager()
    {
        return $this->belongsToMany(
            '\App\Models\Happening\Happening',
            'happening_manager',
            'user_id',
            'happening_id'
        )
            ->withTimestamps()
            ->using('\App\Models\Happening\HappeningManager');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function authoredHappeningPosts()
    {
        return $this->hasMany(
            '\App\Models\Happening\HappeningPost',
            'author_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function authoredHappeningPostComments()
    {
        return $this->hasMany(
            '\App\Models\Happening\HappeningPostComment',
            'author_id',
            'id'
        );
    }

    /* Checklist */
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function authoredChecklists()
    {
        return $this->hasMany(
            '\App\Models\Checklist\Checklist',
            'author_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function checklistEntries()
    {
        return $this->belongsToMany(
            '\App\Models\Checklist\ChecklistEntry',
            'checklist_entry_user',
            'user_id',
            'checklist_entry_id'
        )
            ->withTimestamps()
            ->using('\App\Models\Checklist\ChecklistEntryUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function authoredChecklistEntries()
    {
        return $this->hasMany(
            '\App\Models\Checklist\ChecklistEntry',
            'author_id',
            'id'
        );
    }

    /* Files */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function authoredFiles()
    {
        return $this->hasMany(
            '\App\Models\File\File',
            'author_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function displayedFiles()
    {
        return $this->belongsToMany(
            '\App\Models\File\File',
            'file_user',
            'file_id',
            'user_id'
        )
            ->withTimestamps()
            ->withPivot('read')
            ->using('\App\Models\File\FileUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function authoredFileCategories()
    {
        return $this->hasMany(
            '\App\Models\File\FileCategory',
            'author_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fileAccesses()
    {
        return $this->hasMany(
            '\App\Models\File\FileAccess',
            'user_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fileComments()
    {
        return $this->hasMany(
            '\App\Models\File\FileComment',
            'author_id',
            'id'
        );
    }
}
