<?php

namespace App\Traits\User;

use Illuminate\Database\Query\Builder;

/**
 * Trait Methods
 * @package App\Traits\User
 */
trait Methods
{
    /**
     * Add findByEmail Method
     * @param $email
     * @return
     */
    public static function findByEmail($email)
    {
        return self::where('email', $email)->firstOrFail();
    }

    /**
     * Add findByUsername Method
     * @param $username
     * @return
     */
    public static function findByUsername($username)
    {
        return self::where('username', $username)->first();
    }

    /**
     * Add findForPassport Method
     * @param $identifier
     * @return
     */
    public function findForPassport($identifier)
    {
        return $this->orWhere('email', $identifier)->orWhere('username', $identifier)->first();
    }

    /**
     * Add isAdmin Method
     * @return string|int|array|\Spatie\Permission\Contracts\Role|\Illuminate\Support\Collection
     */
    public function isAdmin()
    {
        return $this->hasRole('admin');
    }

    /**
     * Add isSuperAdmin Method
     * @return string|int|array|\Spatie\Permission\Contracts\Role|\Illuminate\Support\Collection
     */
    public function isSuperAdmin()
    {
        return $this->hasRole('super-admin');
    }

    /**
     * @param string $settingKey
     * @return bool
     */
    public function hasSettingOn(string $settingKey): bool
    {
        try {
            $setting = $this->settings()
                ->whereHas('setting', function (Builder $query) use ($settingKey) {
                    return $query->where('key', $settingKey);
                })
                ->firstOrFail();

            return $setting->value;
        } catch (\Throwable $e) {
            sentrit($e);

            return false;
        }
    }

    /**
     * Add Last Method
     */
    public static function last()
    {
        return self::latest()->first();
    }
}
