<?php

namespace App\Traits\File;

use App\Exceptions\File\FileStoreException;
use App\Exceptions\File\UploadException;
use App\Models\File\File;
use Illuminate\Http\Request;

/**
 * Trait UploadTrait
 * @package App\Traits\File
 */
trait UploadTrait
{
    /**
     * Requires UploadSerivce
     * @param Request $request
     * @param File $file
     * @throws FileStoreException
     * @throws UploadException
     */
    private function handleUpload(Request $request, File $file)
    {
        try {
            $file->filename = $this->upload->handleFileUpload(
                $request,
                'file',
                config('project.uploads.file_dir'),
                config('project.upload.file_disk')
            );
        } catch (\Throwable $exception) {
            throw new UploadException($exception);
        }
        try {
            $file->saveOrFail();
        } catch (\Throwable $exception) {
            throw new FileStoreException($exception);
        }
    }
}