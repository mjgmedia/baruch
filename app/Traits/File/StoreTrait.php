<?php

namespace App\Traits\File;

use App\Exceptions\File\NoPermissionToCrudException;
use App\Exceptions\File\NoPermissionToGlobalCrudException;
use App\Models\Happening\Happening;
use App\Models\Team\Team;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

/**
 * Trait StoreTrait
 * @package App\Traits\File
 */
trait StoreTrait
{
    /**
     * Requires FileService in class
     *
     * @param FormRequest $request
     * @throws NoPermissionToCrudException
     * @throws NoPermissionToGlobalCrudException
     */
    private function isAllowedToStoreFile(FormRequest $request)
    {
        if ($request->validated()['global'] && Gate::denies('file-global-crud')) {
            throw new NoPermissionToGlobalCrudException(Auth::user());
        }
        $model = $request->validated()['team_id'] != null ? Team::findOrFail(
            $request->validated()['team_id']
        ) : Happening::findOrFail($request->validated()['happening_id']);
        if (!$this->service->verifyCrud($model)) {
            throw new NoPermissionToCrudException(Auth::user());
        }
    }
}