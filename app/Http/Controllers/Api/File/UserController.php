<?php

namespace App\Http\Controllers\Api\File;

use App\Exceptions\File\UserOpenedFileUpdateException;
use App\Http\Controllers\Controller;
use App\Http\Requests\File\UserOpenFileRequest;
use App\Models\File\File;
use App\Services\Interfaces\FileServiceInterface;

/**
 * Class UserController
 * @package App\Http\Controllers\Api\File
 */
class UserController extends Controller
{
    /**
     * @var FileServiceInterface
     */
    private FileServiceInterface $service;

    /**
     * UserController constructor.
     * @param $service
     */
    public function __construct(FileServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserOpenFileRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws UserOpenedFileUpdateException
     */
    public function store(UserOpenFileRequest $request)
    {
        $file = File::query()
            ->with('displayed')
            ->where('id', $request->validated()['file_id'])
            ->userHaveAccess($request->user()->id)
            ->firstOrFail();

        $status = $file->displayed()->updateExistingPivot($request->user()->id, ['read' => true]);
        if ($status > 0) {
            return response()->json(
                [
                    'message' => __(''),
                    'code' => 2014,
                    'status' => $status
                ],
                201
            );
        }

        throw new UserOpenedFileUpdateException($status, $file);
    }
}
