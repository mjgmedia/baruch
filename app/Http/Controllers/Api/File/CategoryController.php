<?php

namespace App\Http\Controllers\Api\File;

use App\Exceptions\File\CategoryDeleteException;
use App\Exceptions\File\CategoryUpdateException;
use App\Exceptions\File\NoPermissionForCategoryException;
use App\Http\Controllers\Controller;
use App\Http\Requests\File\CategoryRequest;
use App\Http\Resources\File\FileCategoryResource;
use App\Models\File\FileCategory;
use App\Models\Happening\Happening;
use App\Models\Team\Team;
use App\Services\Interfaces\FileServiceInterface;
use App\Traits\File\StoreTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

/**
 * Class CategoryController
 * @package App\Http\Controllers\Api\File
 */
class CategoryController extends Controller
{
    use StoreTrait;

    /**
     * @var FileServiceInterface
     */
    private FileServiceInterface $service;

    /**
     * CategoryController constructor.
     * @param $service
     */
    public function __construct(FileServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $id
     * @param bool $isTeam
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(int $id, bool $isTeam = false)
    {
        $this->service->isAllowedToCategory($id, $isTeam);

        $data = FileCategory::query()
            ->when(
                $isTeam,
                function ($query) use ($id) {
                    $query->where('team_id', $id);
                },
                function ($query) use ($id) {
                    $query->where('happening_id', $id);
                }
            )
            ->get();

        return FileCategoryResource::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\File\NoPermissionToCrudException
     * @throws \App\Exceptions\File\NoPermissionToGlobalCrudException
     */
    public function store(CategoryRequest $request)
    {
        $this->isAllowedToStoreFile($request);

        $category = FileCategory::create($request->validated());

        return response()->json(
            [
                'message' => __(''),
                'code' => 2011,
                'data' => FileCategoryResource::make($category)
            ],
            201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param FileCategory $category
     * @return FileCategoryResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(FileCategory $category)
    {
        $this->authorize('view', $category);

        return FileCategoryResource::make($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryRequest $request
     * @param FileCategory $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws CategoryUpdateException
     */
    public function update(CategoryRequest $request, FileCategory $category)
    {
        $this->authorize('crud', $category);

        $status = $category->update($request->validated());

        if ($status > 0) {
            return response()->json(
                [
                    'message' => __(''),
                    'code' => 3017,
                    'status' => $status,
                    'data' => FileCategoryResource::make($category)
                ],
                200
            );
        }

        throw new CategoryUpdateException(
            $request->toArray(),
            'Błąd aktualizacji kategorii plików'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param FileCategory $category
     * @return \Illuminate\Http\JsonResponse
     * @throws CategoryDeleteException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(FileCategory $category)
    {
        $this->authorize('crud', $category);

        try {
            $status = $category->delete();
        } catch (\Exception $e) {
            throw new CategoryDeleteException($e);
        }

        return response()->json(
            [
                'message' => __(''),
                'code' =>4011 ,
                'status' => $status
            ],
            200
        );
    }
}
