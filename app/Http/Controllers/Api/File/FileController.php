<?php

namespace App\Http\Controllers\Api\File;

use App\Exceptions\File\FileDeleteException;
use App\Exceptions\File\FileStoreException;
use App\Exceptions\File\FileUpdateException;
use App\Exceptions\File\NoPermissionToCrudException;
use App\Exceptions\File\NoPermissionToGlobalCrudException;
use App\Exceptions\File\NoPermissionToViewFileException;
use App\Exceptions\File\UploadException;
use App\Http\Controllers\Controller;
use App\Http\Requests\File\FileRequest;
use App\Http\Requests\File\FileUpdateRequest;
use App\Http\Resources\File\FileResource;
use App\Models\File\File;
use App\Services\Interfaces\FileServiceInterface;
use App\Services\Interfaces\UploadServiceInterface;
use App\Traits\File\StoreTrait;
use App\Traits\File\UploadTrait;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Auth;

/**
 * Class FileController
 * @package App\Http\Controllers\Api\File
 */
class FileController extends Controller
{
    use UploadTrait;
    use StoreTrait;

    /**
     * @var FileServiceInterface
     */
    private FileServiceInterface $service;
    /**
     * @var UploadServiceInterface
     */
    private UploadServiceInterface $upload;

    /**
     * FileController constructor.
     * @param FileServiceInterface $service
     * @param UploadServiceInterface $upload
     */
    public function __construct(FileServiceInterface $service, UploadServiceInterface $upload)
    {
        $this->service = $service;
        $this->upload = $upload;
    }

    /**
     * Store a newly created resource in storage.
     * Warning: File model contains a different name for field which is storing filename
     * and different field is suing in FormReuest. It happens due to update method below
     * which is using Model->update() method. If field names will be same named - it would
     * be a problem. Of course, we can use same named fields here,m an others there, but
     * consistency of whole Controller is higher priority.
     *
     * @param FileRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws FileStoreException
     * @throws UploadException
     * @throws NoPermissionToCrudException
     * @throws NoPermissionToGlobalCrudException
     */
    public function store(FileRequest $request)
    {
        $this->isAllowedToStoreFile($request);

        $file = new File();
        $file->fill($request->validated());
        $this->handleUpload($request, $file);
        $file->save();

        return response()->json(
            [
                'message' => __(''),
                'code' => 2013,
                'data' => FileResource::make($file)
            ],
            201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param File $file
     * @return FileResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws NoPermissionToViewFileException
     */
    public function show(File $file)
    {
        try {
            $this->authorize('view', $file);
        } catch (AuthorizationException $e) {
            throw new NoPermissionToViewFileException(Auth::user());
        }

        return FileResource::make($file);
    }

    /**
     * Update the specified resource in storage.
     * This method does not change team_id or happening_id
     * those fields are not in FormRequest
     *
     * @param FileUpdateRequest $request
     * @param File $file
     * @return \Illuminate\Http\JsonResponse
     * @throws FileStoreException
     * @throws UploadException
     * @throws NoPermissionToCrudException
     * @throws FileUpdateException
     */
    public function update(FileUpdateRequest $request, File $file)
    {
        try {
            $this->authorize('crud', $file);
        } catch (AuthorizationException $e) {
            throw new NoPermissionToCrudException(Auth::user());
        }

        $status = $file->update($request->validated());

        if ($status > 0) {
            if ($request->validated()['file']) {
                $this->handleUpload($request, $file);
            }

            return response()->json(
                [
                    'message' => __(''),
                    'code' => 3019,
                    'status' => $status,
                    'data' => FileResource::make($file)
                ],
                200
            );
        }

        throw new FileUpdateException(
            $request->toArray(),
            'Błąd podczas aktualizacji pliku'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param File $file
     * @return \Illuminate\Http\JsonResponse
     * @throws FileDeleteException
     * @throws NoPermissionToCrudException
     */
    public function destroy(File $file)
    {
        try {
            $this->authorize('crud', $file);
        } catch (AuthorizationException $e) {
            throw new NoPermissionToCrudException(Auth::user());
        }

        try {
            $status = $file->delete();
        } catch (\Exception $e) {
            throw new FileDeleteException($e);
        }

        return response()->json(
            [
                'message' => __(''),
                'code' => 4013,
                'status' => $status
            ],
            200
        );
    }
}
