<?php

namespace App\Http\Controllers\Api\File;

use App\Exceptions\File\CommentDeleteException;
use App\Exceptions\File\CommentUpdateException;
use App\Http\Controllers\Controller;
use App\Http\Requests\File\CommentRequest;
use App\Http\Resources\File\FileCommentResource;
use App\Models\File\File;
use App\Models\File\FileComment;
use App\Services\Interfaces\FileServiceInterface;

/**
 * Class CommentController
 * @package App\Http\Controllers\Api\File
 */
class CommentController extends Controller
{
    /**
     * @var FileServiceInterface
     */
    private FileServiceInterface $service;

    /**
     * CommentController constructor.
     * @param $service
     */
    public function __construct(FileServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param File $file
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(File $file)
    {
        $this->authorize('view', $file);

        $file->load('comments');

        return FileCommentResource::collection($file->comments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CommentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CommentRequest $request)
    {
        $this->service->isAllowedToComment($request->validated()['file_id']);

        $comment = FileComment::create($request->validated());

        return response()->json(
            [
                'message' => __(''),
                'code' => 2012,
                'data' => FileCommentResource::make($comment)
            ],
            201
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CommentRequest $request
     * @param FileComment $comment
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws CommentUpdateException
     */
    public function update(CommentRequest $request, FileComment $comment)
    {
        $this->authorize('update', $comment);

        $status = $comment->update($request->validated());

        if ($status > 0) {
            return response()->json(
                [
                    'message' => __(''),
                    'code' => 3018,
                    'status' => $status,
                    'data' => FileCommentResource::make($comment)
                ],
                200
            );
        }

        throw new CommentUpdateException(
            $request->toArray(),
            'Błąd podczas aktualizacji komentarza do pliku'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param FileComment $comment
     * @return \Illuminate\Http\JsonResponse
     * @throws CommentDeleteException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(FileComment $comment)
    {
        $this->authorize('delete', $comment);

        try {
            $status = $comment->delete();
        } catch (\Exception $e) {
            throw new CommentDeleteException($e);
        }

        return response()->json(
            [
                'message' => __(''),
                'code' => 4012,
                'status' => $status
            ],
            200
        );
    }
}
