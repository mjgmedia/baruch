<?php

namespace App\Http\Controllers\Api\File;

use App\Exceptions\File\NoPermissionToGlobalFilesException;
use App\Http\Controllers\Controller;
use App\Http\Resources\File\FileResource;
use App\Models\File\File;
use App\Models\File\FileAccess;
use App\Models\Happening\Happening;
use App\Models\Team\Team;
use App\Services\Interfaces\FileServiceInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Spatie\Permission\Models\Role;

/**
 * Class AccessController
 * @package App\Http\Controllers\Api\File
 */
class AccessController extends Controller
{
    /**
     * @var FileServiceInterface
     */
    private FileServiceInterface $service;

    /**
     * AccessController constructor.
     * @param $service
     */
    public function __construct(FileServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @return Builder
     */
    private function query(): Builder
    {
        return FileAccess::query()
            ->where('user_id', Auth::id());
    }

    /**
     * @return Collection
     */
    private function getTeamFilesIds(): Collection
    {
        return $this->query()
            ->whereIn('happening_id', Happening::query()
                ->isMember(Auth::id())
                ->pluck('id'))
            ->select(['id', 'file_id'])
            ->pluck('file_id');
    }

    /**
     * @return Collection
     */
    private function getHappeningFilesIds(): Collection
    {
        return $this->query()
            ->whereIn('team_id', Team::query()
                ->isMember(Auth::id())
                ->pluck('id'))
            ->select(['id', 'file_id'])
            ->pluck('file_id');
    }

    /**
     * @return Collection
     */
    private function getRoleFilesIds(): Collection
    {
        $myRoles = Auth::user()->roles()->pluck('id')->toArray();
        return $this->query()
            ->whereIn('role_id', $myRoles)
            ->select(['id', 'file_id'])
            ->pluck('file_id');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function all()
    {
        $totalIdsCollection = $this
            ->getTeamFilesIds()
            ->merge($this->getHappeningFilesIds())
            ->merge($this->getRoleFilesIds());

        $data = File::query()
            ->whereIn('id', $totalIdsCollection->toArray())
            ->get();

        return FileResource::collection($data);
    }

    /**
     * @param Request $request
     * @param Happening $happening
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function happening(Request $request, Happening $happening = null)
    {
        if ($happening === null) {
            $data = File::query()
                ->whereIn('id', $this->getTeamFilesIds($request))
                ->get();
        } else {
            $data = File::query()
                ->whereIn('id', $this->query()
                    ->where('happening_id', $happening->id)
                    ->select(['id', 'file_id'])
                    ->get()
                    ->pluck('file_id'))
                ->get();
        }

        return FileResource::collection($data);
    }

    /**
     * @param Request $request
     * @param Team $team
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function team(Request $request, Team $team = null)
    {
        if ($team === null) {
            $data = File::query()
                ->whereIn('id', $this->getHappeningFilesIds($request))
                ->get();
        } else {
            $data = File::query()
                ->whereIn('id', $this->query()
                    ->where('team_id', $team->id)
                    ->select(['id', 'file_id'])
                    ->get()
                    ->pluck('file_id'))
                ->get();
        }

        return FileResource::collection($data);
    }

    /**
     * @param Request $request
     * @param Role $role
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function role(Request $request, Role $role = null)
    {
        if ($role === null) {
            $data = File::query()
                ->whereIn('id', $this->getRoleFilesIds($request))
                ->get();
        } else {
            $data = File::query()
                ->whereIn('id', $this->query()
                    ->where('role_id', $role->id)
                    ->select(['id', 'file_id'])
                    ->get()
                    ->pluck('file_id'))
                ->get();
        }

        return FileResource::collection($data);
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws NoPermissionToGlobalFilesException
     */
    public function globally()
    {
        if (Gate::denies('file-global-list')) {
            throw new NoPermissionToGlobalFilesException(Auth::user());
        }

        $data = File::query()
            ->where('global', true)
            ->get();

        return FileResource::collection($data);
    }
}
