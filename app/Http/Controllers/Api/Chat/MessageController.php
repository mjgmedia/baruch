<?php

namespace App\Http\Controllers\Api\Chat;

use App\Events\Chat\DeletedMessageEvent;
use App\Events\Chat\NewMessageEvent;
use App\Events\Chat\UpdatedMessageEvent;
use App\Exceptions\Chat\MessageDeleteException;
use App\Exceptions\Chat\MessageUpdateException;
use App\Exceptions\Chat\WrongChannelException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Chat\StoreMessageRequest;
use App\Http\Resources\Chat\MessageResource;
use App\Models\Chat\ChatChannel;
use App\Models\Chat\ChatMessage;

/**
 * Class MessageController
 * @package App\Http\Controllers\Api\Chat
 */
class MessageController extends Controller
{
    /**
     * @param ChatChannel $channel
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(ChatChannel $channel)
    {
        $this->authorize('show', $channel);

        $messages = ChatMessage::query()
            ->latest()
            ->paginate(50);

        return MessageResource::collection(
            $messages
        );
    }

    /**
     * @param StoreMessageRequest $request
     * @param ChatChannel $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws WrongChannelException
     */
    public function store(StoreMessageRequest $request, ChatChannel $channel)
    {
        $this->authorize('show', $channel);

        if ($channel->id !== $request->validated()['chat_channel_id']) {
            throw new WrongChannelException($request->user(), $channel, $request->toArray());
        }

        $message = ChatMessage::create($request->validated());
        // creation of entries in users() by coping ids from channel is handled in Observer

        event(new NewMessageEvent($message, 'chat_'.$channel->id));

        // This is unique type of answer - due to Chat module need to be extremely lightweight
        return response()->json(
            [
                'code' => 2004,
            ],
            201
        );
    }

    /**
     * @param StoreMessageRequest $request
     * @param ChatMessage $message
     * @return \Illuminate\Http\JsonResponse
     * @throws MessageUpdateException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(StoreMessageRequest $request, ChatMessage $message)
    {
        $this->authorize('updateOrDelete', $message);

        $status = $message->update($request->validated());

        if ($status > 0) {
            event(new UpdatedMessageEvent($message, 'chat_'.$message->channel_id));

            // Update response need to contain a message due to show info to User about success of operation
            return response()->json(
                [
                    'message' => __(''),
                    'code' => 3011,
                    'status' => $status,
                ],
                200
            );
        }

        throw new MessageUpdateException(
            $request->toArray(),
            'Błąd podczas aktualizacji wiadomości na chatcie'
        );
    }

    /**
     * @param ChatMessage $message
     * @return \Illuminate\Http\JsonResponse
     * @throws MessageDeleteException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(ChatMessage $message)
    {
        $this->authorize('updateOrDelete', $message);

        try {
            $status = $message->delete();

            event(new DeletedMessageEvent($message->id, 'chat_'.$message->channel_id));
        } catch (\Throwable $exception) {
            throw new MessageDeleteException($exception);
        }

        // Delete response need to contain a message due to show info to User about success of operation
        return response()->json(
            [
                'status' => $status,
                'message' => __(''),
                'code' => 4004,
            ],
            200
        );
    }
}
