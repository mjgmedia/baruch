<?php

namespace App\Http\Controllers\Api\Chat;

use App\Exceptions\Chat\ChannelDeleteException;
use App\Exceptions\Chat\ChannelUpdateException;
use App\Exceptions\Chat\HappeningChannelTakeException;
use App\Exceptions\Chat\TeamChannelTakenException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Chat\StoreChannelRequest;
use App\Http\Resources\Chat\ChannelResource;
use App\Models\Chat\ChatChannel;
use App\Models\Happening\Happening;
use App\Models\Team\Team;
use App\Services\Interfaces\HappeningServiceInterface;
use App\Services\Interfaces\TeamServiceInterface;
use Illuminate\Support\Facades\Auth;
use Throwable;

/**
 * Class ChannelController
 * @package App\Http\Controllers\Api\Chat
 */
class ChannelController extends Controller
{
    /**
     * @var HappeningServiceInterface
     */
    private HappeningServiceInterface $happeningService;
    /**
     * @var TeamServiceInterface
     */
    private TeamServiceInterface $teamService;

    /**
     * ChannelController constructor.
     * @param HappeningServiceInterface $happeningService
     * @param TeamServiceInterface $teamService
     */
    public function __construct(HappeningServiceInterface $happeningService, TeamServiceInterface $teamService)
    {
        $this->happeningService = $happeningService;
        $this->teamService = $teamService;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return ChannelResource::collection(
            ChatChannel::query()
                ->isMember(Auth::id())
                ->get()
        );
    }

    /**
     * @param ChatChannel $channel
     * @return ChannelResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(ChatChannel $channel)
    {
        $this->authorize('show', $channel);

        $data = ChatChannel::query()
            ->with(
                [
                    'messages' => function ($query) {
                        return $query->latest()->take(50);
                    }
                ]
            )
            ->firstOrFail();

        return ChannelResource::make($data);
    }

    /**
     * @param StoreChannelRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws TeamChannelTakenException
     * @throws HappeningChannelTakeException
     */
    public function store(StoreChannelRequest $request)
    {
        $this->authorize('store', ChatChannel::class);

        if (!empty($request->validated()['team_id'])) {
            $this->teamService->isTeamMember($request->validated()['team_id']);
            // Check for team channel not already created
            if (!$this->checkIsFreeTeam($request->validated()['team_id'])) {
                throw new TeamChannelTakenException();
            }
        }

        if (!empty($request->validated()['happening_id'])) {
            $this->happeningService->isHappeningMember($request->validated()['happening_id']);
            // Check for happening channel not already created
            if (!$this->checkIsFreeHappening($request->validated()['happening_id'])) {
                throw new HappeningChannelTakeException();
            }
        }

        $channel = ChatChannel::create($request->validated());

        // Adding users from Team or Happening
        if (!empty($channel->team_id)) {
            $channel->users()->attach(
                Team::query()
                    ->where('id', 1)
                    ->with('users')
                    ->get()
                    ->pluck('users.*.id')
            );
        }

        if (!empty($channel->happening_id)) {
            $channel->users()->attach(
                Happening::query()
                    ->where('id', 1)
                    ->with('users')
                    ->get()
                    ->pluck('users.*.id')
            );
        }

        return response()->json(
            [
                'data' => ChannelResource::make($channel),
                'message' => __(''),
                'code' => 2003,
            ],
            201
        );
    }

    /**
     * @param StoreChannelRequest $request
     * @param ChatChannel $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws ChannelUpdateException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(StoreChannelRequest $request, ChatChannel $channel)
    {
        $this->authorize('updateOrDelete', $channel);

        $status = $channel->update($request->validated());

        if ($status > 0) {
            return response()->json(
                [
                    'data' => ChannelResource::make($channel),
                    'status' => $status,
                    'message' => __(''),
                    'code' => 3010
                ],
                200
            );
        }

        throw new ChannelUpdateException(
            $request->toArray(),
            'Błąd podczas aktualizacji kanału chatu'
        );
    }

    /**
     * @param ChatChannel $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws ChannelDeleteException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(ChatChannel $channel)
    {
        $this->authorize('updateOrDelete', $channel);

        try {
            $status = $channel->delete();
        } catch (Throwable $exception) {
            throw new ChannelDeleteException($exception);
        }

        return response()->json(
            [
                'status' => $status,
                'message' => __(''),
                'code' => 4003,
            ],
            200
        );
    }

    /**
     * @param int $teamId
     * @return bool
     */
    private function checkIsFreeTeam(int $teamId): bool
    {
        return ChatChannel::query()
                ->where('team_id', $teamId)
                ->count() > 0;
    }

    /**
     * @param Team $team
     * @return \Illuminate\Http\JsonResponse
     */
    public function freeTeam(Team $team)
    {
        $this->teamService->isTeamMember($team->id);

        return response()->json(
            [
                'data' => ['available' => $this->checkIsFreeTeam($team->id)],
                'message' => __(''),
                'code' => 5019,
            ],
            200
        );
    }

    /**
     * @param int $happeningId
     * @return bool
     */
    private function checkIsFreeHappening(int $happeningId): bool
    {
        return ChatChannel::query()
                ->where('happening_id', $happeningId)
                ->count() > 0;
    }

    /**
     * @param Happening $happening
     * @return \Illuminate\Http\JsonResponse
     */
    public function freeHappening(Happening $happening)
    {
        $this->happeningService->isHappeningMember($happening->id);

        return response()->json(
            [
                'data' => ['available' => $this->checkIsFreeHappening($happening->id)],
                'message' => __(''),
                'code' => 5020,
            ],
            200
        );
    }
}
