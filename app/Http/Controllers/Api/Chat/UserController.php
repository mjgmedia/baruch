<?php

namespace App\Http\Controllers\Api\Chat;

use App\Http\Controllers\Controller;
use App\Http\Requests\Chat\UserRequest;
use App\Http\Resources\Chat\UserResource;
use App\Models\Chat\ChatChannel;
use App\Services\Interfaces\ChatServiceInterface;

/**
 * Class UserController
 * @package App\Http\Controllers\Api\Chat
 */
class UserController extends Controller
{
    private ChatServiceInterface $service;

    /**
     * UserController constructor.
     * @param $service
     */
    public function __construct(ChatServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param ChatChannel $channel
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(ChatChannel $channel)
    {
        $this->authorize('show', $channel);

        $channel->load('users');
        return UserResource::collection(
            $channel->users
        );
    }

    /**
     * @param UserRequest $request
     * @param ChatChannel $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(UserRequest $request, ChatChannel $channel)
    {
        $this->authorize('updateOrDelete', $channel);

        // There is no need for checking status - all has been done in service
        $status = $this->service->addToChat($request->validated()['users'], $channel->id);

        return response()->json(
            [
                'status' => $status,
                'message' => __(''),
                'code' => 2005,
            ],
            201
        );

    }

    /**
     * @param UserRequest $request
     * @param ChatChannel $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(UserRequest $request, ChatChannel $channel)
    {
        $this->authorize('updateOrDelete', $channel);

        // There is no need for checking status - all has been done in service
        $status = $this->service->removeFromChat($request->validated()['users'], $channel->id);

        return response()->json(
            [
                'status' => $status,
                'message' => __(''),
                'code' => 4005,
            ],
            200
        );

    }
}
