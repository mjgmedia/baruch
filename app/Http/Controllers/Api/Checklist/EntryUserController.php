<?php

namespace App\Http\Controllers\Api\Checklist;

use App\Events\Checklist\EntryAssignedEvent;
use App\Events\Checklist\EntryDoneEvent;
use App\Events\Checklist\EntryReadyEvent;
use App\Events\Checklist\EntryReceivedEvent;
use App\Events\Checklist\EntryUpdatedEvent;
use App\Exceptions\Checklist\EntryUpdateException;
use App\Exceptions\Checklist\EntryUserDeleteException;
use App\Exceptions\Checklist\EntryUserDoneException;
use App\Exceptions\Checklist\EntryUserReadException;
use App\Exceptions\Checklist\EntryUserStoreException;
use App\Exceptions\Checklist\EntryUserUpdateException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Checklist\UsersRequest;
use App\Http\Resources\Checklist\ChecklistEntryResource;
use App\Http\Resources\User\UserResource;
use App\Models\Checklist\ChecklistEntry;
use App\Services\Interfaces\ChecklistServiceInterface;
use Illuminate\Support\Facades\Auth;

/**
 * Class EntryUserController
 * @package App\Http\Controllers\Api\Checklist
 */
class EntryUserController extends Controller
{
    /**
     * @var ChecklistServiceInterface
     */
    private ChecklistServiceInterface $service;

    /**
     * EntryUserController constructor.
     * @param $serivce
     */
    public function __construct(ChecklistServiceInterface $serivce)
    {
        $this->service = $serivce;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UsersRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws EntryUserStoreException
     */
    public function store(UsersRequest $request)
    {
        $checklistEntry = ChecklistEntry::findOrFail($request->validated()['entry_id']);

        $this->authorize('crudUsers', $checklistEntry);

        $status = $checklistEntry->users()->syncWithoutDetaching($request->validated()['users']);

        if ($status['attached']) {
            event(new EntryAssignedEvent($checklistEntry));

            return response()->json(
                [
                    'status' => $status,
                    'message' => __(''),
                    'code' => 2010,
                    'data' => ChecklistEntryResource::make($checklistEntry)
                ],
                201
            );
        }

        throw new EntryUserStoreException(
            $request->toArray(),
            $status,
            'Błąd podczas dodawania użytkowników do zadania w Checklist'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param ChecklistEntry $checklistEntry
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(ChecklistEntry $checklistEntry)
    {
        $this->authorize('allowedToSee', $checklistEntry);

        return UserResource::collection($checklistEntry->users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UsersRequest $request
     * @param ChecklistEntry $checklistEntry
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws EntryUserUpdateException
     */
    public function update(UsersRequest $request, ChecklistEntry $checklistEntry)
    {
        $this->authorize('crudUsers', $checklistEntry);

        $status = $checklistEntry->users()->sync($request->validated()['users']);

        if ($status['attached'] > 0 || $status['detached'] > 0) {
            event(new EntryUpdatedEvent($checklistEntry));

            return response()->json(
                [
                    'data' => ChecklistEntryResource::make($checklistEntry),
                    'message' => __(''),
                    'status' => $status,
                    'code' => 3016,
                ],
                200
            );
        }

        throw new EntryUserUpdateException(
            $request->toArray(),
            $status,
            'Błąd podczas aktualizacji wpisów o użytkownikach przydzielonych do zadania w Checklist'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param UsersRequest $request
     * @param ChecklistEntry $checklistEntry
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws EntryUserDeleteException
     */
    public function destroy(UsersRequest $request, ChecklistEntry $checklistEntry)
    {
        $this->authorize('crudUsers', $checklistEntry);

        $status = $checklistEntry->users()->detach($request->validated()['users']);

        if ($status > 0) {
            return response()->json(
                [
                    'status' => $status,
                    'code' => 4010,
                    'message' => __(''),
                ],
                200
            );
        }

        throw new EntryUserDeleteException(
            $request->toArray(),
            'Wystąpił błąd poczas usuwania użytkowniów przydzielonych do zadania Checklist'
        );
    }

    /**
     * @param ChecklistEntry $entry
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws EntryUpdateException
     * @throws EntryUserDoneException
     */
    public function done(ChecklistEntry $entry)
    {
        $this->authorize('entry', $entry);

        $status = $entry->users()->updateExistingPivot(
            Auth::id(),
            [
                'done' => true
            ]
        );

        if ($status > 0) {
            event(new EntryDoneEvent($entry));

            if ($entry->blocking()->count() > 0) {
                event(new EntryReadyEvent($entry));
            }

            return response()->json(
                [
                    'message' => __(''),
                    'status' => $status,
                    'code' => 5023,
                ],
                200
            );
        }

        throw new EntryUserDoneException(Auth::user(), $entry);
    }

    /**
     * @param ChecklistEntry $entry
     * @return \Illuminate\Http\JsonResponse
     * @throws EntryUpdateException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws EntryUserReadException
     */
    public function read(ChecklistEntry $entry)
    {
        $this->authorize('entry', $entry);

        $status = $entry->users()->updateExistingPivot(
            Auth::id(),
            [
                'notification_assigned' => true
            ]
        );

        if ($status > 0) {
            event(new EntryReceivedEvent($entry, Auth::user()));

            return response()->json(
                [
                    'message' => __(''),
                    'code' => 5024,
                    'status' => $status
                ],
                200
            );
        }

        throw new EntryUserReadException(Auth::user(), $entry);
    }
}
