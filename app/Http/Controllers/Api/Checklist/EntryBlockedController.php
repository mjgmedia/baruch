<?php

namespace App\Http\Controllers\Api\Checklist;

use App\Exceptions\Checklist\EntryBlockedDeleteException;
use App\Exceptions\Checklist\EntryBlockedStoreException;
use App\Exceptions\Checklist\EntryBlockedUpdateException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Checklist\BlockingBlockedRequest;
use App\Http\Resources\Checklist\ChecklistEntryResource;
use App\Models\Checklist\ChecklistEntry;
use App\Services\Interfaces\ChecklistServiceInterface;

/**
 * Class EntryBlockedController
 * @package App\Http\Controllers\Api\Checklist
 */
class EntryBlockedController extends Controller
{
    /**
     * @var ChecklistServiceInterface
     */
    private ChecklistServiceInterface $service;

    /**
     * EntryBlockedController constructor.
     * @param $service
     */
    public function __construct(ChecklistServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BlockingBlockedRequest $request
     * @param ChecklistEntry $checklistEntry
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws EntryBlockedStoreException
     */
    public function store(BlockingBlockedRequest $request, ChecklistEntry $checklistEntry)
    {
        $this->authorize('update', $checklistEntry);

        $status = $checklistEntry->blocked()->syncWithoutDetaching($request->validated()['entries']);

        if ($status['attached'] > 0) {
            return response()->json(
                [
                    'data' => ChecklistEntryResource::make($checklistEntry),
                    'status' => $status,
                    'message' => __(''),
                    'code' => 2007,
                ],
                201
            );
        }

        throw new EntryBlockedStoreException(
            $request->toArray(),
            $status,
            'Błąd podczas zapisywania zadania blokującego to zadanie Checklisty'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param ChecklistEntry $checklistEntry
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(ChecklistEntry $checklistEntry)
    {
        $this->authorize('allowedToSee', $checklistEntry);

        return ChecklistEntryResource::collection(
            ChecklistEntry::query()
                ->whereIn('id', $checklistEntry->blocked->pluck('id'))
                ->get()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BlockingBlockedRequest $request
     * @param ChecklistEntry $checklistEntry
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws EntryBlockedUpdateException
     */
    public function update(BlockingBlockedRequest $request, ChecklistEntry $checklistEntry)
    {
        $this->authorize('update', $checklistEntry);

        $status = $checklistEntry->blocked()->sync($request->validated()['entries']);

        if ($status['attached'] > 0 || $status['detached'] > 0) {
            return response()->json(
                [
                    'data' => ChecklistEntryResource::make($checklistEntry),
                    'status' => $status,
                    'message' => __(''),
                    'code' => 3013,
                ],
                200
            );
        }

        throw new EntryBlockedUpdateException(
            $request->toArray(),
            $status,
            'Błąd podczas aktualizacji wpisów do zadań blokujących to zadanie Checklisty'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param BlockingBlockedRequest $request
     * @param ChecklistEntry $checklistEntry
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws EntryBlockedDeleteException
     */
    public function destroy(BlockingBlockedRequest $request, ChecklistEntry $checklistEntry)
    {
        $this->authorize('update', $checklistEntry);

        $status = $checklistEntry->blocked()->detach($request->validated()['entries']);

        if ($status > 0) {
            return response()->json(
                [
                    'status' => $status,
                    'data' => ChecklistEntryResource::make($checklistEntry),
                    'message' => __(''),
                    'code' => 4007,
                ],
                200
            );
        }

        throw new EntryBlockedDeleteException(
            $request->toArray(),
            'Błąd podczas usuwania zadania blokującego to zadanie Checklisty'
        );
    }
}
