<?php

namespace App\Http\Controllers\Api\Checklist;

use App\Exceptions\Checklist\EntryBlockingDeleteException;
use App\Exceptions\Checklist\EntryBlockingStoreException;
use App\Exceptions\Checklist\EntryBlockingUpdateException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Checklist\BlockingBlockedRequest;
use App\Http\Resources\Checklist\ChecklistEntryResource;
use App\Models\Checklist\ChecklistEntry;
use App\Services\Interfaces\ChecklistServiceInterface;

/**
 * Class EntryBlockingController
 * @package App\Http\Controllers\Api\Checklist
 */
class EntryBlockingController extends Controller
{
    /**
     * @var ChecklistServiceInterface
     */
    private ChecklistServiceInterface $service;

    /**
     * EntryBlockingController constructor.
     * @param $service
     */
    public function __construct(ChecklistServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BlockingBlockedRequest $request
     * @param ChecklistEntry $checklistEntry
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws EntryBlockingStoreException
     */
    public function store(BlockingBlockedRequest $request, ChecklistEntry $checklistEntry)
    {
        $this->authorize('update', $checklistEntry);

        $status = $checklistEntry->blocking()->syncWithoutDetaching($request->validated()['entries']);

        if ($status['attached'] > 0) {
            return response()->json(
                [
                    'data' => ChecklistEntryResource::make($checklistEntry),
                    'status' => $status,
                    'message' => __(''),
                    'code' => 2008,
                ],
                201
            );
        }

        throw new EntryBlockingStoreException(
            $request->toArray(),
            $status,
            'Błąd podczas dodawania zadań blokujących'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param ChecklistEntry $checklistEntry
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(ChecklistEntry $checklistEntry)
    {
        $this->authorize('allowedToSee', $checklistEntry);

        return ChecklistEntryResource::collection(
            ChecklistEntry::query()
                ->whereIn('id', $checklistEntry->blocking->pluck('id'))
                ->get()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BlockingBlockedRequest $request
     * @param ChecklistEntry $checklistEntry
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws EntryBlockingUpdateException
     */
    public function update(BlockingBlockedRequest $request, ChecklistEntry $checklistEntry)
    {
        $this->authorize('update', $checklistEntry);

        $status = $checklistEntry->blocking()->sync($request->validated()['entries']);

        if ($status['attached'] > 0 || $status['detached'] > 0) {
            return response()->json(
                [
                    'data' => ChecklistEntryResource::make($checklistEntry),
                    'status' => $status,
                    'message' => __(''),
                    'code' => 3014
                ],
                200
            );
        }

        throw new EntryBlockingUpdateException(
            $request->toArray(),
            $status,
            'Błąd podczas aktualizacji wpisu o blokawaniu zadania'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param BlockingBlockedRequest $request
     * @param ChecklistEntry $checklistEntry
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws EntryBlockingDeleteException
     */
    public function destroy(BlockingBlockedRequest $request, ChecklistEntry $checklistEntry)
    {
        $this->authorize('update', $checklistEntry);

        $status =$checklistEntry->blocking()->detach($request->validated()['entries']);
        if ($status > 0) {
            return response()->json(
                [
                    'status' => $status,
                    'data' => ChecklistEntryResource::make($checklistEntry),
                    'message' => __(''),
                    'code' => 4008,
                ],
                200
            );
        }

        throw new EntryBlockingDeleteException(
            $request->toArray(),
            'Błąd podczas usuwania wpisu checklisty o blokowaniu przez to zadanie'
        );
    }
}
