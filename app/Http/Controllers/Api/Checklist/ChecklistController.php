<?php

namespace App\Http\Controllers\Api\Checklist;

use App\Exceptions\Checklist\ChecklistDeleteException;
use App\Exceptions\Checklist\ChecklistUpdateException;
use App\Exceptions\Checklist\NotPatternException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Checklist\StoreChecklistRequest;
use App\Http\Resources\Checklist\ChecklistResource;
use App\Models\Checklist\Checklist;
use App\Models\Checklist\ChecklistEntry;
use App\Models\Happening\Happening;
use App\Models\Team\Team;
use App\Services\Interfaces\ChecklistServiceInterface;
use Illuminate\Support\Facades\Auth;

/**
 * Class ChecklistController
 * @package App\Http\Controllers\Api\Checklist
 */
class ChecklistController extends Controller
{
    /**
     * @var ChecklistServiceInterface
     */
    private ChecklistServiceInterface $service;

    /**
     * ChecklistController constructor.
     * @param $service
     */
    public function __construct(ChecklistServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Happening $happening
     * @param Team $team
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Happening $happening, Team $team = null)
    {
        $this->service->checkChecklist($happening->id, ($team === null ? null : $team->id));

        return ChecklistResource::collection(
            Checklist::query()
                ->where('happening_id', $happening->id)
                ->myEntries()
                ->get()
        );
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function indexPatterns()
    {
        $this->authorize('pattern', Checklist::class);

        /**
         * We need to gather all patterns that User have access to because he might have pattern in another
         * team or happening from past (eg. last convention) and want to copy it.
         */
        return ChecklistResource::collection(
            Checklist::query()
                ->where('author_id', Auth::id())
                ->orWhereIn(
                    'happening_id',
                    Happening::query()
                        ->isMember(Auth::id())
                        ->pluck('id')
                )
                ->orWhereIn(
                    'team_id',
                    Team::query()
                        ->isMember(Auth::id())
                        ->pluck('id')
                )
                ->patterns()
                ->get()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreChecklistRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(StoreChecklistRequest $request)
    {
        $this->service->checkCreateChecklist(
            $request->validated()['happening_id'],
            ($request->validated()['team_id'] === null ? null : $request->validated()['team_id'])
        );

        $checklist = Checklist::create($request->validated());

        return response()->json(
            [
                'data' => ChecklistResource::make($checklist),
                'message' => __(''),
                'code' => 2006,
            ],
            201
        );
    }

    /**
     * @param StoreChecklistRequest $request
     * @param Checklist $checklist
     * @return \Illuminate\Http\JsonResponse
     * @throws NotPatternException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function copyPattern(StoreChecklistRequest $request, Checklist $checklist)
    {
        /**
         * Two-way authorization check - have patterns access and crud permissions to selected team/happening
         * There is no necessity for checking authorization for entries, because if user can create new
         * checklist, so as it author he automatically can create entries into it.
         */
        $this->authorize('pattern', Checklist::class);
        $this->service->checkCreateChecklist(
            $request->validated()['happening_id'],
            ($request->validated()['team_id'] === null ? null : $request->validated()['team_id'])
        );

        if ($checklist->pattern !== true) {
            throw new NotPatternException();
        }

        $newChecklist = Checklist::create($request->validated());

        foreach ($checklist->entries as $entry) {
            ChecklistEntry::create(
                [
                    'name' => $entry->name,
                    'desc' => $entry->desc,
                    'should_done_at' => $entry->should_done_at,
                    'author_id' => Auth::id(),
                    'checklist_id' => $newChecklist->id
                ]
            );
        }

        return response()->json(
            [
                'message' => __(''),
                'code' => 5021,
                'data' => ChecklistResource::make($newChecklist)
            ],
            201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param Checklist $checklist
     * @return ChecklistResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Checklist $checklist)
    {
        $this->authorize('allowedToSee', $checklist);

        return ChecklistResource::make($checklist);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreChecklistRequest $request
     * @param Checklist $checklist
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws ChecklistUpdateException
     */
    public function update(StoreChecklistRequest $request, Checklist $checklist)
    {
        $this->authorize('update', $checklist);

        $status = $checklist->update($request->validated());

        if ($status > 0) {
            return response()->json(
                [
                    'data' => ChecklistResource::make($checklist),
                    'message' => __(''),
                    'code' => 3012,
                    'status' => $status,
                ],
                200
            );
        }

        throw new ChecklistUpdateException(
            $request->toArray(),
            'Błąd podczas aktualizacji checklisty'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Checklist $checklist
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws ChecklistDeleteException
     */
    public function destroy(Checklist $checklist)
    {
        $this->authorize('delete', $checklist);

        try {
            $status = $checklist->delete();
        } catch (\Throwable $exception) {
            throw new ChecklistDeleteException($exception);
        }

        return response()->json(
            [
                'status' => $status,
                'message' => __(''),
                'code' => 4006,
            ],
            200
        );
    }
}
