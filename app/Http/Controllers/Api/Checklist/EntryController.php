<?php

namespace App\Http\Controllers\Api\Checklist;

use App\Events\Checklist\EntryUpdatedEvent;
use App\Exceptions\Checklist\ChecklistEntryDeleteException;
use App\Exceptions\Checklist\EntryMarkAsDoneException;
use App\Exceptions\Checklist\EntryUpdateException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Checklist\StoreChecklistEntryRequest;
use App\Http\Resources\Checklist\ChecklistEntryResource;
use App\Models\Checklist\Checklist;
use App\Models\Checklist\ChecklistEntry;
use App\Services\Interfaces\ChecklistServiceInterface;
use Illuminate\Http\Request;

/**
 * Class EntryController
 * @package App\Http\Controllers\Api\Checklist
 */
class EntryController extends Controller
{
    /**
     * @var ChecklistServiceInterface
     */
    private ChecklistServiceInterface $service;

    /**
     * EntryController constructor.
     * @param $service
     */
    public function __construct(ChecklistServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Checklist $checklist
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Checklist $checklist)
    {
        $this->authorize('allowedToSee', $checklist);

        return ChecklistEntryResource::collection($checklist->myEntries());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreChecklistEntryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreChecklistEntryRequest $request)
    {
        $this->service->canCreateEntry($request->validated()['checklist_id']);

        $checklistEntry = ChecklistEntry::create($request->validated());

        return response()->json(
            [
                'data' => ChecklistEntryResource::make($checklistEntry),
                'message' => __(''),
                'code' => 2009,
            ],
            201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param ChecklistEntry $checklistEntry
     * @return ChecklistEntryResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(ChecklistEntry $checklistEntry)
    {
        $this->authorize('allowedToSee', $checklistEntry);

        return ChecklistEntryResource::make($checklistEntry);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreChecklistEntryRequest $request
     * @param ChecklistEntry $checklistEntry
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws EntryUpdateException
     */
    public function update(StoreChecklistEntryRequest $request, ChecklistEntry $checklistEntry)
    {
        $this->authorize('update', $checklistEntry);

        $status = $checklistEntry->update($request->validated());

        if ($status > 0) {
            event(new EntryUpdatedEvent($checklistEntry));

            return response()->json(
                [
                    'data' => ChecklistEntryResource::make($checklistEntry),
                    'message' => __(''),
                    'code' => 3015,
                ],
                200
            );
        }

        throw new EntryUpdateException(
            $request->toArray(),
            'Błąd podczas aktualizacji wpisu o zadaniu Checklisty'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ChecklistEntry $checklistEntry
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws ChecklistEntryDeleteException
     */
    public function destroy(ChecklistEntry $checklistEntry)
    {
        $this->authorize('delete', $checklistEntry);

        try {
            $status = $checklistEntry->delete();
        } catch (\Exception $e) {
            throw new ChecklistEntryDeleteException($e);
        }

        return response()->json(
            [
                'status' => $status,
                'message' => __(''),
                'code' => 4009
            ],
            200
        );
    }

    /**
     * @param Request $request
     * @param ChecklistEntry $checklistEntry
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws EntryMarkAsDoneException
     */
    public function markAsDone(Request $request, ChecklistEntry $checklistEntry)
    {
        $this->authorize('done', $checklistEntry);

        $status = $checklistEntry->users()
            ->where('user_id', $request->user()->id)
            ->update(['done' => now()]);

        if ($status > 0) {
            return response()->json(
                [
                    'message' => __(''),
                    'code' => 5022
                ],
                201
            );
        }

        throw new EntryMarkAsDoneException($request->user(), $checklistEntry);
    }
}
