<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactUsRequest;
use App\Models\User\User;
use App\Notifications\ContactUsNotification;
use Illuminate\Support\Facades\Notification;

/**
 * Class ContactUsController
 * @package App\Http\Controllers\Api
 */
class ContactUsController extends Controller
{
    /**
     * @param ContactUsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function contact(ContactUsRequest $request)
    {
        $users = User::role('admin')->get();
        Notification::send(
            $users,
            new ContactUsNotification(
                $request->name,
                $request->email,
                $request->subject,
                $request->message
            )
        );
        return response()->json(
            [
                'message' => __('api.contact.message'),
                'code' => 5009
            ],
            200
        );
    }
}
