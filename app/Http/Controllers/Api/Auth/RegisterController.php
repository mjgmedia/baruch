<?php

namespace App\Http\Controllers\Api\Auth;

use App\Exceptions\Auth\ActivationDisabledException;
use App\Exceptions\User\ActivationCodeNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ActivationRequest;
use App\Http\Requests\Auth\RegisterUserRequest;
use App\Models\User\Profile;
use App\Models\User\User;
use App\Services\Interfaces\SettingsServiceInterface;
use Illuminate\Auth\Events\Registered;

/**
 * Class RegisterController
 * @package App\Http\Controllers\Api\Auth
 */
class RegisterController extends Controller
{
    /**
     * @var SettingsServiceInterface
     */
    private SettingsServiceInterface $service;

    /**
     * RegisterController constructor.
     * @param SettingsServiceInterface $service
     */
    public function __construct(SettingsServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param RegisterUserRequest $request
     * @return mixed
     * @throws \App\Exceptions\Auth\OAuthFatalException
     * @throws \App\Exceptions\Auth\OAuthLoginException
     */
    public function register(RegisterUserRequest $request)
    {
        /* create user account */
        $user = User::create($request->validated());
        event(new Registered($user));

        /* create an empty profile */
        $profile = new Profile();
        $user->profile()->save($profile);

        /* create bunch of default user settings */
        $this->service->createSettingsEntries($user);

        /* attach specific role */
        if ($request->has('role')) {
            $user->assignRole(request('role'));
        } else {
            /* attach default role */
            $user->assignRole('user');
        }

        return response()->json(
            [
                'message' => __('api.register.success'),
                'code' => 2002,
            ],
            201
        );
    }

    /**
     * @param ActivationRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ActivationCodeNotFoundException
     * @throws ActivationDisabledException
     */
    public function activate(ActivationRequest $request)
    {
        if (config('project.activation_enabled') != true) {
            throw new ActivationDisabledException();
        }

        try {
            $user = User::where('activation_code', $request->validated()['code'])
                ->whereNull('email_verified_at')
                ->firstOrFail();
        } catch (\Throwable $e) {
            throw new ActivationCodeNotFoundException();
        }

        $user->markEmailAsVerified();

        return response()->json(
            [
                'message' => __('api.register.activation_success'),
                'code' => 3009,
            ],
            200
        );
    }
}
