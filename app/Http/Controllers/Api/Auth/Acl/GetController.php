<?php

namespace App\Http\Controllers\Api\Auth\Acl;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class GetController extends Controller
{
    /**
     * All permissions which apply on the user (inherited from roles and direct)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAllPermissions(Request $request)
    {
        return response()->json(
            [
                'acl' => $request->user()->getAllPermissions(),
                'code' => 5010
            ]
        );
    }

    /**
     * All Permissions  Assigned Directly to a user
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getDirectPermissions(Request $request)
    {
        return response()->json(
            [
                'acl' => $request->user()->permissions,
                'code' => 5011
            ]
        );
    }

    /**
     * All Permissions Inherited From Roles Assigned to a User
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getPermissionsViaRoles(Request $request)
    {
        return response()->json(
            [
                'acl' => $request->user()->getPermissionsViaRoles(),
                'code' => 5012
            ]
        );
    }

    /**
     * get a collection of all defined roles
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getRoles(Request $request)
    {
        return response()->json(
            [
                'acl' => $request->user()->getRoleNames(),
                'code' => 5013
            ]
        );
    }
}
