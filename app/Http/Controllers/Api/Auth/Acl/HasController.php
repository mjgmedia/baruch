<?php

namespace App\Http\Controllers\Api\Auth\Acl;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\Acl\PermissionRequest;
use App\Http\Requests\Auth\Acl\PermissionsRequest;
use App\Http\Requests\Auth\Acl\RoleRequest;
use App\Http\Requests\Auth\Acl\RolesRequest;
use Illuminate\Http\JsonResponse;

/**
 * Class HasController
 * @package App\Http\Controllers\Api\Auth\Acl
 */
class HasController extends Controller
{
    /**
     * Check if it has All The Roles
     * @param RolesRequest $request
     * @return JsonResponse
     */
    public function hasAllRoles(RolesRequest $request)
    {
        return response()->json(
            [
                'acl' => $request->user()->hasAllRoles($request->roles),
                'code' => 5014
            ]
        );
    }

    /**
     * Check Any Permission
     * @param PermissionsRequest $request
     * @return JsonResponse
     */
    public function hasAnyPermission(PermissionsRequest $request)
    {
        return response()->json(
            [
                'acl' => $request->user()->hasAnyPermission($request->permissions),
                'code' => 5015
            ]
        );
    }

    /**
     * Check for Any Role
     * @param RolesRequest $request
     * @return JsonResponse
     */
    public function hasAnyRole(RolesRequest $request)
    {
        return response()->json(
            [
                'acl' => $request->user()->hasAnyRole($request->roles),
                'code' => 5016
            ]
        );
    }

    /**
     * Check Specific Permission
     * @param PermissionRequest $request
     * @return JsonResponse
     */
    public function hasPermissionTo(PermissionRequest $request)
    {
        return response()->json(
            [
                'acl' => $request->user()->hasPermissionTo($request->permission),
                'code' => 5017
            ]
        );
    }

    /**
     * Check For Specific Role
     * @param RoleRequest $request
     * @return JsonResponse
     */
    public function hasRole(RoleRequest $request)
    {
        return response()->json(
            [
                'acl' => $request->user()->hasRole($request->role),
                'code' => 5018
            ]
        );
    }
}
