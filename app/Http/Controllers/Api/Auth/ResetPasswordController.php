<?php

namespace App\Http\Controllers\Api\Auth;

use App\Exceptions\Auth\ResetFailException;
use App\Exceptions\Auth\ResetTimeoutException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Traits\IssueTokenTrait;
use Illuminate\Support\Facades\Password;
use Laravel\Passport\Client;

/**
 * Class ResetPasswordController
 * @package App\Http\Controllers\Api\Auth
 */
class ResetPasswordController extends Controller
{
    use IssueTokenTrait;

    /**
     * @var mixed
     */
    private $client;

    /**
     * ResetPasswordController constructor.
     */
    public function __construct()
    {
        $this->client = Client::where(
            [
                ['password_client', '=', true],
                ['revoked', '=', false],
            ]
        )->first();
    }

    /**
     * @param ResetPasswordRequest $request
     * @return mixed
     * @throws \App\Exceptions\Auth\OAuthFatalException
     * @throws \App\Exceptions\Auth\OAuthLoginException
     * @throws ResetTimeoutException
     * @throws ResetFailException
     */
    public function resetPassword(ResetPasswordRequest $request)
    {
        // Reset Password
        $response = $this->broker()->reset(
            $request->only('email', 'password', 'token'),
            function ($user, $password) {
                $this->reset($user, $password);
            }
        );

        if (Password::INVALID_TOKEN == $response) {
            throw new ResetTimeoutException();
        }

        // Check if We Have Reset Password
        if (Password::PASSWORD_RESET !== $response) {
            throw new ResetFailException();
        }

        // Issue a New Access Token
        return $this->issueToken($request, 'password');
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker();
    }

    /**
     * Reset the given user's password.
     *
     * @param \Illuminate\Contracts\Auth\CanResetPassword $user
     * @param string $password
     * @return void
     */
    protected function reset($user, $password)
    {
        // Change New Pass
        $user->password = $password;
        //  We Reset Resent Count back to Zero
        $user->resent = 0;
        $user->save();
    }
}
