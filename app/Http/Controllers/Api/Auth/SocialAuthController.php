<?php

namespace App\Http\Controllers\Api\Auth;

use App\Exceptions\Auth\OAuthFatalException;
use App\Exceptions\Auth\OAuthLoginException;
use App\Http\Controllers\Controller;
use App\Traits\IssueTokenTrait;
use GuzzleHttp\Exception\ClientException;
use Laravel\Passport\Client;
use Socialite;

/**
 * Class SocialAuthController
 * @package App\Http\Controllers\Api\Auth
 */
class SocialAuthController extends Controller
{
    use IssueTokenTrait;

    /**
     * @var
     */
    private $client;

    /**
     * SocialAuthController constructor.
     */
    public function __construct()
    {
        $this->client = Client::where(
            [
                ['password_client', '=', true],
                ['revoked', '=', false],
            ]
        )->first();
    }

    /**
     * @param $provider
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     * @throws OAuthLoginException
     * @throws OAuthFatalException
     */
    public function socialAuth($provider, $token)
    {
        $params = [
            'grant_type' => 'social',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'access_token' => $token,
            'provider' => $provider,
        ];

        $client = new \GuzzleHttp\Client();

        try {
            $response = $client->post(
                route('passport.token'),
                [
                    'form_params' => $params,
                    'timeout' => 10,
                ]
            );

            $arrBody = json_decode((string)$response->getBody(), true);
            return response()->json($arrBody);
        } catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() == 400) {
                throw new OAuthLoginException();
            }
           throw new OAuthFatalException($e->getResponse()->getStatusCode() . ': '. $e->getMessage());
        }
    }
}