<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RefreshTokenRequest;
use App\Traits\IssueTokenTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Client;

/**
 * Class LoginController
 * @package App\Http\Controllers\Api\Auth
 */
class LoginController extends Controller
{
    use IssueTokenTrait;

    /**
     * @var string
     */
    protected $username = 'email';

    /**
     * @var mixed
     */
    private $client;

    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->client = Client::where(
            [
                ['password_client', '=', true],
                ['revoked', '=', false],
            ]
        )->first();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function check(Request $request)
    {
        if ($request->user()) {
            return response()->json(
                [
                    'authenticated' => true,
                    'code' => 5003
                ],
                200
            );
        }

        return response()->json(
            [
                'authenticated' => false,
                'code' => 108,
            ],
            401
        );
    }

    /**
     * @param LoginRequest $request
     * @return mixed
     * @throws \App\Exceptions\Auth\OAuthFatalException
     * @throws \App\Exceptions\Auth\OAuthLoginException
     */
    public function login(LoginRequest $request)
    {
        return $this->issueToken($request, 'password');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $accessToken = $request->user()->token();
        $accessToken->revoke();

        if (config('app.env') != 'testing') {
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update(['revoked' => true]);
        }

        return response()->json(
            [
                'message' => __('api.login.logout_success'),
                'code' => 5004
            ],
            200
        );
    }

    /**
     * @param RefreshTokenRequest $request
     * @return mixed
     * @throws \App\Exceptions\Auth\OAuthFatalException
     * @throws \App\Exceptions\Auth\OAuthLoginException
     */
    public function refresh(RefreshTokenRequest $request)
    {
        return $this->issueToken($request, 'refresh_token');
    }
}
