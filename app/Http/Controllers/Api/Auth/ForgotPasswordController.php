<?php

namespace App\Http\Controllers\Api\Auth;

use App\Exceptions\Auth\ResetSendmailException;
use App\Exceptions\Auth\ResetTooManyMailsException;
use App\Exceptions\Auth\ResetUserNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ForgotPasswordRequest;
use App\Models\User\User;
use Illuminate\Support\Facades\Password;

/**
 * Class ForgotPasswordController
 * @package App\Http\Controllers\Api\Auth
 */
class ForgotPasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @param ForgotPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ResetSendmailException
     * @throws ResetTooManyMailsException
     * @throws ResetUserNotFoundException
     */
    public function sendResetEmail(ForgotPasswordRequest $request)
    {
        // Check if the Email is Registered
        try {
            $user = User::findByEmail($request->username);
        } catch (\Throwable $exception) {
            throw new ResetUserNotFoundException();
        }

        // Check if We Exceeded Password Reset!

        // If Yes then We Show a Message We Reached the Limit and Avoid Sending More Email
        if ($user->resent >= 3) {
            throw new ResetTooManyMailsException($user->resent);
        }

        // Send Mail
        $broker = $this->getPasswordBroker();

        $sendingResponse = $broker->sendResetLink(['email' => $request->username]);

        // Check if We Sent The Email
        if (Password::RESET_LINK_SENT !== $sendingResponse) {
            throw new ResetSendmailException('Błąd wysyłki maila z resetem hasła');
        }

        // Increase the Resent Number by 1 Each time We Sent An Email
        $user->resent++;
        $user->save();
        // Show Message We Have Successfully Email them the Reset Link
        return response()->json(
            [
                'message' => __('api.forgot_password.send_reset_email.email_success'),
                'code' => 5002,
            ],
            200
        );
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    private function getPasswordBroker()
    {
        return Password::broker();
    }
}
