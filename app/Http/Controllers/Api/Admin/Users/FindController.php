<?php

namespace App\Http\Controllers\Api\Admin\Users;

use App\Exceptions\Admin\EmailNotFound;
use App\Exceptions\UsernameNotFound;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Users\FindEmailRequest;
use App\Http\Requests\Admin\Users\FindUsernameRequest;
use App\Http\Resources\User\UserResource;
use App\Models\User\User;

/**
 * Class FindController
 * @package App\Http\Controllers\Api\Admin\Users
 */
class FindController extends Controller
{
    /**
     * @param FindEmailRequest $request
     * @return UserResource
     * @throws EmailNotFound
     */
    public function email(FindEmailRequest $request)
    {
        try {
            $user = User::findByEmail($request->email);
        } catch (\Throwable $exception) {
            throw new EmailNotFound();
        }

        return new UserResource($user->load('profile'));
    }

    /**
     * @param FindUsernameRequest $request
     * @return UserResource
     * @throws UsernameNotFound
     */
    public function username(FindUsernameRequest $request)
    {
        $user = User::findByUsername($request->username);

        if (!$user) {
            throw new UsernameNotFound();
        }

        return new UserResource($user->load('profile'));
    }
}
