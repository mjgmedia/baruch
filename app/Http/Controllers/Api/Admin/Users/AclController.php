<?php

namespace App\Http\Controllers\Api\Admin\Users;

use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * Class AclController
 * @package App\Http\Controllers\Api\Admin\Users
 */
class AclController extends Controller
{
    /**
     * get a authenticated user
     *
     * @return array
     */
    public function getAllPermissions()
    {
        return Permission::all()->pluck('name')->toArray();
    }

    /**
     * All Permissions Inherited From Roles Assigned to a User
     *
     * @return array
     */
    public function getAllRoles()
    {
        return Role::all()->pluck('name')->toArray();
    }
}
