<?php

namespace App\Http\Controllers\Api\Admin\Users;

use App\Exceptions\Admin\MassDeleteException;
use App\Exceptions\Admin\RevokeAdminUpdate;
use App\Exceptions\Admin\UpdatingRecordFailed;
use App\Exceptions\Admin\UserNotFound;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Users\MassActionsRequest;
use App\Http\Requests\Admin\Users\MassMailRequest;
use App\Http\Requests\Admin\Users\SyncPermissionsRolesRequest;
use App\Http\Resources\User\UserResource;
use App\Models\User\User;
use App\Notifications\Admin\MassMailToUsersNotification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Mews\Purifier\Facades\Purifier;

/**
 * Class MassController
 * @package App\Http\Controllers\Api\Admin\Users
 */
class MassController extends Controller
{
    /**
     * @param MassActionsRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws UpdatingRecordFailed
     */
    public function activate(MassActionsRequest $request)
    {
        $ids = $this->selectExceptSuperAdmin($request->ids);
        $updated = User::whereIn('id', $ids)->update(['email_verified_at' => now()]);

        if (count($ids) !== $updated) {
            throw new UpdatingRecordFailed();
        }

        return response()->json(
            [
                'message' => __('api.admin.users.mass.activate.message'),
                'updated' => $ids,
                'code' => 3003
            ],
            200
        );
    }

    /**
     * @param array $arrIds
     * @return mixed
     */
    private function selectExceptSuperAdmin(array $arrIds)
    {
        $ids = collect($arrIds);
        return $ids->diff(User::role('admin')->get()->pluck('id'))->toArray();
    }

    /**
     * @param MassActionsRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws UpdatingRecordFailed
     */
    public function deactivate(MassActionsRequest $request)
    {
        $ids = $this->selectExceptSuperAdmin($request->ids);
        $updated = User::whereIn('id', $ids)->update(['email_verified_at' => null]);

        if (count($ids) !== $updated) {
            throw new UpdatingRecordFailed();
        }

        return response()->json(
            [
                'message' => __('api.admin.users.mass.deactivate.message'),
                'updated' => $ids,
                'code' => 3004
            ],
            200
        );
    }

    /**
     * @param MassActionsRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws MassDeleteException
     */
    public function delete(MassActionsRequest $request)
    {
        $ids = $this->selectExceptSuperAdmin($request->ids);
        DB::beginTransaction();
        try {
            User::whereIn('id', $ids)->delete();
        } catch (\Exception $e) {
            throw new MassDeleteException($e);
        }

        DB::commit();
        return response()->json(
            [
                'message' => __('api.admin.users.mass.delete.success'),
                'code' => 4002
            ],
            200
        );
    }

    /**
     * @param MassMailRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function mail(MassMailRequest $request)
    {
        $users = User::whereIn('id', $request->ids)->get();
//        HTML Purifier for extra protection against XSS
        $message = Purifier::clean($request->message);
//        This notification allows fot HTML in message.
        Notification::send($users, new MassMailToUsersNotification($request->subject, $message));

        return response()->json(
            [
                'message' => __('api.admin.users.mass.mail.message'),
                'code' => 5001
            ],
            200
        );
    }

    /**
     * @param $id
     * @param SyncPermissionsRolesRequest $request
     * @return UserResource
     * @throws RevokeAdminUpdate
     * @throws UserNotFound
     */
    public function permissions($id, SyncPermissionsRolesRequest $request)
    {
        $user = User::find($id);

        if ($user->isSuperAdmin()) {
            throw new RevokeAdminUpdate();
        }

        if ($user) {
            $user->syncPermissions($request->to_sync);
            $user->load('profile', 'roles', 'permissions');
            return new UserResource($user);
        } else {
            throw new UserNotFound();
        }
    }

    /**
     * @param $id
     * @param SyncPermissionsRolesRequest $request
     * @return UserResource
     * @throws RevokeAdminUpdate
     * @throws UserNotFound
     */
    public function roles($id, SyncPermissionsRolesRequest $request)
    {
        $user = User::find($id);

        if ($user->isSuperAdmin()) {
            throw new RevokeAdminUpdate();
        }

        if ($user) {
            $wynik = $user->syncRoles($request->to_sync);
            $user->load('profile', 'roles', 'permissions');
            return new UserResource($user);
        } else {
            throw new UserNotFound();
        }
    }
}
