<?php

namespace App\Http\Controllers\Api\Admin\Users;

use App\Exceptions\Admin\AccountCreationFailed;
use App\Exceptions\Admin\HasNoPermissionException;
use App\Exceptions\Admin\ModifySuperAdminDeniedException;
use App\Exceptions\Admin\UpdatingRecordFailed;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Users\CreateRequest;
use App\Http\Requests\Admin\Users\UserIdRequest;
use App\Http\Requests\Admin\Users\UserUpdateRequest;
use App\Http\Requests\SingleUserIdRequest;
use App\Http\Resources\User\UserResource;
use App\Models\User\Profile;
use App\Models\User\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class CrudController
 * @package App\Http\Controllers\Api\Admin\Users
 */
class CrudController extends Controller
{
    /**
     * @param CreateRequest $request
     * @return UserResource|JsonResponse
     * @throws AccountCreationFailed
     */
    public function create(CreateRequest $request)
    {
        DB::beginTransaction();
        $user = User::create($request->validated());
        /* create an empty profile */
        $profile = Profile::create($request->validated());
        $user->profile()->save($profile);
        $role = request('roles');
        $user->assignRole($role);

        if (!$user && !$profile) {
            DB::rollback();
            ld('Błąd przy tworzeniu nowego usera CrudController@create', $request, $user, $profile);
            throw new AccountCreationFailed('Błąd przy tworzeniu nowego usera CrudController@create');
        }

        DB::commit();

        return response()->json(
            [
                'data' => UserResource::make($user),
                'code' => 2001
            ],
            201
        );
    }

    /**
     * @param UserIdRequest $request
     * @return JsonResponse
     * @throws UpdatingRecordFailed
     */
    public function delete(UserIdRequest $request)
    {
        $countDeleted = 0;
        foreach ($request->validated()['items'] as $user) {
            $user = User::find($user);

            if (!$user->isSuperAdmin()) {
                $deleted = $user->delete();

                if (!$deleted) {
                    throw new UpdatingRecordFailed();
                }
                $countDeleted++;
            }
        }

        return response()->json(
            [
                'count' => $countDeleted,
                'code' => 4001
            ],
            200
        );
    }

    /**
     * @param Request $request
     * @param User $user
     * @return UserResource
     */
    public function edit(Request $request, User $user)
    {
        return new UserResource($user->load('profile'));
    }

    /**
     * @param Request $request
     * @return JsonResponse|AnonymousResourceCollection
     * @throws HasNoPermissionException
     */
    public function index(Request $request)
    {
        /* USERS PERMISSION CHECKS */
        $allowedPerms = [
            'crud_users',
            'activate_user',
            'deactivate_user',
            'mass_mail',
            'manage_roles',
            'manage_permissions'
        ];
        if (!$request->user()->hasAnyPermission($allowedPerms) && !$request->user()->hasRole('superadmin')) {
            throw new HasNoPermissionException(Auth::user());
        }

        $per_page = $request->per_page ?? 50;
        $sort_by = $request->sort_by;
        $order_by = $request->order_by;
        $users = User::with(['profile'])
            ->when(
                $sort_by,
                function ($query) use ($sort_by, $order_by) {
                    return $query->orderBy($sort_by, $order_by);
                }
            )
            ->paginate($per_page);
        return UserResource::collection($users);
    }

    /**
     * @param UserUpdateRequest $request
     * @param User $user
     * @return JsonResponse
     * @throws UpdatingRecordFailed
     * @throws ModifySuperAdminDeniedException
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        if ($request->password && $request->password_confirmation) {
            $user->password = $request->password;
        }
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->email_verified_at = $user->isSuperAdmin() ? now() : ($request->active ? now() : null);
        $saved = $user->save();

        if (!$saved) {
            throw new UpdatingRecordFailed();
        }

        $profile = $user->profile;
        $updated = $profile->update($request->validated());

        if (!$updated) {
            throw new UpdatingRecordFailed();
        }

        if (!$user->isSuperAdmin()) {
            $user->syncRoles($request->validated()['roles']);
        } else {
            throw new ModifySuperAdminDeniedException();
        }

        return response()->json(
            [
                'message' => __('api.admin.users.crud.update.success'),
                'code' => 3001
            ]
        );
    }

    /**
     * @param SingleUserIdRequest $request
     * @return JsonResponse
     * @throws UpdatingRecordFailed
     * @throws ModifySuperAdminDeniedException
     */
    public function toggleStatus(SingleUserIdRequest $request)
    {
        $user = User::find($request->user_id);

        if ($user->isSuperAdmin()) {
            throw new ModifySuperAdminDeniedException();
        }

        $user->email_verified_at = !$user->email_verified_at == null ? now() : null;
        $saved = $user->save();

        if (!$saved) {
            throw new UpdatingRecordFailed();
        }

        return response()->json(
            [
                'status' => $user->active,
                'code' => 3002,
            ],
            200
        );
    }
}
