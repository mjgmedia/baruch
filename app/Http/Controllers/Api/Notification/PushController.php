<?php

namespace App\Http\Controllers\Api\Notification;

use App\Exceptions\Push\NotificationNotFoundException;
use App\Exceptions\Push\SubscriptionNotFoundException;
use App\Exceptions\Push\WrongEndpointException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Notification\PushDeleteRequest;
use App\Http\Requests\Notification\PushUpdateRequest;
use Illuminate\Http\JsonResponse;
use NotificationChannels\WebPush\PushSubscription;

/**
 * Class PushController
 * @package App\Http\Controllers\Api\Notification
 */
class PushController extends Controller
{
    /**
     * PushController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except('dismiss');
    }

    /**
     * @param PushUpdateRequest $request
     * @return JsonResponse
     */
    public function update(PushUpdateRequest $request)
    {
        $pushData = $request->validated();

        $request->user()->updatePushSubscription(
            $pushData['endpoint'],
            $pushData['public_key'],
            $pushData['auth_token'],
            $pushData['content_encoding']
        );

        return response()->json(null, 204);
    }

    /**
     * @param PushDeleteRequest $request
     * @return JsonResponse
     */
    public function delete(PushDeleteRequest $request)
    {
        $request->user()->deletePushSubscription($request->endpoint);

        return response()->json(null, 204);
    }

    /**
     * @param PushDeleteRequest $request
     * @param $id
     * @return JsonResponse
     * @throws NotificationNotFoundException
     * @throws SubscriptionNotFoundException
     * @throws WrongEndpointException
     */
    public function dismiss(PushDeleteRequest $request, $id)
    {
        if (empty($request->endpoint)) {
            throw new WrongEndpointException(
                $request->toArray(),
                'Nie znaleziono endpointu dla powiadomień PUSH'
            );
        }

        $subscription = PushSubscription::findByEndpoint($request->endpoint);
        if (is_null($subscription)) {
            throw new SubscriptionNotFoundException(
                $request->toArray(),
                'Nie znaleziono subskrypcji PUSH'
            );
        }

        $notification = $subscription->user->notifications()->where('id', $id)->first();
        if (is_null($notification)) {
            throw new NotificationNotFoundException(
                $request->toArray(),
                'Nie znaleziono powiadomienia PUSH'
            );
        }

        $notification->markAsRead();

        return response()->json(null, 201);
    }
}
