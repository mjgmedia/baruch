<?php

namespace App\Http\Controllers\Api\Team;

use App\Exceptions\Team\PostCommentDeleteException;
use App\Exceptions\Team\PostCommentUpdateException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Team\StoreTeamPostCommentRequest;
use App\Http\Resources\Team\TeamPostCommentResource;
use App\Models\Team\TeamPostComment;
use App\Services\Interfaces\TeamServiceInterface;

/**
 * Class PostCommentController
 * @package App\Http\Controllers\Api\Team
 */
class PostCommentController extends Controller
{
    /**
     * @var TeamServiceInterface
     */
    private TeamServiceInterface $service;

    /**
     * PostCommentController constructor.
     * @param $service
     */
    public function __construct(TeamServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTeamPostCommentRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(StoreTeamPostCommentRequest $request)
    {
        $this->service->isTeamMember($request->validated()['team_id']);
        $this->authorize('create', [TeamPostComment::class, $request->validated()['team_id']]);

        $comment = TeamPostComment::create($request->validated());

        return response()->json(
            [
                'data' => TeamPostCommentResource::make($comment),
                'message' => __(''),
                'code' => 2022,
            ],
            201
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreTeamPostCommentRequest $request
     * @param TeamPostComment $comment
     * @return \Illuminate\Http\JsonResponse
     * @throws PostCommentUpdateException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(StoreTeamPostCommentRequest $request, TeamPostComment $comment)
    {
        $this->service->isTeamMember($request->validated()['team_id']);
        $this->authorize('update', $comment);

        $status = $comment->update($request->validated());

        if ($status > 0) {
            return response()->json(
                [
                    'data' => TeamPostCommentResource::make($comment),
                    'message' => __(''),
                    'status' => $status,
                    'code' => 3024
                ],
                200
            );
        }

        throw new PostCommentUpdateException(
            $request->toArray(),
            'Błąd podczas aktualizacji komentarza do posta w zespole'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param TeamPostComment $comment
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(TeamPostComment $comment)
    {
        $this->service->isTeamMember($comment->post->team->id);
        $this->authorize('delete', $comment);

        try {
            $status = $comment->delete();
        } catch (\Throwable $exception) {
            throw new PostCommentDeleteException($exception);
        }

        return response()->json(
            [
                'status' => $status,
                'message' => __(''),
                'code' => 4021,
            ],
            200
        );
    }
}
