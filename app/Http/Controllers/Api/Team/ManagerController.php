<?php

namespace App\Http\Controllers\Api\Team;

use App\Events\Team\ManagerAddedEvent;
use App\Events\Team\ManagerRemovedEvent;
use App\Exceptions\Team\ManagersDeleteException;
use App\Exceptions\Team\ManagersStoreException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Team\TeamManagerRequest;
use App\Models\Team\Team;
use App\Models\User\User;
use App\Services\Interfaces\TeamServiceInterface;

/**
 * Class ManagerController
 * @package App\Http\Controllers\Api\Team
 */
class ManagerController extends Controller
{
    /**
     * @var TeamServiceInterface
     */
    private TeamServiceInterface $service;

    /**
     * ManagerController constructor.
     * @param $service
     */
    public function __construct(TeamServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param TeamManagerRequest $request
     * @param Team $team
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws ManagersStoreException
     */
    public function store(TeamManagerRequest $request, Team $team)
    {
        $this->authorize('managersCrud', $team);

        $users = User::query()
            ->whereIn('id', $request->validated()['managers'])
            ->isTeamMember($team->id)
            ->get();

        $status = $team->managers()->syncWithoutDetaching($users->pluck('id'));
        if ($status['attached'] > 0) {
            $this->service->grantManagerPermission($users, $team->id);
            $users->each(fn($user) => event(new ManagerAddedEvent($team)));

            return response()->json(
                [
                    'message' => __(''),
                    'status' => $status,
                    'code' => 2021,
                ],
                201
            );
        }

        throw new ManagersStoreException(
            $request->toArray(),
            'Błąd podczas dodawania managerów do zespołu'
        );
    }

    /**
     * @param TeamManagerRequest $request
     * @param Team $team
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws ManagersDeleteException
     */
    public function destroy(TeamManagerRequest $request, Team $team)
    {
        $this->authorize('managersCrud', $team);

        // isTeamMember() for security reasons
        $users = User::query()
            ->whereIn('id', $request->validated()['managers'])
            ->isTeamMember($team->id)
            ->get();

        $status = $team->managers()->detach($users->pluck('id'));
        if ($status > 0) {
            $this->service->revokeAllPermissions($users, $team->id);
            $this->service->grantMemberPermission($users, $team->id);
            $users->each(fn($user) => event(new ManagerRemovedEvent($team)));

            return response()->json(
                [
                    'message' => __(''),
                    'code' => 4020,
                    'status' => $status
                ],
                200
            );
        }

        throw new ManagersDeleteException(
            $request->toArray(),
            'Błąd podczas usuwania managerów z zespołu'
        );
    }
}
