<?php

namespace App\Http\Controllers\Api\Team;

use App\Events\Team\UserAddedEvent;
use App\Events\Team\UserRemovedEvent;
use App\Exceptions\Team\UserDeleteException;
use App\Exceptions\Team\UserStoreException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Team\StoreTeamUserRequest;
use App\Http\Requests\Team\UpdateTeamUserRequest;
use App\Http\Resources\Team\TeamResource;
use App\Models\Team\Team;
use App\Models\User\User;
use App\Services\Interfaces\TeamServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserController
 * @package App\Http\Controllers\Api\Team
 */
class UserController extends Controller
{
    /**
     * @var TeamServiceInterface
     */
    private TeamServiceInterface $service;

    /**
     * UserController constructor.
     * @param $service
     */
    public function __construct(TeamServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @param Team $team
     * @return TeamResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Request $request, Team $team)
    {
        $this->service->isTeamMember($team->id);
        $this->authorize('lookup', $team);

        // Load users only when user have right permission to see other members
        if (Auth::user()->hasPermissionTo('team_users_lookup_'.$team->id)) {
            $team->load('users');
        }

        return new TeamResource($team);
    }

    /**
     * @param StoreTeamUserRequest $request
     * @param Team $team
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws UserStoreException
     */
    public function store(StoreTeamUserRequest $request, Team $team)
    {
        $this->authorize('usersCrud', $team);

        $users = User::query()
            ->whereIn('id', $request->validated()['users'])
            ->isNotTeamMember($team->id)
            ->get();

        $status = $team->users()->syncWithoutDetaching($users->pluck('id'));
        if($status['attached'] > 0) {
            $this->service->grantMemberPermission($users, $team->id);
            $this->service->addToChat($users, $team->id);
            $users->each(fn($user) => event(new UserAddedEvent($team, $user)));

            return response()->json(
                [
                    'message' => __(''),
                    'status' => $status,
                    'code' => 2025,
                ],
                201
            );
        }

        throw new UserStoreException(
            $request->toArray(),
            'Błąd podczas dodawania użytkowników do zespołu'
        );
    }

    /**
     * @param UpdateTeamUserRequest $request
     * @param Team $team
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UpdateTeamUserRequest $request, Team $team)
    {
        $this->authorize('usersCrud', $team);

        $users = User::query()
            ->whereIn('id', $request->validated()['users'])
            ->isTeamMember($team->id)
            ->get();
        $this->service->updatePermissions($users, $request->validated()['permissions'], $team->id);

        return response()->json(
            [
                'message' => __(''),
                'code' => 3027,
            ],
            200
        );
    }

    /**
     * @param StoreTeamUserRequest $request
     * @param Team $team
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws UserDeleteException
     */
    public function destroy(StoreTeamUserRequest $request, Team $team)
    {
        $this->authorize('usersCrud', $team);

        $users = User::query()
            ->whereIn('id', $request->validated()['users'])
            ->isTeamMember($team->id)
            ->get();

        $status = $team->users()->detach($users->pluck('id'));

        if($status > 0) {
            $this->service->revokeAllPermissions($users, $team->id);
            $this->service->deleteFromChat($users, $team->id);
            $users->each(fn($user) => event(new UserRemovedEvent($team, $user)));

            return response()->json(
                [
                    'message' => __(''),
                    'status' => $status,
                    'code' => 4024,
                ],
                200
            );
        }

        throw new UserDeleteException(
            $request->toArray(),
            'Błąd podczas usuwania użytkowników z zespołu'
        );
    }
}
