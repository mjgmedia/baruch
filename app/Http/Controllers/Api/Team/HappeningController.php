<?php

namespace App\Http\Controllers\Api\Team;

use App\Events\Team\TeamAddedToHappeningEvent;
use App\Events\Team\TeamRemovedFromHappeningEvent;
use App\Exceptions\Team\HappeningDeleteException;
use App\Exceptions\Team\HappeningStoreException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Team\TeamHappeningRequest;
use App\Http\Resources\Happening\HappeningResource;
use App\Models\Happening\Happening;
use App\Models\Team\Team;
use App\Services\Interfaces\TeamServiceInterface;

/**
 * Class HappeningController
 * @package App\Http\Controllers\Api\Team
 */
class HappeningController extends Controller
{
    /**
     * @var TeamServiceInterface
     */
    private TeamServiceInterface $service;

    /**
     * HappeningController constructor.
     * @param $service
     */
    public function __construct(TeamServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param Team $team
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Team $team)
    {
        $this->authorize('teamCrud', $team);

        $team->load('happenings');
        return HappeningResource::collection($team->happenings);
    }

    /**
     * @param TeamHappeningRequest $request
     * @param Team $team
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws HappeningStoreException
     */
    public function store(TeamHappeningRequest $request, Team $team)
    {
        $this->authorize('teamCrud', $team);

        $happening = Happening::findOrFail($request->validated()['happening_id']);
        $status = $team->happenings()->syncWithoutDetaching($request->validated()['happening_id']);

        if ($status['attached'] > 0) {
            event(new TeamAddedToHappeningEvent($team, $happening));

            return response()->json(
                [
                    'message' => __(''),
                    'code' => 2020,
                    'status' => $status,
                ],
                201
            );
        }

        throw new HappeningStoreException(
            $request->toArray(),
            'Błąd podczas dodawania zespołu do wydarzenia'
        );
    }

    /**
     * @param TeamHappeningRequest $request
     * @param Team $team
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws HappeningDeleteException
     */
    public function destroy(TeamHappeningRequest $request, Team $team)
    {
        $this->authorize('teamCrud', $team);

        $happening = Happening::findOrFail($request->validated()['happening_id']);
        $status = $team->happenings()->detach($request->validated()['happening_id']);

        if ($status > 0) {
            event(new TeamRemovedFromHappeningEvent($team, $happening));

            return response()->json(
                [
                    'message' => __(''),
                    'code' => 4019,
                ],
                200
            );
        }

        throw new HappeningDeleteException(
            $request->toArray(),
            'Błąd podczas usuwania zespołu z wydarzenia'
        );
    }
}
