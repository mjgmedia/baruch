<?php

namespace App\Http\Controllers\Api\Team;

use App\Exceptions\Team\TeamDeleteException;
use App\Exceptions\Team\TeamUpdateException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Team\StoreTeamRequest;
use App\Http\Resources\Team\TeamResource;
use App\Models\Team\Team;
use App\Services\Interfaces\TeamServiceInterface;
use Illuminate\Http\Request;

/**
 * Class TeamController
 * @package App\Http\Controllers\Api\Team
 */
class TeamController extends Controller
{
    /**
     * @var TeamServiceInterface
     */
    private TeamServiceInterface $service;

    /**
     * TeamController constructor.
     * @param $service
     */
    public function __construct(TeamServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return TeamResource::collection(
            Team::isMebmer($request->user()->id)->get()
        );
    }

    /**
     * Returning only basic information about Team (no users() relationship)
     *
     * @param Team $team
     * @return TeamResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Team $team)
    {
        $this->authorize('membersOnly', $team);

        return TeamResource::make($team);
    }

    /**
     * @param StoreTeamRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(StoreTeamRequest $request)
    {
        $this->authorize('create', Team::class);

        $team = Team::create($request->validated());

        // Permissions creating
        $this->service->createPermissions($team->id);

        return response()->json(
            [
                'data' => TeamResource::make($team),
                'message' => __(''),
                'code' => 2024,
            ],
            201
        );

    }

    /**
     * @param StoreTeamRequest $request
     * @param Team $team
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws TeamUpdateException
     */
    public function update(StoreTeamRequest $request, Team $team)
    {
        $this->authorize('teamCrud', $team);

        $status = $team->update($request->validated());
        if ($status > 0) {
            return response()->json(
                [
                    'data' => TeamResource::make($team),
                    'message' => __(''),
                    'code' => 3026,
                    'status' => $status,
                ],
                200
            );
        }

        throw new TeamUpdateException(
            $request->toArray(),
            'Błąd podczas aktualizacji zespołu'
        );
    }

    /**
     * @param Team $team
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws TeamDeleteException
     */
    public function destroy(Team $team)
    {
        $this->authorize('teamCrud', $team);

        try {
            $status = $team->delete();
        } catch (\Throwable $exception) {
            throw new TeamDeleteException($exception);
        }

        // Removing team permissions
        $this->service->deletePermissions($team->id);
        $this->service->deleteChatChannel($team->id);

        return response()->json(
            [
                'message' => __(''),
                'code' => 4023,
                'status' => $status
            ],
            200
        );
    }
}
