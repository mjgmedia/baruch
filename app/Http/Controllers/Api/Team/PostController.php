<?php

namespace App\Http\Controllers\Api\Team;

use App\Exceptions\Team\PostDeleteException;
use App\Exceptions\Team\PostUpdateException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Team\StoreTeamPostRequest;
use App\Http\Requests\Team\StoreTeamUserRequest;
use App\Http\Resources\Team\TeamPostResource;
use App\Models\Team\TeamPost;
use App\Services\Interfaces\TeamServiceInterface;

/**
 * Class PostController
 * @package App\Http\Controllers\Api\Team
 */
class PostController extends Controller
{
    /**
     * @var TeamServiceInterface
     */
    private TeamServiceInterface $service;

    /**
     * PostController constructor.
     * @param $service
     */
    public function __construct(TeamServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $team
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($team)
    {
        $this->service->isTeamMember($team);

        $posts = TeamPost::query()
            ->where('team_id', $team)
            ->get();
        return TeamPostResource::collection($posts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTeamPostRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(StoreTeamPostRequest $request)
    {
        $this->service->isTeamMember($request->validated()['team_id']);
        $this->service->canPost($request->validated()['team_id']);

        $post = TeamPost::create($request->validated());

        return response()->json(
            [
                'data' => TeamPostResource::make($post),
                'message' => __(''),
                'code' => 2023,
            ],
            201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return TeamPostResource
     */
    public function show($id)
    {
        $post = TeamPost::with('comments')->find($id);

        $this->service->isTeamMember($post->team_id);

        return TeamPostResource::make($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreTeamUserRequest $request
     * @param TeamPost $teamPost
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws PostUpdateException
     */
    public function update(StoreTeamUserRequest $request, TeamPost $teamPost)
    {
        $this->service->isTeamMember($request->validated()['team_id']);
        $this->service->canPost($request->validated()['team_id']);

        $this->authorize('update', $teamPost);

        $status = $teamPost->update($request->validated());

        if($status > 0) {
            return response()->json(
                [
                    'data' => TeamPostResource::make($teamPost),
                    'message' => __(''),
                    'code' => 3025,
                    'status' => $status
                ],
                200
            );
        }

        throw new PostUpdateException(
            $request->toArray(),
            'Błąd podczas aktualizacji posta w zespole'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param TeamPost $teamPost
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws PostDeleteException
     */
    public function destroy(TeamPost $teamPost)
    {
        $this->service->isTeamMember($teamPost->team->id);
        $this->service->canPost($teamPost->team->id);
        $this->authorize('delete', $teamPost);

        try {
            $status = $teamPost->delete();
        } catch (\Throwable $exception) {
            throw new PostDeleteException($exception);
        }

        return response()->json(
            [
                'status' => $status,
                'message' => __(''),
                'code' => 4022,
            ],
            200
        );
    }
}
