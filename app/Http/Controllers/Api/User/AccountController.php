<?php

namespace App\Http\Controllers\Api\User;

use App\Events\User\ProfileEditedEvent;
use App\Exceptions\User\AccountUpdateException;
use App\Exceptions\User\AvatarUploadException;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UpdateAccountRequest;
use App\Http\Requests\User\UploadAvatarRequest;
use App\Http\Resources\User\UserResource;
use App\Services\Interfaces\UploadServiceInterface;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Str;

/**
 * Class AccountController
 * @package App\Http\Controllers\Api\User
 */
class AccountController extends Controller
{
    /**
     * @var UploadServiceInterface
     */
    private UploadServiceInterface $service;

    /**
     * AccountController constructor.
     * @param UploadServiceInterface $service
     */
    public function __construct(UploadServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param UpdateAccountRequest $request
     * @return UserResource|\Illuminate\Http\JsonResponse
     * @throws AccountUpdateException
     */
    public function update(UpdateAccountRequest $request)
    {
        $user = $request->user();

        // fill will only assign those in the fillable fields of user
        $user->fill($request->validated());
        $save = $user->save();

        if ($save) {
            event(new ProfileEditedEvent($user));
            return (new UserResource($user->load('profile', 'settings', 'settings.setting')))
                ->additional(
                    [
                        'message' => __('api.user.account.update.success'),
                        'code' => 3005
                    ]
                );
        } else {
            throw new AccountUpdateException();
        }
    }

    /**
     * @param UploadAvatarRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws AvatarUploadException
     */
    public function uploadAvatar(UploadAvatarRequest $request)
    {
        $user = $request->user();

        /** @var string $avatarName */
        $avatarName = sprintf(
            "%s_avatar_%s_%s.jpg",
            $user->id,
            Carbon::now()->toDateString(),
            Str::random(10)
        );
        try {
            $url = $this->service->handleImageUpload(
                $request,
                $avatarName,
                config('project.uploads.avatar_dir'),
                config('project.uploads.avatar_disk')
            );

            $user->photo_url = $url;
            $user->save();

            return response()->json(
                [
                    'status' => __('api.user.account.avatar.success'),
                    'url' => $url,
                    'code' => 5005
                ]
            );
        } catch (Exception $e) {
            throw new AvatarUploadException($e);
        }
    }
}
