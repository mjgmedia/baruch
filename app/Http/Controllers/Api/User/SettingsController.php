<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\PutSettingsRequest;
use App\Http\Resources\Settings\SettingGroupResource;
use App\Http\Resources\Settings\SettingResource;
use App\Models\Settings\Setting;
use App\Models\Settings\SettingGroup;
use App\Services\Interfaces\SettingsServiceInterface;
use Illuminate\Http\Request;

/**
 * Class SettingsController
 * @package App\Http\Controllers\Api\User
 */
class SettingsController extends Controller
{
    /**
     * @var SettingsServiceInterface
     */
    private SettingsServiceInterface $service;

    /**
     * SettingsController constructor.
     * @param SettingsServiceInterface $service
     */
    public function __construct(SettingsServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @param string $setting
     * @return SettingResource
     */
    public function get(Request $request, string $setting)
    {
        return SettingResource::make(
            Setting::where('key', $setting)
                ->with([
                    'users' =>
                    function ($query) use ($request) {
                        return $query->where('user_id', $request->user()->id);
                    }
                ])
                ->permitted()
                ->firstOrFail()
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetch(Request $request)
    {
        /* prepare grouped settings */
        $grouped = SettingGroup::whereHas(
            'settings',
            function ($query) {
                return $query->permitted();
            }
        )->get();

        /* prepare single ones */
        $noGroup = Setting::noGroup()->permitted()->get();

        return response()->json(
            [
                'group' => SettingGroupResource::collection($grouped),
                'rest' => SettingResource::collection($noGroup),
                'code' => 5007
            ],
            200
        );
    }

    /**
     * @param PutSettingsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PutSettingsRequest $request)
    {
        $statusArray = [];
        foreach ($request->settings['group'] as $setting) {
            foreach ($setting['values'] as $subSetting) {
                $statusArray[$subSetting['key']] = $this->service->updateSetting(
                    $request->user(),
                    $subSetting['key'],
                    $subSetting['value']
                );
            }
        }

        foreach ($request->settings['rest'] as $setting) {
            $statusArray[$setting['key']] = $this->service->updateSetting(
                $request->user(),
                $setting['key'],
                $setting['value']
            );
        }

        return response()->json(
            [
                'status' => $statusArray,
                'message' => __('api.settings.update'),
                'code' => 3008
            ],
            200
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        return response()->json(
            [
                'status' => $this->service->resetSettingsToDefault($request->user()),
                'message' => __('api.settings.reset'),
                'code' => 5008
            ],
            200
        );
    }
}
