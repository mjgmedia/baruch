<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class ActivityController
 * @package App\Http\Controllers\Api\User
 */
class ActivityController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request)
    {
        return response()->json(
            [
                'ts' => $request->user()->last_activity,
                'code' => 5006
            ],
            200
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $ts = now();
        $request->user()->last_activity = $ts;
        $request->user()->save();

        return response()->json(
            [
                'ts' => $ts,
                'code' => 3006
            ],
            200
        );
    }
}
