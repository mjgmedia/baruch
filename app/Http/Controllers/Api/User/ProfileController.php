<?php

namespace App\Http\Controllers\Api\User;

use App\Events\User\ProfileEditedEvent;
use App\Exceptions\User\ProfileUpdateException;
use App\Exceptions\User\TokenNotFound;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UpdateProfileRequest;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;

/**
 * Class ProfileController
 * @package App\Http\Controllers\Api\User
 */
class ProfileController extends Controller
{
    /**
     * @param Request $request
     * @return UserResource
     * @throws TokenNotFound
     */
    public function get(Request $request)
    {
        $user = $request->user();

        if (!$user) {
            throw new TokenNotFound();
        }

        return new UserResource($user->load('profile', 'settings', 'settings.setting'));
    }

    /**
     * @param UpdateProfileRequest $request
     * @return UserResource|\Illuminate\Http\JsonResponse
     * @throws ProfileUpdateException
     */
    public function update(UpdateProfileRequest $request)
    {
        $profile = $request->user()->profile;
        $updated = $profile->update($request->validated());

        if ($updated) {
            event(new ProfileEditedEvent($request->user()));
            return (new UserResource($request->user()->load('profile', 'settings', 'settings.setting')))
                ->additional(
                    [
                        'message' => __('api.user.profile.success'),
                        'code' => 3007
                    ]
                );
        } else {
            ld('Błąd aktualizacji usera. ProfileController@update ', $updated);
            throw new ProfileUpdateException('Błąd aktualizacji profilu użytkownika z poziomu ProfileController');
        }
    }
}
