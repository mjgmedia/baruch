<?php

namespace App\Http\Controllers\Api\Happening;

use App\Exceptions\Happening\HappeningDeleteException;
use App\Exceptions\Happening\HappeningUpdateException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Happening\StoreHappeningRequest;
use App\Http\Resources\Happening\HappeningResource;
use App\Models\Happening\Happening;
use App\Services\Interfaces\HappeningServiceInterface;
use Illuminate\Support\Facades\Auth;

/**
 * Class HappeningController
 * @package App\Http\Controllers\Api\Happening
 */
class HappeningController extends Controller
{
    private HappeningServiceInterface $service;

    /**
     * HappeningController constructor.
     * @param $service
     */
    public function __construct(HappeningServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return HappeningResource::collection(
            Happening::isMember(Auth::user())->get()
        );
    }

    /**
     * @param Happening $happening
     * @return HappeningResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Happening $happening)
    {
        $this->authorize('show', $happening);

        return HappeningResource::make($happening);
    }

    /**
     * @param StoreHappeningRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(StoreHappeningRequest $request)
    {
        $this->authorize('create', Happening::class);

        $happening = Happening::create($request->validated());
        $this->service->createPermissions($happening);
        $this->service->grantAllPermissions($request->user(), $happening->id);

        return response()->json(
            [
                'message' => __(''),
                'code' => 2015,
                'data' => HappeningResource::make($happening)
            ],
            201
        );
    }

    /**
     * @param StoreHappeningRequest $request
     * @param Happening $happening
     * @return \Illuminate\Http\JsonResponse
     * @throws HappeningUpdateException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(StoreHappeningRequest $request, Happening $happening)
    {
        $this->authorize('crud', $happening);

        $status = $happening->update($request->validated());

        if ($status > 0) {
            return response()->json(
                [
                    'message' => __(''),
                    'code' => 3020,
                    'status' => $status,
                ],
                200
            );
        }

        throw new HappeningUpdateException(
            $request->toArray(),
            'Błąd podczas aktualizacji wydarzenia'
        );
    }

    /**
     * @param Happening $happening
     * @return \Illuminate\Http\JsonResponse
     * @throws HappeningDeleteException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Happening $happening)
    {
        $this->authorize('crud', $happening);

        try {
            $status = $happening->delete();
        } catch (\Exception $e) {
            throw new HappeningDeleteException($e);
        }

        $this->service->removePermissions($happening->id);
        $this->service->deleteChatChannel($happening->id);

        return response()->json(
            [
                'message' => __(''),
                'code' => 4014,
                'status' => $status
            ],
            200
        );
    }
}
