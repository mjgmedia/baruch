<?php

namespace App\Http\Controllers\Api\Happening;

use App\Events\Happening\ManagerAddedEvent;
use App\Events\Happening\ManagerRemovedEvent;
use App\Exceptions\Happening\ManagerDeleteException;
use App\Exceptions\Happening\ManagerStoreException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Happening\StoreManagerRequest;
use App\Http\Requests\Happening\UpdateManagerRequest;
use App\Http\Resources\User\UserResource;
use App\Models\Happening\Happening;
use App\Models\User\User;
use App\Services\Interfaces\HappeningServiceInterface;

/**
 * Class ManagerController
 * @package App\Http\Controllers\Api\Happening
 */
class ManagerController extends Controller
{
    /**
     * @var HappeningServiceInterface
     */
    private HappeningServiceInterface $service;

    /**
     * ManagerController constructor.
     * @param $service
     */
    public function __construct(HappeningServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param Happening $happening
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Happening $happening)
    {
        $this->authorize('show', $happening);

        return UserResource::collection(
            $happening->managers
        );
    }

    /**
     * @param StoreManagerRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ManagerStoreException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(StoreManagerRequest $request)
    {
        $happening = Happening::findOrFail($request->validated()['happening_id']);
        $this->authorize('managersCrud', $happening);

        $users = User::query()
            ->whereIn('id', $request->validated()['managers'])
            ->isHappeningMember($happening->id)
            ->get();
        $status = $happening->managers()->syncWithoutDetaching($users->pluck('id'));

        if (count($status['attached']) > 0) {
            $this->service->grantManagerPermission($users, $happening->id);
            $users->each(fn($user) => event(new ManagerAddedEvent($happening, $user)));

            return response()->json(
                [
                    'message' => __(''),
                    'code' => 2016,
                    'status' => $status
                ],
                201
            );
        }

        throw new ManagerStoreException(
            $request->validated(),
            'Błąd podczas dodawania managerów do wydarzenia'
        );
    }

    /**
     * @param UpdateManagerRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UpdateManagerRequest $request)
    {
        $happening = Happening::findOrFail($request->validated()['happening_id']);
        $this->authorize('managersCrud', $happening);

        $users = User::query()
            ->whereIn('id', $request->validated()['managers'])
            ->isHappeningMember($happening->id)
            ->get();

        $this->service->updatePermissions($users, $request->validated()['permissions'], $happening->id);

        return response()->json(
            [
                'message' => __(''),
                'code' => 3021,
            ],
            200
        );
    }

    /**
     * @param StoreManagerRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ManagerDeleteException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(StoreManagerRequest $request)
    {
        $happening = Happening::findOrFail($request->validated()['happening_id']);
        $this->authorize('managersCrud', $happening);

        $users = User::query()
            ->whereIn('id', $request->validated()['managers'])
            ->isHappeningMember($happening->id)
            ->get();

        $status = $happening->managers()->detach($users->pluck('id'));

        if ($status > 0) {
            $this->service->withdrawAllPermissions($users, $happening->id);
            $this->service->grantMemberPermissions($users, $happening->id);

            $users->each(fn ($user) => event(new ManagerRemovedEvent($happening, $user)));

            return response()->json(
                [
                    'message' => __(''),
                    'code' => 4015,
                    'status' => $status
                ],
                200
            );
        }

       throw new ManagerDeleteException(
           $request->toArray(),
           'Błąd podczas usuwania managerów z wydarzenia'
       );
    }
}
