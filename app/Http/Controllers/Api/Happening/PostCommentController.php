<?php

namespace App\Http\Controllers\Api\Happening;

use App\Events\Happening\NewCommentEvent;
use App\Exceptions\Happening\PostCommentDeleteException;
use App\Exceptions\Happening\PostCommentUpdateException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Happening\StorePostCommentRequest;
use App\Http\Resources\Happening\HappeningPostCommentResource;
use App\Models\Happening\HappeningPost;
use App\Models\Happening\HappeningPostComment;
use App\Models\Team\TeamPostComment;
use App\Services\Interfaces\HappeningServiceInterface;

/**
 * Class PostCommentController
 * @package App\Http\Controllers\Api\Happening
 */
class PostCommentController extends Controller
{
    /**
     * @var HappeningServiceInterface
     */
    private HappeningServiceInterface $service;

    /**
     * PostCommentController constructor.
     * @param $service
     */
    public function __construct(HappeningServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param HappeningPost $post
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(HappeningPost $post)
    {
        $this->authorize('comments', $post);
    }

    /**
     * @param StorePostCommentRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(StorePostCommentRequest $request)
    {
        $this->service->isHappeningMember($request->validated()['happening_id']);
        $this->authorize('create', [TeamPostComment::class, $request->validated()['happening_id']]);

        $comment = HappeningPostComment::create($request->validated());
        event(new NewCommentEvent($comment));

        return response()->json(
            [
                'data' => HappeningPostCommentResource::make($comment),
                'message' => __(''),
                'code' => 2017,
            ],
            201
        );
    }

    /**
     * @param StorePostCommentRequest $request
     * @param HappeningPostComment $comment
     * @return \Illuminate\Http\JsonResponse
     * @throws PostCommentUpdateException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(StorePostCommentRequest $request, HappeningPostComment $comment)
    {
        $this->authorize('updateOrDelete', $comment);

        $status = $comment->update($request->validated());
        if ($status > 0) {
            return response()->json(
                [
                    'data' => HappeningPostCommentResource::make($comment),
                    'message' => __(''),
                    'code' => 3022,
                    'status' => $status,
                ],
                200
            );
        }

        throw new PostCommentUpdateException(
            $request->toArray(),
            'Błąd podczas aktualizacji komentarza do posta w wydarzeniu'
        );
    }

    /**
     * @param HappeningPostComment $comment
     * @return \Illuminate\Http\JsonResponse
     * @throws PostCommentDeleteException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function delete(HappeningPostComment $comment)
    {
        $this->authorize('updateOrDelete', $comment);

        try {
            $status = $comment->delete();
        } catch (\Throwable $exception) {
            throw new PostCommentDeleteException($exception);
        }

        return response()->json(
            [
                'message' => __(''),
                'code' => 4016,
                'status' => $status
            ],
            200
        );
    }
}
