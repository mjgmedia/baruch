<?php

namespace App\Http\Controllers\Api\Happening;

use App\Events\Happening\UserAddedEvent;
use App\Events\Happening\UserRemovedEvent;
use App\Exceptions\Happening\DeleteUserException;
use App\Exceptions\Happening\StoreUserException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Happening\StoreUserRequest;
use App\Http\Requests\Happening\UpdateUserRequest;
use App\Http\Resources\User\UserResource;
use App\Models\Happening\Happening;
use App\Models\User\User;
use App\Services\Interfaces\HappeningServiceInterface;

/**
 * Class UserController
 * @package App\Http\Controllers\Api\Happening
 */
class UserController extends Controller
{
    /**
     * @var HappeningServiceInterface
     */
    private HappeningServiceInterface $service;

    /**
     * UserController constructor.
     * @param $service
     */
    public function __construct(HappeningServiceInterface $service)
    {
        $this->service = $service;
    }


    /**
     * @param Happening $happening
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Happening $happening)
    {
        $this->authorize('usersIndex', $happening);

        return UserResource::collection(
            $happening->users
        );
    }

    /**
     * @param StoreUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws StoreUserException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(StoreUserRequest $request)
    {
        $happening = Happening::findOrFail($request->validated()['happening_id']);

        $this->authorize('usersCrud', $happening);

        $users = User::query()
            ->whereIn('id', $request->validated()['users'])
            ->isNotHappeningMember($happening->id)
            ->get();

        $status = $happening->users()->syncWithoutDetaching($users->pluck('id'));

        if ($status['attached'] > 0) {
            $this->service->grantMemberPermissions($users, $happening->id);
            $this->service->addToChat($users, $happening->id);
            $users->each(fn($user) => event(new UserAddedEvent($happening, $users)));

            return response()->json(
                [
                    'status' => $status,
                    'message' => __(''),
                    'code' => 2019,
                ],
                201
            );
        }

        throw new StoreUserException(
            $request->toArray(),
            'Błąd podczas dodawania użytkownika do zespołu'
        );
    }

    /**
     * @param UpdateUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UpdateUserRequest $request)
    {
        $happening = Happening::findOrFail($request->validated()['happening_id']);

        $this->authorize('usersCrud', $happening);

        $users = User::query()
            ->whereIn('id', $request->validated()['users'])
            ->isHappeningMember($happening->id)
            ->get();

        $this->service->updatePermissions($users,$request->validated()['permissions'], $happening->id);

        return response()->json(
            [
                'message' => __(''),
                'code' => 3023
            ],
            200
        );
    }

    /**
     * @param StoreUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws DeleteUserException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(StoreUserRequest $request)
    {
        $happening = Happening::findOrFail($request->validated()['happening_id']);

        $this->authorize('usersCrud', $happening);

        $users = User::query()
            ->whereIn('id', $request->validated()['users'])
            ->isHappeningMember($happening->id)
            ->get();

        $status = $happening->users()->detach($users->pluck('id'));
        if ($status > 0) {
            $this->service->withdrawAllPermissions($users, $happening->id);
            $this->service->deleteFromChat($users, $happening->id);
            $users->each(fn($user) => event(new UserRemovedEvent($happening, $user)));

            return response()->json(
                [
                    'status' => $status,
                    'message' => __(''),
                    'code' => 4018,
                ],
                200
            );
        }

        throw new DeleteUserException(
            $request->toArray(),
            'Błąd podczas usuwania pliku'
        );
    }
}
