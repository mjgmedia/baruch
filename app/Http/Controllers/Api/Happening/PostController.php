<?php

namespace App\Http\Controllers\Api\Happening;

use App\Events\Happening\NewPostEvent;
use App\Exceptions\Happening\PostDeleteException;
use App\Exceptions\Happening\PostUpdateException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Happening\StorePostRequest;
use App\Http\Resources\Happening\HappeningPostResource;
use App\Models\Happening\Happening;
use App\Models\Happening\HappeningPost;
use App\Services\Interfaces\HappeningServiceInterface;
use Exception;

/**
 * Class PostController
 * @package App\Http\Controllers\Api\Happening
 */
class PostController extends Controller
{
    /**
     * @var HappeningServiceInterface
     */
    private HappeningServiceInterface $service;

    /**
     * PostController constructor.
     * @param $service
     */
    public function __construct(HappeningServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param Happening $happening
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Happening $happening)
    {
        $this->service->isHappeningMember($happening->id);

        return HappeningPostResource::collection(
            $happening->posts
        );
    }

    /**
     * @param HappeningPost $post
     * @return HappeningPostResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(HappeningPost $post)
    {
        $this->service->isHappeningMember($post->happening->id);

        if ($this->authorize('comments', $post)) {
            $post->load('comments');
        }
        return HappeningPostResource::make($post);
    }

    /**
     * @param StorePostRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StorePostRequest $request)
    {
        $this->service->canPost($request->validated()['happening_id']);

        $post = HappeningPost::create($request->validated());
        event(new NewPostEvent($post));

        return response()->json(
            [
                'data' => HappeningPostResource::make($post),
                'message' => __(''),
                'code' => 2018
            ],
            201
        );
    }

    /**
     * @param StorePostRequest $request
     * @param HappeningPost $post
     * @return \Illuminate\Http\JsonResponse
     * @throws PostUpdateException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(StorePostRequest $request, HappeningPost $post)
    {
        $this->authorize('crud', $post);

        $status = $post->update($request->validated());

        if ($status > 0) {
            return response()->json(
                [
                    'status' => $status,
                    'message' => __(''),
                    'code' => 3023,
                ],
                200
            );
        }

        throw new PostUpdateException(
            $request->toArray(),
            'Bład podczas aktualizacji posta w wydarzeniu'
        );
    }

    /**
     * @param HappeningPost $post
     * @return \Illuminate\Http\JsonResponse
     * @throws PostDeleteException
     */
    public function destroy(HappeningPost $post)
    {
        try {
            $status = $post->delete();
        } catch (Exception $e) {
            throw new PostDeleteException($e);
        }

        return response()->json(
            [
                'status' => $status,
                'message' => __(''),
                'code' => 4017,
            ],
            200
        );
    }
}
