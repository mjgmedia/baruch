<?php

namespace App\Http\Requests\Happening;

use App\Rules\UsersInHappeningRule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateUserRequest
 * @package App\Http\Requests\Happening
 */
class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'happening_id' => 'required|numeric|exists:happenings,id',
            'users' => ['required', 'array', new UsersInHappeningRule($this->all())],
            'permissions' => 'required|array',
            'permissions.*' => 'required|numeric|exists:permissions,id'
        ];
    }
}
