<?php

namespace App\Http\Requests\Happening;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreHappeningRequest
 * @package App\Http\Requests\Happening
 */
class StoreHappeningRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'date_access' => 'required|date',
            'date_start' => 'required|date|gte:date_access',
            'date_end' => 'required|date|gt:date_start',
            'close_after_end' => 'required|boolean',
            'image_url' => 'nullable|image',
        ];
    }
}
