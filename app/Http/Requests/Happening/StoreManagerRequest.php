<?php

namespace App\Http\Requests\Happening;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreManagerRequest
 * @package App\Http\Requests\Happening
 */
class StoreManagerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'happening_id' => 'required|numeric|exists:happenings,id',
            'managers' => 'required|array',
            'managers.*' => 'required|numeric|exists:users,id'
        ];
    }
}
