<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CheckUsernameFree;
use App\Rules\ReCaptcha;

/**
 * Class RegisterUserRequest
 * @package App\Http\Requests\Auth
 */
class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required',
            'username'              => [
                                        new CheckUsernameFree,
                                        'regex:/^([a-z]|[A-Z]|[-]|[_]|[0-9]|[.])+$/',
                                        ],
            'email'                 => [ 'required', 'email', new CheckUsernameFree('email')],
            'password'              => 'required|min:8|confirmed',
            'password_confirmation' => 'required',
            'role'                  => [
                'nullable',
                'exists:roles,name'
            ],
            'recaptcha'                 => [ new ReCaptcha ],
        ];
    }
}
