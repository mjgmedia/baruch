<?php

namespace App\Http\Requests\Auth\Acl;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PermissionsRequest
 * @package App\Http\Requests\Auth\Acl
 */
class PermissionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'permissions' => 'required|array',
            'permissions.*' => 'string',
        ];
    }
}
