<?php

namespace App\Http\Requests\Notification;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PushUpdateRequest
 * @package App\Http\Requests\Notification
 */
class PushUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'endpoint' => 'required|string',
            'public_key' => 'nullable',
            'auth_token' => 'nullable',
            'content_encoding' => 'nullable',
        ];
    }
}
