<?php

namespace App\Http\Requests\File;

use Illuminate\Foundation\Http\FormRequest;

class FileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'file' => 'nullable|file',
            'desc' => 'nullable|string',
            'global' => 'required|boolean',
            'file_category_id' => 'required|numeric|exists:file_categories,id'
        ];
    }
}
