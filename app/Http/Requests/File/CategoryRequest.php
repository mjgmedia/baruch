<?php

namespace App\Http\Requests\File;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'global' => 'required|boolean',
            'team_id' => 'required_without:happening_id|numeric|exists:teams,id',
            'happening_id' => 'required_without:team_id|numeric|exists:happenings,id'
        ];
    }
}
