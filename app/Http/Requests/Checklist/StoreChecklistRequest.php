<?php

namespace App\Http\Requests\Checklist;

use Illuminate\Foundation\Http\FormRequest;

class StoreChecklistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'type' => 'required|numeric|min:1|max:2',
            'pattern' => 'required|boolean',
            'execution_date' => 'required|date',
            'team_id' => 'required_without:happening_id|numeric|exists:teams,id',
            'happening_id' => 'required_without:team_id|numeric|exists:happening,id',
        ];
    }
}
