<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class UpdateProfileRequest
 * @package App\Http\Requests\User
 */
class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => [
                'nullable'
            ],
            'address'  => [
                'nullable'
            ],
            'city'       => [
                'nullable'
            ],
            'postal_code'      => [
                'nullable',
                'regex:/^[0-9]{2}[-]?[0-9]{3}$/'
            ]
        ];
    }
}
