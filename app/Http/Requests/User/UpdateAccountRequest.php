<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Rules\MustMatchPassword;

/**
 * Class UpdateAccountRequest
 * @package App\Http\Requests\User
 */
class UpdateAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => [
                'required'
            ],
            'email'                 => [
                'required',
                'email',
                Rule::unique('users')->ignore(Auth::id())
            ],
            'username'              => [
                'required',
                Rule::unique('users')->ignore(Auth::id())
            ],
            'old_password'          => [
                'required',
                new MustMatchPassword(Auth::user()->password)
            ],
            'password'              => 'nullable|min:8|confirmed',
            'password_confirmation' => 'nullable|required_with:password'
        ];
    }
}
