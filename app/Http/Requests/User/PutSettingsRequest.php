<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class PutSettingsRequest
 * @package App\Http\Requests\User
 */
class PutSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'settings' => 'required|array',
            'settings.group' => 'array',
            'settings.group.key' => 'string',
            'settings.group.values' => 'array',
            'settings.group.*.key' => 'string',
            'settings.group.*.value' => 'boolean',
            'settings.rest' => 'array',
            'settings.rest.*.value' => 'boolean',
            'settings.rest.*.values' => 'array',
        ];
    }
}
