<?php

namespace App\Http\Requests\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateRequest
 * @package App\Http\Requests\Admin\Users
 */
class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required',
            'username'              => 'required',
            'email'                 => 'required|email|unique:users',
            'password'              => 'required|min:8|confirmed',
            'password_confirmation' => 'required',
            'roles'                 => [
                'required',
                'exists:roles,name'
            ],
            'active'                => 'boolean',
            'phone'                 => 'nullable',
            'address'               => 'nullable',
            'city'                  => 'nullable',
            'postal_code'           => 'nullable',
        ];
    }
}
