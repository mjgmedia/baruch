<?php

namespace App\Http\Requests\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

/**
 * Class UserUpdateRequest
 * @package App\Http\Requests\Admin\Users
 */
class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => [
                'nullable',
                Rule::unique('users')->ignore($this->user->id)
            ],
            'name' => 'required',
            'active' => 'required|boolean',
            'password' => 'nullable|min:8|confirmed',
            'password_confirmation' => 'nullable|required_with:password',
            'email' => [
                'nullable',
                Rule::unique('users')->ignore($this->user->id)
            ],
            'address' => 'nullable',
            'phone' => 'nullable',
            'city' => 'nullable',
            'postal_code' => [
                'nullable',
                'regex:/^[0-9]{2}-[0-9]{3}$/',
            ],
            'roles' => [
                'required',
                'exists:roles,name'
            ]
        ];
    }
}
