<?php

namespace App\Http\Resources\File;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FileCommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'comment' => $this->comment,
            'author_id' => $this->author_id,
            'author' => UserResource::make($this->whenLoaded('author')),
            'file_id' => $this->file_id,
            'file' => FileResource::make($this->whenLoaded('file')),
        ];
    }
}
