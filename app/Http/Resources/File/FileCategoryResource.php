<?php

namespace App\Http\Resources\File;

use App\Http\Resources\Happening\HappeningResource;
use App\Http\Resources\Team\TeamResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class FileCategoryResource
 * @package App\Http\Resources\File
 */
class FileCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'global' => $this->global,
            'author' => UserResource::make($this->whenLoaded('author')),
            'team' => TeamResource::make($this->whenLoaded('team')),
            'happening' => HappeningResource::make($this->whenLoaded('happening')),
            'files' => FileResource::collection($this->whenLoaded('files')),
        ];
    }
}
