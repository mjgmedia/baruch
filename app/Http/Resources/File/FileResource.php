<?php

namespace App\Http\Resources\File;

use App\Http\Resources\Happening\HappeningResource;
use App\Http\Resources\Team\TeamResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class FileResource
 * @package App\Http\Resources\File
 */
class FileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'filename' => $this->filename,
            'desc' => $this->desc,
            'global' => $this->global,
            'happening' => HappeningResource::make($this->whenLoaded('happening')),
            'team' => TeamResource::make($this->whenLoaded('team')),
            'author' => UserResource::make($this->whenLoaded('author')),
            'file_category' => FileCategoryResource::make($this->whenLoaded('category')),
            'accesses' => FileAccessResource::collection($this->whenLoaded('accesses')),
            'displayed' => UserResource::collection($this->whenLoaded('displayed')),
            'comments' => FileCommentResource::collection($this->whenLoaded('comments')),
        ];
    }
}
