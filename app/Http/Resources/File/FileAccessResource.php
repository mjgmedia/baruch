<?php

namespace App\Http\Resources\File;

use App\Http\Resources\Happening\HappeningResource;
use App\Http\Resources\Team\TeamResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Access
 * @package App\Http\Resources\File
 */
class FileAccessResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'file' => FileResource::make($this->whenLoaded('file')),
            'user' => UserResource::make($this->whenLoaded('user')),
            'team' => TeamResource::make($this->whenLoaded('team')),
            'happening' => HappeningResource::make($this->whenLoaded('happening')),
            'role' => RoleResource::make($this->whenLoaded('role')),
        ];
    }
}
