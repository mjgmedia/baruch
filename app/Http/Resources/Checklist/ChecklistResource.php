<?php

namespace App\Http\Resources\Checklist;

use App\Http\Resources\Happening\HappeningResource;
use App\Http\Resources\Team\TeamResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ChecklistResource
 * @package App\Http\Resources\Checklist
 */
class ChecklistResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'pattern' => $this->pattern,
            'execution_date' => $this->execution_date,
            'author' => UserResource::make($this->whenLoaded('author')),
            'team' => TeamResource::make($this->whenLoaded('team')),
            'happening' => HapśpeningResource::make($this->whenLoaded('happening')),
            'entries' => ChecklistEntryResource::collection($this->whenLoaded('entries')),
        ];
    }
}
