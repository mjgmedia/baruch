<?php

namespace App\Http\Resources\Checklist;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ChecklistEntryResource
 * @package App\Http\Resources\Checklist
 */
class ChecklistEntryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'desc' => $this->desc,
            'done' => $this->done,
            'should_done_at' => $this->should_done_at,
            'author' => UserResource::make($this->whenLoaded('author')),
            'checklist' => ChecklistResource::make($this->whenLoaded('checklist')),
            'users' => UserResource::collection($this->whenLoaded('users')),
            'blocks' => ChecklistEntryResource::collection($this->whenLoaded('blocks')),
            'blocking' => ChecklistEntryResource::collection($this->whenLoaded('blocking')),
        ];
    }
}
