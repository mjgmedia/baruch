<?php

namespace App\Http\Resources\Team;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TeamPostResource
 * @package App\Http\Resources\Team
 */
class TeamPostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'text' => $this->text,
            'author_id' => $this->author_id,
            'author' => UserResource::make($this->whenLoaded('author')),
            'comments' => TeamPostCommentResource::collection($this->whenLoaded('comments')),
        ];

    }
}
