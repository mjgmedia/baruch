<?php

namespace App\Http\Resources\Team;

use App\Http\Resources\Checklist\ChecklistResource;
use App\Http\Resources\File\FileAccessResource;
use App\Http\Resources\File\FileCategoryResource;
use App\Http\Resources\File\FileResource;
use App\Http\Resources\Happening\HappeningResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TeamResource
 * @package App\Http\Resources\Team
 */
class TeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'avatar' => $this->avatar,
            'users' => UserResource::collection($this->whenLoaded('users')),
            'managers' => UserResource::collection($this->whenLoaded('managers')),
            'happenings' => HappeningResource::collection($this->whenLoaded('happenings')),
            'checklists' => ChecklistResource::collection($this->whenLoaded('checklists')),
            'files' => FileResource::collection($this->whenLoaded('files')),
            'file_categories' => FileCategoryResource::collection($this->whenLoaded('fileCategories')),
            'file_accesses' => FileAccessResource::collection($this->whenLoaded('fileAccesses')),
            'posts' => TeamPostResource::collection($this->whenLoaded('posts')),
        ];
    }
}
