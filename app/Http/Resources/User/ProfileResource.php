<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ProfileResource
 * @package App\Http\Resources
 */
class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'phone'         => $this->phone,
            'address'       => $this->address,
            'city'          => $this->city,
            'postal_code'   => $this->postal_code,
        ];
    }
}
