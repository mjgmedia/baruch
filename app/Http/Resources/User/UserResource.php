<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Settings\SettingUserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 * @package App\Http\Resources
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'username' => $this->username,
            'email' => $this->email,
            'photo_url' => $this->photo_url,
            'active' => !$this->email_verified_at == null,
            'last_activity' => $this->last_activity,
            /* load the user profile that will be use as default for payment and shipment */
            'profile' => new ProfileResource($this->whenLoaded('profile')),
            /* list all roles */
            'roles' => $this->all_roles,
            /* list all users inherited permissions from any role */
            'permissions' => $this->all_permissions,
            /* list all granted permissions to a user */
            /* use mainly in our vue auth permission check */
            'can' => $this->can,
            'settings' => SettingUserResource::collection($this->whenLoaded('settings')),
        ];
    }
}
