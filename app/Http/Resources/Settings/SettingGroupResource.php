<?php

namespace App\Http\Resources\Settings;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SettingGroupResource
 * @package App\Http\Resources
 */
class SettingGroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'key' => $this->key,
            'name' => __('settings.' . $this->key),
            'values' => SettingResource::collection($this->settings),
        ];
    }
}
