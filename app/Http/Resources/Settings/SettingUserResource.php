<?php

namespace App\Http\Resources\Settings;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SettingUserResource
 * @package App\Http\Resources
 */
class SettingUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'setting_id' => $this->setting_id,
            'setting' => SettingResource::make($this->whenLoaded('setting')),
            'value' => $this->value,
        ];
    }
}
