<?php

namespace App\Http\Resources\Chat;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ChannelResource
 * @package App\Http\Resources\Chat
 */
class ChannelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'team_id' => $this->team_id,
            'happening_id' => $this->happening_id,
            'slug' => $this->slug,
            'name' => $this->name,
            'messages' => MessageResource::collection($this->whenLoaded('messages'))
        ];
    }
}
