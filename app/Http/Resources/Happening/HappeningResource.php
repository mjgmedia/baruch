<?php

namespace App\Http\Resources\Happening;

use App\Http\Resources\Checklist\ChecklistResource;
use App\Http\Resources\File\FileAccessResource;
use App\Http\Resources\File\FileCategoryResource;
use App\Http\Resources\File\FileResource;
use App\Http\Resources\Team\TeamResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class HappeningResource
 * @package App\Http\Resources\Happening
 */
class HappeningResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'date_access' => $this->date_access,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'close_after_end' => $this->close_after_end,
            'image_url' => $this->image_url,
            'users' => UserResource::collection($this->whenLoaded('users')),
            'managers' => UserResource::collection($this->whenLoaded('managers')),
            'teams' => TeamResource::collection($this->whenLoaded('teams')),
            'checklists' => ChecklistResource::collection($this->whenLoaded('checklists')),
            'files' => FileResource::collection($this->whenLoaded('files')),
            'file_category' => FileCategoryResource::collection($this->whenLoaded('fileCategories')),
            'file_accesses' => FileAccessResource::collection($this->whenLoaded('fileAccesses')),
            'posts' => HappeningPostResource::collection($this->whenLoaded('posts')),
        ];
    }
}
