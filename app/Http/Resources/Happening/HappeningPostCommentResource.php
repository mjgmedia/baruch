<?php

namespace App\Http\Resources\Happening;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class HappeningPostCommentResource
 * @package App\Http\Resources\Happening
 */
class HappeningPostCommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'author_id' => $this->author_id,
            'author' => UserResource::make($this->whenLoaded('author')),
            'text' => $this->text,
        ];
    }
}
