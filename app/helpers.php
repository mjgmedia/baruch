<?php

if (! function_exists('sentrit')) {
    function sentrit($exception)
    {
        if (config('project.sentry.enable') == true && app()->bound('sentry')) {
            app('sentry')->captureException($exception);
        }
    }
}