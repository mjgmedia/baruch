<?php

namespace App\Events\Happening;

use App\Models\Happening\HappeningPost;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class NewPostEvent
 * @package App\Events\Happening
 */
class NewPostEvent
{
    use Dispatchable;
    use SerializesModels;

    /**
     * @var HappeningPost
     */
    public HappeningPost $post;

    /**
     * Create a new event instance.
     *
     * @param HappeningPost $happeningPost
     */
    public function __construct(HappeningPost $happeningPost)
    {
        $this->post = $happeningPost;
        $this->post->load('happening', 'author');
    }
}
