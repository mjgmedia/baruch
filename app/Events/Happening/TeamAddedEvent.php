<?php

namespace App\Events\Happening;

use App\Models\Happening\Happening;
use App\Models\Team\Team;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class TeamAddedEvent
 * @package App\Events\Happening
 */
class TeamAddedEvent
{
    use Dispatchable;
    use SerializesModels;

    /**
     * @var Happening
     */
    public Happening $happening;
    /**
     * @var Team
     */
    public Team $team;

    /**
     * Create a new event instance.
     *
     * @param Happening $happening
     * @param Team $team
     */
    public function __construct(Happening $happening, Team $team)
    {
        $this->happening = $happening;
        $this->team = $team;
    }
}
