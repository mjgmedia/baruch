<?php

namespace App\Events\Happening;

use App\Models\Happening\Happening;
use App\Models\User\User;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserAddedEvent
 * @package App\Events\Happening
 */
class UserAddedEvent
{
    use Dispatchable;
    use SerializesModels;

    /**
     * @var Happening
     */
    public Happening $happening;
    /**
     * @var User
     */
    public User $user;

    /**
     * Create a new event instance.
     *
     * @param Happening $happening
     * @param User $user
     */
    public function __construct(Happening $happening, User $user)
    {
        $this->happening = $happening;
        $this->user = $user;
    }
}
