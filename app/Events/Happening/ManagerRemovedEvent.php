<?php

namespace App\Events\Happening;

use App\Models\Happening\Happening;
use App\Models\User\User;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class ManagerRemovedEvent
 * @package App\Events\Happening
 */
class ManagerRemovedEvent
{
    use Dispatchable;
    use SerializesModels;

    /**
     * @var Happening
     */
    public Happening $happening;
    /**
     * @var User
     */
    public User $manager;

    /**
     * Create a new event instance.
     *
     * @param Happening $happening
     * @param User $user
     */
    public function __construct(Happening $happening, User $user)
    {
        $this->happening = $happening;
        $this->manager = $user;
    }
}
