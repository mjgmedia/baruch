<?php

namespace App\Events\Happening;

use App\Models\Happening\HappeningPostComment;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class NewCommentEvent
 * @package App\Events\Happening
 */
class NewCommentEvent
{
    use Dispatchable;
    use SerializesModels;

    /**
     * @var HappeningPostComment
     */
    public HappeningPostComment $comment;

    /**
     * Create a new event instance.
     *
     * @param HappeningPostComment $comment
     */
    public function __construct(HappeningPostComment $comment)
    {
        $this->comment = $comment;
        $this->comment->load('author', 'post', 'post.happening');
    }
}
