<?php

namespace App\Events\File;

use App\Models\File\File;
use App\Models\User\User;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class NewCommentEvent
 * @package App\Events\File
 */
class NewCommentEvent
{
    use Dispatchable;
    use SerializesModels;

    /**
     * @var File
     */
    public File $file;
    /**
     * @var User
     */
    public User $commentAuthor;

    /**
     * Create a new event instance.
     *
     * @param File $file
     * @param User $user
     */
    public function __construct(File $file, User $user)
    {
        $this->file = $file;
        $this->commentAuthor = $user;
    }
}
