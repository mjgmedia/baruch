<?php

namespace App\Events\File;

use App\Models\File\File;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class NewFileEvent
 * @package App\Events\File
 */
class NewFileEvent
{
    use Dispatchable;
    use SerializesModels;

    /**
     * @var File
     */
    public File $file;

    /**
     * Create a new event instance.
     *
     * @param File $file
     */
    public function __construct(File $file)
    {
        $this->file = $file;
    }
}
