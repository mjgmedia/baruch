<?php

namespace App\Events\Checklist;

use App\Models\Checklist\ChecklistEntry;
use App\Models\User\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class EntryReceivedEvent
 * @package App\Events\Checklist
 */
class EntryReceivedEvent implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * @var ChecklistEntry
     */
    public ChecklistEntry $entry;
    /**
     * @var User
     */
    public User $user;

    /**
     * Create a new event instance.
     *
     * @param ChecklistEntry $entry
     * @param User $user
     */
    public function __construct(ChecklistEntry $entry, User $user)
    {
        $this->entry = $entry;
        $this->user = $user;
    }
}
