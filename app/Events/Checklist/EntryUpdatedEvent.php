<?php

namespace App\Events\Checklist;

use App\Models\Checklist\ChecklistEntry;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class EntryUpdatedEvent
 * @package App\Events\Checklist
 */
class EntryUpdatedEvent
{
    use Dispatchable;
    use SerializesModels;

    /**
     * @var ChecklistEntry
     */
    public ChecklistEntry $entry;

    /**
     * Create a new event instance.
     *
     * @param ChecklistEntry $checklistEntry
     */
    public function __construct(ChecklistEntry $checklistEntry)
    {
        $this->entry = $checklistEntry;
    }
}
