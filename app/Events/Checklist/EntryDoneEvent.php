<?php

namespace App\Events\Checklist;

use App\Models\Checklist\ChecklistEntry;
use App\Models\User\User;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class EntryDoneEvent
 * @package App\Events\Checklist
 */
class EntryDoneEvent
{
    use Dispatchable;
    use SerializesModels;

    /**
     * @var ChecklistEntry
     */
    public ChecklistEntry $entry;
    /**
     * @var User
     */
    public User $user;

    /**
     * Create a new event instance.
     *
     * @param ChecklistEntry $checklistEntry
     * @param User $user
     */
    public function __construct(ChecklistEntry $checklistEntry, User $user)
    {
        $this->entry = $checklistEntry;
        $this->user = $user;
    }
}
