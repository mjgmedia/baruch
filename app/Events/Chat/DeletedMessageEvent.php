<?php

namespace App\Events\Chat;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class DeletedMessageEvent
 * @package App\Events\Chat
 */
class DeletedMessageEvent implements ShouldBroadcast
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * @var int
     */
    public int $deletedId;
    /**
     * @var string
     */
    public string $channelName;

    /**
     * Create a new event instance.
     *
     * @param int $deletedId
     * @param string $channelName
     */
    public function __construct(int $deletedId, string $channelName)
    {
        $this->deletedId = $deletedId;
        $this->channelName = $channelName;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel($this->channelName);
    }
}
