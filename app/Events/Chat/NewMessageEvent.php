<?php

namespace App\Events\Chat;

use App\Models\Chat\ChatMessage;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class ChatNewMessageEvent
 * @package App\Events\Chat
 */
class NewMessageEvent implements ShouldBroadcast
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * @var ChatMessage
     */
    public ChatMessage $message;
    /**
     * @var string
     */
    public string $channelName;

    /**
     * Create a new event instance.
     *
     * @param ChatMessage $message
     * @param string $channelName
     */
    public function __construct(ChatMessage $message, string $channelName)
    {
        $this->message = $message;
        $this->channelName = $channelName;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel($this->channelName);
    }
}
