<?php

namespace App\Events\Auth;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class LoginFailedEvent
 * @package App\Events\Auth
 */
class LoginFailedEvent
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * @var string
     */
    public $username;
    /**
     * @var string
     */
    public $ip;
    /**
     * @var \Illuminate\Support\Carbon
     */
    public $timestamp;

    /**
     * Create a new event instance.
     *
     * @param string $username
     * @param string $ip
     */
    public function __construct(string $username, string $ip)
    {
        $this->username = $username;
        $this->ip = $ip;
        $this->timestamp = now();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('devs');
    }
}
