<?php

namespace App\Events\Team;

use App\Models\Team\TeamPostComment;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class NewCommentEvent
 * @package App\Events\Team
 */
class NewCommentEvent
{
    use Dispatchable;
    use SerializesModels;

    /**
     * @var TeamPostComment
     */
    public TeamPostComment $comment;

    /**
     * Create a new event instance.
     *
     * @param TeamPostComment $teamPostComment
     */
    public function __construct(TeamPostComment $teamPostComment)
    {
        $this->comment = $teamPostComment;
        $this->comment->load('author', 'post', 'post.team');
    }
}
