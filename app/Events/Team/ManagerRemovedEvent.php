<?php

namespace App\Events\Team;

use App\Models\Team\Team;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class ManagerRemovedEvent
 * @package App\Events\Team
 */
class ManagerRemovedEvent
{
    use Dispatchable;
    use SerializesModels;


    /**
     * @var Team
     */
    public Team $team;

    /**
     * Create a new event instance.
     *
     * @param Team $team
     */
    public function __construct(Team $team)
    {
        $this->team = $team;
    }
}
