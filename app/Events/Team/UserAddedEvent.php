<?php

namespace App\Events\Team;

use App\Models\Team\Team;
use App\Models\User\User;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserAddedEvent
 * @package App\Events\Team
 */
class UserAddedEvent
{
    use Dispatchable;
    use SerializesModels;

    /**
     * @var Team
     */
    public Team $team;

    /**
     * @var
     */
    public User $user;

    /**
     * Create a new event instance.
     *
     * @param Team $team
     * @param User $user
     */
    public function __construct(Team $team, User $user)
    {
        $this->team = $team;
        $this->user = $user;
    }
}
