<?php

namespace App\Events\Team;

use App\Models\Team\Team;
use App\Models\Happening\Happening;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class TeamAddedToHappeningEvent
 * @package App\Events\Team
 */
class TeamAddedToHappeningEvent
{
    use Dispatchable;
    use SerializesModels;

    /**
     * @var Team
     */
    public Team $team;
    /**
     * @var Happening
     */
    public Happening $happening;

    /**
     * Create a new event instance.
     *
     * @param Team $team
     * @param Happening $happening
     */
    public function __construct(Team $team, Happening $happening)
    {
        $this->team = $team;
        $this->happening = $happening;
    }
}
