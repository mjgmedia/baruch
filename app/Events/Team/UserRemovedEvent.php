<?php

namespace App\Events\Team;

use App\Models\Team\Team;
use App\Models\User\User;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserRemovedEvent
 * @package App\Events\Team
 */
class UserRemovedEvent
{
    use Dispatchable;
    use SerializesModels;

    /**
     * @var User
     */
    public User $user;
    /**
     * @var Team
     */
    public Team $team;

    /**
     * Create a new event instance.
     *
     * @param Team $team
     * @param User $user
     */
    public function __construct(Team $team, User $user)
    {
        $this->user = $user;
        $this->team = $team;
    }
}
