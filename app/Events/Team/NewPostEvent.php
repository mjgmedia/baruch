<?php

namespace App\Events\Team;

use App\Models\Team\TeamPost;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class NewPostEvent
 * @package App\Events\Team
 */
class NewPostEvent
{
    use Dispatchable;
    use SerializesModels;

    /**
     * @var TeamPost
     */
    public TeamPost $post;

    /**
     * Create a new event instance.
     *
     * @param TeamPost $post
     */
    public function __construct(TeamPost $post)
    {
        $this->post = $post;
        $this->post->load('author', 'team');
    }
}
