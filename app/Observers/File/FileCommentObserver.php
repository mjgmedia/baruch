<?php

namespace App\Observers\File;

use App\Models\File\FileComment;
use Illuminate\Support\Facades\Auth;

/**
 * Class FileCommentObserver
 * @package App\Observers
 */
class FileCommentObserver
{
    /**
     * Handle the models file file comment "created" event.
     *
     * @param  FileComment  $fileComment
     * @return void
     */
    public function creating(FileComment $fileComment)
    {
        // without this if() {} factories will fail
        if($fileComment->author_id == null) {
            $fileComment->author()->associate(Auth::user());
        }
    }

    /**
     * Handle the models file file comment "updated" event.
     *
     * @param  FileComment  $fileComment
     * @return void
     */
    public function updated(FileComment $fileComment)
    {
        //
    }

    /**
     * Handle the models file file comment "deleted" event.
     *
     * @param  FileComment  $fileComment
     * @return void
     */
    public function deleted(FileComment $fileComment)
    {
        //
    }

    /**
     * Handle the models file file comment "restored" event.
     *
     * @param  FileComment  $fileComment
     * @return void
     */
    public function restored(FileComment $fileComment)
    {
        //
    }

    /**
     * Handle the models file file comment "force deleted" event.
     *
     * @param  FileComment  $fileComment
     * @return void
     */
    public function forceDeleted(FileComment $fileComment)
    {
        //
    }
}
