<?php

namespace App\Observers\File;

use App\Models\File\FileCategory;
use Illuminate\Support\Facades\Auth;

/**
 * Class FileCategoryObserver
 * @package App\Observers
 */
class FileCategoryObserver
{
    /**
     * Handle the models file file category "created" event.
     *
     * @param  FileCategory  $fileCategory
     * @return void
     */
    public function creating(FileCategory $fileCategory)
    {
        // without this if() {} factories will fail
        if($fileCategory->author_id == null) {
            $fileCategory->author()->associate(Auth::user());
        }
    }

    /**
     * Handle the models file file category "updated" event.
     *
     * @param  FileCategory  $fileCategory
     * @return void
     */
    public function updated(FileCategory $fileCategory)
    {
        //
    }

    /**
     * Handle the models file file category "deleted" event.
     *
     * @param  FileCategory  $fileCategory
     * @return void
     */
    public function deleted(FileCategory $fileCategory)
    {
        //
    }

    /**
     * Handle the models file file category "restored" event.
     *
     * @param  FileCategory  $fileCategory
     * @return void
     */
    public function restored(FileCategory $fileCategory)
    {
        //
    }

    /**
     * Handle the models file file category "force deleted" event.
     *
     * @param  FileCategory  $fileCategory
     * @return void
     */
    public function forceDeleted(FileCategory $fileCategory)
    {
        //
    }
}
