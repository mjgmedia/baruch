<?php

namespace App\Observers\File;

use App\Models\File\File;
use Illuminate\Support\Facades\Auth;

/**
 * Class FileObserver
 * @package App\Observers
 */
class FileObserver
{
    /**
     * Handle the models file file "created" event.
     *
     * @param  File  $file
     * @return void
     */
    public function creating(File $file)
    {
        // without this if() {} factories will fail
        if($file->author_id == null) {
            $file->author()->associate(Auth::user());
        }
    }

    /**
     * Handle the models file file "updated" event.
     *
     * @param  File  $file
     * @return void
     */
    public function updated(File $file)
    {
        //
    }

    /**
     * Handle the models file file "deleted" event.
     *
     * @param  File  $file
     * @return void
     */
    public function deleted(File $file)
    {
        //
    }

    /**
     * Handle the models file file "restored" event.
     *
     * @param  File  $file
     * @return void
     */
    public function restored(File $file)
    {
        //
    }

    /**
     * Handle the models file file "force deleted" event.
     *
     * @param  File  $file
     * @return void
     */
    public function forceDeleted(File $file)
    {
        //
    }
}
