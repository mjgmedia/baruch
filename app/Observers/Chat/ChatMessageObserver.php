<?php

namespace App\Observers\Chat;

use App\Models\Chat\ChatMessage;
use Illuminate\Support\Facades\Auth;

/**
 * Class ChatMessageObserver
 * @package App\Observers\Chat
 */
class ChatMessageObserver
{
    /**
     * Handle the chat message "created" event.
     *
     * @param  \App\Models\Chat\ChatMessage  $chatMessage
     * @return void
     */
    public function created(ChatMessage $chatMessage)
    {
        // attaching users of this message from channel users
        $chatMessage->users()->attach(
            $chatMessage->channel->users()->pluck('user_id')
        );
    }

    /**
     * Handle the chat message "updated" event.
     *
     * @param  \App\Models\Chat\ChatMessage  $chatMessage
     * @return void
     */
    public function creating(ChatMessage $chatMessage)
    {
        if ($chatMessage->author_id == null) {
            $chatMessage->author()->associate(Auth::user());
        }
    }

    /**
     * Handle the chat message "deleted" event.
     *
     * @param  \App\Models\Chat\ChatMessage  $chatMessage
     * @return void
     */
    public function deleted(ChatMessage $chatMessage)
    {
        //
    }

    /**
     * Handle the chat message "restored" event.
     *
     * @param  \App\Models\Chat\ChatMessage  $chatMessage
     * @return void
     */
    public function restored(ChatMessage $chatMessage)
    {
        //
    }

    /**
     * Handle the chat message "force deleted" event.
     *
     * @param  \App\Models\Chat\ChatMessage  $chatMessage
     * @return void
     */
    public function forceDeleted(ChatMessage $chatMessage)
    {
        //
    }
}
