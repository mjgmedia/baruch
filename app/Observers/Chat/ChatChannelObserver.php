<?php

namespace App\Observers\Chat;

use App\Models\Chat\ChatChannel;
use Illuminate\Support\Facades\Auth;

class ChatChannelObserver
{
    /**
     * Handle the chat channel "created" event.
     *
     * @param  \App\Models\Chat\ChatChannel  $chatChannel
     * @return void
     */
    public function creating(ChatChannel $chatChannel)
    {
        if ($chatChannel->author_id == null) {
            $chatChannel->author()->associate(Auth::user());
        }
    }

    /**
     * Handle the chat channel "updated" event.
     *
     * @param  \App\Models\Chat\ChatChannel  $chatChannel
     * @return void
     */
    public function updated(ChatChannel $chatChannel)
    {
        //
    }

    /**
     * Handle the chat channel "deleted" event.
     *
     * @param  \App\Models\Chat\ChatChannel  $chatChannel
     * @return void
     */
    public function deleted(ChatChannel $chatChannel)
    {
        //
    }

    /**
     * Handle the chat channel "restored" event.
     *
     * @param  \App\Models\Chat\ChatChannel  $chatChannel
     * @return void
     */
    public function restored(ChatChannel $chatChannel)
    {
        //
    }

    /**
     * Handle the chat channel "force deleted" event.
     *
     * @param  \App\Models\Chat\ChatChannel  $chatChannel
     * @return void
     */
    public function forceDeleted(ChatChannel $chatChannel)
    {
        //
    }
}
