<?php

namespace App\Observers\Happening;

use App\Models\Happening\HappeningPost;
use Illuminate\Support\Facades\Auth;

/**
 * Class HappeningPostObserver
 * @package App\Observers
 */
class HappeningPostObserver
{
    /**
     * Handle the models happening happening post "created" event.
     *
     * @param  HappeningPost  $post
     * @return void
     */
    public function created(HappeningPost $post)
    {
        // without this if() {} factories will fail
        if($post->author_id == null) {
            $post->author()->associate(Auth::user());
        }
    }

    /**
     * Handle the models happening happening post "updated" event.
     *
     * @param  HappeningPost  $post
     * @return void
     */
    public function updated(HappeningPost $post)
    {
        //
    }

    /**
     * Handle the models happening happening post "deleted" event.
     *
     * @param  HappeningPost  $post
     * @return void
     */
    public function deleted(HappeningPost $post)
    {
        //
    }

    /**
     * Handle the models happening happening post "restored" event.
     *
     * @param  HappeningPost  $post
     * @return void
     */
    public function restored(HappeningPost $post)
    {
        //
    }

    /**
     * Handle the models happening happening post "force deleted" event.
     *
     * @param  HappeningPost  $post
     * @return void
     */
    public function forceDeleted(HappeningPost $post)
    {
        //
    }
}
