<?php

namespace App\Observers\Happening;

use App\Models\Happening\HappeningPostComment;
use Illuminate\Support\Facades\Auth;

/**
 * Class HappeningPostCommentObserver
 * @package App\Observers
 */
class HappeningPostCommentObserver
{
    /**
     * Handle the models happening happening post comment "created" event.
     *
     * @param  HappeningPostComment  $comment
     * @return void
     */
    public function creating(HappeningPostComment $comment)
    {
        // without this if() {} factories will fail
        if($comment->author_id == null) {
            $comment->author()->associate(Auth::user());
        }
    }

    /**
     * Handle the models happening happening post comment "updated" event.
     *
     * @param  HappeningPostComment  $comment
     * @return void
     */
    public function updated(HappeningPostComment $comment)
    {
        //
    }

    /**
     * Handle the models happening happening post comment "deleted" event.
     *
     * @param  HappeningPostComment  $comment
     * @return void
     */
    public function deleted(HappeningPostComment $comment)
    {
        //
    }

    /**
     * Handle the models happening happening post comment "restored" event.
     *
     * @param  HappeningPostComment  $comment
     * @return void
     */
    public function restored(HappeningPostComment $comment)
    {
        //
    }

    /**
     * Handle the models happening happening post comment "force deleted" event.
     *
     * @param  HappeningPostComment  $comment
     * @return void
     */
    public function forceDeleted(HappeningPostComment $comment)
    {
        //
    }
}
