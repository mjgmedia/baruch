<?php

namespace App\Observers\Checklist;

use App\Models\Checklist\ChecklistEntry;
use Illuminate\Support\Facades\Auth;

/**
 * Class ChecklistEntryObserver
 * @package App\Observers
 */
class ChecklistEntryObserver
{
    /**
     * Handle the models checklist checklist entry "created" event.
     *
     * @param ChecklistEntry $entry
     * @return void
     */
    public function creating(ChecklistEntry $entry)
    {
        // without this if() {} factories will fail
        if($entry->author_id == null) {
            $entry->author()->associate(Auth::user());
        }
    }

    /**
     * Handle the models checklist checklist entry "updated" event.
     *
     * @param  ChecklistEntry  $entry
     * @return void
     */
    public function updated(ChecklistEntry $entry)
    {
        //
    }

    /**
     * Handle the models checklist checklist entry "deleted" event.
     *
     * @param  ChecklistEntry  $entry
     * @return void
     */
    public function deleted(ChecklistEntry $entry)
    {
        //
    }

    /**
     * Handle the models checklist checklist entry "restored" event.
     *
     * @param  ChecklistEntry  $entry
     * @return void
     */
    public function restored(ChecklistEntry $entry)
    {
        //
    }

    /**
     * Handle the models checklist checklist entry "force deleted" event.
     *
     * @param  ChecklistEntry  $entry
     * @return void
     */
    public function forceDeleted(ChecklistEntry $entry)
    {
        //
    }
}
