<?php

namespace App\Observers\Checklist;

use App\Models\Checklist\Checklist;
use Illuminate\Support\Facades\Auth;

/**
 * Class ChecklistObserver
 * @package App\Observers
 */
class ChecklistObserver
{
    /**
     * Handle the models checklist checklist "created" event.
     *
     * @param  Checklist  $checklist
     * @return void
     */
    public function creating(Checklist $checklist)
    {
        // without this if() {} factories will fail
        if($checklist->author_id == null) {
            $checklist->author()->associate(Auth::user());
        }
    }

    /**
     * Handle the models checklist checklist "updated" event.
     *
     * @param  Checklist  $checklist
     * @return void
     */
    public function updated(Checklist $checklist)
    {
        //
    }

    /**
     * Handle the models checklist checklist "deleted" event.
     *
     * @param  Checklist  $checklist
     * @return void
     */
    public function deleted(Checklist $checklist)
    {
        //
    }

    /**
     * Handle the models checklist checklist "restored" event.
     *
     * @param  Checklist  $checklist
     * @return void
     */
    public function restored(Checklist $checklist)
    {
        //
    }

    /**
     * Handle the models checklist checklist "force deleted" event.
     *
     * @param  Checklist  $checklist
     * @return void
     */
    public function forceDeleted(Checklist $checklist)
    {
        //
    }
}
