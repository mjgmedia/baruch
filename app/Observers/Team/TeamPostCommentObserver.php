<?php

namespace App\Observers\Team;

use App\Models\Team\TeamPostComment;
use Illuminate\Support\Facades\Auth;

/**
 * Class TeamPostCommentObserver
 * @package App\Observers
 */
class TeamPostCommentObserver
{
    /**
     * Handle the models team team post comment "created" event.
     *
     * @param TeamPostComment $comment
     * @return void
     */
    public function creating(TeamPostComment $comment)
    {
        // without this if() factories will fail
        if ($comment->author_id == null) {
            $comment->author()->associate(Auth::user());
        }
    }

    /**
     * Handle the models team team post comment "updated" event.
     *
     * @param TeamPostComment $comment
     * @return void
     */
    public function updated(TeamPostComment $comment)
    {
        //
    }

    /**
     * Handle the models team team post comment "deleted" event.
     *
     * @param TeamPostComment $comment
     * @return void
     */
    public function deleted(TeamPostComment $comment)
    {
        //
    }

    /**
     * Handle the models team team post comment "restored" event.
     *
     * @param TeamPostComment $comment
     * @return void
     */
    public function restored(TeamPostComment $comment)
    {
        //
    }

    /**
     * Handle the models team team post comment "force deleted" event.
     *
     * @param TeamPostComment $comment
     * @return void
     */
    public function forceDeleted(TeamPostComment $comment)
    {
        //
    }
}
