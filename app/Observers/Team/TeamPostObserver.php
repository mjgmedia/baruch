<?php

namespace App\Observers\Team;

use App\Models\Team\TeamPost;
use Illuminate\Support\Facades\Auth;

/**
 * Class TeamPostObserver
 * @package App\Observers
 */
class TeamPostObserver
{
    /**
     * Handle the models team team post "created" event.
     *
     * @param  TeamPost  $post
     * @return void
     */
    public function creating(TeamPost $post)
    {
        // without this if() {} factories will fail
        if ($post->author_id == null) {
            $post->author()->associate(Auth::user());
        }
    }

    /**
     * Handle the models team team post "updated" event.
     *
     * @param  TeamPost  $post
     * @return void
     */
    public function updated(TeamPost $post)
    {
        //
    }

    /**
     * Handle the models team team post "deleted" event.
     *
     * @param  TeamPost  $post
     * @return void
     */
    public function deleted(TeamPost $post)
    {
        //
    }

    /**
     * Handle the models team team post "restored" event.
     *
     * @param  TeamPost  $post
     * @return void
     */
    public function restored(TeamPost $post)
    {
        //
    }

    /**
     * Handle the models team team post "force deleted" event.
     *
     * @param  TeamPost  $post
     * @return void
     */
    public function forceDeleted(TeamPost $post)
    {
        //
    }
}
