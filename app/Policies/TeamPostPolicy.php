<?php

namespace App\Policies;

use App\Models\Team\TeamPost;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeamPostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the team post.
     *
     * @param User $user
     * @param TeamPost $teamPost
     * @return mixed
     */
    public function update(User $user, TeamPost $teamPost)
    {
        return $teamPost->author_id === $user->id;
    }

    /**
     * Determine whether the user can delete the team post.
     *
     * @param User $user
     * @param TeamPost $teamPost
     * @return mixed
     */
    public function delete(User $user, TeamPost $teamPost)
    {
        return $teamPost->author_id === $user->id ||
            $user->hasPermissionTo('team_crud_' . $teamPost->team->id);
    }
}
