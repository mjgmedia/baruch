<?php

namespace App\Policies;

use App\Models\Checklist\Checklist;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class ChecklistPolicy
 * @package App\Policies
 */
class ChecklistPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * @param User $user
     * @param Checklist $checklist
     * @return bool
     */
    public function allowedToSee(User $user, Checklist $checklist)
    {
        if (Checklist::query()
                ->where('id', $checklist->id)
                ->hasAssignments($user->id)
                ->count() > 0) {
            return true;
        }

        if ($user->hasPermissionTo('happening_checklist_entries_view_' . $checklist->happening_id)) {
            return true;
        }

        if ($user->hasPermissionTo('team_checklist_entries_view_' . $checklist->team_id)) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function patterns(User $user)
    {
        return $user->hasPermissionTo('checklist_patterns');
    }

    /**
     * @param User $user
     * @param Checklist $checklist
     * @return bool
     */
    public function update(User $user, Checklist $checklist)
    {
        if ($checklist->author->id === $user->id) {
            return true;
        }

        if ($user->hasPermissionTo('happening_checklist_crud_' . $checklist->happening_id)) {
            return true;
        }

        if ($user->hasPermissionTo('team_checklist_crud_' . $checklist->team_id)) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Checklist $checklist
     * @return bool
     */
    public function delete(User $user, Checklist $checklist)
    {
        return $this->update($user, $checklist);
    }
}
