<?php

namespace App\Policies;

use App\Models\Happening\HappeningPostComment;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class HappeningPostCommentPolicy
 * @package App\Policies
 */
class HappeningPostCommentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create(User $user, int $happeningId)
    {
        return $user->hasPermissionTo('happening_comments_'.$happeningId);
    }

    /**
     * @param User $user
     * @param HappeningPostComment $comment
     * @return bool
     */
    public function updateOrDelete(User $user, HappeningPostComment $comment)
    {
        return $user->hasPermissionTo('happening_comments_'.$comment->post->happening_id) && $comment->author_id == $user->id;
    }
}
