<?php

namespace App\Policies;

use App\Models\Checklist\ChecklistEntry;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class ChecklistEntryPolicy
 * @package App\Policies
 */
class ChecklistEntryPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param ChecklistEntry $entry
     * @return bool
     */
    public function allowedToSee(User $user, ChecklistEntry $entry)
    {
        if ($entry->pluck('users.*.id')->search($user->id)) {
            return true;
        }

        if ($user->hasPermissionTo('happening_checklist_entries_view_' . $entry->checklist->happening_id)) {
            return true;
        }

        if ($user->hasPermissionTo('team_checklist_entries_view' . $entry->checklist->team_id)) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param ChecklistEntry $entry
     * @return bool
     */
    public function delete(User $user, ChecklistEntry $entry)
    {
        return $this->update($user, $entry);
    }

    /**
     * @param User $user
     * @param ChecklistEntry $entry
     * @return bool
     */
    public function update(User $user, ChecklistEntry $entry)
    {
        if ($entry->author->id === $user->id) {
            return true;
        }

        if ($user->hasPermissionTo('happening_checklist_entries_crud_' . $entry->checklist->happening_id)) {
            return true;
        }

        if ($user->hasPermissionTo('team_checklist_entries_crud' . $entry->checklist->team_id)) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param ChecklistEntry $entry
     * @return bool
     */
    public function crudUsers(User $user, ChecklistEntry $entry)
    {
        if ($user->hasPermissionTo('happening_checklist_entries_crud_users_' . $entry->checklist->happening_id)) {
            return true;
        }

        if ($user->hasPermissionTo('team_checklist_entries_crud_users_' . $entry->checklist->team_id)) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param ChecklistEntry $entry
     * @return bool
     */
    public function done(User $user, ChecklistEntry $entry)
    {
        if ($entry->pluck('users.*.id')->search($user->id)) {
            return true;
        }

        if ($user->hasPermissionTo('happening_checklist_done_others_task_' . $entry->checklist->happening_id)) {
            return true;
        }

        if ($user->hasPermissionTo('team_checklist_done_others_task_' . $entry->checklist->team_id)) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param ChecklistEntry $entry
     * @return bool
     */
    public function entry(User $user, ChecklistEntry $entry)
    {
        if ($entry->users()->pluck('user_id')->search($user->id) != false) {
            return true;
        }
        return false;
    }
}
