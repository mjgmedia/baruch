<?php

namespace App\Policies;

use App\Models\File\File;
use App\Models\Happening\Happening;
use App\Models\Team\Team;
use App\Models\User\User;
use App\Services\Interfaces\FileServiceInterface;
use Illuminate\Auth\Access\HandlesAuthorization;

class FilePolicy
{
    use HandlesAuthorization;

    private $service;

    /**
     * FilePolicy constructor.
     * @param $service
     */
    public function __construct(FileServiceInterface $service)
    {
        $this->service = $service;
    }


    /**
     * Determine whether the user can view the file.
     *
     * @param User $user
     * @param File $file
     * @return mixed
     */
    public function view(User $user, File $file)
    {
        $test = File::userHaveAccess($user->id)->pluck('id');
        return $test->contains($file->id);
    }

    /**
     * Determine whether the user can update the file.
     *
     * @param User $user
     * @param File $file
     * @return mixed
     */
    public function crud(User $user, File $file)
    {
        $model = $file->team_id != null ? Team::findOrFail($file->team_id) : Happening::findOrFail($file->happening_id);
        return $this->service->isAllowedToCrud($model);
    }
}
