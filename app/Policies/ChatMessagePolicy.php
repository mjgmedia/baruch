<?php

namespace App\Policies;

use App\Models\Chat\ChatMessage;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class ChatMessagePolicy
 * @package App\Policies
 */
class ChatMessagePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param ChatMessage $message
     * @return bool
     */
    public function updateOrDelete(User $user, ChatMessage $message)
    {
        return $message->author_id === $user->id;
    }
}
