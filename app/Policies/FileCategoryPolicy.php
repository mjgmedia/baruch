<?php

namespace App\Policies;

use App\Models\File\File;
use App\Models\File\FileCategory;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FileCategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the file category.
     *
     * @param User $user
     * @param FileCategory $fileCategory
     * @return mixed
     */
    public function view(User $user, FileCategory $fileCategory)
    {
        return File::query()
            ->where('file_category_id', $fileCategory->id)
            ->userHaveAccess($user->id)
            ->count() > 0;
    }

    /**
     * Determine whether the user can update the file category.
     *
     * @param User $user
     * @param FileCategory $fileCategory
     * @return mixed
     */
    public function crud(User $user, FileCategory $fileCategory)
    {
        if ($fileCategory->file->happening_id && $user->hasPermissionTo(
                'happening_file_category_crud' . $fileCategory->file->happening_id
            )) {
            return true;
        }

        if ($fileCategory->file->team_id && $user->hasPermissionTo(
                'team_file_category_crud' . $fileCategory->file->team_id
            )) {
            return true;
        }

        return false;
    }
}
