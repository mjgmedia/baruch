<?php

namespace App\Policies;

use App\Models\Team\Team;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class TeamPolicy
 * @package App\Policies
 */
class TeamPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Team $team
     * @return bool
     */
    public function lookup(User $user, Team $team)
    {
        return $user->hasPermissionTo('team_users_lookup_'.$team->id);
    }

    /**
     * @param User $user
     * @param Team $team
     * @return bool
     */
    public function teamCrud(User $user, Team $team)
    {
        return $user->hasPermissionTo('team_crud_' . $team->id);
    }

    /**
     * @param User $user
     * @param Team $team
     * @return bool
     */
    public function managersCrud(User $user, Team $team)
    {
        return $user->hasPermissionTo('team_managers_crud_' . $team->id);
    }

    /**
     * @param User $user
     * @param Team $team
     * @return bool
     */
    public function usersCrud(User $user, Team $team)
    {
        return $user->hasPermissionTo('team_users_crud_' . $team->id);
    }

    /**
     * @param User $user
     * @param Team $team
     * @return bool
     */
    public function post(User $user, Team $team)
    {
        return $user->hasPermissionTo('team_post_' . $team->id);
    }

    /**
     * @param User $user
     * @param Team $team
     * @return bool
     */
    public function comments(User $user, Team $team)
    {
        return $user->hasPermissionTo('team_comments_' . $team->id);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create_teams');
    }
}
