<?php

namespace App\Policies;

use App\Models\Happening\HappeningPost;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

/**
 * Class HappeningPostPolicy
 * @package App\Policies
 */
class HappeningPostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param HappeningPost $post
     * @return bool
     */
    public function comments(User $user, HappeningPost $post)
    {
        return $user->hasPermissionTo('happening_comments_'.$post->happening->id);
    }

    /**
     * @param User $user
     * @param HappeningPost $post
     * @return bool
     */
    public function crud(User $user, HappeningPost $post)
    {
        return $user->hasPermissionTo('happening_post_'.$post->happening->id) && $post->author_id == $user->id;
    }
}
