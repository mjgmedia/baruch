<?php

namespace App\Policies;

use App\Models\Happening\Happening;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

/**
 * Class HappeningPolicy
 * @package App\Policies
 */
class HappeningPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Happening $happening
     * @return bool
     */
    public function show(User $user, Happening $happening)
    {
        return (Gate::denies('is-happening-member', $happening->id)) ? false : true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create_happenings');
    }

    /**
     * @param User $user
     * @param Happening $happening
     * @return bool
     */
    public function crud(User $user, Happening $happening)
    {
        return $user->hasPermissionTo('happening_crud_'.$happening->id);
    }

    /**
     * @param User $user
     * @param Happening $happening
     * @return bool
     */
    public function usersCrud(User $user, Happening $happening)
    {
        return $user->hasPermissionTo('happening_user_crud_'.$happening->id);
    }

    /**
     * @param User $user
     * @param Happening $happening
     * @return bool
     */
    public function usersIndex(User $user, Happening $happening)
    {
        return $this->show($user, $happening) && $user->hasPermissionTo('happening_user_list_'.$happening->id);
    }

    /**
     * @param User $user
     * @param Happening $happening
     * @return bool
     */
    public function managersCrud(User $user, Happening $happening)
    {
        return $user->hasPermissionTo('happening_managers_crud_'.$happening->id);
    }
}
