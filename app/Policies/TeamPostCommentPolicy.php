<?php

namespace App\Policies;

use App\Models\Team\TeamPostComment;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeamPostCommentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create team post comments.
     *
     * @param User $user
     * @param int $teamId
     * @return mixed
     */
    public function create(User $user, int $teamId)
    {
        return $user->hasPermissionTo('team_comments_' . $teamId);
    }

    /**
     * Determine whether the user can update the team post comment.
     *
     * @param User $user
     * @param TeamPostComment $teamPostComment
     * @return mixed
     */
    public function update(User $user, TeamPostComment $teamPostComment)
    {
        return $this->create($user, $teamPostComment->post->team->id) && $teamPostComment->author_id === $user->id;
    }

    /**
     * Determine whether the user can delete the team post comment.
     *
     * @param User $user
     * @param TeamPostComment $teamPostComment
     * @return mixed
     */
    public function delete(User $user, TeamPostComment $teamPostComment)
    {
        return $this->update($user, $teamPostComment) || $user->hasPermissionTo(
                'team_crud_' . $teamPostComment->post->team->id
            );
    }
}
