<?php

namespace App\Policies;

use App\Models\File\FileComment;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FileCommentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the file comment.
     *
     * @param User $user
     * @param FileComment $fileComment
     * @return mixed
     */
    public function update(User $user, FileComment $fileComment)
    {
        return $fileComment->author_id === $user->id;
    }

    /**
     * Determine whether the user can delete the file comment.
     *
     * @param User $user
     * @param FileComment $fileComment
     * @return mixed
     */
    public function delete(User $user, FileComment $fileComment)
    {
        if ($fileComment->file->happening_id && $user->hasPermissionTo(
                'happening_file_crud' . $fileComment->file->happening_id
            )) {
            return true;
        }

        if ($fileComment->file->team_id && $user->hasPermissionTo(
                'team_file_crud' . $fileComment->file->team_id
            )) {
            return true;
        }

        return $fileComment->author_id === $user->id;
    }
}
