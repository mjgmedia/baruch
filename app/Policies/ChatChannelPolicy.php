<?php

namespace App\Policies;

use App\Models\Chat\ChatChannel;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class ChatChannelPolicy
 * @package App\Policies
 */
class ChatChannelPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function store(User $user)
    {
        return $user->hasPermissionTo('chat_channel_create');
    }

    /**
     * @param User $user
     * @param ChatChannel $channel
     * @return bool
     */
    public function updateOrDelete(User $user, ChatChannel $channel)
    {
        if ($channel->team_id != null) {
            return $user->hasPermissionTo('team_chat_crud_'.$channel->team_id);
        }

        if ($channel->happening_id != null) {
            return $user->hasPermissionTo('happening_chat_crud_'.$channel->happening_id);
        }

        return $channel->author_id === $user->id;
    }

    /**
     * @param User $user
     * @param ChatChannel $channel
     * @return mixed
     */
    public function show(User $user, ChatChannel $channel)
    {
        $channels = ChatChannel::query()
            ->isMember($user->id)
            ->pluck('id');
        return $channels->containts($channel->id);
    }
}
