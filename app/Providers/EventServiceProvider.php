<?php

namespace App\Providers;

use App\Listeners\Auth\NewUserRegisteredListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider
 * @package App\Providers
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
            NewUserRegisteredListener::class,
        ],
//        CHAT
        'App\Events\Chat\NewMessageEvent' => [
            'App\Listeners\Chat\NewMessageListener'
        ],
//        TEAMS
        'App\Events\Team\UserAddedEvent' => [
            'App\Listeners\Team\UserAddedListener'
        ],
        'App\Events\Team\UserRemovedEvent' => [
            'App\Listeners\Team\UserRemovedListener'
        ],
        'App\Events\Team\ManagerAddedEvent' => [
            'App\Listeners\Team\ManagerAddedListener'
        ],
        'App\Events\Team\ManagerRemovedEvent' => [
            'App\Listeners\Team\ManagerRemovedListener'
        ],
        'App\Events\Team\TeamAddedToHappeningEvent' => [
            'App\Listeners\Team\TeamAddedToHappeningListener'
        ],
        'App\Events\Team\TeamRemovedFromHappeningEvent' => [
            'App\Listeners\Team\TeamRemovedFromHappeningListener'
        ],
        'App\Events\Team\NewPostEvent' => [
            'App\Listeners\Team\NewPostListener',
        ],
        'App\Events\Team\NewCommentEvent' => [
            'App\Listeners\Team\NewCommentListener'
        ],
//        HAPPENING
        'App\Events\Happening\UserAddedEvent' => [
            'App\Listeners\Happening\UserAddedListener'
        ],
        'App\Events\Happening\UserRemovedEvent' => [
            'App\Listeners\Happening\UserRemovedListener'
        ],
        'App\Events\Happening\TeamAddedEvent' => [
            'App\Listeners\Happening\TeamAddedListener'
        ],
        'App\Events\Happening\TeamRemovedEvent' => [
            'App\Listeners\Happening\TeamRemovedListener'
        ],
        'App\Events\Happening\ManagerAddedEvent' => [
            'App\Listeners\Happening\ManagerAddedListener'
        ],
        'App\Events\Happening\ManagerRemovedEvent' => [
            'App\Listeners\Happening\ManagerRemovedListener'
        ],
        'App\Events\Happening\NewPostEvent' => [
            'App\Listeners\Happening\NewPostListener'
        ],
        'App\Events\Happening\NewCommentEvent' => [
            'App\Listeners\Happening\NewCommentListener'
        ],

//        Checklists
        'App\Events\Checklist\EntryAssignedEvent' => [
            'App\Listeners\Checklist\EntryAssignedListener'
        ],
        'App\Events\Checklist\EntryUpdatedEvent' => [
            'App\Listeners\Checklist\EntryUpdatedListener'
        ],
        'App\Events\Checklist\EntryDoneEvent' => [
            'App\Listeners\Checklist\EntryDoneListener'
        ],
        'App\Events\Checklist\EntryReadyEvent' => [
            'App\Listeners\Checklist\EntryReadyListener'
        ],
        'App\Events\Checklist\EntryReceivedEvent' => [
            'App\Listeners\Checklist\EntryReceivedListener'
        ],

//        Files
        'App\Events\File\NewFileEvent' => [
            'App\Listeners\File\NewFileListener'
        ],
        'App\Events\File\UpdatedFileEvent' => [
            'App\Listeners\File\UpdatedFileListener'
        ],
        'App\Events\File\NewCommentEvent' => [
            'App\Listeners\File\NewCommentListener'
        ],
        'App\Events\File\ResponseToCommentEvent' => [
            'App\Listeners\File\ResponseToCommentListener'
        ],
        'App\Events\File\ReadNewFileEvent' => [
            'App\Listeners\File\ReadNewFileListener'
        ],

//        Rest of app
        'App\Events\Auth\OAuthFatalErrorEvent' => [
            'App\Listeners\Auth\OAuthFatalErrorListener'
        ],
        'App\Events\Auth\LoginFailedEvent' => [
            'App\Listeners\Auth\LoginFailedListener'
        ],
        'Laravel\Passport\Events\AccessTokenCreated' => [
            'App\Listeners\RevokeOldTokensListener'
        ],

        'Laravel\Passport\Events\RefreshTokenCreated' => [
            'App\Listeners\PruneOldTokensListener'
        ],

        'App\Events\User\ProfileEditedEvent' => [
            'App\Listeners\User\ProfileEditedListener'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        //
    }
}
