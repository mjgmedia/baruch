<?php

namespace App\Providers;

use App\Models\Chat\ChatChannel;
use App\Models\Chat\ChatMessage;
use App\Models\Checklist\Checklist;
use App\Models\Checklist\ChecklistEntry;
use App\Models\File\File;
use App\Models\File\FileCategory;
use App\Models\File\FileComment;
use App\Models\Happening\HappeningPost;
use App\Models\Happening\HappeningPostComment;
use App\Models\Team\TeamPost;
use App\Models\Team\TeamPostComment;
use App\Observers\Chat\ChatChannelObserver;
use App\Observers\Chat\ChatMessageObserver;
use App\Observers\Checklist\ChecklistEntryObserver;
use App\Observers\Checklist\ChecklistObserver;
use App\Observers\File\FileCategoryObserver;
use App\Observers\File\FileCommentObserver;
use App\Observers\File\FileObserver;
use App\Observers\Happening\HappeningPostCommentObserver;
use App\Observers\Happening\HappeningPostObserver;
use App\Observers\Team\TeamPostCommentObserver;
use App\Observers\Team\TeamPostObserver;
use App\Resolvers\SocialUserResolver;
use Coderello\SocialGrant\Resolvers\SocialUserResolverInterface;
use Illuminate\Database\Schema\Builder;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        SocialUserResolverInterface::class => SocialUserResolver::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Builder::defaultStringLength(191);
        Checklist::observe(ChecklistObserver::class);
        ChecklistEntry::observe(ChecklistEntryObserver::class);
        File::observe(FileObserver::class);
        FileCategory::observe(FileCategoryObserver::class);
        FileComment::observe(FileCommentObserver::class);
        HappeningPost::observe(HappeningPostObserver::class);
        HappeningPostComment::observe(HappeningPostCommentObserver::class);
        TeamPost::observe(TeamPostObserver::class);
        TeamPostComment::observe(TeamPostCommentObserver::class);
        ChatChannel::observe(ChatChannelObserver::class);
        ChatMessage::observe(ChatMessageObserver::class);
    }
}
