<?php

namespace App\Providers;

use App\Services\ChatService;
use App\Services\ChecklistService;
use App\Services\FileService;
use App\Services\HappeningService;
use App\Services\Interfaces\ChatServiceInterface;
use App\Services\Interfaces\ChecklistServiceInterface;
use App\Services\Interfaces\FileServiceInterface;
use App\Services\Interfaces\HappeningServiceInterface;
use App\Services\Interfaces\SettingsServiceInterface;
use App\Services\Interfaces\SocialAccountsServiceInterface;
use App\Services\Interfaces\TeamServiceInterface;
use App\Services\Interfaces\UploadServiceInterface;
use App\Services\SettingsService;
use App\Services\SocialAccountsService;
use App\Services\TeamService;
use App\Services\UploadService;
use Illuminate\Support\ServiceProvider;

/**
 * Class ServicesServiceProvider
 * @package App\Providers
 */
class ServicesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            SocialAccountsServiceInterface::class,
            SocialAccountsService::class
        );

        $this->app->bind(
            UploadServiceInterface::class,
            UploadService::class
        );

        $this->app->bind(
            SettingsServiceInterface::class,
            SettingsService::class
        );

        $this->app->bind(
            ChatServiceInterface::class,
            ChatService::class
        );

        $this->app->bind(
            TeamServiceInterface::class,
            TeamService::class
        );

        $this->app->bind(
            HappeningServiceInterface::class,
            HappeningService::class
        );

        $this->app->bind(
            FileServiceInterface::class,
            FileService::class
        );

        $this->app->bind(
            ChecklistServiceInterface::class,
            ChecklistService::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
