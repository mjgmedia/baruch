<?php

namespace App\Providers;

use App\Models\Happening\Happening;
use App\Models\Team\Team;
use App\Models\User\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

/**
 * Class AuthServiceProvider
 * @package App\Providers
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Gate::before(
            function ($user, $ability) {
                return $user->hasRole('superadmin') ? true : null;
            }
        );

        Gate::define(
            'is-team-member',
            function ($user, $teamId) {
                return Team::where('id', $teamId)->isMember($user->id)->count() > 0;
            }
        );

        Gate::define(
            'is-happening-member',
            function ($user, $happeningId) {
                return Happening::where('id', $happeningId)->isMember($user->id)->count() > 0;
            }
        );

        Gate::define(
            'team-post',
            function (User $user, int $teamId) {
                return $user->hasPermissionTo('team_post_' . $teamId);
            }
        );

        Gate::define(
            'happening-post',
            function (User $user, int $happeningId) {
                return $user->hasPermissionTo('happening_post_' . $happeningId);
            }
        );

        Gate::define(
            'file-global-crud',
            function (User $user) {
                return $user->hasPermissionTo('file_global_crud');
            }
        );

        Gate::define(
            'file-global-comment',
            function (User $user) {
                return $user->hasPermissionTo('file_global_comment');
            }
        );

        Gate::define(
            'file-global-list',
            function (User $user) {
                return $user->hasPermissionTo('file_global_list');
            }
        );
    }
}
