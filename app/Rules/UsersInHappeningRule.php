<?php

namespace App\Rules;

use App\Models\Happening\HappeningUser;
use Illuminate\Contracts\Validation\Rule;

/**
 * Class UsersInHappeningRule
 * @package App\Rules
 */
class UsersInHappeningRule implements Rule
{
    /**
     * @var array
     */
    private $request;

    /**
     * Create a new rule instance.
     *
     * @param array $request
     */
    public function __construct(array $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return HappeningUser::query()
            ->where('happening_id', $this->request['happening_id'])
            ->whereIn('id', $value)
            ->count() > count($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.users_in_happening');
    }
}
