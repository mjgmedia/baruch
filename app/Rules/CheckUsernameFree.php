<?php

namespace App\Rules;

use App\Models\User\User;
use Illuminate\Contracts\Validation\Rule;

/**
 * Class CheckUsernameFree
 * @package App\Rules
 */
class CheckUsernameFree implements Rule
{
    private string $field;

    /**
     * Create a new rule instance.
     *
     * @param string $field
     */
    public function __construct($field = 'username')
    {
        $this->field = $field;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return User::where($this->field, $value)->count() <= 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.username_not_free.message');
    }
}
