<?php

namespace App\Listeners\Auth;

use App\Models\User\User;
use App\Notifications\Admin\NewUserRegisteredNotification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Notification;

/**
 * Class NewUserRegisteredListener
 * @package App\Listeners\Auth
 */
class NewUserRegisteredListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param Registered $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $users = User::permission(['notifications_user_created'])->get();
        Notification::send($users, new NewUserRegisteredNotification($event->user));
    }
}
