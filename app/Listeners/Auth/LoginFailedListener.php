<?php

namespace App\Listeners\Auth;

use App\Events\Auth\LoginFailedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

/**
 * Class LoginFailedListener
 * @package App\Listeners\Auth
 */
class LoginFailedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param LoginFailedEvent $event
     * @return void
     */
    public function handle(LoginFailedEvent $event)
    {
        Log::channel('failed_login_attemps')
            ->warning(
                "Nie udana próba logowania: {$event->username} z IP: {$event->ip} ({$event->timestamp})"
            );
    }
}
