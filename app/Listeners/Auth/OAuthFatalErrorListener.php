<?php

namespace App\Listeners\Auth;

use App\Events\Auth\OAuthFatalErrorEvent;
use App\Models\User\User;
use App\Notifications\Admin\OAuthFatalErrorNotification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

/**
 * Class OAuthFatalErrorListener
 * @package App\Listeners\Auth
 */
class OAuthFatalErrorListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param OAuthFatalErrorEvent $event
     * @return void
     */
    public function handle(OAuthFatalErrorEvent $event)
    {
        Log::channel('oauth_errors')
            ->critical('Błąd serwera OAuth (Passport): ' . $event->code . ': ' . $event->message);

        $users = User::permission(['notifications_oauth_error'])->get();
        Notification::send($users, new OAuthFatalErrorNotification($event));
    }
}
