<?php

namespace App\Listeners\Happening;

use App\Events\Happening\UserAddedEvent;
use App\Models\User\User;
use App\Notifications\Happening\UserAddedToHappeningNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class UserAddedListener
 * @package App\Listeners\Happening
 */
class UserAddedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserAddedEvent  $event
     * @return void
     */
    public function handle(UserAddedEvent $event)
    {
        Notification::send(
            User::query()
                ->whereIn('id', $event->happening->users->pluck('id'))
                ->onSetting('happening_notify_about_new_members')
                ->get(),
            new UserAddedToHappeningNotification($event->happening, $event->user)
        );
    }
}
