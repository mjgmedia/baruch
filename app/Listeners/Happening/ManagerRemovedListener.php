<?php

namespace App\Listeners\Happening;

use App\Events\Happening\ManagerRemovedEvent;
use App\Models\User\User;
use App\Notifications\Happening\InformationUpdatedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class ManagerRemovedListener
 * @package App\Listeners\Happening
 */
class ManagerRemovedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ManagerRemovedEvent  $event
     * @return void
     */
    public function handle(ManagerRemovedEvent $event)
    {
        Notification::send(
            User::query()
                ->whereIn('id', $event->happening->users->pluck('id'))
                ->onSetting('happening_notify_member_profile_changed')
                ->get(),
            new InformationUpdatedNotification($event->happening)
        );
    }
}
