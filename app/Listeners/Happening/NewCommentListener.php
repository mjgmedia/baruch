<?php

namespace App\Listeners\Happening;

use App\Events\Happening\NewCommentEvent;
use App\Notifications\Happening\NewCommentNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;
use Mockery\Matcher\Not;

/**
 * Class NewCommentListener
 * @package App\Listeners\Happening
 */
class NewCommentListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewCommentEvent  $event
     * @return void
     */
    public function handle(NewCommentEvent $event)
    {
        Notification::send(
            $event->comment->post->happeing->users,
            new NewCommentNotification($event->comment)
        );
    }
}
