<?php

namespace App\Listeners\Happening;

use App\Events\Happening\NewPostEvent;
use App\Notifications\Happening\NewPostNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class NewPostListener
 * @package App\Listeners\Happening
 */
class NewPostListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewPostEvent  $event
     * @return void
     */
    public function handle(NewPostEvent $event)
    {
        Notification::send(
            $event->post->happening->users,
            new NewPostNotification($event->post)
        );
    }
}
