<?php

namespace App\Listeners\Happening;

use App\Events\Happening\TeamRemovedEvent;
use App\Models\User\User;
use App\Notifications\Happening\RemovedTeamNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class TeamRemovedListener
 * @package App\Listeners\Happening
 */
class TeamRemovedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TeamRemovedEvent  $event
     * @return void
     */
    public function handle(TeamRemovedEvent $event)
    {
        Notification::send(
            User::query()
                ->whereIn('id', $event->happening->users->pluck('id'))
                ->onSetting('happening_notify_about_deleted_teams')
                ->get(),
            new RemovedTeamNotification($event->happening, $event->team)
        );
    }
}
