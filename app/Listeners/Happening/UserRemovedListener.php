<?php

namespace App\Listeners\Happening;

use App\Events\Happening\UserRemovedEvent;
use App\Models\User\User;
use App\Notifications\Happening\UserDeletedFromHappeningNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class UserRemovedListener
 * @package App\Listeners\Happening
 */
class UserRemovedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRemovedEvent  $event
     * @return void
     */
    public function handle(UserRemovedEvent $event)
    {
        Notification::send(
            User::query()
                ->whereIn('id', $event->happening->users->pluck('id'))
                ->onSetting('happening_notify_about_deleted_members')
                ->get(),
            new UserDeletedFromHappeningNotification($event->happening, $event->user)
        );
    }
}
