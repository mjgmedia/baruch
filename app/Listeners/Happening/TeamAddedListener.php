<?php

namespace App\Listeners\Happening;

use App\Events\Happening\TeamAddedEvent;
use App\Models\User\User;
use App\Notifications\Happening\AddedTeamNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class TeamAddedListener
 * @package App\Listeners\Happening
 */
class TeamAddedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TeamAddedEvent  $event
     * @return void
     */
    public function handle(TeamAddedEvent $event)
    {
        Notification::send(
            User::whereIn('id', $event->happening->users->pluck('id'))
                ->onSetting('happening_notify_about_new_teams')
                ->get(),
            new AddedTeamNotification($event->happening, $event->team)
        );
    }
}
