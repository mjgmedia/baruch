<?php

namespace App\Listeners\Happening;

use App\Events\Happening\ManagerAddedEvent;
use App\Models\User\User;
use App\Notifications\Happening\InformationUpdatedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class ManagerAddedListener
 * @package App\Listeners\Happening
 */
class ManagerAddedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ManagerAddedEvent $event
     * @return void
     */
    public function handle(ManagerAddedEvent $event)
    {
        Notification::send(
            User::query()
                ->whereIn('id', $event->happening->users->pluck('id'))
                ->onSetting('happening_notify_member_profile_changed')
                ->get(),
            new InformationUpdatedNotification($event->happening)
        );
    }
}
