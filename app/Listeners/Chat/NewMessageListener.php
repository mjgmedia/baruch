<?php

namespace App\Listeners\Chat;

use App\Events\Chat\NewMessageEvent;
use App\Jobs\Chat\PushUnreadMessageJob;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewMessageListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param NewMessageEvent $event
     * @return void
     */
    public function handle(NewMessageEvent $event)
    {
        PushUnreadMessageJob::dispatch($event->message)->delay(config('project.chat.push_unread_delay'));
    }
}
