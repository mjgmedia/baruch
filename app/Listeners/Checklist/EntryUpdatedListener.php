<?php

namespace App\Listeners\Checklist;

use App\Events\Checklist\EntryUpdatedEvent;
use App\Notifications\Checklist\EntryUpdatedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class EntryUpdatedListener
 * @package App\Listeners\Checklist
 */
class EntryUpdatedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EntryUpdatedEvent  $event
     * @return void
     */
    public function handle(EntryUpdatedEvent $event)
    {
        Notification::send($event->entry->users, new EntryUpdatedNotification($event->entry));
    }
}
