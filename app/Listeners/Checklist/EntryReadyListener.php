<?php

namespace App\Listeners\Checklist;

use App\Events\Checklist\EntryReadyEvent;
use App\Notifications\Checklist\EntryReadyNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class EntryReadyListener
 * @package App\Listeners\Checklist
 */
class EntryReadyListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EntryReadyEvent  $event
     * @return void
     */
    public function handle(EntryReadyEvent $event)
    {
        Notification::send($event->entry->users, new EntryReadyNotification($event->entry));
    }
}
