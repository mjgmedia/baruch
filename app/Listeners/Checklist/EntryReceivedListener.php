<?php

namespace App\Listeners\Checklist;

use App\Events\Checklist\EntryReceivedEvent;
use App\Notifications\Checklist\EntryReceivedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class EntryReceivedListener
 * @package App\Listeners\Checklist
 */
class EntryReceivedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EntryReceivedEvent  $event
     * @return void
     */
    public function handle(EntryReceivedEvent $event)
    {
        Notification::send(
            $event->entry->checklist->managers(),
            new EntryReceivedNotification($event->entry, $event->user)
        );
    }
}
