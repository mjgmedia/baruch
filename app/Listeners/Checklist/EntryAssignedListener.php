<?php

namespace App\Listeners\Checklist;

use App\Events\Checklist\EntryAssignedEvent;
use App\Notifications\Checklist\EntryAssignedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class EntryAssignedListener
 * @package App\Listeners\Checklist
 */
class EntryAssignedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EntryAssignedEvent  $event
     * @return void
     */
    public function handle(EntryAssignedEvent $event)
    {
        Notification::send($event->entry->users, new EntryAssignedNotification($event->entry));
    }
}
