<?php

namespace App\Listeners\Checklist;

use App\Events\Checklist\EntryDoneEvent;
use App\Notifications\Checklist\EntryDoneNotifiacation;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class EntryDoneListener
 * @package App\Listeners\Checklist
 */
class EntryDoneListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EntryDoneEvent  $event
     * @return void
     */
    public function handle(EntryDoneEvent $event)
    {
        Notification::send(
            $event->entry->checklist->managers(),
            new EntryDoneNotifiacation($event->entry, $event->user)
        );
    }
}
