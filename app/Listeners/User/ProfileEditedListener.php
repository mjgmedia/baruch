<?php

namespace App\Listeners\User;

use App\Events\User\ProfileEditedEvent;
use App\Notifications\Happening\UserProfileEditedHappeningNotification;
use App\Notifications\Team\UserProfileEditedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class ProfileEditedListener
 * @package App\Listeners\User
 */
class ProfileEditedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ProfileEditedEvent $event
     * @return void
     */
    public function handle(ProfileEditedEvent $event)
    {
        foreach($event->user->teams as $team) {
            Notification::send($team->managers, new UserProfileEditedNotification($event->user));
        }

        foreach ($event->user->happenings as $happening) {
            Notification::send($happening->managers, new UserProfileEditedHappeningNotification($event->user));
        }
    }
}
