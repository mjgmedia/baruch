<?php

namespace App\Listeners\Team;

use App\Events\Team\NewCommentEvent;
use App\Notifications\Team\NewCommentNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class NewCommentListener
 * @package App\Listeners\Team
 */
class NewCommentListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewCommentEvent  $event
     * @return void
     */
    public function handle(NewCommentEvent $event)
    {
        Notification::send(
            $event->comment->post->team->users,
            new NewCommentNotification($event->comment)
        );
    }
}
