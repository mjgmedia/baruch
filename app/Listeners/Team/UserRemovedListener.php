<?php

namespace App\Listeners\Team;

use App\Events\Team\UserRemovedEvent;
use App\Models\Settings\Setting;
use App\Models\User\User;
use App\Notifications\Team\UserDeletedFromTeamNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class UserRemovedListener
 * @package App\Listeners\Team
 */
class UserRemovedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserRemovedEvent $event
     * @return void
     */
    public function handle(UserRemovedEvent $event)
    {
        $setting = Setting::query()
            ->where('key', 'team_notify_about_deleted_members')
            ->get();
        $users = User::query()
            ->whereHas(
                'settings',
                function ($query) use ($setting) {
                    return $query->where(
                        [
                            ['setting', '=', $setting->id],
                        ]
                    );
                }
            )
            ->whereHas('teams', function($query) use ($event) {
                return $query->where('team_user.team_id', $event->team->id);
            })
            ->get();
        Notification::send($users, new UserDeletedFromTeamNotification($event->team, $event->user));
    }
}
