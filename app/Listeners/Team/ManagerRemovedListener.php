<?php

namespace App\Listeners\Team;

use App\Events\Team\ManagerRemovedEvent;
use App\Models\User\User;
use App\Notifications\Team\AboutUpdatedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class ManagerRemovedListener
 * @package App\Listeners\Team
 */
class ManagerRemovedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ManagerRemovedEvent  $event
     * @return void
     */
    public function handle(ManagerRemovedEvent $event)
    {
        Notification::send(
            User::query()
                ->whereIn('id', $event->team->users->pluck('id'))
                ->onSetting('team_notify_member_profile_changed')
                ->get(),
            new AboutUpdatedNotification($event->team)
        );
    }
}
