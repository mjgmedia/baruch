<?php

namespace App\Listeners\Team;

use App\Events\Team\TeamAddedToHappeningEvent;
use App\Models\User\User;
use App\Notifications\Team\AddedToHappeningNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class TeamAddedToHappeningListener
 * @package App\Listeners\Team
 */
class TeamAddedToHappeningListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TeamAddedToHappeningEvent  $event
     * @return void
     */
    public function handle(TeamAddedToHappeningEvent $event)
    {
        Notification::send(
            User::query()
                ->whereIn('id', $event->team->users->pluck('id'))
                ->onSetting('team_notify_member_profile_changed')
                ->get(),
            new AddedToHappeningNotification(
                $event->team,
                $event->happening
            )
        );
    }
}
