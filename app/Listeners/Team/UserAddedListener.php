<?php

namespace App\Listeners\Team;

use App\Events\Team\UserAddedEvent;
use App\Models\Settings\Setting;
use App\Models\User\User;
use App\Notifications\Team\UserAddedToTeamNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class UserAddedListener
 * @package App\Listeners\Team
 */
class UserAddedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserAddedEvent  $event
     * @return void
     */
    public function handle(UserAddedEvent $event)
    {
        $setting = Setting::query()
            ->where('key', 'team_notify_about_new_members')
            ->get();
        $users = User::query()
            ->whereHas(
                'settings',
                function ($query) use ($setting) {
                    return $query->where(
                        [
                            ['setting', '=', $setting->id],
                        ]
                    );
                }
            )
            ->whereHas('teams', function($query) use ($event) {
                return $query->where('team_user.team_id', $event->team->id);
            })
            ->get();
        Notification::send($users, new UserAddedToTeamNotification($event->team, $event->user));
    }
}
