<?php

namespace App\Listeners\Team;

use App\Events\Team\TeamRemovedFromHappeningEvent;
use App\Models\User\User;
use App\Notifications\Team\RemovedFromHappeningNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class TeamRemovedFromHappeningListener
 * @package App\Listeners\Team
 */
class TeamRemovedFromHappeningListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TeamRemovedFromHappeningEvent  $event
     * @return void
     */
    public function handle(TeamRemovedFromHappeningEvent $event)
    {
        Notification::send(
            User::query()
                ->whereIn('id', $event->team->users->pluck('id'))
                ->onSetting('team_notify_member_profile_changed')
                ->get(),
            new RemovedFromHappeningNotification(
                $event->team,
                $event->happening
            )
        );
    }
}
