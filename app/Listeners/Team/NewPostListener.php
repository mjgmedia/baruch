<?php

namespace App\Listeners\Team;

use App\Events\Team\NewPostEvent;
use App\Notifications\Team\NewPostNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class NewPostListener
 * @package App\Listeners\Team
 */
class NewPostListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewPostEvent  $event
     * @return void
     */
    public function handle(NewPostEvent $event)
    {
        Notification::send(
            $event->post->team->users(),
            new NewPostNotification($event->post)
        );
    }
}
