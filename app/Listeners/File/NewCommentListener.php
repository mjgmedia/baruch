<?php

namespace App\Listeners\File;

use App\Events\File\NewCommentEvent;
use App\Models\User\User;
use App\Notifications\File\CommentToFileNotification;
use App\Services\Interfaces\FileServiceInterface;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class NewCommentListener
 * @package App\Listeners\File
 */
class NewCommentListener implements ShouldQueue
{
    /**
     * @var FileServiceInterface
     */
    private FileServiceInterface $service;

    /**
     * Create the event listener.
     *
     * @param FileServiceInterface $fileService
     */
    public function __construct(FileServiceInterface $fileService)
    {
        $this->service = $fileService;
    }

    /**
     * Handle the event.
     *
     * @param NewCommentEvent $event
     * @return void
     */
    public function handle(NewCommentEvent $event)
    {
        $users = User::query()
            ->whereIn('id', $this->service->getUsersWithAccess($event->file->id))
            ->onSetting('file_notify_about_new_comments')
            ->get();
        Notification::send($users, new CommentToFileNotification($event->file, $event->commentAuthor));
    }
}
