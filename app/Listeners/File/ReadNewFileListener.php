<?php

namespace App\Listeners\File;

use App\Events\File\ReadNewFileEvent;
use App\Models\User\User;
use App\Notifications\File\ReadNewFileNotification;
use App\Services\Interfaces\HappeningServiceInterface;
use App\Services\Interfaces\TeamServiceInterface;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class ReadNewFileListener
 * @package App\Listeners\File
 */
class ReadNewFileListener implements ShouldQueue
{
    /**
     * @var TeamServiceInterface
     */
    private TeamServiceInterface $teamService;
    /**
     * @var HappeningServiceInterface
     */
    private HappeningServiceInterface $happeningService;

    /**
     * Create the event listener.
     *
     * @param TeamServiceInterface $teamService
     * @param HappeningServiceInterface $happeningService
     */
    public function __construct(TeamServiceInterface $teamService, HappeningServiceInterface $happeningService)
    {
        $this->teamService = $teamService;
        $this->happeningService = $happeningService;
    }

    /**
     * Handle the event.
     *
     * @param ReadNewFileEvent $event
     * @return void
     */
    public function handle(ReadNewFileEvent $event)
    {
        // Team managers first
        $users = User::query()
            ->whereIn('id', $this->teamService->getManagersForUser($event->userWhoRead->id))
            ->onSetting('file_team_notify_about_open')
            ->get();
        Notification::send($users, new ReadNewFileNotification($event->file, $event->userWhoRead));
        // then Happening managers
        $users = User::query()
            ->whereIn('id', $this->happeningService->getManagersForUser($event->userWhoRead->id))
            ->onSetting('file_happening_notify_about_open')
            ->get();
        Notification::send($users, new ReadNewFileNotification($event->file, $event->userWhoRead));
    }
}
