<?php

namespace App\Listeners\File;

use App\Events\File\UpdatedFileEvent;
use App\Models\User\User;
use App\Notifications\File\UpdatedFileNotification;
use App\Services\Interfaces\FileServiceInterface;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class UpdatedFileListener
 * @package App\Listeners\File
 */
class UpdatedFileListener implements ShouldQueue
{
    /**
     * @var FileServiceInterface
     */
    private FileServiceInterface $service;

    /**
     * Create the event listener.
     *
     * @param FileServiceInterface $fileService
     */
    public function __construct(FileServiceInterface $fileService)
    {
        $this->service = $fileService;
    }

    /**
     * Handle the event.
     *
     * @param UpdatedFileEvent $event
     * @return void
     */
    public function handle(UpdatedFileEvent $event)
    {
        $users = User::query()
            ->whereIn('id', $this->service->getUsersWithAccess($event->file->id))
            ->onSetting('file_notify_about_responses_to_comments')
            ->get();
        Notification::send($users, new UpdatedFileNotification($event->file));
    }
}
