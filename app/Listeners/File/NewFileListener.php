<?php

namespace App\Listeners\File;

use App\Events\File\NewFileEvent;
use App\Models\User\User;
use App\Notifications\File\NewFileNotification;
use App\Services\Interfaces\FileServiceInterface;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

/**
 * Class NewFileListener
 * @package App\Listeners\File
 */
class NewFileListener implements ShouldQueue
{
    /**
     * @var FileServiceInterface
     */
    private FileServiceInterface $service;

    /**
     * Create the event listener.
     *
     * @param FileServiceInterface $fileService
     */
    public function __construct(FileServiceInterface $fileService)
    {
        $this->service = $fileService;
    }

    /**
     * Handle the event.
     *
     * @param NewFileEvent $event
     * @return void
     */
    public function handle(NewFileEvent $event)
    {
        $users = User::query()
            ->whereIn('id', $this->service->getUsersWithAccess($event->file->id))
            ->get();
        Notification::send($users, new NewFileNotification($event->file));
    }
}
