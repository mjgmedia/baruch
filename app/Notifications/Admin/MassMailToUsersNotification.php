<?php

namespace App\Notifications\Admin;

use App\Notifications\Core\MailExtended;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushChannel;
use NotificationChannels\WebPush\WebPushMessage;

/**
 * Class MassMailToUsersNotification
 * @package App\Notifications
 */
class MassMailToUsersNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var string
     */
    private $subject;
    /**
     * @var string
     */
    private $message;

    /**
     * Create a new notification instance.
     *
     * @param string $subject
     * @param string $message
     */
    public function __construct(string $subject, string $message)
    {
        $this->subject = $subject;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast', WebPushChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailExtended())
            ->line(__('notifications.admin.mass_mail.intro'))
            ->line(__('notifications.admin.mass_mail.subject'))
            ->line($this->subject)
            ->content(__('notifications.admin.mass_mail.message'))
            ->line($this->message)
            ->line(__('notifications.greetings'));
    }

    /**
     * @param $notifiable
     * @param $notification
     * @return WebPushMessage
     */
    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage())
            ->title(__('notifications.admin.mass_mail.intro') . config('app.name'))
            ->body(__('notifications.admin.mass_mail.web_push'))
            ->data(['id' => $notification->id]);
    }

    /**
     * @param $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'subject' => $this->subject,
                'message' => $this->message,
            ]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
