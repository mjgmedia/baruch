<?php

namespace App\Notifications\Admin;

use App\Events\Auth\OAuthFatalErrorEvent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushChannel;
use NotificationChannels\WebPush\WebPushMessage;

/**
 * Class OAuthFatalErrorNotification
 * @package App\Notifications\Admin
 */
class OAuthFatalErrorNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var \App\Events\Auth\OAuthFatalErrorEvent
     */
    public $event;

    /**
     * Create a new notification instance.
     *
     * @param OAuthFatalErrorEvent $event
     */
    public function __construct(OAuthFatalErrorEvent $event)
    {
        $this->event = $event;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast', WebPushChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->line(__('notifications.oauth.mail.code') . $this->event->code)
            ->line(__('notifications.oauth.mail.message'))
            ->line($this->event->message);
    }

    /**
     * @param $notifiable
     * @param $notification
     * @return WebPushMessage
     */
    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage())
            ->title(__('notifications.oauth.webpush.title'))
            ->body(__('notifications.oauth.webpush.message'))
            ->data(['id' => $notification->id]);
    }

    /**
     * @param $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'code' => $this->event->code,
                'message' => $this->event->message,
            ]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
