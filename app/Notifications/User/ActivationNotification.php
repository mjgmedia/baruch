<?php

namespace App\Notifications\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Class ActivationNotification
 * @package App\Notifications\User
 */
class ActivationNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var string
     */
    private string $code;

    /**
     * Create a new notification instance.
     *
     * @param string $code
     */
    public function __construct(string $code)
    {
        $this->code = $code;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->line(__('notifications.activation.line') . config('app.name'))
            ->line(__('notifications.activation.line2'))
            ->action(__('notifications.activation.cta'), config('project.activation_url') . $this->code)
            ->line(__('notifications.activation.ignore'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
