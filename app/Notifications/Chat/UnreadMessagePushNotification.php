<?php

namespace App\Notifications\Chat;

use App\Models\Chat\ChatMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushChannel;
use NotificationChannels\WebPush\WebPushMessage;

/**
 * Class UnreadMessagePushNotification
 * @package App\Notifications\Chat
 */
class UnreadMessagePushNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var ChatMessage
     */
    private ChatMessage $message;

    /**
     * Create a new notification instance.
     *
     * @param ChatMessage $message
     */
    public function __construct(ChatMessage $message)
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [WebPushChannel::class];
    }


    /**
     * @param $notifiable
     * @param $notification
     * @return WebPushMessage
     */
    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage())
            ->title(__('notifications.chat.unread_push'))
            ->body($this->message->user->name . ': ' . $this->message->message)
            ->data(['id' => $notification->id]);
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
