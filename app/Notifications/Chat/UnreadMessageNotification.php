<?php

namespace App\Notifications\Chat;

use App\Models\Chat\ChatMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;

/**
 * Class UnreadMessageNotification
 * @package App\Notifications\Chat
 */
class UnreadMessageNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var ChatMessage
     */
    private ChatMessage $message;

    /**
     * Create a new notification instance.
     *
     * @param ChatMessage $message
     */
    public function __construct(ChatMessage $message)
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    /**
     * @param $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'author' => $this->message->user,
                'id' => $this->message->id,
                'users' => $this->message->users,
                'message' => $this->message->message,
                'channel' => $this->message->channel->id,
            ]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
