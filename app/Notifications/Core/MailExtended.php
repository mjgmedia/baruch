<?php

namespace App\Notifications\Core;

use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class MailExtended
 * @package App\Notifications\Core
 */
class MailExtended extends MailMessage
{
    /**
     * The notification's data.
     *
     * @var string|null
     */
    public $viewData;

    /**
     * Set the content of the notification.
     *
     * @param $content
     * @return $this
     */
    public function content($content)
    {
        $this->viewData['content'] = $content;

        return $this;
    }

    /**
     * Get the data array for the mail message.
     *
     * @return array
     */
    public function data()
    {
        return array_merge($this->toArray(), $this->viewData);
    }
}