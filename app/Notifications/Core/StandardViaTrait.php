<?php

namespace App\Notifications\Core;

use NotificationChannels\WebPush\WebPushChannel;

/**
 * Trait StandardViaTrait
 * @package App\Notifications\Core
 */
trait StandardViaTrait
{
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if ($notifiable->last_activity <= now()->subMinute()) {
            return $notifiable->webpush ? [WebPushChannel::class] : ['mail'];
        }
        return ['broadcast'];
    }
}