<?php

namespace App\Notifications\File;

use App\Models\File\File;
use App\Notifications\Core\StandardViaTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;

/**
 * Class NewFileNotification
 * @package App\Notifications\File
 */
class NewFileNotification extends Notification implements ShouldQueue
{
    use Queueable;
    use StandardViaTrait;

    /**
     * @var File
     */
    private File $file;

    /**
     * Create a new notification instance.
     *
     * @param File $file
     */
    public function __construct(File $file)
    {
        $this->file = $file;
    }

    /**
     * @param $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'name' => $this->file->name,
                'id' => $this->file->id,
                'author' => $this->file->user->name,
            ]
        );
    }

    /**
     * @return string
     */
    private function getMessage()
    {
        return __('notifications.file.new_file ') .
            $this->file->name;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line($this->getMessage())
            ->action(__('notifications.file.new_file_action'), url('/'));
    }

    /**
     * @param $notifiable
     * @param $notification
     * @return WebPushMessage
     */
    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage())
            ->title(__('notifications.file.file') . $this->file->name)
            ->body($this->getMessage())
            ->data(['id' => $notification->id]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
