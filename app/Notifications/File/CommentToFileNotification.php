<?php

namespace App\Notifications\File;

use App\Models\File\File;
use App\Models\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Notifications\Core\StandardViaTrait;
use NotificationChannels\WebPush\WebPushMessage;

/**
 * Class CommentToFileNotification
 * @package App\Notifications\File
 */
class CommentToFileNotification extends Notification implements ShouldQueue
{
    use Queueable;
    use StandardViaTrait;

    /**
     * @var File
     */
    private File $file;
    /**
     * @var User
     */
    private User $commentAuthor;

    /**
     * Create a new notification instance.
     *
     * @param File $file
     * @param User $user
     */
    public function __construct(File $file, User $user)
    {
        $this->file = $file;
        $this->commentAuthor = $user;
    }

    /**
     * @param $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'name' => $this->file->name,
                'id' => $this->file->id,
                'comment_author' => $this->commentAuthor->name,
            ]
        );
    }

    /**
     * @return string
     */
    private function getMessage()
    {
        return __('notifications.file.to_file') . $this->file->name .
            __('notifications.file.new_comment');
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line($this->getMessage())
                    ->action(__('notifications.file.new_comment_action'), url('/'));
    }

    /**
     * @param $notifiable
     * @param $notification
     * @return WebPushMessage
     */
    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage())
            ->title(__('notifications.file.file') . $this->file->name)
            ->body($this->getMessage())
            ->data(['id' => $notification->id]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
