<?php

namespace App\Notifications\Happening;

use App\Models\Happening\HappeningPostComment;
use App\Notifications\Core\StandardViaTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;

/**
 * Class NewCommentNotification
 * @package App\Notifications\Happening
 */
class NewCommentNotification extends Notification implements ShouldQueue
{
    use Queueable;
    use StandardViaTrait;

    /**
     * @var HappeningPostComment
     */
    private HappeningPostComment $comment;

    /**
     * Create a new notification instance.
     *
     * @param HappeningPostComment $comment
     */
    public function __construct(HappeningPostComment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @param $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'post' => $this->comment->post->name,
                'happening' => $this->comment->post->happening->name,
                'id' => $this->comment->id,
                'author' => $this->comment->author->name,
            ]
        );
    }

    /**
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|string|null
     */
    private function getMessage()
    {
        return __('notifications.happening.user') . $this->comment->author->name .
            __('notifications.happening.new_comment') . $this->comment->post->author->name .
            '(' . $this->comment->post->created_at . ') ' . __('notifications.happening.in_feed') .
            $this->comment->happening->name;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line($this->getMessage());
    }

    /**
     * @param $notifiable
     * @param $notification
     * @return WebPushMessage
     */
    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage())
            ->title(__('notifications.happening.happening') . $this->comment->post->happening->name)
            ->body($this->getMessage())
            ->data(['id' => $notification->id]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
