<?php

namespace App\Notifications\Happening;

use App\Models\Happening\Happening;
use App\Models\Team\Team;
use App\Notifications\Core\StandardViaTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;

/**
 * Class AddedTeamNotification
 * @package App\Notifications\Happening
 */
class AddedTeamNotification extends Notification implements ShouldQueue
{
    use Queueable;
    use StandardViaTrait;

    /**
     * @var Team
     */
    private Team $team;
    /**
     * @var Happening
     */
    private Happening $happening;

    /**
     * Create a new notification instance.
     *
     * @param Happening $happening
     * @param Team $team
     */
    public function __construct(Happening $happening, Team $team)
    {
        $this->happening = $happening;
        $this->team = $team;
    }

    /**
     * @param $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'name' => $this->happening->name,
                'id' => $this->happening->id,
                'image_url' => $this->happening->image_url,
                'team' => $this->team->name,
            ]
        );
    }

    /**
     * @return string
     */
    private function getMessage()
    {
        return __('notifications.happening.team') . $this->team->name .
            __('notifications.happening.team_added_to');
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line($this->getMessage());
    }

    /**
     * @param $notifiable
     * @param $notification
     * @return WebPushMessage
     */
    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage())
            ->title(__('notifications.happening.happening') . $this->happening->name)
            ->body($this->getMessage())
            ->data(['id' => $notification->id]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
