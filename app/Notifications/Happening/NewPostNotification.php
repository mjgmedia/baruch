<?php

namespace App\Notifications\Happening;

use App\Models\Happening\HappeningPost;
use App\Notifications\Core\StandardViaTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;

/**
 * Class NewPostNotification
 * @package App\Notifications\Happening
 */
class NewPostNotification extends Notification implements ShouldQueue
{
    use Queueable;
    use StandardViaTrait;

    /**
     * @var HappeningPost
     */
    private HappeningPost $post;

    /**
     * Create a new notification instance.
     *
     * @param HappeningPost $post
     */
    public function __construct(HappeningPost $post)
    {
        $this->post = $post;
    }

    /**
     * @param $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'happening' => $this->post->happening->name,
                'id' => $this->post->id,
                'author' => $this->post->author->name,
            ]
        );
    }

    /**
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|string|null
     */
    private function getMessage()
    {
        return __('notifications.happening.user') . $this->post->author->name .
            __('notifications.happening.new_post') . $this->post->happening->name;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line($this->getMessage());
    }

    /**
     * @param $notifiable
     * @param $notification
     * @return WebPushMessage
     */
    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage())
            ->title(__('notifications.happening.happening') . $this->post->happening->name)
            ->body($this->getMessage())
            ->data(['id' => $notification->id]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
