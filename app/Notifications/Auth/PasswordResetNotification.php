<?php

namespace App\Notifications\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Class PasswordResetNotification
 * @package App\Notifications
 */
class PasswordResetNotification extends Notification
{
    use Queueable;

    /**
     * @var
     */
    public $token;

    /**
     * Create a new notification instance.
     *
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject(__('notifications.password_reset_subject'))
            ->greeting('Witaj ' . $notifiable->name)
            ->line(__('notifications.password_reset_line_1'))
            ->action(__('notifications.password_reset_cta'), url('forgotpassword/' . $this->token))
            ->line(__('notifications.password_reset_line_2'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
