<?php

namespace App\Notifications\Team;

use App\Models\Team\TeamPostComment;
use App\Notifications\Core\StandardViaTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;

/**
 * Class NewCommentNotification
 * @package App\Notifications\Team
 */
class NewCommentNotification extends Notification implements ShouldQueue
{
    use Queueable;
    use StandardViaTrait;

    /**
     * @var TeamPostComment
     */
    private TeamPostComment $comment;

    /**
     * Create a new notification instance.
     *
     * @param TeamPostComment $comment
     */
    public function __construct(TeamPostComment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @param $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'team' => $this->comment->post->team->name,
                'id' => $this->comment->id,
                'post' => $this->comment->post->name,
                'author' => $this->comment->author->name,
            ]
        );
    }

    /**
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|string|null
     */
    private function getMessage()
    {
        return __('notifications.team.user') . $this->comment->author->name .
            __('notifications.team.new_comment') . $this->comment->post->author->name .
            '(' . $this->comment->post->created_at . ') ' . __('notifications.team.in_feed') .
            $this->comment->happening->name;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line($this->getMessage());
    }

    /**
     * @param $notifiable
     * @param $notification
     * @return WebPushMessage
     */
    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage())
            ->title(__('notifications.team.team') . $this->comment->post->team->name)
            ->body($this->getMessage())
            ->data(['id' => $notification->id]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
