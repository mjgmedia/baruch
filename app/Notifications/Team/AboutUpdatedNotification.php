<?php

namespace App\Notifications\Team;

use App\Models\Team\Team;
use App\Notifications\Core\StandardViaTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;

/**
 * Class AboutUpdatedNotification
 * @package App\Notifications\Team
 */
class AboutUpdatedNotification extends Notification implements ShouldQueue
{
    use Queueable;
    use StandardViaTrait;

    /**
     * @var Team
     */
    private Team $team;

    /**
     * Create a new notification instance.
     *
     * @param Team $team
     */
    public function __construct(Team $team)
    {
        $this->team = $team;
        $this->team->load('managers');
    }

    /**
     * @param $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'name' => $this->team->name,
                'id' => $this->team->id,
                'avatar' => $this->team->avatar,
                'managers' => $this->team->managers,
            ]
        );
    }

    /**
     * @return string
     */
    private function makeManagersString(): string
    {
        $strManagers = '';
        foreach ($this->team->managers as $manager) {
            $strManagers = $manager->name . ', ';
        }
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line(__('notifications.team.about_updated'))
            ->line(__('notifications.team.about_updated_more') . $this->makeManagersString());
    }

    /**
     * @param $notifiable
     * @param $notification
     * @return WebPushMessage
     */
    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage())
            ->title(__('notifications.team.about_updated'))
            ->body(__('notifications.team.about_updated_more') . $this->makeManagersString())
            ->data(['id' => $notification->id]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
