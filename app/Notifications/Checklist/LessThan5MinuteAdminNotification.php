<?php

namespace App\Notifications\Checklist;

use App\Models\Checklist\ChecklistEntry;
use App\Notifications\Core\StandardViaTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;

/**
 * Class LessThan5MinuteAdminNotification
 * @package App\Notifications\Checklist
 */
class LessThan5MinuteAdminNotification extends Notification implements ShouldQueue
{
    use Queueable;
    use StandardViaTrait;

    /**
     * @var ChecklistEntry
     */
    private ChecklistEntry $entry;

    /**
     * Create a new notification instance.
     *
     * @param ChecklistEntry $entry
     */
    public function __construct(ChecklistEntry $entry)
    {
        $this->entry = $entry;
    }

    /**
     * @return string
     */
    private function users(): string
    {
        $strUsers = '';
        $this->entry->users->each(function ($item) use ($strUsers) {
            $strUsers = $strUsers.$item->name.', ';
        });
        return $strUsers;
    }

    /**
     * @param $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'name' => $this->entry->name,
                'should_done_at' => $this->entry->should_done_at,
                'users' => $this->entry->users,
                'users_string' => $this->users(),
            ]
        );
    }

    /**
     * @return string
     */
    private function getMessage()
    {
        return sprintf(
            "%s%s%s%s%s%s%s%s",
            __('notifications.checklist.entry'),
            $this->entry->name,
            __('notifications.checklist.should_done'),
            $this->entry->should_done_at->toDateTimeString(),
            __('notifications.checklist.however'),
            __('notifications.checklist.less_5'),
            __('notifications.checklist.users'),
            $this->users(),
        );
    }

    /**
     * @param $notifiable
     * @param $notification
     * @return WebPushMessage
     */
    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage())
            ->title(__('notifications.checklist.entry') . $this->entry->name)
            ->body($this->getMessage())
            ->data(['id' => $notification->id]);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line(__('notifications.checklist.entry') . $this->entry->name)
            ->line($this->getMessage());
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
