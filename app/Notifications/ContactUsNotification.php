<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushChannel;
use NotificationChannels\WebPush\WebPushMessage;

/**
 * Class ContactUsNotification
 * @package App\Notifications
 */
class ContactUsNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var string
     */
    private string $name;
    /**
     * @var string
     */
    private string $email;
    /**
     * @var string
     */
    private string $subject;
    /**
     * @var string
     */
    private string $message;

    /**
     * Create a new notification instance.
     *
     * @param $name
     * @param $email
     * @param $subject
     * @param $message
     */
    public function __construct(string $name, string $email, string $subject, string $message)
    {
        $this->name = $name;
        $this->email = $email;
        $this->subject = $subject;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast', WebPushChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->line(__('notifications.contact.line1') . config('app.name'))
            ->line(__('notifications.contact.from'))
            ->line($this->name)
            ->line(__('notifications.contact.email'))
            ->line($this->email)
            ->line(__('notifications.contact.subject'))
            ->line($this->subject)
            ->line(__('notifications.contact.message'))
            ->line($this->message)
            ->line(__('notifications.greetings'));
    }

    /**
     * @param $notifiable
     * @param $notification
     * @return WebPushMessage
     */
    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage())
            ->title(__('notifications.contact.line1') . config('app.name'))
            ->body(__('notifications.contact.web_push'))
            ->data(['id' => $notification->id]);
    }

    /**
     * @param $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'name' => $this->name,
                'email' => $this->email,
                'subject' => $this->subject,
                'message' => $this->message,
            ]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
