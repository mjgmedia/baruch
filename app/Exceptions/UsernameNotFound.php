<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;

/**
 * Class UsernameNotFound
 * @package App\Exceptions
 */
class UsernameNotFound extends Exception
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function render(Request $request)
    {
        if ($request->wantsJson()) {
            return response()->json(
                [
                    'message' => sprintf(
                        "%s%s%s",
                        __('exceptions.username_not_found.not_found'),
                        $request->username,
                        __('exceptions.not_found')
                    ),
                    'code' => 206,
                ],
                404
            );
        }

        return redirect(config('app.url'));
    }
}
