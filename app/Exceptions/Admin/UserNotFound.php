<?php

namespace App\Exceptions\Admin;

use Exception;
use Illuminate\Http\Request;

/**
 * Class UserNotFound
 * @package App\Exceptions
 */
class UserNotFound extends Exception
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function render(Request $request)
    {
        return response()->json(
            [
                'message' => sprintf(
                    "%s%s%s",
                    __('exceptions.user_not_found.user_id'),
                    $request->id,
                    __('exceptions.user_not_found.not_found')
                ),
                'code' => 908
            ],
            404
        );
    }
}
