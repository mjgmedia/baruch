<?php

namespace App\Exceptions\Admin;

use Exception;
use Illuminate\Http\Request;

/**
 * Class EmailNotFound
 * @package App\Exceptions
 */
class EmailNotFound extends Exception
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render(Request $request)
    {
        return response()->json(
            [
                'message' => sprintf(
                    "%s%s%s",
                    __('exceptions.email_not_found.exception_title'),
                    $request->email,
                    __('exceptions.not_found')
                ),
                'code' => 902,
            ],
            404
        );
    }
}
