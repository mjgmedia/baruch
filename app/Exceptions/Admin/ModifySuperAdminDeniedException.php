<?php

namespace App\Exceptions\Admin;

use Exception;

/**
 * Class ModifySuperAdminDeniedException
 * @package App\Exceptions\Admin
 */
class ModifySuperAdminDeniedException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('api.admin.users.crud.toggle_status.message'),
                'code' => 905,
            ],
            400
        );
    }
}
