<?php

namespace App\Exceptions\Admin;

use Exception;
use Illuminate\Http\Request;

/**
 * Class AccountCreationFailed
 * @package App\Exceptions
 */
class AccountCreationFailed extends Exception
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function render(Request $request)
    {
        return response()->json(
            [
                'message' => __('exceptions.account_creation_failed.message'),
                'code' => 901
            ],
            400
        );
    }

    public function report()
    {
        sentrit($this);
    }
}
