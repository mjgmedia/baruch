<?php

namespace App\Exceptions\Admin;

use Exception;
use Illuminate\Http\Request;

/**
 * Class UpdatingRecordFailed
 * @package App\Exceptions
 */
class UpdatingRecordFailed extends Exception
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function render(Request $request)
    {
        return response()->json(
            [
                'message' => __('exceptions.updating_record_failed.message'),
                'code' => 907,
            ],
            400
        );
    }
}
