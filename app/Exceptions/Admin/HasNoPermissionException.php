<?php

namespace App\Exceptions\Admin;

use Illuminate\Contracts\Auth\Authenticatable;
use Exception;
use Illuminate\Support\Facades\Log;

/**
 * Class HasNoPermissionException
 * @package App\Exceptions\Admin
 */
class HasNoPermissionException extends Exception
{
    /**
     * @var User
     */
    private Authenticatable $user;

    /**
     * HasNoPermissionException constructor.
     * @param $user
     */
    public function __construct(Authenticatable $user)
    {
        $this->user = $user;
        parent::__construct();
    }

    /**
     * Report this to Log
     */
    public function report()
    {
        Log::channel('security')->info('Próba wejścia bez uprawnień do panelu administracyjnego');
        ld($this->user);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('api.admin.users.crud.no_permission'),
                'code' => 903,
            ],
            401
        );
    }
}
