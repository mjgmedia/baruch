<?php

namespace App\Exceptions\Admin;

use Exception;
use Throwable;

/**
 * Class MassDeleteException
 * @package App\Exceptions\Admin
 */
class MassDeleteException extends Exception
{
    /**
     * @var Throwable
     */
    private Throwable $exception;

    /**
     * MassDeleteException constructor.
     * @param $exception
     */
    public function __construct(Throwable $exception)
    {
        parent::__construct();
        $this->exception = $exception;
    }

    /**
     * Log that caught exception to Sentry
     */
    public function report()
    {
        sentrit($this->exception);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('api.admin.users.mass.delete.fail'),
                'code' => 904,
            ],
            500
        );
    }

}
