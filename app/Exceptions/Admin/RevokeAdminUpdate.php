<?php

namespace App\Exceptions\Admin;

use Exception;
use Illuminate\Http\Request;

/**
 * Class RevokeAdminUpdate
 * @package App\Exceptions
 */
class RevokeAdminUpdate extends Exception
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function render(Request $request)
    {
        return response()->json(
            [
                'message' => __('exceptions.revoke_admin_update.message'),
                'code' => 906,
            ],
            400
        );
    }
}
