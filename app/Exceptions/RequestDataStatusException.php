<?php

namespace App\Exceptions;

/**
 * Class RequestDataStatusException
 * @package App\Exceptions
 */
class RequestDataStatusException extends RequestDataException
{
    /**
     * @var array
     */
    private array $status;

    /**
     * RequestDataStatusException constructor.
     * @param array $requestData
     * @param array $status
     * @param string $message
     */
    public function __construct(array $requestData, array $status, string $message)
    {
        $this->status = $status;
        parent::__construct($requestData, $message);
    }
}
