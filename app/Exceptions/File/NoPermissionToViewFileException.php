<?php

namespace App\Exceptions\File;

use Illuminate\Contracts\Auth\Authenticatable;
use Exception;

/**
 * Class NoPermissionToViewFileException
 * @package App\Exceptions\File
 */
class NoPermissionToViewFileException extends Exception
{
    /**
     * @var User
     */
    private Authenticatable $user;

    /**
     * NoPermissionToViewFileException constructor.
     * @param $user
     */
    public function __construct(Authenticatable $user)
    {
        $this->user = $user;
        parent::__construct();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 513,
            ],
            403
        );
    }

    /**
     * Report user which try to access file without access right's to the Log
     */
    public function report()
    {
        ld('Próba nieautoryzowanego dostępu do pliku');
        ld($this->user);
    }
}
