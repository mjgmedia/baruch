<?php

namespace App\Exceptions\File;

use Illuminate\Contracts\Auth\Authenticatable;
use Exception;

/**
 * Class NoPermissionToCrudException
 * @package App\Exceptions\File
 */
class NoPermissionToCrudException extends Exception
{
    /**
     * @var User
     */
    private Authenticatable $user;

    /**
     * NoPermissionToCrudException constructor.
     * @param $user
     */
    public function __construct(Authenticatable $user)
    {
        $this->user = $user;
        parent::__construct();
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 510,
            ],
            403
        );
    }

    /**
     * Log user who try to access to file crud
     */
    public function report()
    {
        ld('Nieautoryzowana próba dostępu do tworzenia plików');
        ld($this->user);
    }
}
