<?php

namespace App\Exceptions\File;

use App\Exceptions\CaughtException;

/**
 * Class CategoryDeleteException
 * @package App\Exceptions\File
 */
class CategoryDeleteException extends CaughtException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 501,
            ],
            500
        );
    }
}
