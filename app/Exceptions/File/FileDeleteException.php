<?php

namespace App\Exceptions\File;

use App\Exceptions\CaughtException;

/**
 * Class FileDeleteException
 * @package App\Exceptions\File
 */
class FileDeleteException extends CaughtException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 505,
            ],
            500
        );
    }
}
