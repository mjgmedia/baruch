<?php

namespace App\Exceptions\File;

use App\Exceptions\RequestDataException;

/**
 * Class CategoryUpdateException
 * @package App\Exceptions\File
 */
class CategoryUpdateException extends RequestDataException
{
     /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 502,
            ],
            500
        );
    }
}
