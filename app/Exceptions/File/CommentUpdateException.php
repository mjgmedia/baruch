<?php

namespace App\Exceptions\File;

use App\Exceptions\RequestDataException;

/**
 * Class CommentUpdateException
 * @package App\Exceptions\File
 */
class CommentUpdateException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 504,
            ],
            500
        );
    }
}
