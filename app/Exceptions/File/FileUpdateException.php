<?php

namespace App\Exceptions\File;

use App\Exceptions\RequestDataException;

/**
 * Class FileUpdateException
 * @package App\Exceptions\File
 */
class FileUpdateException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 507,
            ],
            500
        );
    }
}
