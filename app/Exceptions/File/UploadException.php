<?php

namespace App\Exceptions\File;

use App\Exceptions\CaughtException;


/**
 * Class UploadException
 * @package App\Exceptions\File
 */
class UploadException extends CaughtException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 514,
            ],
            500
        );
    }
}
