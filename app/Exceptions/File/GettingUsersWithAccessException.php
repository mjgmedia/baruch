<?php

namespace App\Exceptions\File;

use App\Exceptions\CaughtException;


/**
 * Class GettingUsersWithAccessException
 * @package App\Exceptions\File
 */
class GettingUsersWithAccessException extends CaughtException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 508,
            ],
            500
        );
    }
}
