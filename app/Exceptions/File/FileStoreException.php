<?php

namespace App\Exceptions\File;

use App\Exceptions\CaughtException;

/**
 * Class FileStoreException
 * @package App\Exceptions\File
 */
class FileStoreException extends CaughtException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 506,
            ],
            500
        );
    }
}
