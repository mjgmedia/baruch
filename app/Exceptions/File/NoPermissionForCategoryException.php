<?php

namespace App\Exceptions\File;

use Illuminate\Contracts\Auth\Authenticatable;
use Exception;

/**
 * Class NoPermissionForCategoryException
 * @package App\Exceptions\File
 */
class NoPermissionForCategoryException extends Exception
{
    /**
     * @var User
     */
    private Authenticatable $user;

    /**
     * NoPermissionForCategoryException constructor.
     * @param $user
     */
    public function __construct(Authenticatable $user)
    {
        $this->user = $user;
        parent::__construct();
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 509,
            ],
            403
        );
    }

    /**
     * Report to log file
     */
    public function report()
    {
        ld('Próba nieautoryzowanego dostępu do kategorii plików.');
        ld($this->user);
    }
}
