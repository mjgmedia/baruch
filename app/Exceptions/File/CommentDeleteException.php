<?php

namespace App\Exceptions\File;

use App\Exceptions\CaughtException;

/**
 * Class CommentDeleteException
 * @package App\Exceptions\File
 */
class CommentDeleteException extends CaughtException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 503,
            ],
            500
        );
    }
}
