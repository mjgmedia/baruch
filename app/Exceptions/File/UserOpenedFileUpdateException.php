<?php

namespace App\Exceptions\File;

use App\Models\File\File;
use Exception;

/**
 * Class UserOpenedFileUpdateException
 * @package App\Exceptions\File
 */
class UserOpenedFileUpdateException extends Exception
{
    /**
     * @var int
     */
    private int $status;
    /**
     * @var File
     */
    private File $fileModel;

    /**
     * UserOpenedFileUpdateException constructor.
     * @param int $status
     * @param File $file
     */
    public function __construct(int $status, File $file)
    {
        $this->status = $status;
        $this->fileModel = $file;
        parent::__construct('Błąd podczas zapisywania odczytu pliku przez użytkownika');
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 515,
            ],
            500
        );
    }

    /**
     * Report this exception to Sentry and log it
     */
    public function report()
    {
        sentrit($this);
        ld($this);
    }
}
