<?php

namespace App\Exceptions\File;

use Illuminate\Contracts\Auth\Authenticatable;
use Exception;

/**
 * Class NoPermissionToGlobalFilesException
 * @package App\Exceptions\File
 */
class NoPermissionToGlobalFilesException extends Exception
{
    /**
     * @var User
     */
    private Authenticatable $user;

    /**
     * NoPermissionToGlobalFilesException constructor.
     * @param $user
     */
    public function __construct(Authenticatable $user)
    {
        $this->user = $user;
        parent::__construct();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 512,
            ],
            403
        );
    }

    /**
     * Report that user which try to access to global files to log
     */
    public function report()
    {
        ld('Próba nieautoryzowanego dostępu do globalnych plików');
        ld($this->user);
    }
}
