<?php

namespace App\Exceptions\File;

use Illuminate\Contracts\Auth\Authenticatable;
use Exception;

/**
 * Class NoPermissionToGlobalCrudException
 * @package App\Exceptions\File
 */
class NoPermissionToGlobalCrudException extends Exception
{
    /**
     * @var User
     */
    private Authenticatable $user;

    /**
     * NoPermissionToGlobalCrudException constructor.
     * @param $user
     */
    public function __construct(Authenticatable $user)
    {
        $this->user = $user;
        parent::__construct();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 511,
            ],
            403
        );
    }

    /**
     * Report that user which try to crud global files
     */
    public function report()
    {
        ld('Nieautoryzowana próba dodania/edycji plików globalnych');
        ld($this->user);
    }
}
