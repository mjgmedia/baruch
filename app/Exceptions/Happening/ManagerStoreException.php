<?php

namespace App\Exceptions\Happening;

use App\Exceptions\RequestDataException;

/**
 * Class ManagerStoreException
 * @package App\Exceptions\Happening
 */
class ManagerStoreException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 605
            ],
            500
        );
    }
}
