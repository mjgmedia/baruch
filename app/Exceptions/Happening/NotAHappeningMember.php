<?php

namespace App\Exceptions\Happening;

use Exception;

/**
 * Class NotAHappeningMember
 * @package App\Exceptions\Happening
 */
class NotAHappeningMember extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 606,
            ],
            403
        );
    }
}
