<?php

namespace App\Exceptions\Happening;

use App\Exceptions\RequestDataException;

/**
 * Class PostCommentUpdateException
 * @package App\Exceptions\Happening
 */
class PostCommentUpdateException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 610,
            ],
            500
        );
    }
}
