<?php

namespace App\Exceptions\Happening;

use Exception;

/**
 * Class NotAllowedToSeePostException
 * @package App\Exceptions\Happening
 */
class NotAllowedToSeePostException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 607,
            ],
            403
        );
    }
}
