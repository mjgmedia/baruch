<?php

namespace App\Exceptions\Happening;

use App\Exceptions\CaughtException;

/**
 * Class PostCommentDeleteException
 * @package App\Exceptions\Happening
 */
class PostCommentDeleteException extends CaughtException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 609
            ],
            500
        );
    }
}
