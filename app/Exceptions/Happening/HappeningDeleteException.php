<?php

namespace App\Exceptions\Happening;

use App\Exceptions\CaughtException;

/**
 * Class HappeningDeleteException
 * @package App\Exceptions\Happening
 */
class HappeningDeleteException extends CaughtException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 602,
            ],
            500
        );
    }
}
