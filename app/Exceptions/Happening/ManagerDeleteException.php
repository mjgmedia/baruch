<?php

namespace App\Exceptions\Happening;

use App\Exceptions\RequestDataException;

/**
 * Class ManagerDeleteException
 * @package App\Exceptions\Happening
 */
class ManagerDeleteException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 604
            ],
            500
        );
    }
}
