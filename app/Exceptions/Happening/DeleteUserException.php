<?php

namespace App\Exceptions\Happening;

use App\Exceptions\RequestDataException;

/**
 * Class DeleteUserException
 * @package App\Exceptions\Happening
 */
class DeleteUserException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 601,
            ],
            500
        );
    }
}
