<?php

namespace App\Exceptions\Happening;

use App\Exceptions\RequestDataException;

/**
 * Class PostUpdateException
 * @package App\Exceptions\Happening
 */
class PostUpdateException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 612,
            ],
            500
        );
    }
}
