<?php

namespace App\Exceptions\Happening;

use App\Exceptions\RequestDataException;

/**
 * Class HappeningUpdateException
 * @package App\Exceptions\Happening
 */
class HappeningUpdateException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 603,
            ],
            500
        );
    }
}
