<?php

namespace App\Exceptions\Happening;

use App\Exceptions\CaughtException;

/**
 * Class PostDeleteException
 * @package App\Exceptions\Happening
 */
class PostDeleteException extends CaughtException
{
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 611,
            ],
            500
        );
    }
}
