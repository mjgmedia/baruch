<?php

namespace App\Exceptions\Happening;

use Exception;

/**
 * Class NotAMemberException
 * @package App\Exceptions\Happening
 */
class NotAMemberException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 608,
            ],
            403
        );
    }
}
