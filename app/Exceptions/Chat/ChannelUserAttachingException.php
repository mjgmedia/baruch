<?php

namespace App\Exceptions\Chat;

use Exception;

/**
 * Class ChannelUserAttachingException
 * @package App\Exceptions\Chat
 */
class ChannelUserAttachingException extends Exception
{
    /**
     * @var array
     */
    private array $usersIds;
    /**
     * @var int
     */
    private int $id;
    /**
     * @var string
     */
    private string $field;

    /**
     * ChannelUserAttachingException constructor.
     * @param array $usersIds
     * @param int $id
     * @param string $field
     */
    public function __construct(array $usersIds, int $id, string $field)
    {
        $this->usersIds = $usersIds;
        $this->id = $id;
        $this->field = $field;

        parent::__construct('Bład podczas dodawania userów do chatu');
    }

    /**
     * Report this exception to Sentry and log it
     */
    public function report()
    {
        sentrit($this);
        ld($this);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => '',
                'code' => 305
            ],
            500
        );
    }
}
