<?php

namespace App\Exceptions\Chat;

use App\Exceptions\CaughtException;

/**
 * Class ChatChannelDeleteException
 * @package App\Exceptions\Team
 */
class ChannelDeleteInServiceException extends CaughtException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 302,
            ],
            500
        );
    }
}
