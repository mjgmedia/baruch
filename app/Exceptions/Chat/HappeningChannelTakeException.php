<?php

namespace App\Exceptions\Chat;

use Exception;

/**
 * Class HappeningChannelTakeException
 * @package App\Exceptions\Chat
 */
class HappeningChannelTakeException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 307,
            ],
            500
        );
    }
}
