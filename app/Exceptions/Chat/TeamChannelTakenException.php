<?php

namespace App\Exceptions\Chat;

use Exception;

/**
 * Class TeamChannelTakenException
 * @package App\Exceptions\Chat
 */
class TeamChannelTakenException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 311,
            ],
            200
        );
    }
}
