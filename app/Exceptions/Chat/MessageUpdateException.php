<?php

namespace App\Exceptions\Chat;

use App\Exceptions\RequestDataException;

/**
 * Class MessageUpdateException
 * @package App\Exceptions\Chat
 */
class MessageUpdateException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 310,
            ],
            500
        );
    }
}
