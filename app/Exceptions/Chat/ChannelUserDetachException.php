<?php

namespace App\Exceptions\Chat;

use Exception;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ChatChannelUserDetachException
 * @package App\Exceptions\Team
 */
class ChannelUserDetachException extends Exception
{
    /**
     * @var Model
     */
    private Model $model;
    /**
     * @var string
     */
    private string $part;

    /**
     * ChatChannelUserDetachException constructor.
     * @param $model
     * @param $part
     */
    public function __construct(Model $model, string $part)
    {
        $this->model = $model;
        $this->part = $part;
        parent::__construct('Błąd podczas usuwania użytkownika z wiadomości lub kanału chatu');
    }

    /**
     * Report this exception to Sentry and log it
     */
    public function report()
    {
        sentrit($this);
        ld($this);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 306
            ],
            500
        );
    }
}
