<?php

namespace App\Exceptions\Chat;

use App\Exceptions\CaughtException;

/**
 * Class MessageDeleteException
 * @package App\Exceptions\Chat
 */
class MessageDeleteException extends CaughtException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 308,
            ],
            500
        );
    }
}
