<?php

namespace App\Exceptions\Chat;

use App\Models\Chat\ChatChannel;
use App\Models\User\User;
use Exception;

/**
 * Class WrongChannelException
 * @package App\Exceptions\Chat
 */
class WrongChannelException extends Exception
{
    /**
     * @var User
     */
    private User $user;
    /**
     * @var ChatChannel
     */
    private ChatChannel $channel;
    /**
     * @var array
     */
    private array $requestData;

    /**
     * WrongChannelException constructor.
     * @param User $user
     * @param ChatChannel $channel
     * @param array $requestData
     */
    public function __construct(User $user, ChatChannel $channel, array $requestData)
    {
        $this->user = $user;
        $this->channel = $channel;
        $this->requestData = $requestData;

        parent::__construct('Próba włamania się na kanał chatu');
    }

    /**
     * Report this exception to Sentry and log it
     */
    public function report()
    {
        sentrit($this);
        ld($this);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 312,
            ],
            500
        );
    }
}
