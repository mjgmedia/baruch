<?php

namespace App\Exceptions\Chat;

use App\Exceptions\CaughtException;

/**
 * Class ChannelNotFoundException
 * @package App\Exceptions\Chat
 */
class ChannelNotFoundException extends CaughtException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 303,
            ],
            500
        );
    }
}
