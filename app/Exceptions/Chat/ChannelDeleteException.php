<?php

namespace App\Exceptions\Chat;

use App\Exceptions\CaughtException;

/**
 * Class ChannelDeleteException
 * @package App\Exceptions\Chat
 */
class ChannelDeleteException extends CaughtException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 301,
            ],
            500
        );
    }
}
