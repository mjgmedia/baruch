<?php

namespace App\Exceptions\Chat;

use App\Exceptions\RequestDataException;

/**
 * Class ChannelUpdateException
 * @package App\Exceptions\Chat
 */
class ChannelUpdateException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 304,
            ],
            500
        );
    }
}
