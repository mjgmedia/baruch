<?php

namespace App\Exceptions\Chat;

use App\Models\Chat\ChatMessage;
use Exception;

/**
 * Class MessagesUsersAttachingException
 * @package App\Exceptions\Chat
 */
class MessagesUsersAttachingException extends Exception
{
    /**
     * @var array
     */
    private array $usersIds;
    /**
     * @var ChatMessage
     */
    private ChatMessage $chatMessage;

    /**
     * MessagesUsersAttachingException constructor.
     * @param $usersIds
     * @param $chatMessage
     */
    public function __construct(array $usersIds, ChatMessage $chatMessage)
    {
        $this->usersIds = $usersIds;
        $this->chatMessage = $chatMessage;
        parent::__construct('Błąd podczas dodawania usera do wiadomości chatu');
    }

    /**
     * Report this exception to Sentry and log it
     */
    public function report()
    {
        sentrit($this);
        ld($this);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 309,
            ],
            500
        );
    }
}
