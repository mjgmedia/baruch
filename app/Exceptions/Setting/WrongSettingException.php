<?php

namespace App\Exceptions\Setting;

use Exception;

class WrongSettingException extends Exception
{
    public function render()
    {
        return response()->json(
            [
                'message' => __('exceptions.settings.non_exists'),
                'code' => 701,
            ],
            404
        );
    }
}
