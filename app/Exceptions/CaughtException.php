<?php

namespace App\Exceptions;

use Exception;
use Throwable;

/**
 * Class CaughtException
 * @package App\Exceptions
 */
class CaughtException extends Exception
{
    /**
     * @var Throwable
     */
    private Throwable $exception;

    /**
     * CaughtException constructor.
     * @param $exception
     */
    public function __construct(Throwable $exception)
    {
        $this->exception = $exception;
        parent::__construct();
    }

    /**
     *
     */
    public function report()
    {
        sentrit($this->exception);
    }
}
