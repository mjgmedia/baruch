<?php

namespace App\Exceptions\Checklist;

use Exception;

/**
 * Class NoAuthForChecklistEntryCreating
 * @package App\Exceptions\Checklist
 */
class NoAuthForChecklistEntryCreating extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 418,
            ],
            403
        );
    }
}
