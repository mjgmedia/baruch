<?php

namespace App\Exceptions\Checklist;

use App\Exceptions\RequestDataException;

/**
 * Class EntryBlockingDeleteException
 * @package App\Exceptions\Checklist
 */
class EntryBlockedDeleteException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 404
            ],
            500
        );
    }
}
