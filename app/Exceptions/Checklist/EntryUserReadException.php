<?php

namespace App\Exceptions\Checklist;

use App\Models\Checklist\ChecklistEntry;
use Exception;
use Illuminate\Contracts\Auth\Authenticatable as User;

class EntryUserReadException extends Exception
{
    /**
     * @var User
     */
    private User $user;
    /**
     * @var ChecklistEntry
     */
    private ChecklistEntry $entry;

    /**
     * EntryUserDoneException constructor.
     * @param $user
     * @param $entry
     */
    public function __construct(User $user, ChecklistEntry $entry)
    {
        $this->user = $user;
        $this->entry = $entry;
        parent::__construct('Błąd podczas oznaczania zadania jako przeczytane przez usera');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 414,
            ],
            500
        );
    }

    /**
     *
     */
    public function report()
    {
        sentrit($this);
        ld($this);
    }
}
