<?php

namespace App\Exceptions\Checklist;

use App\Models\Checklist\ChecklistEntry;
use App\Models\User\User;
use Exception;

/**
 * Class EntryMarkAsDoneException
 * @package App\Exceptions\Checklist
 */
class EntryMarkAsDoneException extends Exception
{
    /**
     * @var User
     */
    private User $user;
    /**
     * @var ChecklistEntry
     */
    private ChecklistEntry $checklistEntry;

    /**
     * EntryMarkAsDoneException constructor.
     * @param $user
     * @param $checklistEntry
     */
    public function __construct(User $user, ChecklistEntry $checklistEntry)
    {
        $this->user = $user;
        $this->checklistEntry = $checklistEntry;
        parent::__construct('Błąd podczas oznaczania zadania jako wykonane');
    }

    /**
     * Report this exception to Sentry and log it
     */
    public function report()
    {
        sentrit($this);
        ld($this);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 410
            ],
            500
        );
    }
}
