<?php

namespace App\Exceptions\Checklist;

use App\Exceptions\RequestDataException;

/**
 * Class EntryUserDeleteException
 * @package App\Exceptions\Checklist
 */
class EntryUserDeleteException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 412,
            ],
            500
        );
    }
}
