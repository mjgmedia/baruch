<?php

namespace App\Exceptions\Checklist;

use App\Exceptions\RequestDataStatusException;

/**
 * Class EntryUserUpdateException
 * @package App\Exceptions\Checklist
 */
class EntryUserUpdateException extends RequestDataStatusException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 416,
            ],
            500
        );
    }
}
