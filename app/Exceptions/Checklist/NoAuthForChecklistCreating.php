<?php

namespace App\Exceptions\Checklist;

use Illuminate\Contracts\Auth\Authenticatable;
use Exception;

/**
 * Class NoAuthForChecklistCreating
 * @package App\Exceptions
 */
class NoAuthForChecklistCreating extends Exception
{
    /**
     * @var User
     */
    private Authenticatable $user;

    /**
     * NoAuthForChecklistCreating constructor.
     * @param $user
     */
    public function __construct(Authenticatable $user)
    {
        $this->user = $user;
        parent::__construct();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 417,
            ],
            403
        );
    }

    /**
     *
     */
    public function report()
    {
        ld('Nie autoryzowana próba dostępu do tworzenia checklisty');
        ld($this->user);
    }
}
