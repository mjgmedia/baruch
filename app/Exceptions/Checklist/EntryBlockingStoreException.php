<?php

namespace App\Exceptions\Checklist;

use App\Exceptions\RequestDataStatusException;

/**
 * Class EntryBlockingStoreException
 * @package App\Exceptions\Checklist
 */
class EntryBlockingStoreException extends RequestDataStatusException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 408
            ],
            500
        );
    }
}
