<?php

namespace App\Exceptions\Checklist;

use App\Exceptions\CaughtException;
use Illuminate\Http\JsonResponse;

/**
 * Class ChecklistEntryDeleteException
 * @package App\Exceptions\Checklist
 */
class ChecklistEntryDeleteException extends CaughtException
{
    /**
     * @return JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 402,
            ],
            500
        );
    }
}
