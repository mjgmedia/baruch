<?php

namespace App\Exceptions\Checklist;

use App\Exceptions\RequestDataStatusException;

class EntryBlockingUpdateException extends RequestDataStatusException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 409
            ],
            500
        );
    }
}
