<?php

namespace App\Exceptions\Checklist;

use Exception;

/**
 * Class NotPatternException
 * @package App\Exceptions\Checklist
 */
class NotPatternException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 419,
            ],
            404
        );
    }
}
