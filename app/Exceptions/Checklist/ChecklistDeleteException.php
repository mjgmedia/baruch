<?php

namespace App\Exceptions\Checklist;

use App\Exceptions\CaughtException;

/**
 * Class ChecklistDeleteException
 * @package App\Exceptions\Checklist
 */
class ChecklistDeleteException extends CaughtException
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 401,
            ],
            500
        );
    }
}
