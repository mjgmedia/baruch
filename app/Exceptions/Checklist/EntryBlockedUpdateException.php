<?php

namespace App\Exceptions\Checklist;

use App\Exceptions\RequestDataStatusException;

class EntryBlockedUpdateException extends RequestDataStatusException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 406
            ],
            500
        );
    }
}
