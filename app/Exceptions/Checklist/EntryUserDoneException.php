<?php

namespace App\Exceptions\Checklist;

use App\Models\Checklist\ChecklistEntry;
use \Illuminate\Contracts\Auth\Authenticatable as User;
use Exception;

/**
 * Class EntryUserDoneException
 * @package App\Exceptions\Checklist
 */
class EntryUserDoneException extends Exception
{
    /**
     * @var User
     */
    private User $user;
    /**
     * @var ChecklistEntry
     */
    private ChecklistEntry $entry;

    /**
     * EntryUserDoneException constructor.
     * @param $user
     * @param $entry
     */
    public function __construct(User $user, ChecklistEntry $entry)
    {
        $this->user = $user;
        $this->entry = $entry;
        parent::__construct('Błąd podczas oznaczania zadania jako wykonane przez usera');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 413,
            ],
            500
        );
    }

    /**
     *
     */
    public function report()
    {
        sentrit($this);
        ld($this);
    }
}
