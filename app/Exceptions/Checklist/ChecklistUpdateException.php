<?php

namespace App\Exceptions\Checklist;

use App\Exceptions\RequestDataException;

/**
 * Class ChecklistUpdateException
 * @package App\Exceptions\Checklist
 */
class ChecklistUpdateException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 403,
            ],
            500
        );
    }
}
