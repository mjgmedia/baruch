<?php

namespace App\Exceptions\Checklist;

use App\Exceptions\RequestDataStatusException;

/**
 * Class EntryUserStoreException
 * @package App\Exceptions\Checklist
 */
class EntryUserStoreException extends RequestDataStatusException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 415,
            ],
            500
        );
    }
}
