<?php

namespace App\Exceptions\Checklist;

use App\Exceptions\RequestDataException;

/**
 * Class EntryUpdateException
 * @package App\Exceptions\Checklist
 */
class EntryUpdateException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 411,
            ],
            500
        );
    }
}
