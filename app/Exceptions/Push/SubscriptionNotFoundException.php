<?php

namespace App\Exceptions\Push;

use App\Exceptions\RequestDataException;

/**
 * Class SubscriptionNotFoundException
 * @package App\Exceptions\Push
 */
class SubscriptionNotFoundException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => 'Subscription not found.',
                'code' => 204
            ],
            404
        );
    }
}
