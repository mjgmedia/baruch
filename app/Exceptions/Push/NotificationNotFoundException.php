<?php

namespace App\Exceptions\Push;

use App\Exceptions\RequestDataException;

/**
 * Class NotificationNotFoundException
 * @package App\Exceptions\Push
 */
class NotificationNotFoundException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => 'Notification not found.',
                'code' => 203
            ],
            404
        );
    }
}
