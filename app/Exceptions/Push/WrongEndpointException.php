<?php

namespace App\Exceptions\Push;

use App\Exceptions\RequestDataException;

/**
 * Class WrongEndpointException
 * @package App\Exceptions\Push
 */
class WrongEndpointException extends RequestDataException
{
     /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => 'Endpoint missing.',
                'code' => 205
            ],
            403
        );
    }
}
