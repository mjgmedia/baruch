<?php

namespace App\Exceptions\User;

use Exception;
use Illuminate\Http\Request;

/**
 * Class TokenNotFound
 * @package App\Exceptions
 */
class TokenNotFound extends Exception
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render(Request $request)
    {
        return response()->json(
            [
                'message' => __('exceptions.user_token_not_found.message'),
                'code' => 202,
            ],
            404
        );
    }
}
