<?php

namespace App\Exceptions\User;

use Exception;

/**
 * Class ProfileUpdateException
 * @package App\Exceptions\Admin
 */
class ProfileUpdateException extends Exception
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('api.user.profile.errors'),
                'code' => 201,
            ],
            500
        );
    }

    /**
     * Report this exception to Sentry
     */
    public function report()
    {
        sentrit($this);
    }
}
