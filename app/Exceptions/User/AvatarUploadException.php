<?php

namespace App\Exceptions\User;

use Exception;
use Throwable;

/**
 * Class AvatarUploadException
 * @package App\Exceptions\User
 */
class AvatarUploadException extends Exception
{
    /**
     * @var Throwable
     */
    private Throwable $exception;

    /**
     * AvatarUploadException constructor.
     * @param $exception
     */
    public function __construct(Throwable $exception)
    {
        parent::__construct();
        $this->exception = $exception;
    }

    /**
     * Report that caught exception to Sentry
     */
    public function report()
    {
        sentrit($this->exception);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'status' => __('api.user.account.avatar.error'),
                'code' => 208
            ]
        );
    }


}
