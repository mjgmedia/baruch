<?php

namespace App\Exceptions\User;

use Exception;

/**
 * Class AccountUpdateException
 * @package App\Exceptions\User
 */
class AccountUpdateException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('api.user.account.update.error'),
                'code' => 207
            ]
        );
    }
}
