<?php

namespace App\Exceptions\User;

use Exception;

/**
 * Class ActivationCodeNotFoundException
 * @package App\Exceptions\User
 */
class ActivationCodeNotFoundException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('api.register.activation_error'),
                'code' => 209
            ],
            404
        );
    }
}
