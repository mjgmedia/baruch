<?php

namespace App\Exceptions;

use Illuminate\Foundation\Http\Exceptions\MaintenanceModeException as Exception;

class MaintenanceModeException extends Exception
{
    public function render()
    {
        return response()->json(
            [
                'message' => __('api.maintenance'),
                'retry' => $this->retryAfter,
            ],
            503
        );
    }
}
