<?php

namespace App\Exceptions;

use Exception;

/**
 * Class RequestDataException
 * @package App\Exceptions
 */
class RequestDataException extends Exception
{
    /**
     * @var array
     */
    private array $requestData;

    /**
     * RequestDataException constructor.
     * @param array $requestData
     * @param string $message
     */
    public function __construct(array $requestData, string $message='')
    {
        $this->requestData = $requestData;
        parent::__construct($message);
    }

    /**
     * Report this exception to Sentry and log it
     */
    public function report()
    {
        sentrit($this);
        ld($this);
    }
}
