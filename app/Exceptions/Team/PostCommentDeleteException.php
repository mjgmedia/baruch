<?php

namespace App\Exceptions\Team;

use App\Exceptions\CaughtException;
use Exception;
use Throwable;

/**
 * Class PostCommentDeleteException
 * @package App\Exceptions\Team
 */
class PostCommentDeleteException extends CaughtException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 806,
            ],
            500
        );
    }
}
