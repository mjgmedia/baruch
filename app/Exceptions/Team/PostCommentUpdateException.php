<?php

namespace App\Exceptions\Team;

use App\Exceptions\RequestDataException;

/**
 * Class PostCommentUpdateException
 * @package App\Exceptions\Team
 */
class PostCommentUpdateException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 807
            ],
            500
        );
    }
}
