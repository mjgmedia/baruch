<?php

namespace App\Exceptions\Team;

use App\Exceptions\RequestDataException;

/**
 * Class UserDeleteException
 * @package App\Exceptions\Team
 */
class UserDeleteException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 812
            ],
            500
        );
    }
}
