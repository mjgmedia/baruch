<?php

namespace App\Exceptions\Team;

use Exception;

/**
 * Class NotATeamMember
 * @package App\Exceptions\Team
 */
class NotATeamMember extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 805,
            ],
            403
        );
    }
}
