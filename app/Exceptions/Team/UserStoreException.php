<?php

namespace App\Exceptions\Team;

use App\Exceptions\RequestDataException;

/**
 * Class UserStoreException
 * @package App\Exceptions\Team
 */
class UserStoreException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 813
            ],
            500
        );
    }
}
