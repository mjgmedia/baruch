<?php

namespace App\Exceptions\Team;

use App\Exceptions\RequestDataException;

/**
 * Class ManagersStoreException
 * @package App\Exceptions\Team
 */
class ManagersStoreException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 804,
            ],
            500
        );
    }
}
