<?php

namespace App\Exceptions\Team;

use App\Exceptions\RequestDataException;

/**
 * Class HappeningDeleteException
 * @package App\Exceptions\Team
 */
class HappeningDeleteException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 801,
            ],
            500
        );
    }
}
