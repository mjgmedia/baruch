<?php

namespace App\Exceptions\Team;

use App\Exceptions\CaughtException;

/**
 * Class PostDeleteException
 * @package App\Exceptions\Team
 */
class PostDeleteException extends CaughtException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 808,
            ],
            500
        );
    }
}
