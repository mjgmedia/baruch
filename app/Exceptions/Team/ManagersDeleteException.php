<?php

namespace App\Exceptions\Team;

use App\Exceptions\RequestDataException;

/**
 * Class ManagersDeleteException
 * @package App\Exceptions\Team
 */
class ManagersDeleteException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 803,
            ],
            500
        );
    }
}
