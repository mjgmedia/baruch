<?php

namespace App\Exceptions\Team;

use App\Exceptions\CaughtException;

/**
 * Class TeamDeleteException
 * @package App\Exceptions\Team
 */
class TeamDeleteException extends CaughtException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 810,
            ],
            500
        );
    }
}
