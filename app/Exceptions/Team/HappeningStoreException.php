<?php

namespace App\Exceptions\Team;

use App\Exceptions\RequestDataException;

/**
 * Class HappeningStoreException
 * @package App\Exceptions\Team
 */
class HappeningStoreException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 802
            ],
            500
        );
    }
}
