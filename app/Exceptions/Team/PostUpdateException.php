<?php

namespace App\Exceptions\Team;

use App\Exceptions\RequestDataException;

/**
 * Class PostUpdateException
 * @package App\Exceptions\Team
 */
class PostUpdateException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 809,
            ],
            500
        );
    }
}
