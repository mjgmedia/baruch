<?php

namespace App\Exceptions\Team;

use App\Exceptions\RequestDataException;

/**
 * Class TeamUpdateException
 * @package App\Exceptions\Team
 */
class TeamUpdateException extends RequestDataException
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __(''),
                'code' => 811,
            ],
            500
        );
    }
}
