<?php

namespace App\Exceptions\Auth;

use Exception;
use Throwable;

/**
 * Class OAuthRequestException
 * @package App\Exceptions\Auth
 */
class OAuthRequestException extends Exception
{
    /**
     * @var Throwable
     */
    private Throwable $exception;

    /**
     * OAuthRequestException constructor.
     * @param $exception
     */
    public function __construct(Throwable $exception)
    {
        parent::__construct();
        $this->exception = $exception;
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('exceptions.auth.request'),
                'code' => 113,
            ],
            500
        );
    }

    /**
     * Report exception to logs and Sentry
     */
    public function report()
    {
        ld($this->exception);
        sentrit($this->exception);
    }
}
