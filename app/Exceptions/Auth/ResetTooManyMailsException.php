<?php

namespace App\Exceptions\Auth;

use Exception;

/**
 * Class ResetTooManyMailsException
 * @package App\Exceptions\Auth
 */
class ResetTooManyMailsException extends Exception
{
    /**
     * @var
     */
    private $resent;

    /**
     * ResetTooManyMailsException constructor.
     * @param $resent
     */
    public function __construct($resent)
    {
        parent::__construct();
        $this->resent = $resent;
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => sprintf(
                    "%s%s%s",
                    __('api.forgot_password.send_reset_email.limit'),
                    $this->resent,
                    __('api.forgot_password.send_reset_email.limit_exceeded')
                ),
                'code' => 106,
            ],
            429
        );
    }
}
