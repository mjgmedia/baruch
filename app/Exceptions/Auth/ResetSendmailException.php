<?php

namespace App\Exceptions\Auth;

use Exception;

/**
 * Class ResetSendmailException
 * @package App\Exceptions\Auth
 */
class ResetSendmailException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('api.forgot_password.send_reset_email.email_error'),
                'code' => 104,
            ],
            500
        );
    }

    public function report()
    {
        sentrit($this);
    }
}
