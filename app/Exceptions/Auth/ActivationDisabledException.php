<?php

namespace App\Exceptions\Auth;

use Exception;

/**
 * Class ActivationDisabledException
 * @package App\Exceptions\Auth
 */
class ActivationDisabledException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('exceptions.activation.disabled'),
                'code' => 111
            ],
            500
        );
    }
}
