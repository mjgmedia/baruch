<?php

namespace App\Exceptions\Auth;

use Exception;

/**
 * Class OAuthLoginException
 * @package App\Exceptions\Auth
 */
class OAuthLoginException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('api.login.wrong_credentials'),
                'code' => 102,
            ],
            422
        );
    }
}
