<?php

namespace App\Exceptions\Auth;

use Exception;

/**
 * Class ResetFailException
 * @package App\Exceptions\Auth
 */
class ResetFailException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('api.reset_password.fail'),
                'code' => 103,
            ],
            500
        );
    }

    /**
     *
     */
    public function report()
    {
        $this->message = 'Błąd resetu hasła [500] - problem mechanizmu resetu Laravela. ResetPasswordController@resetPassword';
        sentrit($this);
    }
}
