<?php

namespace App\Exceptions\Auth;

use Exception;

/**
 * Class AccountNotActivatedException
 * @package App\Exceptions\Auth
 */
class AccountNotActivatedException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('auth.user_not_activated'),
                'code' => 110,
            ],
            500
        );
    }
}
