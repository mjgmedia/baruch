<?php

namespace App\Exceptions\Auth;

use Exception;

/**
 * Class ResetUserNotFoundException
 * @package App\Exceptions\Auth
 */
class ResetUserNotFoundException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('api.forgot_password.send_reset_email.link_sent'),
                'code' => 107
            ],
            200
        );
    }
}
