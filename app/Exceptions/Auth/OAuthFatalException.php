<?php

namespace App\Exceptions\Auth;

use Exception;

/**
 * Class OAuthFatalException
 * @package App\Exceptions\Auth
 */
class OAuthFatalException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('api.login.oauth_error'),
                'code' => 101
            ],
            500
        );
    }

    /**
     *
     */
    public function report()
    {
        sentrit($this);
    }
}
