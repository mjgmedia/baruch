<?php

namespace App\Exceptions\Auth;

use Exception;
use Throwable;

/**
 * Class OAuthConnectException
 * @package App\Exceptions\Auth
 */
class OAuthConnectException extends Exception
{
    /**
     * @var Throwable
     */
    private Throwable $exception;

    /**
     * OAuthConnectException constructor.
     * @param $exception
     */
    public function __construct(Throwable $exception)
    {
        parent::__construct();
        $this->exception = $exception;
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('exceptions.auth.connection'),
                'code' => 112,
            ],
            500
        );
    }

    /**
     *
     */
    public function report()
    {
        ld($this->exception);
        sentrit($this->exception);
    }
}
