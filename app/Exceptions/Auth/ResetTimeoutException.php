<?php

namespace App\Exceptions\Auth;

use Exception;

/**
 * Class ResetTimeoutException
 * @package App\Exceptions\Auth
 */
class ResetTimeoutException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('api.reset_password.timeout'),
                'code' => 105,
            ],
            408
        );
    }
}
