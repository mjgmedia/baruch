<?php

namespace App\Exceptions\Auth;

use Exception;

/**
 * Class AccountNotFoundException
 * @package App\Exceptions\Auth
 */
class AccountNotFoundException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('auth.user_not_found'),
                'code' => 109
            ],
            500
        );
    }
}
