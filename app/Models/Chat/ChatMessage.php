<?php

namespace App\Models\Chat;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ChatMessage
 * @package App\Models
 */
class ChatMessage extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'message',
        'author_id',
        'chat_channel_id',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'message' => 'string',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(
            '\App\Models\User\User',
            'author_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel()
    {
        return $this->belongsTo(
            '\App\Models\Chat\ChatChannel',
            'chat_channel_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(
            '\App\Models\User\User',
            'chat_message_user',
            'chat_message_id',
            'user_id'
        )
            ->withTimestamps()
            ->withPivot('read')
            ->using('\App\Models\Chat\ChatChannelUser');
    }

    /**
     * @param $query
     * @param $userId
     * @return mixed
     */
    public function scopeUnreadMessages($query, $userId)
    {
        return $query->whereHas(
            'users',
            function (Builder $query) use ($userId) {
                return $query->whereNull('chat_message_user.read')
                    ->where('chat_message_user.user_id', '=', $userId);
            }
        );
    }
}