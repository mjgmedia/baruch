<?php

namespace App\Models\Chat;

use Cviebrock\EloquentSluggable\Sluggable;
use http\Message;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ChatChannel
 * @property int author_id
 * @package App\Models
 */
class ChatChannel extends Model
{
    use Sluggable;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'author_id'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'slug' => 'string',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team()
    {
        return $this->belongsTo(
            '\App\Models\Team\Team',
            'team_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function happening()
    {
        return $this->belongsTo(
            '\App\Models\Happening\Happening',
            'happening_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(
            '\App\Models\User\User',
            'chat_channel_user',
            'chat_channel_id',
            'user_id'
        )
            ->withTimestamps()
            ->using('\App\Models\Chat\ChatChannelUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(
            '\App\Models\Chat\ChatMessage',
            'chat_channel_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(
            '\App\Models\User\User',
            'author_id',
            'id'
        );
    }

    /**
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * @param $query
     * @param $userId
     * @return mixed
     */
    public function scopeWithUnreadMessages($query, $userId)
    {
        return $query->with(
            [
                'messages' => function ($query) use ($userId) {
                    return $query->unreadMessages($userId);
                }
            ]
        );
    }

    /**
     * @param Builder $query
     * @param int $userId
     * @return Builder
     */
    public function scopeIsMember(Builder $query, int $userId)
    {
        return $query->whereHas('users', function(Builder $query) use ($userId) {
            return $query->where('user_id', $userId);
        });
    }
}
