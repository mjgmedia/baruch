<?php

namespace App\Models\Chat;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class ChatChannelUser
 * @package App\Models
 */
class ChatChannelUser extends Pivot
{
    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var string
     */
    public $table = 'chat_channel_user';
}
