<?php

namespace App\Models\Chat;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class ChatMessageUser
 * @package App\Models
 */
class ChatMessageUser extends Pivot
{
    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var string
     */
    public $table = 'chat_message_user';
}
