<?php

namespace App\Models\Happening;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class HappeningManager
 * @package App\Models
 */
class HappeningManager extends Pivot
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    public $table = 'happening_manager';
}
