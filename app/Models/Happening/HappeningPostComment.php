<?php

namespace App\Models\Happening;

use Illuminate\Database\Eloquent\Model;

class HappeningPostComment extends Model
{
    public $timestamps = true;
    public $table = 'happening_post_comments';

    /**
     * @var string[]
     */
    protected $casts = [
        'text' => 'string',
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'created_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(
            '\App\Models\User\User',
            'author_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(
            '\App\Models\Happening\HappeningPost',
            'post_id',
            'id'
        );
    }
}
