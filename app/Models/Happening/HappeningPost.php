<?php

namespace App\Models\Happening;

use Illuminate\Database\Eloquent\Model;

/**
 * Class HappeningPost
 * @property int id
 * @package App\Models
 */
class HappeningPost extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var string
     */
    public $table = 'happening_posts';

    /**
     * @var string[]
     */
    protected $casts = [
        'text' => 'string'
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'created_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function happening()
    {
        return $this->belongsTo(
            '\App\Models\Happening\Happening',
            'happening_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(
            '\App\Models\User\User',
            'user_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(
            '\App\Models\Happening\HappeningPostComment',
            'post_id',
            'id'
        );
    }
}
