<?php

namespace App\Models\Happening;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Happening
 * @property int id
 * @package App\Models
 * @method static findOrFail($happening_id):Happening
 * @method static isMember(\Illuminate\Contracts\Auth\Authenticatable|null $user):Builder
 * @method static create(array $validated)
 */
class Happening extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'date_access',
        'date_start',
        'date_end',
        'close_after_end',
        'image_url'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'close_after_end' => 'boolean',
        'image_url' => 'string',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'date_access',
        'date_end',
        'date_start',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(
            '\App\Models\User\User',
            'happening_user',
            'happening_id',
            'user_id'
        )
            ->using('\App\Models\Happening\HappeningUser')
            ->withTimestamps();
    }

    /**
     * @param Builder $query
     * @param int $userId
     * @return Builder
     */
    public function scopeIsMember(Builder $query, int $userId)
    {
        return $query->whereHas('users', function ($query) use ($userId) {
            return $query->where('happening_user.user_id', $userId);
        });
    }

    /**
     * @param Builder $query
     * @param int $userId
     * @return Builder
     */
    public function scopeIsNotMember(Builder $query, int $userId)
    {
        return $query->whereDoesntHave('users', function ($query) use ($userId) {
            return $query->where('happening_user.user_id', $userId);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function managers()
    {
        return $this->belongsToMany(
            '\App\Models\User\User',
            'happening_manager',
            'happening_id',
            'user_id'
        )
            ->using('\App\Models\Happening\HappeningManager')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teams()
    {
        return $this->belongsToMany(
            '\App\Models\Team\Team',
            'team_happening',
            'happening_id',
            'team_id'
        )
            ->using('\App\Models\Happening\TeamHappening')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function checklists()
    {
        return $this->hasMany(
            '\App\Models\Checklist\Checklist',
            'happening_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(
            '\App\Models\File\File',
            'happening_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fileCategories()
    {
        return $this->hasMany(
            '\App\Models\File\FileCategory',
            'happening_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fileAccesses()
    {
        return $this->hasMany(
            '\App\Models\File\FileAccess',
            'happening_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(
            '\App\Models\Happening\HappeningPost',
            'happening_id',
            'id'
        );
    }
}
