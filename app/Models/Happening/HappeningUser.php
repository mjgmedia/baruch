<?php

namespace App\Models\Happening;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class HappeningUser
 * @package App\Models
 */
class HappeningUser extends Pivot
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    public $table = 'happening_user';
}
