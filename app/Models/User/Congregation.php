<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Congregation
 * @package App\Models
 */
class Congregation extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profiles()
    {
        return $this->hasMany('\App\Models\User\Profile', 'congregation_id', 'id');
    }
}
