<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Profile
 * @package App\Models
 * @method static create(int[]|string[] $array)
 * @method static where(string $string, array|int|string $key)
 * @method static find($id)
 * @method static whereIn(string $string, $ids)
 */
class Profile extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $fillable = [
        'phone',
        'address',
        'city',
        'postal_code',
        'elder',
        'servant',
        'pioneer',
        'congregation_id'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'elder' => 'boolean',
        'servant' => 'boolean',
        'city' => 'string',
        'postal_code' => 'string',
        'phone' => 'string',
        'pioneer' => 'boolean',
    ];

    /**
     * @var string
     */
    protected $table = 'profiles';

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function congregation()
    {
        return $this->belongsTo(
            '\App\Models\User\Congregation',
            'congregation_id',
            'id'
        );
    }
}
