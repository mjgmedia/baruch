<?php

namespace App\Models\User;

use App\Notifications\Auth\PasswordResetNotification;
use App\Notifications\User\ActivationNotification;
use App\Traits\User\Methods;
use App\Traits\User\Mutators;
use App\Traits\User\Relationships;
use App\Traits\User\Scopes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;
use NotificationChannels\WebPush\HasPushSubscriptions;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 * @property mixed name
 * @property int id
 * @package App\Models
 * @method static create(int[]|string[] $array)
 * @method static where(string $string, array|int|string $key)
 * @method static find($id)
 * @method static whereIn(string $string, $ids)
 * @method static role(string $string)
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Scopes;
    use Methods;
    use Mutators;
    use Relationships;
    use HasApiTokens;

    // Passport

    use HasPushSubscriptions;

    // NotificationsChannels

    use HasRoles;

    // Laravel-permissions

    use Notifiable;

    // Laravel

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'username',
        'active',
        'webpush'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
        'webpush' => 'boolean',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'email_verified_at',
        'last_activity',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'all_permissions',
        'can',
        'all_roles'
    ];

    /**
     * Override Password Reset Default Built in Laravel
     *
     * @param string $token
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }

    public function sendEmailVerificationNotification()
    {
        if (config('app.activation_enabled')) {
            $this->activation_code = Str::uuid();
            $this->notify(new ActivationNotification($this->activation_code));
        }
    }

    public function markEmailAsVerified()
    {
        return $this->forceFill(
            [
                'email_verified_at' => $this->freshTimestamp(),
                'activation_code' => ''
            ]
        )->save();
    }
}
