<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SocialAccount
 * @package App\Models
 * @method static create(int[]|string[] $array)
 * @method static where(string $string, array|int|string $key)
 * @method static find($id)
 * @method static whereIn(string $string, $ids)
 */
class SocialAccount extends Model
{
    /**
     * @var array
     */
    public $fillable = [
        'provider',
        'provider_user_id',
        'user_id',
    ];
    /**
     * @var string
     */
    protected $table = 'social_accounts';

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
