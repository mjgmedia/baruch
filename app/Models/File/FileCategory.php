<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FileCategory
 * @property int id
 * @package App\Models
 */
class FileCategory extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var string
     */
    public $table = 'file_categories';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'global'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'global' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(
            '\App\Models\User\User',
            'author_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team()
    {
        return $this->belongsTo(
            '\App\Models\Team\Team',
            'team_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function happening()
    {
        return $this->belongsTo(
            '\App\Models\Happening\Happening',
            'happening_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(
            '\App\Models\File\File',
            'file_category_id',
            'id'
        );
    }
}
