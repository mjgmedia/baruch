<?php

namespace App\Models\File;

use App\Models\Happening\Happening;
use App\Models\Happening\HappeningUser;
use App\Models\Team\TeamUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;

/**
 * Class File
 * @property mixed comments
 * @property int id
 * @package App\Models
 * @method static userHaveAccess($id)
 * @method static where(string $string, $id)
 */
class File extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var string
     */
    public $table = 'files';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'filename',
        'desc',
        'global',
        'happening_id',
        'team_id',
        'file_category_id',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'filename' => 'string',
        'desc' => 'string',
        'global' => 'boolean',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * This is an advanced scope query to select those files
     * which user have access through any possible way.
     * @param Builder $query
     * @param int $userId
     * @return Builder
     */
    public function scopeUserHaveAccess(Builder $query, int $userId)
    {
        return $query->whereHas('accesses', function($query) use ($userId) {
            return $query->where('user_id', $userId)
                ->orWhereIn('team_id', TeamUser::query()
                    ->where('user_id', $userId)
                    ->pluck('team_id')
                )
                ->orWhereIn('happening_id', HappeningUser::query()
                    ->where('user_id', $userId)
                    ->pluck('happening_id')
                )
                ->orWhereIn('role_id', Role::query()
                    ->whereHas('users', function ($query) use ($userId) {
                        return $query->where('model_id', $userId);
                    })
                    ->pluck('id')
                );
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function happening()
    {
        return $this->belongsTo(
            '\App\Models\Happening\Happening',
            'happening_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team()
    {
        return $this->belongsTo(
            '\App\Models\Team\Team',
            'team_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(
            '\App\Models\User\User',
            'author_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(
            '\App\Models\File\FileCategory',
            'file_category_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function displayed()
    {
        return $this->belongsToMany(
            '\App\Models\User\User',
            'file_user',
            'file_id',
            'user_id'
        )
            ->withTimestamps()
            ->withPivot('read')
            ->using('\App\Models\File\FileUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accesses()
    {
        return $this->hasMany(
            '\App\Models\File\FileAccess',
            'file_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(
            '\App\Models\File\FileComment',
            'file_id',
            'id'
        );
    }
}
