<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FileComment
 * @package App\Models
 */
class FileComment extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var string
     */
    public $table = 'file_comments';

    /**
     * @var string[]
     */
    protected $casts = [
        'comment' => 'string'
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'created_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file()
    {
        return $this->belongsTo(
            '\App\Models\File\File',
            'file_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(
            '\App\Models\User\User',
            'author_id',
            'id'
        );
    }
}
