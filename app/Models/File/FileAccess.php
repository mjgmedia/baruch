<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FileAccess
 * @package App\Models
 */
class FileAccess extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var string
     */
    public $table = 'file_accesses';

    /**
     * @var array
     */
    public $fillable = [
        'file_id',
        'user_id',
        'role_id',
        'team_id',
        'happening_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file()
    {
        return $this->belongsTo(
            '\App\Models\File\File',
            'file_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
            '\App\Models\User\User',
            'user_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team()
    {
        return $this->belongsTo(
            '\App\Models\Team\Team',
            'team_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function happening()
    {
        return $this->belongsTo(
            '\App\Models\Happening\Happening',
            'happening_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(
            '\Spatie\Permission\Models\Role',
            'role_id',
            'id'
        );
    }
}
