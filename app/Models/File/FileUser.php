<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Relations\Pivot;

class FileUser extends Pivot
{
    public $timestamps = true;
    public $table = 'file_user';
}
