<?php

namespace App\Models\Team;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TeamPost
 * @property int id
 * @package App\Models
 */
class TeamPost extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var string
     */
    public $table = 'team_posts';

    protected $fillable = [
        'text',
        'team_id',
        'author_id'
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'text' => 'string',
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'created_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(
            '\App\Models\User\User',
            'author_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team()
    {
        return $this->belongsTo(
            '\App\Models\Team\Team',
            'team_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(
            '\App\Models\Team\TeamPostComment',
            'post_id',
            'id'
        );
    }
}
