<?php

namespace App\Models\Team;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Team
 * @package App\Models
 * @method static findOrFail($team_id)
 * @method static isMebmer($id)
 */
class Team extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'avatar',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'avatar' => 'string',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(
            '\App\Models\User\User',
            'team_user',
            'team_id',
            'user_id'
        )
            ->withTimestamps()
            ->using('\App\Models\Team\TeamUser');
    }

    /**
     * @param Builder $query
     * @param int $userId
     * @return Builder
     */
    public function scopeIsMember(Builder $query, int $userId)
    {
        return $query->whereHas('users', function ($query) use ($userId) {
            return $query->where('team_user.user_id', $userId);
        });
    }

    /**
     * @param Builder $query
     * @param int $userId
     * @return Builder
     */
    public function scopeIsNotMember(Builder $query, int $userId)
    {
        return $query->whereDoesntHave('users', function ($query) use ($userId) {
            return $query->where('team_user.user_id', $userId);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function managers()
    {
        return $this->belongsToMany(
            '\App\Models\User\User',
            'team_manager',
            'team_id',
            'user_id'
        )
            ->withTimestamps()
            ->using('\App\Models\Team\TeamManager');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function happenings()
    {
        return $this->belongsToMany(
            '\App\Models\Happening\Happening',
            'team_happening',
            'team_id',
            'happening_id'
        )
            ->withTimestamps()
            ->using('\App\Models\Team\TeamHappening');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function checklists()
    {
        return $this->hasMany(
            '\App\Models\Checklist\Checklist',
            'team_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(
            '\App\Models\File\File',
            'team_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fileCategories()
    {
        return $this->hasMany(
            '\App\Models\File\FileCategory',
            'team_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fileAccesses()
    {
        return $this->hasMany(
            '\App\Models\File\FileAccess',
            'team_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(
            '\App\Models\Team\TeamPost',
            'team_id',
            'id'
        );
    }
}
