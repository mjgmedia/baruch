<?php

namespace App\Models\Team;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class TeamManager
 * @package App\Models
 */
class TeamManager extends Pivot
{
    /**
     * @var string
     */
    public $table = 'team_manager';
    /**
     * @var bool
     */
    public $timestamps = true;
}
