<?php

namespace App\Models\Team;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class TeamHappening
 * @package App\Models
 */
class TeamHappening extends Pivot
{
    /**
     * @var string
     */
    public $table = 'team_happening';
    /**
     * @var bool
     */
    public $timestamps = true;
}
