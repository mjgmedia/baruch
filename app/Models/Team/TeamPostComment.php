<?php

namespace App\Models\Team;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TeamPostComment
 * @package App\Models
 */
class TeamPostComment extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var string
     */
    protected $table = 'team_post_comments';

    /**
     * @var string[]
     */
    protected $fillable = [
        'text',
        'post_id',
        'author_id'
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'text' => 'string',
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'created_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(
            '\App\Models\User\User',
            'author_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(
            '\App\Models\Team\TeamPost',
            'team_id',
            'id'
        );
    }
}
