<?php

namespace App\Models\Team;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class TeamUser
 * @package App\Models
 */
class TeamUser extends Pivot
{
    /**
     * @var string
     */
    public $table = 'team_user';
    /**
     * @var bool
     */
    public $timestamps = true;
}
