<?php

namespace App\Models\Checklist;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class Checklist
 * @property mixed entries
 * @property mixed happening_id
 * @property mixed team_id
 * @property int id
 * @package App\Models
 * @method static where(string $string, $id)
 * @method static whereHas(string $string, \Closure $param)
 */
class Checklist extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var string
     */
    public $table = 'checklists';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'type',
        'pattern',
        'execution_date',
        'author_id',
        'team_id',
        'happening_id'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'execution_date'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'type' => 'integer',
        'pattern' => 'boolean',
    ];

    /**
     * @param Builder $query
     * @param int $userId
     * @return mixed
     */
    public function scopeHasAssignments(Builder $query, int $userId) {
        return $query->entries()->whereHas('users', function(Builder $query) use ($userId) {
            return $query->where('checklist_entry_user.user_id', $userId);
        });
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopePatterns(Builder $query)
    {
        return $query->where('pattern', true);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(
            '\App\Models\User\User',
            'author_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team()
    {
        return $this->belongsTo(
            '\App\Models\Team\Team',
            'team_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function happening()
    {
        return $this->belongsTo(
            '\App\Models\Happening\Happening',
            'happening_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entries()
    {
        return $this->hasMany(
            '\App\Models\Checklist\ChecklistEntry',
            'checklist_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function myEntries()
    {
        return $this->entries()->whereHas('users', function($query) {
           return $query->where('checklist_entry_user.user_id', Auth::id());
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function managers()
    {
        $teamId = $this->team_id;
        $happeningId = $this->happening_id;
        return User::query()
            ->whereHas('happeningManager', function ($query) use ($happeningId) {
                return $query->where('happening_id', $happeningId);
            })
            ->when($teamId != null, function ($query) use ($teamId) {
                return $query->orWhereHas('teamManager', function($query) use ($teamId) {
                    return $query->where('team_id', $teamId);
                });
            })
            ->get();
    }
}
