<?php

namespace App\Models\Checklist;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ChecklistEntry
 * @property mixed name
 * @property mixed should_done_at
 * @property \App\Models\Checklist\Checklist checklist
 * @property mixed users
 * @package App\Models
 */
class ChecklistEntry extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var string
     */
    public $table = 'checklist_entries';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'desc',
        'should_done_at',
        'author_id',
        'checklist_id',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'desc' => 'string',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'should_done_at',
    ];

    public function resolveRouteBinding($value, $field = null)
    {
        return $this->where('id', $value)->with('checklist', 'users')->firstOrFail();
    }

    /**
     * @param Builder $query
     * @param int $userId
     * @return Builder
     */
    public function scopeIsAssignee(Builder $query, int $userId)
    {
        return $query->whereHas('users', function (Builder $query) use ($userId) {
            return $query->where('checklist_entry_user.user_id', $userId);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(
            '\App\Models\User\User',
            'author_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function checklist()
    {
        return $this->belongsTo(
            '\App\Models\Checklist\Checklist',
            'checklist_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(
            '\App\Models\User\User',
            'checklist_entry_user',
            'checklist_entry_id',
            'user_id'
        )
            ->withTimestamps()
            ->withPivot('done', 'notification_assigned')
            ->using('\App\Models\Checklist\ChecklistEntryUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function blocked()
    {
        return $this->belongsToMany(
            '\App\Models\ChecklistEntry',
            'checklist_entry_block',
            'blocking_id',
            'blocked_by_id'
        )
            ->withTimestamps()
            ->using('\App\Models\Checklist\ChecklistEntryBlocks');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function blocking()
    {
        return $this->belongsToMany(
            '\App\Models\ChecklistEntry',
            'checklist_entry_block',
            'blocked_by_id',
            'blocking_id'
        )
            ->withTimestamps()
            ->using('\App\Models\Checklist\ChecklistEntryBlocks');
    }
}
