<?php

namespace App\Models\Checklist;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class ChecklistEntryBlock
 * @package App\Models
 */
class ChecklistEntryBlock extends Pivot
{
    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var string
     */
    public $table = 'checklist_entry_block';
}
