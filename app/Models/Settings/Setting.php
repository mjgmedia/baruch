<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;

/**
 * Class Setting
 * @package App\Models
 * @method static create(int[]|string[] $array)
 * @method static where(string $string, array|int|string $key)
 * @method static find($id)
 * @method static whereIn(string $string, $ids)
 */
class Setting extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $casts = [
        'key' => 'string',
        'default_value' => 'boolean',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'key',
        'permission_id',
        'default_value',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(
            '\App\Models\User\User',
            'setting_user',
            'setting_id',
            'user_id'
        )
            ->withPivot('value')
            ->withTimestamps()
            ->using('App\Models\Settings\SettingUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function permission()
    {
        return $this->belongsTo(
            Permission::class,
            'permission_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(
            '\App\Models\Settings\SettingGroup',
            'group_id',
            'id'
        );
    }

    /**
     * @param Builder $query
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function scopeGroup(Builder $query)
    {
        return $query->whereNotNull('group_id');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeNoGroup(Builder $query)
    {
        return $query->whereNull('group_id');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopePermitted(Builder $query)
    {
        $arrIds = Auth::user()->getAllPermissions()->pluck('id')->toArray();
        return $query->whereIn('permission_id', $arrIds)->orWhereNull('permission_id');
    }
}
