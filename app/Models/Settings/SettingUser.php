<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class SettingUser
 * @package App\Models
 * @method static create(int[]|string[] $array)
 * @method static where(string $string, array|int|string $key)
 * @method static find($id)
 * @method static whereIn(string $string, $ids)
 */
class SettingUser extends Pivot
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $casts = [
        'value' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function setting()
    {
        return $this->belongsTo(
            '\App\Models\Settings\Setting',
            'setting_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
            '\App\Models\User\User',
            'user_id',
            'id'
        );
    }
}
