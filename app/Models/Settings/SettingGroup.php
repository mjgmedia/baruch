<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SettingGroup
 * @package App\Models
 * @method static create(int[]|string[] $array)
 * @method static where(string $string, array|int|string $key)
 * @method static find($id)
 * @method static whereIn(string $string, $ids)
 */
class SettingGroup extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * @var array
     */
    public $casts = [
        'key' => 'string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function settings()
    {
        return $this->hasMany(
            '\App\Models\Settings\Setting',
            'group_id',
            'id'
        );
    }
}
