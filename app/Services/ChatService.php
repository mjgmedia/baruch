<?php

namespace App\Services;

use App\Services\Interfaces\ChatServiceInterface;
use App\Traits\Chat\AddToChat;
use App\Traits\Chat\DeleteFromChat;

/**
 * Class ChatService
 * @package App\Services
 */
class ChatService implements ChatServiceInterface
{
    use AddToChat;
    use DeleteFromChat;

    /**
     * @param array $usersIds
     * @param int $channelId
     * @return array
     * @throws \App\Exceptions\Chat\ChannelNotFoundException
     * @throws \App\Exceptions\Chat\ChannelUserAttachingException
     * @throws \App\Exceptions\Chat\MessagesUsersAttachingException
     */
    public function addToChat(array $usersIds, int $channelId): array
    {
        $status = $this->_addToChannel($usersIds, $channelId);
        $status[] = $this->_addToMessages($usersIds, $channelId);
        return $status;
    }

    /**
     * @param array $usersIds
     * @param int $channelId
     * @return array
     * @throws \App\Exceptions\Chat\ChannelNotFoundException
     * @throws \App\Exceptions\Chat\ChannelUserDetachException
     */
    public function removeFromChat(array $usersIds, int $channelId): array
    {
        return $this->_deleteFromChat($channelId, $usersIds);
    }
}