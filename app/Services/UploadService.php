<?php

namespace App\Services;

use App\Services\Interfaces\UploadServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
 * Class UploadService
 * @package App\Services
 */
class UploadService implements UploadServiceInterface
{
    /**
     * @param Request $request
     * @param string $name
     * @param string $path
     * @param string $disk
     * @return string
     */
    public function handleImageUpload(Request $request, string $name, string $path, string $disk): string
    {
        $img = Image::make($request->file);
//        Save image to disk
        Storage::disk($disk)
            ->put($path . '/' . $name, $img->encode());

        return $path . '/' . $name;
    }

    /**
     * @param Request $request
     * @param string $name
     * @param string $path
     * @param string $disk
     * @return string
     */
    public function handleFileUpload(Request $request, string $name, string $path, string $disk): string
    {
        $path = Storage::disk($disk)
            ->putFileAs($path, $request->file, $name);

        return $path;
    }

    /**
     * @param string $path
     * @param string $disk
     * @return bool
     */
    public function deleteFile(string $path, string $disk): bool
    {
        return Storage::disk($disk)
            ->delete($path);
    }
}