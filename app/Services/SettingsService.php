<?php

namespace App\Services;

use App\Exceptions\Setting\WrongSettingException;
use App\Models\Settings\Setting;
use App\Models\Settings\SettingGroup;
use App\Models\User\User;
use App\Services\Interfaces\SettingsServiceInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Throwable;

/**
 * Class SettingsService
 * @package App\Services
 */
class SettingsService implements SettingsServiceInterface
{
    /**
     * @param User $user
     * @return bool
     */
    public function resetSettingsToDefault(User $user): bool
    {
        foreach (config('project.settings') as $settingKey => $settingDefault) {
            if (is_array($settingDefault)) {
                foreach ($settingDefault as $key => $value) {
                    $this->updateSetting($user, $key, $value);
                }
            } else {
                $this->updateSetting($user, $settingKey, $settingDefault);
            }
        }

        return true;
    }

    /**
     * @param User $user
     * @param string $key
     * @param bool $value
     * @return bool
     * @throws WrongSettingException
     */
    public function updateSetting(User $user, string $key, bool $value): bool
    {
        try {
            $setting = Setting::query()
                ->where('key', $key)
                ->with(
                    [
                        'users' => function ($query) use ($user) {
                            return $query->where('user_id', $user->id);
                        }
                    ]
                )
                ->firstOrFail();
            if ($this->checkPermission($user, $setting)) {
                $pivot = $setting->users->first();
                $pivot->pivot->value = $value;
                return $pivot->pivot->save();
            }
        } catch (ModelNotFoundException $e) {
            // create new if user has perms
            try {
                $setting = Setting::where('key', $key)->firstOrFail();
                if ($this->checkPermission($user, $value)) {
                    $setting->users()->attach($user, ['value' => $value]);
                    return $setting->save();
                }
            } catch (Throwable $exception) {
                throw new WrongSettingException();
            }
        }

        return false;
    }

    /**
     * @param User $user
     * @param Setting $setting
     * @return bool
     */
    private function checkPermission(User $user, Setting $setting): bool
    {
        if ($setting->permission) {
            if (!$user->hasPermissionTo($setting->permission->name)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param User $user
     */
    public function createSettingsEntries(User $user): void
    {
        foreach (config('project.settings') as $key => $value) {
            if (is_array($value)) {
                $group = SettingGroup::query()
                    ->where('key', $key)
                    ->firstOrFail();

                foreach ($value as $k => $v) {
                    $setting = Setting::query()
                        ->where(
                            [
                                ['key', '=', $k],
                                ['group_id', '=', $group->id],
                            ]
                        )
                        ->first();
                    
                    if ($this->checkPermission($user, $setting)) {
                        $setting->users()->attach($user, ['value' => $v]);
                        $setting->save();
                    }
                }
            } else {
                $setting = Setting::query()
                    ->where('key', '=', $key)
                    ->firstOrFail();
                if ($this->checkPermission($user, $setting)) {
                    $setting->users()->attach($user, ['value' => $value]);
                    $setting->save();
                }
            }
        }
    }

    /**
     * @param string $settingKey
     * @return bool
     */
    private function checkSettingExist(string $settingKey): bool
    {
        return in_array($settingKey, config('project.settings'));
    }
}