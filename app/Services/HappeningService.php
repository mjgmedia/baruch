<?php

namespace App\Services;

use App\Exceptions\Happening\NotAllowedToSeePostException;
use App\Exceptions\Happening\NotAMemberException;
use App\Exceptions\Team\ChatChannelDeleteException;
use App\Models\Chat\ChatChannel;
use App\Models\Happening\Happening;
use App\Models\User\User;
use App\Services\Interfaces\HappeningServiceInterface;
use App\Traits\Chat\AddToChat;
use App\Traits\Chat\DeleteFromChat;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;

/**
 * Class HappeningService
 * @package App\Services
 */
class HappeningService implements HappeningServiceInterface
{
    use DeleteFromChat;
    use AddToChat;

    /**
     * @param int $userId
     * @return mixed
     */
    public function getManagersForUser(int $userId): array
    {
        return Happening::query()
            ->isMember($userId)
            ->with('managers')
            ->get()
            ->pluck('managers.*.id')
            ->toArray();
    }

    /**
     * @param int $happeningId
     * @return bool|\Illuminate\Http\JsonResponse
     * @throws NotAMemberException
     */
    public function isHappeningMember(int $happeningId): bool
    {
        if (Gate::denies('is-happening-member', $happeningId)) {
            throw new NotAMemberException();
        }

        return true;
    }

    /**
     * @param int $happeningId
     * @return bool
     * @throws NotAllowedToSeePostException
     */
    public function canPost(int $happeningId): bool
    {
        if (Gate::denies('happening-post', $happeningId)) {
            throw new NotAllowedToSeePostException();
        }

        return true;
    }

    /**
     * @return Collection
     */
    public function getPermissions(int $happeningId): Collection
    {
        return Permission::query()
            ->where('name', 'like', 'happening%_'.$happeningId)
            ->get()
            ->pluck('name');
    }

    /**
     * @param Happening $happening
     */
    public function createPermissions(Happening $happening): void
    {
        $permissions = Permission::query()
            ->where('name', 'like', 'happening%_0')
            ->get();
        foreach ($permissions as $permission) {
            Permission::create(
                [
                    'name' => Str::substr($permission->name, 0, -2) . $happening->id,
                ]
            );
        }
    }

    /**
     * @param int $happeningId
     */
    public function removePermissions(int $happeningId): void
    {
        $permissions = Permission::query()
            ->where('name', 'like', 'happening%_'.$happeningId)
            ->get();;
        Permission::query()
            ->whereIn('id', $permissions->pluck('id'))
            ->delete();
    }

    /**
     * @param User $user
     * @param int $happeningId
     */
    public function grantAllPermissions(User $user, int $happeningId): void
    {
        $permissions = $this->getPermissions($happeningId);

        foreach ($permissions as $permission) {
            $user->givePermissionTo($permission);
        }
    }

    /**
     * @param Collection $users
     * @param int $happeningId
     */
    public function grantMemberPermissions(Collection $users, int $happeningId): void
    {
        $permissions = [
            'happening_user_list_'.$happeningId,
        ];

        foreach ($users as $user) {
            foreach($permissions as $permission) {
                $user->givePermissionTo($user, $permission);
            }
        }
    }

    /**
     * @param Collection $users
     * @param int $happeningId
     */
    public function grantManagerPermission(Collection $users, int $happeningId): void
    {
        $permissions = [
            'happening_access_always_'.$happeningId,
            'happening_user_list_'.$happeningId,
            'happening_user_crud_'.$happeningId,
            'happening_crud_'.$happeningId,
            'happening_checklist_entries_crud_'.$happeningId,
            'happening_checklist_entries_crud_users_'.$happeningId,
            'happening_checklist_entries_view_'.$happeningId,
            'happening_checklist_done_others_task_'.$happeningId,
            'happening_file_crud_'.$happeningId,
            'happening_file_comment_'.$happeningId,
            'happening_chat_crud_'.$happeningId,
        ];

        foreach ($users as $user) {
            foreach($permissions as $permission) {
                $user->givePermissionTo($user, $permission);
            }
        }
    }

    /**
     * @param Collection $users
     * @param int $happeningId
     */
    public function withdrawAllPermissions(Collection $users, int $happeningId): void
    {
        $permissions = $this->getPermissions($happeningId);

        foreach ($users as $user) {
            foreach ($permissions as $permission) {
                $user->revokePermissionTo($permission);
            }
        }
    }

    /**
     * @param Collection $users
     * @param array $permissionsIds
     * @param int $happeningId
     */
    public function updatePermissions(Collection $users, array $permissionsIds, int $happeningId): void
    {
        $permissions = Permission::query()
            ->whereIn('id', $permissionsIds)
            ->where('name', 'like', 'team_%_'.$happeningId)
            ->pluck('name');

        foreach ($users as $user) {
            $this->withdrawAllPermissions($user, $happeningId);

            foreach ($permissions as $permission) {
                $user->givePermissionTo($permission);
            }
        }
    }

    /**
     * @param int $happeningId
     * @throws ChatChannelDeleteException
     */
    public function deleteChatChannel(int $happeningId): void
    {
        $this->_deleteChatChannel($happeningId, 'happening_id');
    }

    /**
     * @param int $happeningId
     * @param int $userId
     * @throws \App\Exceptions\Team\ChatChannelUserDetachException
     * @throws \App\Exceptions\Chat\ChannelNotFoundException
     */
    public function deleteFromChat(Collection $users, int $happeningId): void
    {
        $this->_deleteFromChat($happeningId, $users->pluck('id')->toArray(), 'happening_id');
    }

    /**
     * @param Collection $users
     * @param int $happeningId
     * @throws \App\Exceptions\Chat\ChannelNotFoundException
     * @throws \App\Exceptions\Chat\ChannelUserAttachingException
     * @throws \App\Exceptions\Chat\MessagesUsersAttachingException
     */
    public function addToChat(Collection $users, int $happeningId): void
    {
        $usersIds = $users->pluck('id')->toArray();
        $this->_addToChannel($usersIds, $happeningId, 'team_id');
        $this->_addToMessages($usersIds, $happeningId, 'team_id');
    }
}