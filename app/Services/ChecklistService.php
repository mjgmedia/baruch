<?php

namespace App\Services;

use App\Exceptions\Checklist\NoAuthForChecklistCreating;
use App\Exceptions\Checklist\NoAuthForChecklistEntryCreating;
use App\Exceptions\Happening\NotAHappeningMember;
use App\Exceptions\Team\NotATeamMember;
use App\Models\Checklist\Checklist;
use App\Services\Interfaces\ChecklistServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

/**
 * Class ChecklistService
 * @package App\Services
 */
class ChecklistService implements ChecklistServiceInterface
{
    /**
     * @param int $happeningId
     * @param int|null $teamId
     * @return bool
     * @throws NoAuthForChecklistCreating
     * @throws NotAHappeningMember
     * @throws NotATeamMember
     */
    public function checkChecklist(int $happeningId, int $teamId = null)
    {
        if (Gate::denies('is-happening-member', $happeningId)) {
            throw new NotAHappeningMember();
        }

        if (!empty($teamId)) {
            if (Gate::denies('is-team-member', $teamId)) {
                throw new NotATeamMember();
            }
        }

        return true;
    }

    /**
     * @param int $happeningId
     * @param int|null $teamId
     * @return bool
     * @throws NoAuthForChecklistCreating
     * @throws NotAHappeningMember
     * @throws NotATeamMember
     */
    public function checkCreateChecklist(int $happeningId, int $teamId = null): bool
    {
        if ($this->checkChecklist($happeningId, $teamId)) {
            if (!empty($teamId) && !Auth::user()->hasPermissionTo('team_checklist_crud_'.$teamId)) {
                throw new NoAuthForChecklistCreating(Auth::user());
            }

            if (!Auth::user()->hasPermissionTo('happening_checklist_crud_'.$happeningId)) {
                throw new NoAuthForChecklistCreating(Auth::user());
            }
        }

        return true;
    }

    /**
     * @param int $checklistId
     * @return bool
     * @throws NoAuthForChecklistEntryCreating
     */
    public function canCreateEntry(int $checklistId)
    {
        // Waliadacja po autrostwie checklisty lub uprawnienie do tworzenie wpisów w każdej checkliście
        $checklist = Checklist::findOrFail($checklistId);

        if (Auth::id() === $checklist->author->id) {
            return true;
        }

        if (Auth::user()->hasPermissionTo('happening_checklist_entries_crud' . $checklist->happening_id)) {
            return true;
        }

        if ($checklist->team_id !== null && Auth::user()->hasPermissionTo(
                'team_checklist_entries_crud_' . $checklist->team_id
            )) {
            return true;
        }

        throw new NoAuthForChecklistEntryCreating();
    }
}