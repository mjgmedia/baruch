<?php

namespace App\Services\Interfaces;

/**
 * Interface ChecklistServiceInterface
 * @package App\Services\Interfaces
 */
interface ChecklistServiceInterface
{
    /**
     * @param int $happeningId
     * @param int|null $teamId
     * @return mixed
     */
    public function checkChecklist(int $happeningId, int $teamId = null);

    /**
     * @param int $happeningId
     * @param int|null $teamId
     * @return bool
     */
    public function checkCreateChecklist(int $happeningId, int $teamId = null): bool;

    /**
     * @param int $checklistId
     * @return mixed
     */
    public function canCreateEntry(int $checklistId);
}