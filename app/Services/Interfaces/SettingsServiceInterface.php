<?php

namespace App\Services\Interfaces;

use App\Models\User\User;

/**
 * Interface SettingsServiceInterface
 * @package App\Services\Interfaces
 */
interface SettingsServiceInterface
{
    /**
     * @param User $user
     * @param string $key
     * @param bool $value
     * @return bool
     */
    public function updateSetting(User $user, string $key, bool $value): bool;

    /**
     * @param User $user
     * @return bool
     */
    public function resetSettingsToDefault(User $user): bool;

    /**
     * @param User $user
     */
    public function createSettingsEntries(User $user): void;
}