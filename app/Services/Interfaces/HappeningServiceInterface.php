<?php

namespace App\Services\Interfaces;

use App\Models\Happening\Happening;
use App\Models\User\User;
use Illuminate\Support\Collection;

/**
 * Interface HappeningServiceInterface
 * @package App\Services\Interfaces
 */
interface HappeningServiceInterface
{
    /**
     * @param int $userId
     * @return array
     */
    public function getManagersForUser(int $userId): array;

    /**
     * @param int $happeningId
     * @return bool
     */
    public function isHappeningMember(int $happeningId): bool;

    /**
     * @param int $happeningId
     * @return bool
     */
    public function canPost(int $happeningId): bool;

    /**
     * @return Collection
     */
    public function getPermissions(): Collection;

    /**
     * @param Happening $happening
     */
    public function createPermissions(Happening $happening): void;

    /**
     * @param int $happeningId
     */
    public function removePermissions(int $happeningId): void;

    /**
     * @param User $user
     * @param int $happeningId
     */
    public function grantAllPermissions(User $user, int $happeningId): void;

    /**
     * @param Collection $users
     * @param int $happeningId
     */
    public function grantMemberPermissions(Collection $users, int $happeningId): void;

    /**
     * @param Collection $users
     * @param array $permissions
     * @param int $happeningId
     */
    public function grantManagerPermission(Collection $users, int $happeningId): void;

    /**
     * @param Collection $users
     * @param int $happeningId
     */
    public function withdrawAllPermissions(Collection $users, int $happeningId): void;

    /**
     * @param Collection $users
     * @param array $permissionsIds
     * @param int $happeningId
     */
    public function updatePermissions(Collection $users, array $permissionsIds, int $happeningId): void;

    /**
     * @param int $happeningId
     */
    public function deleteChatChannel(int $happeningId): void;

    /**
     * @param Collection $users
     * @param int $happeningId
     */
    public function deleteFromChat(Collection $users, int $happeningId): void;

    /**
     * @param Collection $users
     * @param int $happeningId
     */
    public function addToChat(Collection $users, int $happeningId): void;
}