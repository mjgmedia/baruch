<?php

namespace App\Services\Interfaces;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface FileServiceInterface
 * @package App\Services\Interfaces
 */
interface FileServiceInterface
{
    /**
     * @param int $fileId
     * @return array
     */
    public function getUsersWithAccess(int $fileId): array;

    /**
     * @param int $userId
     * @return array
     */
    public function getAllManagersForUser(int $userId): array;

    /**
     * @param Model $model
     * @return bool
     */
    public function isAllowedToCrud(Model $model): bool;

    /**
     * @param int $fileId
     * @return bool
     */
    public function isAllowedToComment(int $fileId): bool;

    /**
     * @param int $id
     * @param bool $isTeam
     * @return bool
     */
    public function isAllowedToCategory(int $id, bool $isTeam = false): bool;
}