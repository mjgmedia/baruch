<?php

namespace App\Services\Interfaces;

/**
 * Interface ChatServiceInterface
 * @package App\Services\Interfaces
 */
interface ChatServiceInterface
{
    /**
     * @param array $usersIds
     * @param int $channelId
     * @return array
     */
    public function addToChat(array $usersIds, int $channelId): array;

    /**
     * @param array $usersIds
     * @param int $channelId
     * @return array
     */
    public function removeFromChat(array $usersIds, int $channelId): array;
}