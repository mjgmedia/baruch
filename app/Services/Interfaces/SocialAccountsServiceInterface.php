<?php

namespace App\Services\Interfaces;

use App\Models\User\User;
use Laravel\Socialite\Two\User as ProviderUser;

/**
 * Interface SocialAccountsServiceInterface
 * @package App\Services\Interfaces
 */
interface SocialAccountsServiceInterface
{
    /**
     * @param ProviderUser $providerUser
     * @param string $provider
     * @return User
     */
    public function findOrCreate(ProviderUser $providerUser, string $provider): User;
}