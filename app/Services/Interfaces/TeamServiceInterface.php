<?php

namespace App\Services\Interfaces;

use App\Models\Team\Team;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface TeamServiceInterface
 * @package App\Services\Interfaces
 */
interface TeamServiceInterface
{
    /**
     * @param int $userId
     * @return array
     */
    public function getManagersForUser(int $userId): array;

    /**
     * @param int $teamId
     * @return mixed
     */
    public function isTeamMember(int $teamId);

    /**
     * @param int $teamId
     * @return mixed
     */
    public function canPost(int $teamId);

    /**
     * @param int $teamId
     * @return Collection
     */
    public function getPermissions(int $teamId): Collection;

    /**
     * @param int $teamId
     */
    public function createPermissions(int $teamId): void;

    /**
     * @param int $teamId
     */
    public function deletePermissions(int $teamId): void;

    /**
     * @param Collection $users
     * @param int $teamId
     */
    public function grantMemberPermission(Collection $users, int $teamId): void;

    /**
     * @param Collection $users
     * @param int $teamId
     */
    public function grantManagerPermission(Collection $users, int $teamId): void;

    /**
     * @param User $user
     * @param int $teamId
     */
    public function grantAllPermission(User $user, int $teamId): void;

    /**
     * @param Collection $users
     * @param array $permissions
     * @param int $teamId
     */
    public function updatePermissions(Collection $users, array $permissions, int $teamId): void;

    /**
     * @param Collection $users
     * @param int $teamId
     */
    public function revokeAllPermissions(Collection $users, int $teamId): void;

    /**
     * @param int $teamId
     */
    public function deleteChatChannel(int $teamId): void;

    /**
     * @param Collection $users
     * @param int $teamId
     */
    public function deleteFromChat(Collection $users, int $teamId): void;

    /**
     * @param Collection $users
     * @param int $teamId
     */
    public function addToChat(Collection $users, int $teamId): void;
}