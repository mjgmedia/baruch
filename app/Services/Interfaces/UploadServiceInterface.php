<?php

namespace App\Services\Interfaces;

use Illuminate\Http\Request;

/**
 * Interface UploadServiceInterface
 * @package App\Services\Interfaces
 */
interface UploadServiceInterface
{
    /**
     * @param Request $request
     * @param string $name
     * @param string $path
     * @param string $disk
     * @return string
     */
    public function handleImageUpload(Request $request, string $name, string $path, string $disk): string;

    /**
     * @param Request $request
     * @param string $name
     * @param string $path
     * @param string $disk
     * @return string
     */
    public function handleFileUpload(Request $request, string $name, string $path, string $disk): string;

    /**
     * @param string $path
     * @param string $disk
     * @return bool
     */
    public function deleteFile(string $path, string $disk): bool;
}