<?php

namespace App\Services;

use App\Exceptions\Team\ChatChannelDeleteException;
use App\Models\Chat\ChatChannel;
use App\Models\Team\Team;
use App\Models\User\User;
use App\Services\Interfaces\TeamServiceInterface;
use App\Traits\Chat\AddToChat;
use App\Traits\Chat\DeleteFromChat;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;

/**
 * Class TeamService
 * @package App\Services
 */
class TeamService implements TeamServiceInterface
{
    use DeleteFromChat;
    use AddToChat;

    /**
     * @param int $userId
     * @return array
     */
    public function getManagersForUser(int $userId): array
    {
        return Team::query()
            ->isMember($userId)
            ->with('managers')
            ->get()
            ->pluck('managers.*.id')
            ->toArray();
    }

    /**
     * @param int $teamId
     * @return bool|\Illuminate\Http\JsonResponse|mixed
     */
    public function isTeamMember(int $teamId)
    {
        if (Gate::denies('is-team-member', $teamId)) {
            return response()->json(
                [
                    'status' => __('')
                ],
                403
            );
        }

        return true;
    }

    /**
     * @param int $teamId
     * @return bool|\Illuminate\Http\JsonResponse|mixed
     */
    public function canPost(int $teamId)
    {
        if (Gate::denies('team-post', $teamId)) {
            return response()->json(
                [
                    'status' => __('')
                ],
                403
            );
        }

        return true;
    }

    /**
     * @param int $teamId
     * @return Collection|\Illuminate\Support\Collection
     */
    public function getPermissions(int $teamId): Collection
    {
        return Permission::query()
            ->where('name', 'like', 'team%_'.$teamId)
            ->get()
            ->pluck('name');
    }

    /**
     * @param int $teamId
     */
    public function createPermissions(int $teamId): void
    {
        $permissions = Permission::query()
            ->where('name', 'like', 'team%_0')
            ->get();
        foreach ($permissions as $permission) {
            Permission::create(
                [
                    'name' => Str::substr($permission->name, 0, -2) . $teamId,
                ]
            );
        }
    }

    /**
     * @param int $teamId
     */
    public function deletePermissions(int $teamId): void
    {
        $permissions = $this->getPermissions($teamId);
        Permission::query()
            ->whereIn('id', $permissions->pluck('id'))
            ->delete();
    }

    /**
     * @param Collection $users
     * @param int $teamId
     */
    public function grantMemberPermission(Collection $users, int $teamId): void
    {
        $permissions = [
            'team_users_lookup_'.$teamId,
            'team_checklist_entries_view_'.$teamId,
        ];

        foreach($users as $user) {
            foreach ($permissions as $permission) {
                $user->givePermissionTo($permission);
            }
        }
    }

    /**
     * @param Collection $users
     * @param int $teamId
     */
    public function grantManagerPermission(Collection $users, int $teamId): void
    {
        $permissions = [
            'team_users_lookup_'.$teamId,
            'team_crud_'.$teamId,
            'team_post_'.$teamId,
            'team_comments_'.$teamId,
            'team_managers_notifications_'.$teamId,
            'team_checklist_entries_crud_'.$teamId,
            'team_checklist_entries_crud_users_'.$teamId,
            'team_checklist_entries_view_'.$teamId,
            'team_checklist_done_others_task_'.$teamId,
            'team_file_crud_'.$teamId,
            'team_file_comment_'.$teamId,
            'team_chat_crud_'.$teamId
        ];

        foreach($users as $user) {
            foreach ($permissions as $permission) {
                $user->givePermissionTo($permission);
            }
        }
    }

    /**
     * @param User $user
     * @param int $teamId
     */
    public function grantAllPermission(User $user, int $teamId): void
    {
        $permissions = $this->getPermissions($teamId);

        foreach ($permissions as $permission) {
            $user->givePermissionTo($permission);
        }
    }

    /**
     * @param Collection $users
     * @param array $permissions
     * @param int $teamId
     */
    public function updatePermissions(Collection $users, array $permissions, int $teamId): void
    {
        $roles = Permission::query()
            ->whereIn('name', $permissions)
            ->where('name', 'like', 'team%_'.$teamId)
            ->get();

        foreach ($users as $user) {
            $this->revokeAllPermissions($user, $teamId);
            foreach ($permissions as $permission) {
                $user->givePermissionTo($permission);
            }
        }
    }

    /**
     * @param Collection $users
     * @param int $teamId
     */
    public function revokeAllPermissions(Collection $users, int $teamId): void
    {
        $permissions = $this->getPermissions($teamId);

        foreach ($users as $user) {
            foreach ($permissions as $permission) {
                $user->revokePermissionTo($permission);
            }
        }
    }

    /**
     * @param int $teamId
     * @throws ChatChannelDeleteException
     * @throws \App\Exceptions\Chat\ChannelNotFoundException
     */
    public function deleteChatChannel(int $teamId): void
    {
        $this->_deleteChatChannel($teamId, 'team_id');
    }

    /**
     * @param Collection $users
     * @param int $teamId
     * @throws \App\Exceptions\Chat\ChannelNotFoundException
     * @throws \App\Exceptions\Team\ChatChannelUserDetachException
     */
    public function deleteFromChat(Collection $users, int $teamId): void
    {
        $this->_deleteFromChat($teamId, $users->pluck('id')->toArray(), 'team_id');
    }

    /**
     * @param Collection $users
     * @param int $teamId
     * @throws \App\Exceptions\Chat\ChannelNotFoundException
     * @throws \App\Exceptions\Chat\ChannelUserAttachingException
     * @throws \App\Exceptions\Chat\MessagesUsersAttachingException
     */
    public function addToChat(Collection $users, int $teamId): void
    {
        $usersIds = $users->pluck('id')->toArray();
        $this->_addToChannel($usersIds, $teamId, 'team_id');
        $this->_addToMessages($usersIds, $teamId, 'team_id');
    }
}