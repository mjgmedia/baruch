<?php

namespace App\Services;

use App\Exceptions\File\GettingUsersWithAccessException;
use App\Exceptions\File\NoPermissionForCategoryException;
use App\Models\File\File;
use App\Models\File\FileAccess;
use App\Models\Happening\Happening;
use App\Models\Team\Team;
use App\Models\User\User;
use App\Services\Interfaces\FileServiceInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

/**
 * Class FileService
 * @package App\Services
 */
class FileService implements FileServiceInterface
{
    /**
     * @param int $fileId
     * @return array
     * @throws GettingUsersWithAccessException
     */
    public function getUsersWithAccess(int $fileId): array
    {
        try {
            $fileAccessList = FileAccess::query()
                ->where('file_id', $fileId)
                ->get();

            $arrUsers = collect();

            foreach ($fileAccessList as $fileAccess) {
                if ($fileAccess->user) {
                    $arrUsers->push($fileAccess->user->id);
                }
                if ($fileAccess->team) {
                    $arrUsers->merge($fileAccess->team->users->pluck('id'));
                }
                if ($fileAccess->happening) {
                    $arrUsers->merge($fileAccess->happengin->users->pluck('id'));
                }
                if ($fileAccess->role) {
                    $arrUsers->merge(
                        User::role($fileAccess->role->name)
                            ->get()
                            ->pluck('id')
                    );
                }
            }

            return $arrUsers->toArray();
        } catch (\Throwable $exception) {
            throw new GettingUsersWithAccessException($exception);
        }
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getAllManagersForUser(int $userId): array
    {
        $teamManagers = Team::query()
            ->isMember($userId)
            ->with('managers')
            ->get()
            ->pluck('managers.*.id');
        $happeningManagers = Happening::query()
            ->isMember($userId)
            ->with('managers')
            ->get()
            ->pluck('managers.*.id');

        return $teamManagers->merge($happeningManagers)->unique()->toArray();
    }

    /**
     * @param Model $model
     * @return bool
     */
    public function isAllowedToCrud(Model $model): bool
    {
        $perm = Model::class === Team::class ? 'team_file_crud_'.$model->id : 'happening_file_crud'.$model->id;

        if (Auth::user()->hasPermissionTo($perm)) {
            return true;
        }

        return false;
    }

    /**
     * @param int $fileId
     * @return bool
     */
    public function isAllowedToComment(int $fileId): bool
    {
        $file = File::findOrFail($fileId);
        $perm = $file->team_id != null ? 'team_file_comment_'.$file->team_id : 'happening_file_comment_'.$file->happening_id;

        return Auth::user()->hasPermissionTo($perm);
    }

    /**
     * @param int $id
     * @param bool $isTeam
     * @return bool
     * @throws NoPermissionForCategoryException
     */
    public function isAllowedToCategory(int $id, bool $isTeam = false): bool
    {
        $model = $isTeam ? Team::findOrFail($id) : Happening::findOrFail($id);

        if(Gate::denies($isTeam === true ? 'is-team-member' : 'is-happening-member', $model->id)) {
            throw new NoPermissionForCategoryException(Auth::user());
        }
    }
}