<?php

namespace App\Services;

use App\Models\User\SocialAccount;
use App\Models\User\User;
use App\Services\Interfaces\SocialAccountsServiceInterface;
use Illuminate\Support\Str;
use Laravel\Socialite\Two\User as ProviderUser;

/**
 * Class SocialAccountsService
 * @package App\Services
 */
class SocialAccountsService implements SocialAccountsServiceInterface
{
    /**
     * Find or create user instance by provider user instance and provider name.
     *
     * @param ProviderUser $providerUser
     * @param string $provider
     *
     * @return User
     */
    public function findOrCreate(ProviderUser $providerUser, string $provider): User
    {
        // check if there is social account for this provider and his user_id
        $linkedSocialAccount = SocialAccount::query()
            ->where(
                [
                    ['provider', '=', $provider],
                    ['provider_user_id', '=', $providerUser->getId()]
                ]
            )
            ->first();

        if ($linkedSocialAccount) {
            // Social Account exists in our DB
            return $linkedSocialAccount->user;
        } else {
            // User did not found, so let's create a new one
            $user = null;
            // At first check email - maybe user exists but now he logging via social
            if ($email = $providerUser->getEmail()) {
                $user = User::query()
                    ->where('email', $email)
                    ->first();
            }
            // if user not exists in DB - create a new one

            $usernameFound = false;
            $username = Str::slug($providerUser->getName());

            while ($usernameFound === false) {
                if (User::query()
                        ->where('username', '=', $username)
                        ->count() <= 0) {
                    $usernameFound = true;
                } else {
                    $username = $username . Str::random(2);
                }
            }

            if (!$user) {
                $user = User::create(
                    [
                        'username' => $username,
                        'name' => $providerUser->getName(),
                        'email' => $providerUser->getEmail(),
                    ]
                );
            }
            // and link this social account for him (in both cases)
            $user->socialAccounts()->create(
                [
                    'provider_user_id' => $providerUser->getId(),
                    'provider' => $provider,
                ]
            );
            return $user;
        }
    }
}