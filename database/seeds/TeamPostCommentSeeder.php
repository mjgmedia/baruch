<?php

use Illuminate\Database\Seeder;
use Tests\Factories\TeamPostCommentFactory;

/**
 * Class TeamPostCommentSeeder
 */
class TeamPostCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TeamPostCommentFactory::new()
            ->times(10)
            ->create();
    }
}
