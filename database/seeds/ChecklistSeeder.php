<?php

use Illuminate\Database\Seeder;
use Tests\Factories\ChecklistFactory;

/**
 * Class ChecklistSeeder
 */
class ChecklistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ChecklistFactory::new()
            ->times(5)
            ->create();
    }
}
