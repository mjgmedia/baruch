<?php

use App\Models\User\User;
use App\Models\User\Profile;
use Illuminate\Database\Seeder;

/**
 * Class AdminSeeder
 */
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // mateusz
        $user = User::create(
            [
                'id' => 1,
                'name' => config('project.admin.name'),
                'email' => config('project.admin.email'),
                'password' => config('project.admin.password'),
                'username' => config('project.admin.username'),
                'last_activity' => now(),
                'email_verified_at' => now()
            ]
        );

        $user->assignRole('superadmin');
        $profile = factory(Profile::class, 1)->create()->first();
        $user->profile()->save($profile);
        $user->save();

        // Adam
        $user = User::create(
            [
                'id' => 2,
                'name' => 'Mateusz',
                'email' => 'mg@local',
                'password' => 'mg',
                'username' => 'mg',   //minimum 8 length (frontend checks it and does not pass)
                'last_activity' => now(),
                'email_verified_at' => now()
            ]
        );

        $user->assignRole('superadmin');
        $profile = factory(Profile::class, 2)->create()->first();
        $user->profile()->save($profile);
        $user->save();
    }
}
