<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

/**
 * Class RolesAndPermissionsSeeder
 */
class RolesAndPermissionsSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        Permission::create(['name' => 'crud_users']);
        Permission::create(['name' => 'activate_user']);
        Permission::create(['name' => 'deactivate_user']);
        Permission::create(['name' => 'mass_mail']);
        // Notifications about new users etc.
        Permission::create(['name' => 'notifications_user_created']);
        Permission::create(['name' => 'notifications_oauth_error']);

        /* Total roles & permissions control */
        Permission::create(['name' => 'manage_roles']);
        Permission::create(['name' => 'manage_permissions']);

        Permission::create(['name' => 'chat_channel_create']);

//        Teams permissions
        Permission::create(['name' => 'create_teams']);

        Permission::create(['name' => 'team_users_lookup_0']);
        Permission::create(['name' => 'team_users_crud_0']);
        Permission::create(['name' => 'team_crud_0']);
        Permission::create(['name' => 'team_post_0']);
        Permission::create(['name' => 'team_comments_0']);
        Permission::create(['name' => 'team_managers_crud_0']);
        Permission::create(['name' => 'team_managers_notifications_0']);
        Permission::create(['name' => 'team_checklist_crud_0']);
        Permission::create(['name' => 'team_checklist_entries_crud_0']);
        Permission::create(['name' => 'team_checklist_entries_crud_users_0']);
        Permission::create(['name' => 'team_checklist_entries_view_0']);
        Permission::create(['name' => 'team_checklist_done_others_task_0']);
        Permission::create(['name' => 'team_file_crud_0']);
        Permission::create(['name' => 'team_file_category_crud_0']);
        Permission::create(['name' => 'team_file_comment_0']);
        Permission::create(['name' => 'team_chat_crud_0']);

        // Files permission
        Permission::create(['name' => 'file_global_list']);
        Permission::create(['name' => 'file_global_comment']);
        Permission::create(['name' => 'file_global_crud']);

        // Happening
        Permission::create(['name' => 'create_happenings']);

        Permission::create(['name' => 'happening_access_always_0']);
        Permission::create(['name' => 'happening_user_list_0']);
        Permission::create(['name' => 'happening_crud_0']);
        Permission::create(['name' => 'happening_user_crud_0']);
        Permission::create(['name' => 'happening_display_all_0']);
        Permission::create(['name' => 'happening_post_0']);
        Permission::create(['name' => 'happening_comments_0']);
        Permission::create(['name' => 'happening_managers_crud_0']);
        Permission::create(['name' => 'happening_checklist_crud_0']);
        Permission::create(['name' => 'happening_checklist_entries_crud_0']);
        Permission::create(['name' => 'happening_checklist_entries_crud_users_0']);
        Permission::create(['name' => 'happening_checklist_entries_view_0']);
        Permission::create(['name' => 'happening_checklist_done_others_task_0']);
        Permission::create(['name' => 'happening_file_crud_0']);
        Permission::create(['name' => 'happening_file_category_crud_0']);
        Permission::create(['name' => 'happening_file_comment_0']);
        Permission::create(['name' => 'happening_chat_crud_0']);

        // Checklist
        Permission::create(['name' => 'checklist_entries_view']);
        Permission::create(['name' => 'checklist_done_others_task']);
        Permission::create(['name' => 'checklist_global_view']);
        Permission::create(['name' => 'checklist_patterns']);

        Permission::create(['name' => 'access_all']);

        // Telescope & Horizon
        Permission::create(['name' => 'horizon']);
        Permission::create(['name' => 'telescope']);

        $role = Role::create(['name' => 'superadmin']);


        $role = Role::create(['name' => 'user']);

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo('crud_users');
        $role->givePermissionTo('activate_user');
        $role->givePermissionTo('deactivate_user');
        $role->givePermissionTo('notifications_user_created');

        $role = Role::create(['name' => 'devs']);
        $role->givePermissionTo('notifications_oauth_error');
        $role->givePermissionTo('telescope');
        $role->givePermissionTo('horizon');
    }
}
