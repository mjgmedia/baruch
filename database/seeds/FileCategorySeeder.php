<?php

use Illuminate\Database\Seeder;
use Tests\Factories\FileCategoryFactory;

/**
 * Class FileCategorySeeder
 */
class FileCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FileCategoryFactory::new()
            ->times(5)
            ->create();
    }
}
