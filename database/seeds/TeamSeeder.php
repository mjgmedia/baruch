<?php

use Illuminate\Database\Seeder;
use Tests\Factories\TeamFactory;

/**
 * Class TeamSeeder
 */
class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TeamFactory::new()
            ->times(5)
            ->create();
    }
}
