<?php

use App\Models\User\User;
use App\Models\User\Profile;
use Illuminate\Database\Seeder;
use Tests\Factories\UserFactory;

/**
 * Class DummyUserSeeder
 */
class DummyUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = UserFactory::new()
            ->times(2)
            ->create();

        $users->each(fn ($user) => $user->assignRole('user'));
    }
}
