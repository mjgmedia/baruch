<?php

use App\Models\Settings\SettingGroup;
use Illuminate\Database\Seeder;

/**
 * Class SettingGroupSeeder
 */
class SettingGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (config('project.settings') as $key => $value) {
            if (is_array($value)) {
                $group = SettingGroup::create(
                    [
                        'key' => $key,
                    ]
                );

                $group->save();
            }
        }
    }
}
