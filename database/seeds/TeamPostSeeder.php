<?php

use Illuminate\Database\Seeder;
use Tests\Factories\TeamPostFactory;

/**
 * Class TeamPostSeeder
 */
class TeamPostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TeamPostFactory::new()
            ->times(5)
            ->create();
    }
}
