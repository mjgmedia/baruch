<?php

use Illuminate\Database\Seeder;
use Tests\Factories\HappeningPostFactory;

/**
 * Class HappeningPostSeeder
 */
class HappeningPostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HappeningPostFactory::new()
            ->times(10)
            ->create();
    }
}
