<?php

use Illuminate\Database\Seeder;
use Tests\Factories\FileFactory;

/**
 * Class FileSeeder
 */
class FileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FileFactory::new()
            ->times(10)
            ->create();
    }
}
