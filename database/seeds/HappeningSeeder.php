<?php

use Illuminate\Database\Seeder;
use Tests\Factories\HappeningFactory;

class HappeningSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HappeningFactory::new()
            ->times(10)
            ->create();
    }
}
