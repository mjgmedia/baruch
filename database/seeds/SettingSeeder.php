<?php

use App\Models\Settings\Setting;
use App\Models\Settings\SettingGroup;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

/**
 * Class SettingSeeder
 */
class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (config('project.settings') as $key => $value) {
            if (is_array($value)) {
                $group = SettingGroup::where('key', $key)->first();

                foreach ($value as $k => $v) {
                    $setting = Setting::create(
                        [
                            'key' => $k,
                            'default_value' => $v,
                            'group_id' => $group->id,
                        ]
                    );

                    $this->setPermission($setting, $k);
                    $setting->save();
                }
            } else {
                $setting = Setting::create(
                    [
                        'key' => $key,
                        'default_value' => $value,
                    ]
                );
                $this->setPermission($setting, $key);
                $setting->save();
            }
        }
    }

    private function setPermission(Setting $setting, string $key)
    {
        if (in_array($key, config('project.settings_permissions'))) {
            $permission = Permission::where(
                'name',
                array_search($key, config('project.settings_permissions'))
            )->first();
            $setting->permission()->associate($permission);
        }
    }
}
