<?php

use Illuminate\Database\Seeder;
use Tests\Factories\ChecklistEntryFactory;

/**
 * Class ChecklistEntrySeeder
 */
class ChecklistEntrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ChecklistEntryFactory::new()
            ->times(25)
            ->create();
    }
}
