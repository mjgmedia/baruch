<?php

use Illuminate\Database\Seeder;
use Tests\Factories\CongregationFactory;

class CongregationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CongregationFactory::new()
            ->times(5)
            ->create();
    }
}
