<?php

use Illuminate\Database\Seeder;
use Tests\Factories\FileCommentFactory;

/**
 * Class FileCommentSeeder
 */
class FileCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FileCommentFactory::new()
            ->times(20)
            ->create();
    }
}
