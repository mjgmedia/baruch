<?php

use App\Models\Settings\Setting;
use App\Models\User\User;
use Illuminate\Database\Seeder;

/**
 * Class SettingUserSeeder
 */
class SettingUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $settings = Setting::all();

        foreach ($settings as $setting) {
            foreach ($users as $user) {
                $setting->users()->attach(
                    $user,
                    ['value' => $setting->default_value]
                );
            }
        }
    }
}
