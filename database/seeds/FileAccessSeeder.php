<?php

use Illuminate\Database\Seeder;
use Tests\Factories\FileAccessFactory;

/**
 * Class FileAccessSeeder
 */
class FileAccessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FileAccessFactory::new()
            ->times(50)
            ->create();
    }
}
