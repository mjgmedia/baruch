<?php

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->passportInstall();
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(SettingGroupSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(SettingUserSeeder::class);

        if (App::environment(['testing', 'staging'])) {
            // This Seeder Would Not Be Run on Production
            $this->call(DummyUserSeeder::class);
        }
    }

    /**
     *
     */
    private function passportInstall()
    {
        \Artisan::call(
            'passport:keys',
            [
                '--force' => true,
                '-n' => true
            ]
        );
        \Artisan::call(
            'passport:client',
            [
                '--password' => true,
                '-n' => true
            ]
        );
    }
}
