<?php

use Illuminate\Database\Seeder;
use Tests\Factories\HappeningPostCommentFactory;

/**
 * Class HappeningPostCommentSeeder
 */
class HappeningPostCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HappeningPostCommentFactory::new()
            ->times(10)
            ->create();
    }
}
