<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateFileAccessesTable
 */
class CreateFileAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_accesses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('file_id')->constrained()->onDelete('CASCADE');
            $table->foreignId('user_id')->nullable()->constrained()->onDelete('CASCADE');
            $table->foreignId('team_id')->nullable()->constrained()->onDelete('CASCADE');
            $table->foreignId('happening_id')->nullable()->constrained()->onDelete('CASCADE');
            $table->foreignId('role_id')->nullable()->constrained()->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_accesses');
    }
}
