<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateSettingsTable
 */
class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key')->unique();
            $table->foreignId('permission_id')->nullable();
//            $table->foreign('permission_id')->references('id')->on(config('permission.table_names'))->onDelete('SET NULL');
            $table->foreignId('group_id')->nullable();
            $table->foreign('group_id')->references('id')->on('setting_groups')->onDelete('SET NULL');
            $table->boolean('default_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
