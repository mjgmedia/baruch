<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateChecklistEntriesTable
 */
class CreateChecklistEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklist_entries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('desc');
            $table->dateTime('should_done_at')->index();
            $table->foreignId('author_id')->nullable();
            $table->foreign('author_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->foreignId('checklist_id')->constrained()->onDelete('CASCADE');
            $table->boolean('notification_five')->default(false);
            $table->boolean('notification_overdue')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checklist_entries');
    }
}
