<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateHappeningPostCommentsTable
 */
class CreateHappeningPostCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('happening_post_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('author_id');
            $table->foreign('author_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->foreignId('post_id');
            $table->foreign('post_id')->references('id')->on('happening_posts')->onDelete('CASCADE');
            $table->text('text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('happening_post_comments');
    }
}
