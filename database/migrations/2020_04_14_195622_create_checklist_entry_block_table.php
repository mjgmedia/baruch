<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateChecklistEntryBlockTable
 */
class CreateChecklistEntryBlockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklist_entry_block', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('blocking_id');
            $table->foreign('blocking_id')->references('id')->on('checklist_entries')->onDelete('CASCADE');
            $table->foreignId('blocked_by_id');
            $table->foreign('blocked_by_id')->references('id')->on('checklist_entries')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checklist_entry_block');
    }
}
