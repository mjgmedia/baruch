<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateProfilesTable
 */
class CreateProfilesTable extends Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('user_id')->nullable()->constrained()->onDelete('CASCADE');
            $table->string('phone')->nullable();
            $table->boolean('elder')->nullable();
            $table->boolean('servant')->nullable();
            $table->boolean('pioneer')->nullable();
            $table->string('email_jwpub')->nullable();
            $table->foreignId('congregation_id')->nullable();
            $table->foreign('congregation_id')->references('id')->on('congregations')->onDelete('SET NULL');
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('postal_code')->nullable();
            $table->timestamps();
        });
    }
}
