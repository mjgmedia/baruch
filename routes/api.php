<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

require base_path('routes/api/auth.php');

/**
 * USERS MANAGEMENT
 */
Route::prefix('admin')
    ->name('admin.')
    ->middleware(['auth:api'])
    ->group(
        function () {
            require base_path('routes/api/admin/users.php');
        }
    );

require base_path('routes/api/user.php');
require base_path('routes/api/others.php');
require base_path('routes/api/notifications.php');
require base_path('routes/api/checklist.php');
require base_path('routes/api/team.php');
require base_path('routes/api/file.php');
require base_path('routes/api/chat.php');
