<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * AUTH SECTION
 */
Route::name('auth.')
    ->prefix('auth')
    ->group(
        function () {
            /**
             * Basic Auth routes - with no auth middleware checking
             */
            Route::post('/register', 'Auth\RegisterController@register')
                ->name('register');
            Route::post('/activate', 'Auth\RegisterController@activate')
                ->name('activate');
            Route::post('/login', 'Auth\LoginController@login')
                ->name('login');
            Route::post('/refresh', 'Auth\LoginController@refresh')
                ->name('refresh');
            Route::post('/social/{provider}/{token}', 'Auth\SocialAuthController@socialAuth')
                ->name('social');
            Route::post('/send_reset_email', 'Auth\ForgotPasswordController@sendResetEmail')
                ->name('forgotpassword');
            Route::post('/reset_password', 'Auth\ResetPasswordController@resetPassword')
                ->name('resetpassword');

            /**
             * Basic Auth routes - those with auth middleware
             */
            Route::middleware(['auth:api'])
                ->group(
                    function () {
                        //? Router For Logout
                        Route::post('/logout', 'Auth\LoginController@logout')
                            ->name('logout');
                        //? Router Check For Auth User
                        Route::post('/check', 'Auth\LoginController@check')
                            ->name('check');

                        Route::get('/permissions', 'Auth\Acl\GetController@getAllPermissions')
                            ->name('permissions');
                        Route::get('/roles', 'Auth\Acl\GetController@getRoles')
                            ->name('roles');
                    }
                );
        }
    );