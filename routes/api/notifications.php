<?php

/**
 * NOTIFICATIONS BLOCK
 */
Route::prefix('notifications')
    ->name('notifications.')
    ->group(
        function () {
            /**
             * PUSH NOTIFICATIONS
             */
            Route::prefix('push')
                ->name('push.')
                ->group(
                    function () {
                        Route::post('/update', 'Notification\PushController@update')
                            ->name('update');

                        Route::post('/delete', 'Notification\PushController@delete')
                            ->name('delete');

                        Route::post('/dismiss/{id}', 'Notification\PushController@dismiss')
                            ->name('dismiss');
                    }
                );
        }
    );
