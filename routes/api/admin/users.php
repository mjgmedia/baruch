<?php

Route::prefix('users')
    ->name('users.')
    ->group(
        function () {
            // BASIC CRUD ACTIONS
            Route::name('crud.')
                ->group(
                    function () {
                        Route::get('/', 'Admin\Users\CrudController@index')
                            ->name('index');
                        Route::post('/create', 'Admin\Users\CrudController@create')
                            ->name('create')
                            ->middleware('permission:crud_users');
                        Route::get('/edit/{user}', 'Admin\Users\CrudController@edit')
                            ->name('edit')
                            ->middleware('permission:crud_users');
                        Route::post('/update/{user}', 'Admin\Users\CrudController@update')
                            ->name('update')
                            ->middleware('permission:crud_users');
                        Route::post('/delete', 'Admin\Users\CrudController@delete')
                            ->name('delete')
                            ->middleware('permission:crud_users');
                        Route::post('/toggle_status', 'Admin\Users\CrudController@toggleStatus')
                            ->name('toggle_status')
                            ->middleware('permission:activate_user|deactivate_user');
                    }
                );

            // FIND USER BY
            Route::prefix('find')
                ->name('find.')
                ->group(
                    function () {
                        Route::post('/username', 'Admin\Users\FindController@username')
                            ->name('username')
                            ->middleware('permission:crud_users');
                        Route::post('/email', 'Admin\Users\FindController@email')
                            ->name('email')
                            ->middleware('permission:crud_users');
                    }
                );

            // MASS ACTIONS
            Route::prefix('mass')
                ->name('mass.')
                ->group(
                    function () {
                        Route::post('/mail', 'Admin\Users\MassController@mail')
                            ->name('mail')
                            ->middleware('permission:mass_mail');
                        Route::post('/delete', 'Admin\Users\MassController@delete')
                            ->name('delete')
                            ->middleware('permission:crud_users');
                        Route::post('/activate', 'Admin\Users\MassController@activate')
                            ->name('activate')
                            ->middleware('permission:activate_user');
                        Route::post('/deactivate', 'Admin\Users\MassController@deactivate')
                            ->name('deactivate')
                            ->middleware('permission:deactivate_user');
                        Route::post('/roles/{id}', 'Admin\Users\MassController@roles')
                            ->name('roles')
                            ->middleware('permission:manage_roles');
                        Route::post('/permissions/{id}', 'Admin\Users\MassController@permissions')
                            ->name('permissions')
                            ->middleware('permission:manage_permissions');
                    }
                );

            // ACL
            Route::prefix('acl')
                ->name('acl.')
                ->group(
                    function () {
                        Route::get('roles', 'Admin\Users\AclController@getAllRoles')
                            ->name('roles')
                            ->middleware('permission:manage_roles');
                        Route::get('permissions', 'Admin\Users\AclController@getAllPermissions')
                            ->name('permissions')
                            ->middleware('permission:manage_permissions');
                    }
                );
        }
    );