<?php

Route::prefix('file')
    ->name('file.')
    ->middleware(['auth:api'])
    ->group(function() {
         // Access Controller
        Route::prefix('access')
            ->name('group.')
            ->group(function () {
                Route::get('/', 'File\AccessController@all')
                    ->name('all');
                Route::get('/happening/{happening}', 'File\AccessController@happenging')
                    ->name('happening');
                Route::get('/team/{team}', 'File\AccessController@team')
                    ->name('team');
                Route::get('/role/{role}', 'File\AccessController@role')
                    ->name('role');
                Route::get('/global', 'File\AccessController@globally')
                    ->name('globally');
            });

        // Category Controller
        Route::prefix('category')
            ->name('category.')
            ->group(function () {
                Route::get('/show/{category}', 'File\CategoryController@show')
                    ->name('show');
                Route::get('/{id}/{isTeam}', 'File\CategoryController@index')
                    ->name('index');
                Route::get('/{id}', 'File\CategoryController@index')
                    ->name('index');
                Route::put('/', 'File\CategoryController@store')
                    ->name('store');
                Route::patch('/{category}', 'File\CategoryController@update')
                    ->name('update');
                Route::delete('/{category}', 'File\CategoryController@destroy')
                    ->name('destroy');
            });

        // Comment Controller
        Route::prefix('comment')
            ->name('comment.')
            ->group(function () {
                Route::get('/{file}', 'File\CommentController@index')
                    ->name('index');
                Route::put('/', 'File\CommentController@store')
                    ->name('store');
                Route::patch('/{comment}', 'File\CommentController@update')
                    ->name('update');
                Route::delete('/{comment}', 'File\CommentController@destroy')
                    ->name('destory');
            });

        // User Controller
        Route::prefix('user')
            ->name('user')
            ->group(function () {
                Route::put('/', 'File\UserController@store')
                    ->name('store');
            });

        // File Controller
        Route::get('/{file}', 'File\FileController@show')
            ->name('show');

        Route::put('/', 'File\FileController@store')
            ->name('store');

        Route::patch('/{file}', 'File\FileController@update')
            ->name('update');

        Route::delete('/{file}', 'File\FileController@destory')
            ->name('destory');
    });