<?php

Route::prefix('chat')
    ->name('chat.')
    ->middleware(['auth:api'])
    ->group(
        function () {
            Route::apiResource('channel', 'Chat\ChannelController');

            Route::get('/message/{channel}', 'Chat\MessageController@index')
                ->name('message.index');

            Route::post('/message/{channel}', 'Chat\MessageController@store')
                ->name('message.store');

            Route::apiResource('message', 'Chat\MessageController')
                ->only(
                    [
                        'update',
                        'destroy'
                    ]
                );

            Route::prefix('user')
                ->name('user.')
                ->group(
                    function () {
                        Route::get('/{channel}', 'Chat\UserController@index')
                            ->name('index');

                        Route::post('/{channel}', 'Chat\UserController@store')
                            ->name('store');

                        Route::delete('/{channel}', 'Chat\UserController@destroy')
                            ->name('destory');
                    }
                );
        }
    );