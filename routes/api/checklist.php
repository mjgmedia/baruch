<?php

Route::prefix('checklist')
    ->name('checklist.')
    ->middleware(['auth:api'])
    ->group(
        function () {
            Route::prefix('checklist')
                ->name('checklist.')
                ->group(
                    function () {
                        Route::get('/patterns', 'Checklist\ChecklistController@indexPatterns')
                            ->name('patterns.index');

                        Route::put('/patterns', 'Checklist\ChecklistController@copyPattern')
                            ->name('patterns.copy');

                        Route::get('/{team}', 'Checklist\ChecklistController@index')
                            ->name('index');

                        Route::put('/', 'Checklist\ChecklistController@store')
                            ->name('store');

                        Route::get('/{checklist}', 'Checklist\ChecklistController@show')
                            ->name('show');

                        Route::patch('/{checklist}', 'Checklist\ChecklistController@update')
                            ->name('update');

                        Route::delete('/{checklist}', 'Checklist\ChecklistController@destroy')
                            ->name('destroy');
                    }
                );

            Route::prefix('entryBlocked')
                ->name('entryBlocked.')
                ->group(
                    function () {
                        Route::put('/{checklistEntry}', 'Checklist\EntryBlockedController@store')
                            ->name('store');

                        Route::get('/{checklistEntry}', 'Checklist\EntryBlockedController@show')
                            ->name('show');

                        Route::patch('/{checklistEntry}', 'Checklist\EntryBlockedController@update')
                            ->name('update');

                        Route::delete('/{checklistEntry}', 'Checklist\EntryBlockedController@destroy')
                            ->name('destroy');
                    }
                );

            Route::prefix('entryBlocking')
                ->name('entryBlocking.')
                ->group(
                    function () {
                        Route::put('/{checklistEntry}', 'Checklist\EntryBlockingController@store')
                            ->name('store');

                        Route::get('/{checklistEntry}', 'Checklist\EntryBlockingController@show')
                            ->name('show');

                        Route::patch('/{checklistEntry}', 'Checklist\EntryBlockingController@update')
                            ->name('update');

                        Route::delete('/{checklistEntry}', 'Checklist\EntryBlockingController@destroy')
                            ->name('destroy');
                    }
                );

            Route::prefix('entry')
                ->name('entry.')
                ->group(
                    function () {
                        Route::get('/{checklist}', 'Checklist\EntryController@index')
                            ->name('index');

                        Route::put('/', 'Checklist\EntryController@store')
                            ->name('store');

                        Route::get('/{checklistEntry}', 'Checklist\EntryController@show')
                            ->name('show');

                        Route::patch('/{checklistEntry}', 'Checklist\EntryController@update')
                            ->name('update');

                        Route::delete('/{checklistEntry}', 'Checklist\EntryController@destroy')
                            ->name('destroy');

                        Route::get('/mark/{checklistEntry}', 'Checklist\EntryController@markAsDone')
                            ->name('markAsDone');
                    }
                );

            Route::prefix('entryUser')
                ->name('entryUser.')
                ->group(
                    function () {
                        Route::get('/{entry}/done', 'Checklist\EntryUserController@done')
                            ->name('done');

                        Route::get('/{entry}/read', 'Checklist\EntryUserController@read')
                            ->name('read');

                        Route::put('/', 'Checklist\EntryUserController@store')
                            ->name('store');

                        Route::get('/{checklistEntry}', 'Checklist\EntryUserController@show')
                            ->name('show');

                        Route::patch('/{checklistEntry}', 'Checklist\EntryUserController@update')
                            ->name('update');

                        Route::delete('/{checklistEntry}', 'Checklist\EntryUserController@destroy')
                            ->name('destroy');
                    }
                );
        }
    );