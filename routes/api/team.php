<?php

Route::prefix('team')
    ->name('team.')
    ->middleware(['auth:api'])
    ->group(
        function (){
            Route::prefix('happening')
                ->name('happening.')
                ->group(
                    function () {
                        Route::get('/{team}', 'Team\HappeningController@index')
                            ->name('index');

                        Route::put('/{team}', 'Team\HappeningController@store')
                            ->name('store');

                        Route::delete('/{team}', 'Team\HappeningController@destroy')
                            ->name('destroy');
                    }
                );


            Route::prefix('manager')
                ->name('manager.')
                ->group(
                    function () {
                        Route::put('/{team}', 'Team\ManagerController@store')
                            ->name('store');

                        Route::delete('/{team}', 'Team\ManagerController@destroy')
                            ->name('destroy');
                    }
                );


            Route::prefix('postComment')
                ->name('postComment.')
                ->group(
                    function () {
                        Route::put('/', 'Team\PostCommentController@store')
                            ->name('store');

                        Route::patch('/{comment}', 'Team\PostCommentController@update')
                            ->name('update');

                        Route::delete('/{comment}', 'Team\HappeningController@destroy')
                            ->name('destroy');
                    }
                );


            Route::prefix('post')
                ->name('post.')
                ->group(
                    function () {
                        Route::get('/{team}', 'Team\PostController@index')
                            ->name('index');

                        Route::put('/', 'Team\PostController@store')
                            ->name('store');

                        Route::get('/{id}', 'Team\PostController@show')
                            ->name('show');

                        Route::patch('/{teamPost}', 'Team\PostController@update')
                            ->name('update');

                        Route::delete('/{id}', 'Team\PostController@destroy')
                            ->name('destroy');
                    }
                );


            Route::prefix('team')
                ->name('team.')
                ->group(
                    function () {
                        Route::get('/', 'Team\TeamController@index')
                            ->name('index');

                        Route::get('/{team}', 'Team\TeamController@show')
                            ->name('show');

                        Route::put('/', 'Team\TeamController@store')
                            ->name('store');

                        Route::patch('/{team}', 'Team\TeamController@update')
                            ->name('update');

                        Route::delete('/', 'Team\TeamController@destroy')
                            ->name('destroy');
                    }
                );


            Route::prefix('user')
                ->name('user.')
                ->group(
                    function () {
                        Route::get('/{team}', 'Team\UserController@index')
                            ->name('index');

                        Route::put('/{team}', 'Team\UserController@store')
                            ->name('store');

                        Route::patch('/{team}', 'Team\UserController@update')
                            ->name('update');

                        Route::delete('/{team}', 'Team\UserController@destroy')
                            ->name('destroy');
                    }
                );


        }


    );