<?php

/**
 * USER ACTIONS
 */
Route::prefix('user')
    ->name('user.')
    ->middleware(['auth:api'])
    ->group(
        function () {
            // ACTIVITY
            Route::prefix('activity')
                ->name('activity.')
                ->group(
                    function () {
                        Route::get('/get', 'User\ActivityController@get')
                            ->name('get');

                        Route::get('/update', 'User\ActivityController@update')
                            ->name('update');
                    }
                );

            // PROFILE
            Route::prefix('profile')
                ->name('profile.')
                ->group(
                    function () {
                        Route::post('/update', 'User\ProfileController@update')
                            ->name('update');
                        Route::post('/get', 'User\ProfileController@get')
                            ->name('get');
                    }
                );

            // ACCOUNT
            Route::prefix('account')
                ->name('account.')
                ->group(
                    function () {
                        Route::post('/update', 'User\AccountController@update')
                            ->name('update');

                        Route::post('/upload', 'User\AccountController@uploadAvatar')
                            ->name('upload');
                    }
                );

            // SETTINGS
            Route::prefix('settings')
                ->name('settings.')
                ->group(
                    function () {
                        Route::get('/fetch', 'User\SettingsController@fetch')
                            ->name('fetch');
                        Route::get('/get/{setting}', 'User\SettingsController@get')
                            ->name('get');
                        Route::post('/update', 'User\SettingsController@update')
                            ->name('update');
                        Route::get('/reset', 'User\SettingsController@reset')
                            ->name('reset');
                    }
                );

            /**
             * ACL
             **/
            Route::name('acl.')
                ->prefix('acl')
                ->middleware(['auth:api'])
                ->group(
                    function () {
                        //? Helpers We Can Use For Permission and Roles
                        /**
                         * GETTERS
                         */
                        Route::prefix('get')
                            ->name('get.')
                            ->group(
                                function () {
                                    Route::get(
                                        '/permissions_via_roles',
                                        'Auth\Acl\GetController@getPermissionsViaRoles'
                                    )
                                        ->name('permissions_via_roles');
                                    Route::get('/direct_permissions', 'Auth\Acl\GetController@getDirectPermissions')
                                        ->name('direct_permissions');
                                    Route::get('/permissions', 'Auth\Acl\GetController@getAllPermissions')
                                        ->name('permissions');
                                    Route::get('/roles', 'Auth\Acl\GetController@getRoles')
                                        ->name('roles');
                                }
                            );

                        /**
                         * CHECKERS
                         */
                        Route::prefix('has')
                            ->name('has.')
                            ->group(
                                function () {
                                    Route::post('/permission_to', 'Auth\Acl\HasController@hasPermissionTo')
                                        ->name('permission_to');
                                    Route::post('/any_permission', 'Auth\Acl\HasController@hasAnyPermission')
                                        ->name('any_permission');
                                    Route::post('/role', 'Auth\Acl\HasController@hasRole')
                                        ->name('role');
                                    Route::post('/any_role', 'Auth\Acl\HasController@hasAnyRole')
                                        ->name('any_role');
                                    Route::post('/all_roles', 'Auth\Acl\HasController@hasAllRoles')
                                        ->name('all_roles');
                                }
                            );
                    }
                );
        }
    );