SET FOREIGN_KEY_CHECKS=0;
TRUNCATE contents;
TRUNCATE content_contacts;
TRUNCATE content_files;
TRUNCATE content_htmls;
TRUNCATE content_image_items;
TRUNCATE content_images;
TRUNCATE content_map_marker_pivot;
TRUNCATE content_map_markers;
TRUNCATE content_maps;
TRUNCATE pages;
TRUNCATE page_content;
TRUNCATE settings;
TRUNCATE setting_groups;
TRUNCATE setting_user;
TRUNCATE users;
TRUNCATE profiles;
TRUNCATE social_accounts;
TRUNCATE push_subscriptions;
TRUNCATE password_resets;
TRUNCATE permissions;
TRUNCATE role_has_permissions;
TRUNCATE roles;
TRUNCATE model_has_permissions;
TRUNCATE model_has_roles;
TRUNCATE media;
SET FOREIGN_KEY_CHECKS=1;
