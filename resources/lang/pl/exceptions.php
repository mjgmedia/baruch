<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Exceptions Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in exceptions responses.
    |
    */

    'not_found' => ' not found.',

    'user_not_found' => [
        'user_id' => 'User ID: ',
        'not_found' => ' Not Found!',
    ],

    'revoke_admin_update' => [
        'message' => 'Modifying Super Admin is Not Allowed!',
    ],

    'account_creation_failed' => [
        'message' => 'Tworzenie nowego użytkownika zakończone niepowodzeniem.'
    ],

    'updating_record_failed' => [
        'message' => 'Aktualizacja danych zakończona niepowodzeniem.'
    ],

    'email_not_found' => [
        'exception_title' => 'Email Not Found Exception: ',
    ],

    'username_not_found' => [
        'exception_title' => 'Username Not Found Exception: ',
    ],

    'user_token_not_found' => [
        'message' => 'User Token Not Found Exception: This Resource Can Only Be Loaded With Authorized User Token',
    ],

    'activation' => [
        'disabled' => 'System aktywacji jest wyłączony. Skontaktuj się z Administratorem systemu.'
    ],

    'auth' => [
        'connection' => 'Wystąpił błąd podczas połączenia do serwera autoryzacji. Problem został zgłoszony do zespołu programistycznego.',
        'request' => 'Wystąpił błąd podczas przesyłania danych do serwera autoryzacji. Problem został zgłoszony do zespołu programistycznego.',
    ],

    'settings' => [
        'non_exists' => 'Nie ma takiego ustawienia, podano zły kod ustawienia.'
    ]
];