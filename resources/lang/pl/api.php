<?php

return [
    /*
    |--------------------------------------------------------------------------
    | API Controllers Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by API Controllers in their responses.
    |
    */

    'maintenance' => 'Trwają prace na serwerze, prosimy spórbuj ponownie później.',

    'forgot_password' => [
        'send_reset_email' => [
            'link_sent' => 'Profile Updated!',
            'limit' => 'Request Limit of (',
            'limit_exceeded' => ') Exceeded.',
            'email_error' => 'Failed To Send Reset Link.',
            'email_success' => 'Password Reset Link Sent!',
        ],
    ],

    'register' => [
        'success' => 'Zarejestrowaliśmy Twoje nowe konto w serwisie. Teraz sprawdź podaną w formularzu skrzynkę email. Znajdziesz tam mail z linkiem do aktywacji konta.',
        'activation_success' => 'Twoje konto jest teraz aktywne. Możesz się zalogować',
        'activation_error' => 'Nie odnaleźliśmy podanego kodu aktywacji w naszej bazie. Spróbuj zarejestrować się ponownie.',
    ],

    'login' => [
        'logout_success' => 'Zostałeś wylogowany.',
        'wrong_credentials' => 'Dane do logowania są nieprawidłowe. Sprawdź podany login i hasło.',
        'oauth_error' => 'Wystąpił błąd po stronie serwera autoryzacji. System automatycznie zgłosił sprawę do 
                          zespołu programistycznego. Spróbuj ponownie później.',
    ],

    'reset_password' => [
        'fail' => 'Failed To Reset Password.',
        'timeout' => 'Ważność linku wygasła, proszę wygeneruj link ponownie.',
    ],

    'social' => [
        'auth_error' => 'An Error Occured, please retry later'
    ],

    'user' => [
        'profile' => [
            'update' => [
                'success' => 'Profile zaktualizowany!',
                'error' => 'Wystąpił błąd podczas aktualizacji danych. Sprawdź podane przez Ciebie dane i ich format',
            ],
        ],
        'account' => [
            'update' => [
                'success' => 'Dane konta zaktualizowane pomyślnie!',
                'error' => 'Wystąpił błąd podczas aktualizacji danych. Sprawdź podane przez Ciebie dane i ich format'
            ],
            'avatar' => [
                'success' => 'Avatar wgrany na serwer pomyślnie.',
                'error' => 'Wystąpił problem z wgraniem pliku na serwer. Nasz zespół programistyczny został powiadomiony. Nie próbuj ponownie.',
            ],
        ],
    ],

    'contact' => [
        'message' => 'Wiadomość wysłana',
    ],

    'admin' => [
        'users' => [
            'crud' => [
                'no_permission' => 'Nie masz uprawnień do tego modułu!',
                'edit' => [
                    'user_not_found' => 'Cant Find User With ID of ',
                ],
                'update' => [
                    'error' => 'Cant Find User With ID of ',
                    'success' => 'User Account Updated!'
                ],
                'toggle_status' => [
                    'message' => 'You Cannot Modify Super Admin!',
                ],
            ],
            'mass' => [
                'activate' => [
                    'message' => 'Selected Users Activated!',
                ],
                'deactivate' => [
                    'message' => 'Selected Users Deactivated!',
                ],
                'delete' => [
                    'fail' => 'Failed To Delete Selected Users!',
                    'success' => 'Successfully Deleted Selected Users.'
                ],
                'mail' => [
                    'message' => 'Wiadomość przekazano do wysłania!'
                ],
            ],
        ],
    ],

    'settings' => [
        'update' => 'Ustawienia zaktualizowane.',
        'reset' => 'Przywrócono ustawienia domyślne.',
    ],

];