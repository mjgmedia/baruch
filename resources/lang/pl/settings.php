<?php

return [
    'push_notifications' => 'Powiadomienia PUSH',
    'test' => 'Test grupa',
    'test1' => 'Test 1',
    'team_notify_about_new_members' => 'Powiadamiaj o nowych osobach w zespole',
    'team_notify_about_deleted_members' => 'Powiadamiaj o odejściach osób z zespołu',
    'team_notify_member_profile_changed' => 'Powiadamiaj o zmianach w danych kontaktowych osób z zespołu',
    'team_notify_about_tasks_done' => 'Powiadamiaj o wykonaniu zadań',
    'file_notify_about_new_comments' => 'Powiadamiaj mnie o nowych komentarzach',
    'file_notify_about_responses_to_comments' => 'Powiadamiaj mnie o odpowiedziach do komentarzy',
    'file_team_notify_about_open' => 'Powiadamiaj o otwraciu pliku przez członków zespołu',
    'file_happening_notify_about_open' => 'Powiadamiaj mnie o otwarciu pliku przez członków wydarzeniar',
    'happening_notify_about_new_members' => 'Powiadamiaj mnie o nowych osobach zespole',
    'happening_notify_about_deleted_members' => 'Powiadamiaj mnie o skreśleniach z zespołu',
    'happening_notify_about_new_teams' => 'Powiadamiaj mnie o skreśleniach z zespołu',
    'happening_notify_about_deleted_teams' => 'Powiadamiaj mnie o skreśleniach z zespołu',
    'happening_notify_member_profile_changed' => 'Powiadamiaj o zmianie danych w profilach użytkowników',
];