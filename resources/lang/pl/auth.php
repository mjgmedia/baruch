<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logout' => 'Wyloguj',
    'login' => 'Zaloguj',
    'logged' => 'Jesteś zalogowany!',
    'not_logged' => 'Zaloguj się, aby skorzystać z aplikacji',
    'email' => 'Adres email',
    'password' => 'Hasło',
    'remember' => 'Zapamiętaj mnie',
    'dashboard' => 'Dashboard',
    'failed'   => 'Błędny login lub hasło.',
    'throttle' => 'Za dużo nieudanych prób logowania. Proszę spróbować za :seconds sekund.',
    'user_not_found' => 'Błędne dane logowania',
    'user_not_activated' => 'Twoje konto nie jest aktywowane. Sprawdź swoją skrzynkę email.'
];
