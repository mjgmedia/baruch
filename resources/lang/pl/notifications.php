<?php

return [
    // Laravel backup
    'exception_message' => 'Błąd: :message',
    'exception_trace' => 'Zrzut błędu: :trace',
    'exception_message_title' => 'Błąd',
    'exception_trace_title' => 'Zrzut błędu',

    'backup_failed_subject' => 'Tworzenie kopii zapasowej aplikacji :application_name nie powiodło się',
    'backup_failed_body' => 'Ważne: Wystąpił błąd podczas tworzenia kopii zapasowej aplikacji :application_name',

    'backup_successful_subject' => 'Pomyślnie utworzono kopię zapasową aplikacji :application_name',
    'backup_successful_subject_title' => 'Nowa kopia zapasowa!',
    'backup_successful_body' => 'Wspaniała wiadomość, nowa kopia zapasowa aplikacji :application_name została pomyślnie utworzona na dysku o nazwie :disk_name.',

    'cleanup_failed_subject' => 'Czyszczenie kopii zapasowych aplikacji :application_name nie powiodło się.',
    'cleanup_failed_body' => 'Wystąpił błąd podczas czyszczenia kopii zapasowej aplikacji :application_name',

    'cleanup_successful_subject' => 'Kopie zapasowe aplikacji :application_name zostały pomyślnie wyczyszczone',
    'cleanup_successful_subject_title' => 'Kopie zapasowe zostały pomyślnie wyczyszczone!',
    'cleanup_successful_body' => 'Czyszczenie kopii zapasowych aplikacji :application_name na dysku :disk_name zakończone sukcesem.',

    'healthy_backup_found_subject' => 'Kopie zapasowe aplikacji :application_name na dysku :disk_name są poprawne',
    'healthy_backup_found_subject_title' => 'Kopie zapasowe aplikacji :application_name są poprawne',
    'healthy_backup_found_body' => 'Kopie zapasowe aplikacji :application_name są poprawne. Dobra robota!',

    'unhealthy_backup_found_subject' => 'Ważne: Kopie zapasowe aplikacji :application_name są niepoprawne',
    'unhealthy_backup_found_subject_title' => 'Ważne: Kopie zapasowe aplikacji :application_name są niepoprawne. :problem',
    'unhealthy_backup_found_body' => 'Kopie zapasowe aplikacji :application_name na dysku :disk_name są niepoprawne.',
    'unhealthy_backup_found_not_reachable' => 'Miejsce docelowe kopii zapasowej nie jest osiągalne. :error',
    'unhealthy_backup_found_empty' => 'W aplikacji nie ma żadnej kopii zapasowych tej aplikacji.',
    'unhealthy_backup_found_old' => 'Ostatnia kopia zapasowa wykonania dnia :date jest zbyt stara.',
    'unhealthy_backup_found_unknown' => 'Niestety, nie można ustalić dokładnego błędu.',
    'unhealthy_backup_found_full' => 'Kopie zapasowe zajmują zbyt dużo miejsca. Obecne użycie dysku :disk_usage jest większe od ustalonego limitu :disk_limit.',

    // Ours
    'password_reset_subject' => 'Have You Requested to Reset Your Password?',
    'password_reset_line_1' => 'Click the Button Below To Reset Your Password.',
    'password_reset_cta' => 'Reset Your Password',
    'password_reset_line_2' => 'If You Did\'nt Request For This Password Reset. Just Ignore this Email.',

    'greetings' => 'Pozdrawiamy, zespół Digital Marketing Rywal-RHC',

    'contact' => [
        'line1' => 'Otrzymałeś wiadomość z serwisu ',
        'from' => 'Od: ',
        'email' => 'Podany adres email: ',
        'subject' => 'Temat: ',
        'message' => 'Treść wiadomości: ',
        'web_push' => 'Nowa próba kontaktu przez formularz na stronie. Szczegóły przesłane na maila.'
    ],

    'admin' => [
        'mass_mail' => [
            'intro' => 'Wiadomość od Administracji serwisu: ',
            'subject' => 'Temat wiadomości: ',
            'message' => 'Treść wiadomości: ',
            'web_push' => 'Nowa wiadomość od Administracji serwisu przesłana do Twój adres email.'
        ],
        'new_user' => [
            'intro' => 'Zarejestrował się nowy użytkownik w serwisie.',
            'username' => 'Nazwa użytkownika:',
            'email' => 'Adres email:',
        ]
    ],

    'oauth' => [
        'mail' => [
            'code' => 'Wystąpił błąd krytyczny serwera OAuth. Kod błędu: ',
            'message' => 'Treść błędu: ',
        ],
        'webpush' => [
            'title' => 'Błąd krytyczny serwera OAuth',
            'message' => 'Wystąpił błąd krytyczny serwera OAuth. Więcej informacji na poczcie email.'
        ]
    ],

    'activation' => [
        'line' => 'Założyłeś konto w serwisie ',
        'line2' => 'Abyś mógł się zalogować po raz pierwszy, musisz aktywować konto klikając w poniższy link:',
        'ignore' => 'Jeśli nie zakładałeś konta w serwisie, zignoruj tą wiadomość.',
        'cta' => 'Aktywuj konto',
    ],

    'chat' => [
        'unread_push' => 'Nowa wiadomość na chacie'
    ],

    'team' => [
        'you_added' => 'Zostałeś dołączony do zespołu ',
        'you_removed' => 'Zostałeś odłączony od zespołu ',
        'about_updated' => 'Informacje o zespole zostały zaktualizowane.',
        'about_updated_more' => 'Aktualni nadzorcy zespołu: ',
        'team' => 'Zespół ',
        'added_to_happening' => ' został dodany do wydarzenia ',
        'remove_from_happening' => ' został usunięty z wydarzenia ',
        'team_created' => ' został stworzony',
        'team_deleted' => ' został usunięty',
        'user' => 'Użytkownik ',
        'user_added' => ' dołączył do zespołu ',
        'user_deleted' => ' został usunięty z zespołu',
        'user_profile_edited' => ' edytował dane osobowe. Sprawdź te zmiany w jego profilu.',
        'team_system' => 'System zespołów',
        'new_post' => ' dodał nowy post w feedzie zespołu ',
        'new_comment' => ' dodał nowy komentarz do postu ',
        'in_feed' => ' w feedzie zespołu ',
    ],

    'happening' => [
        'happening' => 'Wydarzenie ',
        'team' => 'Zespół ',
        'user' => 'Użytkownik ',
        'team_added_to' => '',
        'team_removed_from' => '',
        'information_updated' => '',
        'user_added_to' => '',
        'user_removed_from' => '',
        'user_profile_edited' => '',
        'created' => '',
        'deleted' => '',
        'new_post' => ' dodał nowy post w feedzie wydarzenia ',
        'new_comment' => ' dodał nowy komentarz do postu ',
        'in_feed' => ' w feedzie wydarzenia ',
    ],

    'file' => [
        'to_file' => 'Do pliku ',
        'file' => 'Plik ',
        'new_comment' => ' pojawił się nowy komentarz',
        'response_to_comment' => '',
        'updated_file' => ' został zaktualizowany.',
        'read_by' => ' został przeczytany przez użytkownika ',
        'new_file ' => 'Udostępniono nowy plik: ',
        'new_comment_action' => '',
        'new_file_action' => '',
        'file_read_action' => '',
        'response_to_comment_action' => '',
        'updated_file_action' => '',
    ],

    'checklist' => [
        'checklist' => 'Checklista ',
        'entry' => 'Zadanie ',
        'should_done' => 'powinno zostać wykonane w terminie: ',
        'however' => 'Jednak ',
        'overdue' => ' przekroczono czas przeznaczony na jego wykonanie.',
        'less_5' => ' zostało jedynie 5 minut na jego wykonanie.',
        'users' => 'Użytkownicy przydzieleni do zadania: ',
        'done_by' => ' zostało wykonane przez ',
        'unlocked' => ' zostało odblokowane przez zadanie zależne. Możesz zabierać się do pracy.',
        'received' => ' zostało odczytane przez użytkownika ',
        'details' => 'Informacje dodatkowe do zadania ',
        'updated' => ' zostały zaktualizowane. Proszę zapoznaj się z nimi od razu.',
        'assigned' => 'Zostałeś przydzielony do wykonania zadania ',
        'assignees' => '. Wszyscy przydzieleni do tego zadania: '
    ]

];
