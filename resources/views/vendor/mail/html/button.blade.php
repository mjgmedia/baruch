<table cellpadding="0" cellspacing="0" width="600" class="w320">
  <tr>
    <td class="button">
      <div><!--[if mso]>
        <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{{ $url }}" style="height:45px;v-text-anchor:middle;width:155px;" arcsize="15%" strokecolor="#ffffff" fillcolor="#ff6f6f">
          <w:anchorlock/>
          <center style="color:#ffffff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;">{{ $slot }}</center>
        </v:roundrect>
        <![endif]--><a  class="button-mobile" href="{{ $url }}"
                        style="background-color:#ff6f6f;border-radius:5px;color:#ffffff;display:inline-block;font-family:'Cabin', Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;">{{ $slot }}</a></div>
    </td>
  </tr>
</table>
