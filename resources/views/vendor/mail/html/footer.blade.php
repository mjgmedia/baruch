<tr>
  <td align="center" valign="top" width="100%" style="background-color: #fff; height: 100px;">
    <center>
      <table cellspacing="0" cellpadding="0" width="600" class="w320">
        <tr>
          <td style="padding: 25px 0 25px">
            {{ Illuminate\Mail\Markdown::parse($slot) }}
          </td>
        </tr>
      </table>
    </center>
  </td>
</tr>