# Bootstrap API

Bootstrap API to boilerplate stworzony o przyśpieszeniu procesu tworzenia nowych aplikacji webowych. Zawiera zestaw narzędzi, 
komponentów i konfiguracji, które zazwyczaj są używane w tworzonych przez nas aplikacjach.

Więcej informacji w dokumentacji zawartej w folderze `/docs`