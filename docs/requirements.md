# Wymagania

* PHP >= 7.2
* MySQL (or MariaDB) >= 5.7 (>= 10.2)
* ffmpeg
* jpegoptim 
* optipng 
* pngquant 
* gifsicle
* svgo
* sqlite

## Rozszerzenia PHP
* exif
* json
* gd
* imagick
* Ghostscript
* sqlite3
* gmp

## Rozszerzenie PECL
* redis
* swoole