# Kody odpowiedzi

## Błędy
1xx - błędy Auth
101 - OAuth Fatal Error
102 - OAuth Login Failed
103 - Reset Fail
104 - Reset Sendmail Fatal
105 - Reset Timeout
106 - Reset Too Many Mails Sended
107 - Reset User Not Found
108 - Odpytanie check usera czy ma aktywny token - nie ma
109 - User not found
110 - user account not activated
111 - aktywacja zablokowana
112 - błąd połączenia z Passport 
113 - błąd żądania do Passporta

2xx - błędy User
201 - Profile update error
202 - Token not found
203 - Notification not found
204 - Subscription not found
205 - Wrong Endpoint
206 - username not found
207 - Accout update error
208 - Avatar upload error
209 - Activation Code not found

3xx - błędy Chat
301 - Chat/ChannelDeleteException
302 - Chat/ChannelDeleteInServiceException
303 - Chat/ChannelNotFoundException
304 - Chat/ChannelUpdateException
305 - Chat/ChannelUserAttachingException
306 - Chat/ChannelUserDetachException
307 - Chat/HappeningChannelTakeException
308 - Chat/MessageDeleteException
309 - Chat/MessagesUsersAttachingException
310 - Chat/MessageUpdateException
311 - Chat/TeamChannelTakenException
312 - Chat/WrongChannelException

4xx - błędy Checklist
401 - Checklist/ChecklistDeleteException
402 - Checklist/ChecklistEntryDeleteException
403 - Checklist/ChecklistUpdateException
404 - Checklist/EntryBlockedDeleteException
405 - Checklist/EntryBlockedStoreException
406 - Checklist/EntryBlockedUpdateException
407 - Checklist/EntryBlockingDeleteException
408 - Checklist/EntryBlockingStoreException
409 - Checklist/EntryBlockingUpdateException
410 - Checklist/EntryMarkAsDoneException
411 - Checklist/EntryUpdateException
412 - Checklist/EntryUserDeleteException
413 - Checklist/EntryUserDoneException
414 - Checklist/EntryUserReadException
415 - Checklist/EntryUserStoreException
416 - Checklist/EntryUserUpdateException
417 - Checklist/NoAuthForChecklistCreating
418 - Checklist/NoAuthForChecklistEntryCreating
419 - Checklist/NotPatternException

5xx - błędy File
501 - File/CategoryDeleteException
502 - File/CategoryUpdateException
503 - File/CommentDeleteException
504 - File/CommentUpdateException
505 - File/FileDeleteException
506 - File/FileStoreException
507 - File/FileUpdateException
508 - File/GettingUsersWithAccessException
509 - File/NoPermissionForCategoryException
510 - File/NoPermissionToCrudException
511 - File/NoPermissionToGlobalCrudException
512 - File/NoPermissionToGlobalFilesException
513 - File/NoPermissionToViewFileException
514 - File/UploadException
515 - File/UserOpenedFileUpdateException

6xx - błędy Happening
601 - Happening/DeleteUserException
602 - Happening/HappeningDeleteException
603 - Happening/HappeningUpdateException
604 - Happening/ManagerDeleteException
605 - Happening/ManagerStoreException
606 - Happening/NotAHappeningMember
607 - Happening/NotAllowedToSeePostException
608 - Happening/NotAMemberException
609 - Happening/PostCommentDeleteException
610 - Happening/PostCommentUpdateException
611 - Happening/PostDeleteException
612 - Happening/PostUpdateException
613 - Happening/StoreUserException

7xx - błędy Setting
701 - Exceptions Settings Non Exists

8xx - błędy Team
801 - Team/HappeningDeleteException
802 - Team/HappeningStoreException
803 - Team/ManagersDeleteException
804 - Team/ManagersStoreException
805 - Team/NotATeamMember
806 - Team/PostCommentDeleteException
807 - Team/PostCommentUpdateException
808 - Team/PostDeleteException
809 - Team/PostUpdateException
810 - Team/TeamDeleteException
811 - Team/TeamUpdateException
812 - Team/UserDeleteException
813 - Team/UserStoreException

9xx - błędy Admin
901 - Account creation failed
902 - email not found
903 - has no permission
904 - mass delete fatal
905 - modify super-admin denied
906 - revoke admin update
907 - updating failed
908 - user not found

1xxx - błędy aplikacji

## Odpowiedzi store
2xxx

2001 - Admin/Users/CrudController@create
2002 - Auth/RegisterController@register
2003 - Chat/ChanelController@store
2004 - Chat/MessageController@store
2005 - Chat/UserController@store
2006 - Checklist/ChecklistController@store
2007 - Checklist/EntryBlockedController@store
2008 - Checklist/EntryBlockingController@store
2009 - Checklist/EntryController@store
2010 - Checklist/EntryUserController@store
2011 - File/CategoryController@store
2012 - File/CommentController@store
2013 - File/FileController@store
2014 - File/UserController@store
2015 - Happening/HappeningController@store
2016 - Happening/ManagerController@store
2017 - Happening/PostCommentController@store
2018 - Happening/PostController@store
2019 - Happening/UserController@store
2020 - Team/HappeningController@store
2021 - Team/ManagerController@store
2022 - Team/PostCommentController@store
2023 - Team/PostController@store
2024 - Team/TeamController@store
2025 - Team/UserController@store
2026 - User/AccountController@store

## Odpowiedzi update
3xxx

3001 - Admin/Users/CrudController@update
3002 - Admin/Users/CrudController@toggleStatus
3003 - Admin/Users/MassController@activate
3004 - Admin/Users/MassController@deactivate
3005 - User/AccountController@update
3006 - User/ActivityController@update
3007 - User/ProfileController@update
3008 - User/SettingsController@update
3009 - Auth/RegisterController@activate
3010 - Chat/ChanelController@update
3011 - Chat/MessageController@update
3012 - Checklist/ChecklistController@update
3013 - Checklist/EntryBlockedController@update
3014 - Checklist/EntryBlockingController@update
3015 - Checklist/EntryController@update
3016 - Checklist/EntryUserController@update
3017 - File/CategoryController@update
3018 - File/CommentController@update
3019 - File/FileController@update
3020 - Happening/HappeningController@update
3021 - Happening/ManagerController@update
3022 - Happening/PostCommentController@update
3023 - Happening/PostController@update
3023 - Happening/UserController@update
3024 - Team/PostCommentController@update
3025 - Team/PostController@update
3026 - Team/TeamController@update
3027 - Team/UserController@update

## Odpowiedzi delete/destroy
4xxx

4001 - CrudController@delete
4002 - Admin/Users/MassController@delete
4003 - Chat/ChanelController@destroy
4004 - Chat/MessageController@destroy
4005 - Chat/UserController@destroy
4006 - Checklist/ChecklistController@destroy
4007 - Checklist/EntryBlockedController@destroy
4008 - Checklist/EntryBlockingController@destroy
4009 - Checklist/EntryController@destroy
4010 - Checklist/EntryUserController@destroy
4011 - File/CategoryController@destroy
4012 - File/CommentController@destroy
4013 - File/FileController@destroy
4014 - Happening/HappeningController@destroy
4015 - Happening/ManagerController@destroy
4016 - Happening/PostCommentController@delete
4017 - Happening/PostController@destroy
4018 - Happening/UserController@destroy
4019 - Team/HappeningController@destroy
4020 - Team/ManagerController@destroy
4021 - Team/PostCommentController@destroy
4022 - Team/PostController@destroy
4023 - Team/TeamController@udestroy
4024 - Team/UserController@destroy

## Odpowiedzi get i inne
5xxx

5001 - Admin/Users/MassController@mail
5002 - Auth/ForgotPasswordController@sendResetEmail
5003 - Auth/LoginController@check
5004 - Auth/LoginController@logout
5005 - User/AccountController@uploadAvatar
5006 - User/ActivityController@get
5007 - SettingsController@fetch
5008 - SettingsController@reset
5009 - ContactUsController@contact
5010 - Auth/Acl/GetController@getAllPermissions
5011 - Auth/Acl/GetController@getDirectPermissions
5012 - Auth/Acl/GetController@getPermissionsViaRoles
5013 - Auth/Acl/GetController@getRoles
5014 - Auth/Acl/HasController@hasAllRoles
5015 - Auth/Acl/HasController@hasAnyPermission
5016 - Auth/Acl/HasController@hasAnyRole
5017 - Auth/Acl/HasController@hasPermissionTo
5018 - Auth/Acl/HasController@hasRole
5019 - Chat/ChanelController@freeTeam
5020 - Chat/ChanelController@freeHappening
5021 - Checklist/ChecklistController@copyPattern
5022 - Checklist/EntryController@markAsDone
5023 - Checklist/EntryController@done
5024 - Checklist/EntryController@read
