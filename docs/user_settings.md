# Ustawienia użytkowników
Aby umożliwić przechowywanie ustawień użytkowników i dostęp do nich z poziomu kodu backendu w projekcie posługujemy się biblioteką `laravel-model-settings`. Tworzy ona dodatką tablę oraz proste metody do modelu `User` które umożliwiają CRUD ustawień.

Ustawienia są realizowane przez kilka plików w systemie:
* `config/project.php`
* `resources/lang/settings.php`
* `app\Services\SettingsService.php`
* `app\Http\Controllers\Api\User\SettingsController.php`

Najmniejszą jednostką jaką otrzymuje frontend jest tablica z trzema elementami:
1. `key`
2. `value`
3. `name`

Ale w backendzie te rzeczy pochodzą z różnych miejsc. 
Klucze (`key`) są definiowane w pliku konfiguracyjnym `config/project.php` w tablicy `settings`. każdy klucz musi być uniklany - to bardzo ważne - inaczej system nie będzie działał poprawnie. System dopuszcza jedno poziomowe stopniowanie.
```php
'settings' => [
    'model_name' => [
        'flat' => true,
        'extra_level' => [
            'setting' => true,
        ],  
    ],
],
```

**Uwaga - system przyjmuje tylko wartości typu `boolean`!**


Nazwy czytelne dla użytkownika są przechowywane w pliku `resources/lang/settings.php` w postaci zwykłej tablicy. Tutaj nie wyróżniamy poziomowania, po prostu odwołujemy się po kluczach. To pierwszy z powodów dla którego muszą być unikalne.

Wartości zaś są przechowywane w bazie danych. Do obsługi działań CRUD został stworzony `SettingsService` który umożliwia aktualizacje pojedyńczego ustawienia i reset do ustawień domyślnych. 

Definicje do bazy danych są tworzone na podstawie kluczy i wartości domyślnych zapisanych w konfiguracji w pliku `config/project.php` poprzez `SettingSeeder`. System ustawień jest połączony z systemem ról i uprawnień. Zablokowanie jakiegoś ustawienia jest możliwe tylko przez **1 uprawnienie**. Definicje blokad układa się w jednopoziomowej tabeli w pliku konfiguracyjnym `config/project.php`.

## Sprawdzanie ustawień użytkownika
Aby sprawdzić czy dany user ma zaznaczone wybrane ustawienie, można skorzystać ze scope: `scopeOnSetting` dostępny w modelu `User`. Użycie jest bardzo proste:
```php
User::onSetting('setting_key');
```

Gdy mamy już model pojedyńczego użytkownika, możemy użyć metody `hasSettingOn` do której należy podać klucz danego ustawienia a w odpowiedzi zwrotnej dostaniemy wartość true/false (bool).
Przykład:
```php
$user = new User();
$result = $user->hasSettingOn('setting_key');
``` 