# Envoy

Envoy zawiera gotowe polecenia:

* `backup` - całkowity backup danych z tabel
* `restore` - listuje posiadane backupy i możliwość ich przywrócenia
* `deploy` - deployment na produkcyjny
* `db_sync_from` - synchronizacja bazy danych localhost (dest) z produkcyjną (source)
* `db_sync_to` - synchronizacja bazy danych producyjnej (dest) z localhost (source)
* `commit` - szybki commit z gotowym opisem
* `resources` - szybki commit w submodule resources
* `db_refresh` - migracja całej bazy danych na nowo, instalacja Passport, Voyagera, przywrócenie userów
* `install` - kopia .env, pobieranie bibliotek przez Compsera, wygenerowanie klucza, migracja, instalacja Voyagera i 
Passport, wygerowanie kluczy VAPID
* `run` - uruchamia serwery dla trybu developerskiego - api, websockets, horizon i ui
