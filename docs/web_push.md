# WebPush
Do realizacji Web Push API w backendzie bootstrap wykorzystuje `laravel-notification-channels/web-push` a we 
frontendzie robi to nasz kod w ServiceWorker'ze (`sw.js`) oraz `App.vue`.

## Zasada działania Web Push API
![Schemat Web PUSH API](https://i.stack.imgur.com/DzEDv.jpg)

1. Przeglądarka prosi o pozwolenie na obsługę `Notification API` (użytkownik musi kliknąć zgode)
2. Kod aplikacji łączy się z `serviceWorker` który rozpoczyna procedurę rejestrowania nowej sukskrypcji Push. 
Przeglądarka loguje się w usłudze `WebPushAPI` (zapewnia producent przeglądarki)
3. Zwrotnie z subskrypcją zwraca URL i ID. Service Worker zaczyna nasłuchiwać powiadomień PUSH.
4. Przekazujemy dane subskrypcji do naszego API i łączymy z użytkownikiem (wniosek - uchwyty API do obsługi PUSH będą 
miały włączonego auth:api middleware'a)
5. API może teraz wypchnąć powiadomienie.
6. ServiceWorker odbiera powiadomienie PUSH i przekazuje do systemu. Każde powiadomienie to event w JS, można więc 
wykonać jakąś funkcję - tak też się dzieje w RHC Bootstrap.
7. Wyświetlenie powiadomienia przez system.

## Generowanie powiadomień z poziomu API
Aby wygenerować nowe powiadomienie należy skorzystać z wbudowanego systemu powiadomień w Laravel. Nowe powiadomienie 
generujemy poleceniem:
```bash
php artisan make:notification SomeNewNameNotification
```

Teraz do nowo wygenerowanego powiadomienia dodajemy dwie klasy z `NotificationChannels\WebPush`:
```php
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;
```

Dodajemy `WebPushChannel` jako jeden z metod przekazania powiadomienia (może być jedynym):
```php
public function via($notifiable)
{
    return ['mail', WebPushChannel::class];
}
```

Definiujemy teraz metodę `toWebPush` która zawiera definicję tego co przekażemy do PUSH:
```php
public function toWebPush($notifiable, $notification)
{
    return (new WebPushMessage)
        ->title(__('notifications.admin.mass_mail.intro').config('app.name'))
        ->body(__('notifications.admin.mass_mail.web_push'))
        ->data(['id' => $notification->id]);
}
```