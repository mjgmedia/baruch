# Styl kodowania
Trzymamy się standardów PSR-1 i PSR-12.

W skrócie:

* tylko tagi <?php ?>
* kodowanie znaków: UTF-8 bez BOM
* każdy plik powinien posiadać albo deklarakcje albo kod wykonawczy - NIE OBIE TE RZECZY
* nazwy klas pisane `StudlyCaps`
* każda klasa posiada odpowiedni namespace w nagłówku (zgodny z standardem PSR-0 i PSR-4)
* stałe w klasach pisane są `DRUKOWANYMI` literami.
* wszystkie metody mają nazwy pisane `camelCase()`
* wszystkie zmienne posiadają nazwy w zapisanie `camelCase`. Jeśli sytuacja jest krytyczna bądź ewidentnie chcemy coś 
odróżnić można użyć stylu `under_score`.
* wcięcia bazują na 4 spacjach, nie tabulatorze
* maksymalna długość linii to 120 znaków
* widoczność zmiennych w klasach musi być zdeklarowana
* w przypadku metod statycznych deklaracja widoczności *poprzedza* deklaracje `static`
* nawiasy otwierające ciało klasy i metod muszą zaczynać się w pustej linii zaraz po deklaracji lub ciele.
* nawiasy otwierające instrukcje/pętle/itd. muszą być w jednej linii, zamykające po ciele deklaracji
* nawiasy dla instrukcji i pętli nie mogą mieć spacji po (dla otwierającego) i przed (dla kończącego)
* wszystkie pliki muszą mieć Unix'owe zakończenie linii (LF)
* wszystkie pliki zawierające tylko kod PHP nie mogą mieć kończącego tagu
* wszystkie pliki PHP muszą kończyć jedną pustą linią
* wszystkie słowa kluczowe składni PHP muszą być pisane małymi literami
* wszystkie deklaracje typów zmiennych pisane zapisem skrótowym (tj. `bool` a nie `boolean`)
* po deklaracji `namespace` zawsze zostaje 1 pusta linia
* po deklaracjach bloku deklaracji `use` zawsze zostaje 1 linia pusta
* każdy plik z deklaracjami powinien zawierać otwierający komentarz blokowy
* przy inicjacji obiektu, przy nazwie klasy zawsze powinna być para nawiasów nawet jeśli konstruktor jest pusty 
(nie ma argumentów)
* słowa kluczowe `extends` i `implements` powinny być w jednej linii z nazwą klasy
* każdy importowany `trait` powinien mieć swoje słowo kluczowe `use`

Więcej informacji:
[PSR-1](https://www.php-fig.org/psr/psr-1/)
[PSR-12](https://www.php-fig.org/psr/psr-12/)

Dodatkowo:
* wszystkie wartości przekazywane w Requestach i FormRequestach są w formacie `under_score`.
* gdzie to tylko możliwe każda metoda w klasach typu Controller i Service powinna mieć deklaracje typów dla 
przyjmowanych argumentów