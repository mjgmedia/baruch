# Recaptcha

Sprawdzanie recaptchy odbywa się za pomocą własnego Rule: ReCaptcha (`/app/Rules/ReCaptcha`). Uruchamianie recaptchy we frontend obydwa się przy pomocy VueReCaptcha. Uruchomienie i pobranie tokena recaptchy odbywa się w każdym z `pages` uzywającym formularza i ReCaptchy.