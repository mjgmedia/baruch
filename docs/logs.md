# Log

Log oprócz standardowych ustawień Laravela dostępne są dodatkowe kanały logowania:

* Debug (daily)
* Mail (daily)
* Failed login attemps (daily)
* OAuth errors (slack)

Debug jest używany w kodzie, szczególnie w Listener, Command itp. 
Mail jest używany do przechwytywanie błędów związanych z wysyłką maili.
Failed login attemps wyłapuje nieudane próby logowania, pozwala to przeanalizować próby ataków na system logowania.
OAuth errors to błędy HTTP 500 przy logowaniu (system wypuszcza zapytanie do serwera OAuth - gdy ten zwróci błąd 500 
tworzony jest Event, a w Listenerze do niego tworzony jest wpis w tym logu).

## ld()

Zrzut dowolnej struktury danych do loga można łatwo dokonać za pomocą funkcji `ld()`.
Pochodzi ona z **spatie/laravel-log-dumper** ([więcej](https://github.com/spatie/laravel-log-dumper))