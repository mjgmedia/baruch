# Homestead
Laravel Homestead to biblioteka umożliwiająca łatwe i proste uruchomienie identycznego środowiska developerskiego. Znacznie ułatwia to dołączenie do zespołu programistycznego oraz testowanie aplikacji w takich samych warunkach.

### Oprogramowanie
Aby uruchomić Homestead na komputerze, potrzebnych jest kilka elementów:
* [Vagrant](https://www.vagrantup.com/downloads.html)
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

Dodatkowo na Windows należy uruchomić poniższą komendę w konsoli z uprawnieniami administracyjnymi:
```
bcdedit /set hypervisorlaunchtype off
```
Po uruchomieniu polecenia należy uruchomić ponownie komputer.

## Plik Homestead.yaml
Aby rozpocząć korzystanie z Homestead, należy skopiować domyślny plik z konfiguracją: `Homestead.default.yaml` do 
pliku `Homestead.yaml`. Można to zrobić poleceniem:
```bash
cp Homestead.default.yaml Homestead.yaml
```

Następnie w pliku `Homestead.yaml` należy zmienić ścieżkę z projektem na właściwą:
```yaml
folders:
    -
        map: /sciezka/dorepo/api
        to: /home/vagrant/code  
    -
        map: /sciezka/do/repo/ui
        to: /home/vagrant/app
        type: "rsync"
        options:
            rsync__args: ["--verbose", "--archive", "--delete", "-zz"]
            rsync__exclude: ["node_modules"]
```

W systemie Windows nie należy stosować skrótów i zmiennych systemowych, tylko podać pełną ścieżkę do folderu — używając jednak Unixowych slashy: 
```
C:/Users/mgostanski/PhpstormProjects/bootstrap_api
```

## Przygotowanie /etc/hosts
Aby był możliwy dostęp do naszego projektu przez przeglądarkę należy w systemie Windows edytować plik 
`C:Windows/system32/drivers/etc/hosts` lub zainstalować 
[Bonjour Print Services for Windows](https://support.apple.com/kb/DL999?viewlocale=en_US&locale=en_US).

Do pliku `hosts` należy dodać poniższe trzy linijki — przy czym pierwsza z nich może być już utworzona przez Homestead.
```
192.168.10.10  homestead.test
192.168.10.10  api.homestead.test
192.168.10.10  socket.homestead.test
```

Jeśli adres IP we wpisie automatycznie wygenerowanym przez Homestead się różni — nie zmieniaj go, co więcej użyj również w pozostałych wpisach.

## Korzystanie z Homestead
Korzystanie z Homestead sprowadza się do dwóch kroków. Pierwszy z nich to uruchomienie homestead. Czynimy to poleceniem: 
```bash
vagrant up
```

Uruchomienie trwa chwilę, szczególnie pierwsze, podczas którego będzie potrzebne hasło administratora. Każde kolejne uruchomienie jest szybsze. Homestead uruchamiamy tylko raz po uruchomieniu komputera!

Drugi krok to podłączenie się do ssh, co możemy uczynić za pomocą polecenia:
```bash
vagrant ssh
```

## Uruchomienie aplikacji frontendowej
Aby uruchomić budowanie aplikacji frontendowej (PWA) należy wpisać polecenie: `build-app`. Gdy się sfinalizuje, można uruchomić serwer ponownie poleceniami:
 ```bash
pm2 stop all
cd /home/vagrant/app
pm2 start npm -- start
```