# Sentry
Sentry to zewnętrzna usługa do przechwytywania informacji o błędach w kodzie. W RHC Bootstrap działa zarówno w API i UI. Dzięki temu błędy które zdarzają się użytkownikom można naprawić szybciej i prościej dzięki kompletowi informacji o błędzie.

Sentry jest bezpłatne do 5000 błędów/mc, 1 user i 30 dni historii błędów. W większości wypadków będzie to wystarczające. Inaczej można tworzyć osobnego usera dla każdego projektu.

Integracja z Sentry jest prosta. Dla API wystarczy wykorzystać gotową bibliotekę: `sentry-laravel`. Przesyłanie danych do Sentry zapewnia moedyfikacja Exception handlera (`app\Exceptions\Handler.php`):
```php
if (app()->bound('sentry') && $this->shouldReport($exception)) {
    app('sentry')->captureException($exception);
}
```

W pliku .env podajemy URL dla naszego projektu:
```
SENTRY_LARAVEL_DSN=https://<key>@sentry.io/<project>
```

Jeśli chcemy wyłączyć sentry, wystarczy usunąć zawartość stałej `SENTRY_LARAVEL_DSN`.

## Raportowanie złożone

Jednak czasami używamy innego Exception który ma zwrócić odpowiedź JSON, a sam nie zawiera informacji o błędzie. Wtedy możemy skorzystać z możliwości zbudowania własnego konstruktora w klasie `Exception` który jako zmienną przyjmie wyjątek który realnie powoduje problem. Następnie w metodzie `report()` używając helpera `sentrit()` przekazujemy realny wyjątek do rejestracji w Sentry a funkcją `render()` zwracamy odpowiedź JSON dla usera.

Przykład:
```php
class MassDeleteException extends Exception
{
    /**
     * @var Throwable
     */
    private $exception;

    /**
     * MassDeleteException constructor.
     * @param $exception
     */
    public function __construct(Throwable $exception)
    {
        parent::__construct();
        $this->exception = $exception;
    }

    /**
     *
     */
    public function report()
    {
        sentrit($this->exception);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json(
            [
                'message' => __('api.admin.users.mass.delete.fail'),
                'code' => 304,
            ],
            500
        );
    }

}
```