# Backup

Laravel-backup dba o backup całego projektu (node-modules i vendor) i bazy danych. Pliki są wyrzucane na skonfigurowany w config/filesystems.php system SFTP i Amazon S3. W pliku .env są odpowiednie zmienne do konfiguracji dostępu do zewnątrznego serwera SFTP oraz Amazon S3.

Konfiguracja systemu backupu mieści się łącznie w 3 miejscach:
* `config/backup.php` - tutaj ogólna konfiguracja systemu - jakie foldery, bazydanych itp.
* `config/database.php` - tutaj mamy możliwość dorzucić opcje dotyczące zrzutu bazy danych takie jak np. export tylko 
danych, export bez wybranych tabel, export objęty jedną transakcją itp.
* `.env` - tutaj w stałych podajemy wartości dla połączeń z serwerem SFTP i Amazon S3.

## Włącz/wyłącz dysk SFTP
Aby nie używać kopiowania na serwer SFTP wystarczy jak w stałej w pliku .env zostawisz **pustego stringa** - ale
 **nie** wartość null
```
BACKUP_SFTP_HOST=""
```

Uwaga: system nadal będzie traktował to jako punkt do backupu - więc nie zdziw się gdy dostaniesz powiadomienie na 
Slacku i mailu.
Gdy chcesz całkowicie to wyłączyć musisz edytować plik `config/backup.php`
```
'disks' => [
  's3',
  // zakomentuj linię jak poniżej
  // (env('BACKUP_SFTP_HOST') == '' || env('BACKUP_SFTP_HOST') == null ) ? '' : 'sftp',
],
```

## Backupy automatyczne
Backupy są uruchamiane z automatu poprzez wbudowany w Laravel Task Scheduler. Każdego dnia o 00:30 będzie uruchamiane czyszczenie dysków backupowych a o 01:00 rozpocznie się backup.