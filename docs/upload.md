# Upload plików
Do uploadu plików (obrazów) po stronie backendu wykorzystywany jest `UploadService`, który zawiera podstawowy kod odpowiadający za stworzenie obrazu z przesłanych danych. Jest on wykorzystywany przede wszystkim w User\AccountController przy uploadzie avatara użytkownika. `UploadService` metoda `handleUpload` oczekuje że podamy jej nazwę pliku, ścieżkę oraz dysk (Filesystem).

Te wartości dla funkcji `upload` w `AccountController` są podane w pliku .env
```dotenv
UPLOAD_AVATAR_DIR="avatar/"
UPLOAD_AVATAR_DISK="public"
```

Dla kolejnych funkcjonalności związanych z uploadem należy postąpić tak samo:
* wykorzystać UploadService 
* dodać kolejne wartości do `.env` i `config/project.php`