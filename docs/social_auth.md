# Socialite
Logowanie po zewnętrznych providerach, mając swoje logowanie na bazie OAuth jest nieco skomplikowane. Za całość tej czynności po stronie API odpowiada `Laravel Socialite` i `coderello/laravel-passport-social-grant`. Laravel Socialite generalnie obsługuje logowanie poprzez zewnętrznych providerów, ale potrzebny jest nowy rodzaj autoryzacji po stronie naszego serwera OAuth (Passport) żeby wykorzystać wyciągnięte od providera dane o użytkowniku - email, imię i nazwisko, email itp. wykorzystać do stworzenia usera w naszym systemie ponieważ wszystko odnosi się do usera. 

 Ważne jest to aby zarówno w pliku .env dla API jak i .env dla aplikacji PWA były te same klucze witryn. Callback jest obsługiwany przez aplikację PWA, która otrzymaną odpowiedź od providera przesyła do API.

Uwaga: Wszystkie stałe z URL'ami zakładają że stała z adresem serwisu nie mają kończącego slasha.

## Przebieg autoryzacji
1. Użytkownik klika przycisk na stronie. Pokazuje mu się osobne okno przeglądarki do zalogowania się u zewnętrznego providera (np. Google).
2. Po zalogowaniu, zwrócone są do API dane użytkownika
3. API sprawdza czy taki zewnętrzny token istnieje w bazie i zwraca 
podłączonego Usera z naszej bazy danych lub go tworzy na podstawie otrzymanych danych
4. API łączy się z serwerem OAuth w trybie (grant-type) social, podaje email i token od zewnętrznego providera.
5. Serwer OAuth w odpowiedzi wydaje access_token w naszym systemie
6. API zwraca uzyskany access_token do UI
7. UI przejmuje access_token i dalej zachowuje się jak podczas normalnego zalogowania.