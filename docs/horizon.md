# Horizon

Horizon jest domyślnie skonfigurowany na dostęp poprzez ścieżkę domyślnie: `/horizon`. Można ją łatwo zmienić w pliku `.env`. 

## Uruchomienie horizona:
```
php artisan horizon
```

Pomocne jest trzymanie horizona na tak zwanym screenie. `screen` to aplikacja w linuxie, która pozwala uruchomić dodatkową powłokę (bash/zsh/itd) którą można zostawić w stanie "zawieszenia". Ponieważ podczas normalnego połączenia ssh - każdy proces uruchomiony w terminalu jest automatycznie zabijany w chwili utracenia połączenia. Dlatego uruchomienie Horizona w screen zapewnia że będzie działał on cały czas.

**Stworzenie nowego screen**
```
screen -dmS nazwa_screena
```

> Jeśli na serwerze działa kilka różnych aplikacji web, najlepiej przyjąć następujący schemat: **nazwa_apki_usluga** 
> czyli np.`rhc_horizon`

Po utworzeniu screen powyższą komendą możesz nie zauważyć różnicy - screen już działa i jest w trybie odłączonym (`detached`). Teraz trzeba się do niego połączyć:
```
screen -r nazwa_screena
```

Teraz możesz uruchomić dolecowy proces. Wyjście z screena - czyli odłączenie go - uzyskuje się naciskając po sobie dwie kombinacje klawiszy:
`Ctrl+A` i `Ctrl+D`

**Lista aktywnych screen**

Czasami gdy screen jest kilka lub dawno ich nie używałeś - można łatwo zapomnieć nazwę. Poniższa komenda pokaże wszystkie aktywne screen:
```
screen -list
```

## Konfiguracja przy wielu instancjach Horizon
Gdy uruchamiasz kolejną instancję Horizon na serwerze jest potrzebnych kilka zmian w ustawieniach aby workery zadań w kolejce nie gryzły się ze sobą. Ustaw inny numer bazy danych do przechowywanie kolejki - `REDIS_QUEUE_DB`. Redis ma 16 baz danych (od 0 do 15). Zmień nazwę tagu jakim posługuje się horizon: `HORIZON_PREFIX` oraz domyslną nazwę kolejki: `REDIS_QUEUE`.

## Konfiguracja za pomocą supervisord:
[https://laravel.com/docs/6.x/horizon#deploying-horizon](https://laravel.com/docs/6.x/horizon#deploying-horizon)

## Pierwsze uruchomienie Horizona:
Przed pierwszym uruchomieniem horizona, należy uruchomić poniższe polecenie:
```
php artisan horizon:publish
```