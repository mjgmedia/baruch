# Swoole

Swoole to asynchroniczny framework dla PHP. Jest on napisany w C i dystrybuowany jako rozszerzenie do PHP instalowane przez `pecl`. Swoole jest przyrównywany do node.js dla PHP.

Integracja tego rozszerzenia z Laravelem odbywa się przez pakiet `laravel-swoole`. Jest on już dodany do `composer.json` zatem jest zainstalowany. Projekt używa automatycznego ładowania ServiceProviderów - zatem cała konfiguracja sprowadza się do przeanalizowania pliku `config/swoole_http.php`.

## Instalacja rozszerzenia
Przed pobrianiem paczek composerem, należy zainstalować rozszerzenie swoole poleceniem:
```
pecl install swoole
```

Następnie dodać ładowanie rozszerzenia do pliku konfiguracyjnego `php.ini`:
```
# Jeśli nie znasz lokalizacji pliku php.ini:
php -i | grep php.ini 

sudo echo "extension=swoole.so" >> php.ini

# Sprawdźmy czy działa:
php -m | grep swoole 
```

## Uruchomienie serwera Swoole

Uruchomienie jest bardzo proste:
```
php artisan swoole:http start
```

## Konfiguracja z nginx

Proponowana konfiguracja dla nginx (jako reverse-proxy):

```
gzip on;
gzip_min_length 1024;
gzip_comp_level 2;
gzip_types text/plain text/css text/javascript application/json application/javascript application/x-javascript application/xml application/x-httpd-php image/jpeg image/gif image/png font/ttf font/otf image/svg+xml;
gzip_vary on;
gzip_disable "msie6";

upstream swoole {
    server 127.0.0.1:5200 weight=5 max_fails=3 fail_timeout=30s;
    keepalive 16;
}
server {
    listen 80;
    server_name laravels.com;
    root /yourpath/laravel-s-test/public;
    access_log /yourpath/log/nginx/$server_name.access.log  main;
    autoindex off;
    index index.html index.htm;
  
    location / {
        try_files $uri @laravel;
    }
  
    location @laravel {
        # proxy_connect_timeout 60s;
        # proxy_send_timeout 60s;
        # proxy_read_timeout 120s;
        proxy_http_version 1.1;
        proxy_set_header Connection "";
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Real-PORT $remote_port;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header Scheme $scheme;
        proxy_set_header Server-Protocol $server_protocol;
        proxy_set_header Server-Name $server_name;
        proxy_set_header Server-Addr $server_addr;
        proxy_set_header Server-Port $server_port;
        
        proxy_pass http://swoole;
    }
}
```