# Broadcasting

Broadcasting to metoda na aktywną komunikacje z użytkownikiem korzystającego z frontendu. W backendzie obsługa sprowadza się do stworzenia Event'ów które mają zaimplementowany interfejs `ShouldBroadcast` oraz metodę `broadcastOn()` zawierającą informację na którym kanale powinien być nadany event. 

```php
public function broadcastOn()
{
    return new PrivateChannel('order.'.$this->update->order_id);
}
```

Event może być też wywołany wyłącznie z ograniczeniem jego transmisji do kanałów broadcastowych:

```php
event(new ShippingStatusUpdated($update));

broadcast(new ShippingStatusUpdated($update));
// bez usera który wywołuje funkcje
broadcast(new ShippingStatusUpdated($update))->toOthers();
```

Dodatkowo każdy kanał należy odpowiednio zabezpieczyć (lub nie) w pliku `routes/channels.php`:

```php
Broadcast::channel('order.{orderId}', function ($user, $orderId) {
    return $user->id === Order::findOrNew($orderId)->user_id;
});
```

Za transmisję danych poprzez websockets odpowiada **laravel-websockets**. Uruchomienie jest proste podobnie jak Horizona:
```bash
php artisan websockets:serve
```

Oczywiście w środowisku produkcyjnym trzeba to zapakować do supervisora: [instrukcja](https://docs.beyondco.de/laravel-websockets/1.0/basic-usage/starting.html#keeping-the-socket-server-running-with-supervisord)

## Zmiana nazwy eventu (odbieranego przez frontend)
Aby uprościć odbiór przez frontend można zastosować aliasy do oryginalnych nazw eventów:
```php
public function broadcastAs()
{
    return 'server.created';
}
```

## Kanały
Domyślnie w całym RHC Bootstrap są skonfigurowane dwa kanały dla userów z rolą **devs** - gdzie otrzymują informacje 
na temat błędu HTTP 500 zwróconego przez serwer OAuth lub nieudanych prób logowania. Całość jest połączona z 
uprawnieniami.

## Powiadomienia
Komunikację za pomocą websockets umożliwia nie tylko przesyłanie eventów ale również powiadomień. W klasie powiadomienia trzeba dodać metodę `toBroadcast()`.
Przykład:
```php
public function toBroadcast($notifiable)
{
    return new BroadcastMessage(
        [
            'code' => $this->event->code,
            'message' => $this->event->message,
        ]
    );
}
```

Oraz dodać metodę broadcast do tablicy zwracanej z funkcji `via()`.
```php
public function via($notifiable)
{
    return ['mail', 'broadcast', WebPushChannel::class];
}
```