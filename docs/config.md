# Konfiguracja

Całość konfiguracji jest dostępna w pliku `.env` - jest to specjalny zabieg, aby móc aktualizować projekty budowane na tym bootstrapie.
Konfiguracja jest podzielona na następujące części:

* Ustawienia apki
* Log
* Baza danych
* Cache
* Sesje
* Kolejka
* Horizon
* Broadcasting
* WebPush
* Mail serwer
* System plików
* Backup
* Socialite
* Recaptcha
* Sentry
* Telescope
* Telescope Watchers
* Admin
* Upload plików

## Redis

Konfiguracja używa trzech z 16 baz Redisa. Podział jest następujący:

* 0 - kolejka (`REDIS_QUEUE_DB`)
* 1 - cache DB (`REDIS_CACHE_DB`)
* 2 - sesje (`REDIS_SESSION_DB`)

## Horizon

Horizon jest domyślnie skonfigurowany na dostęp poprzez ścieżkę domyślnie: `/horizon`. Można ją łatwo zmienić w pliku `.env`. 

### Konfiguracja przy wielu instancjach Horizon
Gdy uruchamiasz kolejną instancję Horizon na serwerze jest potrzebnych kilka zmian w ustawieniach aby workery zadań w kolejce nie gryzły się ze sobą. Ustaw inny numer bazy danych do przechowywanie kolejki - `REDIS_QUEUE_DB`. Redis ma 16 baz danych (od 0 do 15). Zmień nazwę tagu jakim posługuje się horizon: `HORIZON_PREFIX` oraz domyslną nazwę kolejki: `REDIS_QUEUE`.

## System plików Amazon S3
Laravel wykorzystuje flysystem (`league/flysystem-aws-s3-v3`) do wykorzystywania S3 jako systemu plików który można 
wykorzystać jako storage np. do wrzucanych plików przez użytkowników.

```
AWS_ACCESS_KEY_ID=AKIAISZJCJURUFW7J7SA
AWS_SECRET_ACCESS_KEY=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
AWS_DEFAULT_REGION=eu-central-1
AWS_BUCKET=rhc-backup
# AWS_URL=
```

`AWS_ACCESS_KEY_ID` i `AWS_SECRET_ACCESS_KEY` te dwa klucze należy wygenerować w AWS Console. Kliknij na nazwę 
użytkownika w AWS Console, wybierz z menu *Your Security Credentials* i następnie rozwiń *Access keys (access key ID 
and secret access key)* i kliknij na przycisk *Create New Access Key*

`AWS_DEFAULT_REGION` należy ustawić zgodnie z regionem w którym isnieje bucket podany w `AWS_BUCKET`

## Domyślny storage
Domyślny filesystem do uzywania z facadem `Storage` ustawia się w pliku .env:
```
FILESYSTEM_DRIVER=public
```

