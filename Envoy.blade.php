@include('vendor/autoload.php');

@servers(['web' => 'localhost'])

@task('install')
    php artisan migrate
    php artisan passport:install
    php artisan webpush:vapid
    php artisan horizon:publish
    php artisan telescope:publish
@endtask

@task('dev')
    screen -dm php artisan websockets:serve
    screen -dm php artisan horizon
    screen -dm php artisan serve
@endtask