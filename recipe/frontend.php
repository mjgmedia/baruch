<?php

namespace Deployer;

require 'recipe/npm.php';

desc('Execute npm run production');
task('frontend:production', 'cd frontend && {{bin/npm}} run build');

desc('Execute npm install');
task('frontend:install', 'cd frontend && {{bin/npm}} install');
