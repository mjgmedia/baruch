<?php

namespace Tests\Factories;

use App\Models\User\Congregation;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;

/**
 * Class CongregationFactory
 * @package Tests\Factories
 */
class CongregationFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = Congregation::class;

    /**
     * @param array $extra
     * @return Congregation
     */
    public function create(array $extra = []): Congregation
    {
        return $this->build($extra);
    }

    /**
     * @param array $extra
     * @return Congregation
     */
    public function make(array $extra = []): Congregation
    {
        return $this->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'name' => $faker->company
        ];
    }

}
