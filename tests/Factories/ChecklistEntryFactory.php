<?php

namespace Tests\Factories;

use App\Models\Checklist\ChecklistEntry;
use App\Models\User\User;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;

/**
 * Class ChecklistEntryFactory
 * @package Tests\Factories
 */
class ChecklistEntryFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = ChecklistEntry::class;

    /**
     * @param array $extra
     * @return ChecklistEntry
     */
    public function create(array $extra = []): ChecklistEntry
    {
        return $this
            ->with(User::class, 'users', rand(1, 5))
            ->build($extra);
    }

    /**
     * @param array $extra
     * @return ChecklistEntry
     */
    public function make(array $extra = []): ChecklistEntry
    {
        return $this->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'name' => $faker->name,
            'desc' => $faker->text,
            'should_done_at' => now()->addDays($faker->numberBetween(1, 30)),
            'author_id' => UserFactory::new()->create()->id,
            'checklist_id' => ChecklistFactory::new()->create()->id,
        ];
    }

}
