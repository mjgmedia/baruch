<?php

namespace Tests\Factories;

use App\Models\File\FileAccess;
use App\Models\Happening\Happening;
use App\Models\Team\Team;
use App\Models\User\User;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;

/**
 * Class FileAccessFactory
 * @package Tests\Factories
 */
class FileAccessFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = FileAccess::class;

    /**
     * @param array $extra
     * @return FileAccess
     */
    public function create(array $extra = []): FileAccess
    {
        $randomSwitcher = rand(1, 3);
        $className = null;
        $relationName = null;

        switch ($randomSwitcher) {
            case 1:
                $className = User::class;
                $relationName = 'user';
                break;
            case 2:
                $className = Team::class;
                $relationName = 'team';
                break;
            case 3:
                $className = Happening::class;
                $relationName = 'happening';
                break;
        }

        return $this
            ->with($className, $relationName)
            ->build($extra);
    }

    /**
     * @param array $extra
     * @return FileAccess
     */
    public function make(array $extra = []): FileAccess
    {
        return $this->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'file_id' => FileFactory::new()->create()->id,
        ];
    }

}
