<?php

namespace Tests\Factories;

use App\Models\File\FileCategory;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;

/**
 * Class FileCategoryFactory
 * @package Tests\Factories
 */
class FileCategoryFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = FileCategory::class;

    /**
     * @param array $extra
     * @return FileCategory
     */
    public function create(array $extra = []): FileCategory
    {
        return $this->build($extra);
    }

    /**
     * @param array $extra
     * @return FileCategory
     */
    public function make(array $extra = []): FileCategory
    {
        return $this->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'name' => $faker->name,
            'global' => $faker->numberBetween(0, 1),
            'author_id' => UserFactory::new()->create()->id,
        ];
    }

}
