<?php

namespace Tests\Factories;

use App\Models\Happening\HappeningPostComment;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;

/**
 * Class lHappeningPostCommentFactory
 * @package Tests\Factories
 */
class HappeningPostCommentFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = HappeningPostComment::class;

    /**
     * @param array $extra
     * @return HappeningPostComment
     */
    public function create(array $extra = []): HappeningPostComment
    {
        return $this->build($extra);
    }

    /**
     * @param array $extra
     * @return HappeningPostComment
     */
    public function make(array $extra = []): HappeningPostComment
    {
        return $this->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'text' => $faker->text,
            'post_id' => HappeningPostFactory::new()->create()->id,
            'author_id' => UserFactory::new()->create()->id,
        ];
    }

}
