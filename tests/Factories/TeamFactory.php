<?php

namespace Tests\Factories;

use App\Models\Team\Team;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;

/**
 * Class TeamFactory
 * @package Tests\Factories
 */
class TeamFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = Team::class;

    /**
     * @param array $extra
     * @return Team
     */
    public function create(array $extra = []): Team
    {
        return $this->build($extra);
    }

    /**
     * @param array $extra
     * @return Team
     */
    public function make(array $extra = []): Team
    {
        return $this->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'name' => $faker->name,
            'avatar' => $faker->imageUrl(),
        ];
    }

}
