<?php

namespace Tests\Factories;

use App\Models\Chat\ChatChannel;
use App\Models\Happening\Happening;
use App\Models\Team\Team;
use App\Models\User\User;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/**
 * Class ChatChannelFactory
 * @package Tests\Factories
 */
class ChatChannelFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = ChatChannel::class;

    /**
     * @param array $extra
     * @return ChatChannel
     */
    public function create(array $extra = []): ChatChannel
    {
        $extras = rand(1, 100) > 50 ? ['class' => Team::class, 'relation' => 'team'] : [
            'class' => Happening::class,
            'relation' => 'happening'
        ];
        return $this
            ->with($extras['class'], $extras['relation'])
            ->with(User::class, 'users', rand(1, 5))
            ->build($extra);
    }

    /**
     * @param array $extra
     * @return ChatChannel
     */
    public function make(array $extra = []): ChatChannel
    {
        return $this->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'name' => $faker->name,
            'slug' => Str::slug($faker->name),
            'author_id' => UserFactory::new()->create()->id,
        ];
    }

}
