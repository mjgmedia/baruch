<?php

namespace Tests\Factories;

use App\Models\User\Profile;
use App\Models\User\User;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/**
 * Class UserFactory
 * @package Tests\Factories
 */
class UserFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = User::class;

    /**
     * @param array $extra
     * @return User
     */
    public function create(array $extra = []): User
    {
        return $this
            ->withFactory(ProfileFactory::new(), 'profile')
            ->build($extra);
    }

    /**
     * @param array $extra
     * @return User
     */
    public function make(array $extra = []): User
    {
        return $this
            ->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'name' => $faker->name,
            'username' => $faker->unique()->userName,
            'email' => $faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => 'password', // password
            'last_activity' => now(),
            'remember_token' => Str::random(10),
        ];
    }

}
