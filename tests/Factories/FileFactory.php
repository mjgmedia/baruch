<?php

namespace Tests\Factories;

use App\Models\File\File;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;

/**
 * Class FileFactory
 * @package Tests\Factories
 */
class FileFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = File::class;

    /**
     * @param array $extra
     * @return File
     */
    public function create(array $extra = []): File
    {
        return $this->build($extra);
    }

    /**
     * @param array $extra
     * @return File
     */
    public function make(array $extra = []): File
    {
        return $this->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'name' => $faker->name,
            'filename' => $faker->imageUrl(),
            'desc' => $faker->text,
            'global' => $faker->numberBetween(0, 1),
            'author_id' => UserFactory::new()->create()->id,
            'file_category_id' => FileCategoryFactory::new()->create()->id,
        ];
    }

}
