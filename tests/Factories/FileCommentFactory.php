<?php

namespace Tests\Factories;

use App\Models\File\FileComment;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;

/**
 * Class FileCommentFactory
 * @package Tests\Factories
 */
class FileCommentFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = FileComment::class;

    /**
     * @param array $extra
     * @return FileComment
     */
    public function create(array $extra = []): FileComment
    {
        return $this->build($extra);
    }

    /**
     * @param array $extra
     * @return FileComment
     */
    public function make(array $extra = []): FileComment
    {
        return $this->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'comment' => $faker->text,
            'file_id' => FileFactory::new()->create()->id,
            'author_id' => UserFactory::new()->create()->id,
        ];
    }

}
