<?php

namespace Tests\Factories;

use App\Models\Happening\Happening;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;

/**
 * Class HappeningFactory
 * @package Tests\Factories
 */
class HappeningFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = Happening::class;

    /**
     * @param array $extra
     * @return Happening
     */
    public function create(array $extra = []): Happening
    {
        return $this->build($extra);
    }

    /**
     * @param array $extra
     * @return Happening
     */
    public function make(array $extra = []): Happening
    {
        return $this->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'name' => $faker->name,
            'close_after_end' => $faker->numberBetween(0, 1),
            'image_url' => $faker->imageUrl(),
            'date_access' => now(),
            'date_start' => now()->addMonth(1),
            'date_end' => now()->addMonth(1),
        ];
    }

}
