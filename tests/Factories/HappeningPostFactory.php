<?php

namespace Tests\Factories;

use App\Models\Happening\HappeningPost;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;

/**
 * Class HappeningPostFactory
 * @package Tests\Factories
 */
class HappeningPostFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = HappeningPost::class;

    /**
     * @param array $extra
     * @return HappeningPost
     */
    public function create(array $extra = []): HappeningPost
    {
        return $this->build($extra);
    }

    /**
     * @param array $extra
     * @return HappeningPost
     */
    public function make(array $extra = []): HappeningPost
    {
        return $this->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'text' => $faker->text,
            'happening_id' => HappeningFactory::new()->create()->id,
            'author_id' => UserFactory::new()->create()->id,
        ];
    }

}
