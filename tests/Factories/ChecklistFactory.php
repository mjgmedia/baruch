<?php

namespace Tests\Factories;

use App\Models\Checklist\Checklist;
use App\Models\Team\Team;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;

/**
 * Class ChecklistFactory
 * @package Tests\Factories
 */
class ChecklistFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = Checklist::class;

    /**
     * @param array $extra
     * @return Checklist
     */
    public function create(array $extra = []): Checklist
    {
        if (rand(1, 100) > 50) {
            return $this
                ->with(Team::class, 'team')
                ->build($extra);
        }

        return $this->build($extra);
    }

    /**
     * @param array $extra
     * @return Checklist
     */
    public function make(array $extra = []): Checklist
    {
        return $this->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'name' => $faker->name,
            'type' => $faker->numberBetween(1, 2),
            'pattern' => $faker->numberBetween(0, 1),
            'execution_date' => now()->addDays($faker->numberBetween(1, 30)),
            'author_id' => UserFactory::new()->create()->id,
            'happening_id' => HappeningFactory::new()->create()->id,
        ];
    }

}
