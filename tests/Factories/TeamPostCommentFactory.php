<?php

namespace Tests\Factories;

use App\Models\Team\TeamPostComment;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;

/**
 * Class TeamPostCommentFactory
 * @package Tests\Factories
 */
class TeamPostCommentFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = TeamPostComment::class;

    /**
     * @param array $extra
     * @return TeamPostComment
     */
    public function create(array $extra = []): TeamPostComment
    {
        return $this->build($extra);
    }

    /**
     * @param array $extra
     * @return TeamPostComment
     */
    public function make(array $extra = []): TeamPostComment
    {
        return $this->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'text' => $faker->text,
            'author_id' => UserFactory::new()->create()->id,
            'post_id' => TeamPostFactory::new()->create()->id,
        ];
    }

}
