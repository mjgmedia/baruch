<?php

namespace Tests\Factories;

use App\Models\Chat\ChatMessage;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;

/**
 * Class ChatMessageFactory
 * @package Tests\Factories
 */
class ChatMessageFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = ChatMessage::class;

    /**
     * @param array $extra
     * @return ChatMessage
     */
    public function create(array $extra = []): ChatMessage
    {
        return $this->build($extra);
    }

    /**
     * @param array $extra
     * @return ChatMessage
     */
    public function make(array $extra = []): ChatMessage
    {
        return $this->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'message' => $faker->text,
            'author_id' => UserFactory::new()->create()->id,
            'chat_channel_id' => ChatChannelFactory::new()->create()->id,
        ];
    }

}
