<?php

namespace Tests\Factories;

use App\Models\User\Profile;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;

/**
 * Class ProfileFactory
 * @package Tests\Factories
 */
class ProfileFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = Profile::class;

    /**
     * @param array $extra
     * @return Profile
     */
    public function create(array $extra = []): Profile
    {
        return $this->build($extra);
    }

    /**
     * @param array $extra
     * @return Profile
     */
    public function make(array $extra = []): Profile
    {
        return $this->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'phone' => $faker->isbn10,
            'address' => $faker->streetAddress,
            'city' => $faker->city,
            'postal_code' => $faker->postcode,
        ];
    }

}
