<?php

namespace Tests\Factories;

use App\Models\Team\TeamPost;
use Christophrumpel\LaravelFactoriesReloaded\BaseFactory;
use Faker\Generator as Faker;

/**
 * Class TeamPostFactory
 * @package Tests\Factories
 */
class TeamPostFactory extends BaseFactory
{
    /**
     * @var string
     */
    protected string $modelClass = TeamPost::class;

    /**
     * @param array $extra
     * @return TeamPost
     */
    public function create(array $extra = []): TeamPost
    {
        return $this->build($extra);
    }

    /**
     * @param array $extra
     * @return TeamPost
     */
    public function make(array $extra = []): TeamPost
    {
        return $this
            ->build($extra, 'make');
    }

    /**
     * @param Faker $faker
     * @return array
     */
    public function getDefaults(Faker $faker): array
    {
        return [
            'text' => $faker->text,
            'author_id' => UserFactory::new()->create()->id,
            'team_id' => TeamFactory::new()->create()->id,
        ];
    }

}
