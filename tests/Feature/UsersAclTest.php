<?php

use App\Models\User\User;
use Laravel\Passport\Passport;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

beforeEach(function () {
    $this->user = factory(User::class)->create();
    Passport::actingAs($this->user);
});

test('get all permissions', function () {
    $this->user->assignRole('superadmin');
    $response = $this->json('GET', route('admin.users.acl.permissions'));
    // $response->dump();
    $response->assertStatus(200);
});

test('get all roles', function () {
    $this->user->assignRole('superadmin');
    $response = $this->json('GET', route('admin.users.acl.roles'));
    // $response->dump();
    $response->assertStatus(200);
});

test('sync permissions', function () {
    $this->user->assignRole('superadmin');
    $perms = Permission::inRandomOrder()->get()->random(4)->pluck('name');
    $response = $this->json(
        'POST',
        route('admin.users.mass.permissions', ['id' => $this->user->id]),
        [
            'to_sync' => $perms
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'data',
            ]
        );
});

test('sync roles', function () {
    $this->user->assignRole('superadmin');
    $roles = Role::inRandomOrder()->get()->random(2)->pluck('name');
    $response = $this->json(
        'POST',
        route('admin.users.mass.roles', ['id' => $this->user->id]),
        [
            'to_sync' => $roles
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'data',
            ]
        );
});