<?php

use App\Models\User\User;
use Laravel\Passport\Passport;

beforeEach(function () {
    $this->user = factory(User::class)->create();
    Passport::actingAs($this->user);
});

test('mass activate', function () {
    $this->user->assignRole('admin');
    $users = User::inRandomOrder()->get()->random(4)->pluck('id');
    $response = $this->json(
        'POST',
        route('admin.users.mass.activate'),
        [
            'ids' => $users
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'message',
                'code',
                'updated',
            ]
        );
});

test('mass deactivate', function () {
    $this->user->assignRole('admin');
    $users = User::inRandomOrder()->get()->random(4)->pluck('id');
    $response = $this->json(
        'POST',
        route('admin.users.mass.deactivate'),
        [
            'ids' => $users
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'message',
                'code',
                'updated'
            ]
        );
});

test('mass delete', function () {
    $this->user->assignRole('admin');
    factory(User::class, 5)->create();
    $users = User::inRandomOrder()->get()->random(4)->pluck('id');
    $response = $this->json(
        'POST',
        route('admin.users.mass.delete'),
        [
            'ids' => $users
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'message',
                'code',
            ]
        );
});

test('mass email', function () {
    $this->user->assignRole('superadmin');
    $users = User::inRandomOrder()->get()->random(4)->pluck('id');
    $response = $this->json(
        'POST',
        route('admin.users.mass.mail'),
        [
            'subject' => $this->faker->sentence(6),
            'message' => $this->faker->paragraph(10),
            'ids' => $users
        ]
    );

//         $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'message',
                'code',
            ]
        );
});