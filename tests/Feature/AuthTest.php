<?php

use App\Models\User\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;

uses(WithoutMiddleware::class);

test('try to register with already used username', function () {
    $user = factory(User::class)->create();

    $response = $this->json(
        'POST',
        route('auth.register'),
        [
            'name' => $user->name,
            'username' => $user->username,
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password',
            'role' => '',
            'token' => '',
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(422)
        ->assertJsonStructure(
            [
                'message',
                'errors',
            ]
        );
});

test('try to register with already taken email', function () {
    $user = factory(User::class)->create();

    $response = $this->json(
        'POST',
        route('auth.register'),
        [
            'name' => $this->faker->name,
            'username' => $this->faker->username,
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password',
            'role' => '',
            'token' => '',
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(422)
        ->assertJsonStructure(
            [
                'message',
                'errors',
            ]
        );
});

test ('try to register user', function () {
    $response = $this->json(
        'POST',
        route('auth.register'),
        [
            'name' => $this->faker->name,
            'username' => $this->faker->unique()->userName,
            'email' => $this->faker->unique()->safeEmail,
            'password' => 'password',
            'password_confirmation' => 'password',
            'role' => '',
            'token' => '',
        ]
    );

//         $response->dump();

    $response
        ->assertStatus(201)
        ->assertJsonStructure(
            [
                'message',
                'code',
            ]
        );
});

test('try to activate user (correct code)', function () {
    $user = factory(User::class)->create();
    $code = Str::uuid();
    $user->activation_code = $code;
    $user->email_verified_at = null;
    $user->save();

    $response = $this->json(
        'POST',
        route('auth.activate'),
        ['code' => $code]
    );

    //$response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'message',
                'code',
            ]
        );
});

test('try to activate user with incorrect code', function () {
    $user = factory(User::class)->create();
    $code = Str::uuid();
    $user->activation_code = $code;
    $user->email_verified_at = null;
    $user->save();

    $response = $this->json(
        'POST',
        route('auth.activate'),
        ['code' => $this->faker->numberBetween(11100,99999)]
    );

    //$response->dump();

    $response
        ->assertStatus(422)
        ->assertJsonStructure(
            [
                'message',
                'errors',
            ]
        );
});

test('ask about forgotten password', function () {
    $user = factory(User::class)->create();

    $response = $this->json(
        'POST',
        route('auth.forgotpassword'),
        [
            'username' => $user->email,
            'token' => '',
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'message',
                'code',
            ]
        );
});

test('ask about forgotten password with wrong email', function () {
    $response = $this->json(
        'POST',
        route('auth.forgotpassword'),
        [
            'username' => '',
            'token' => '',
        ]
    );
//    dump($response);

//     $response->dump();

    $response
        ->assertStatus(422)
        ->assertJsonStructure(
            [
                'message',
                'errors',
            ]
        );
});

test('reset password with correct token', function () {
    $user = factory(User::class)->create();
    $broker = Password::broker();
    $token = $broker->createToken($user);

    $response = $this->json(
        'POST',
        route('auth.resetpassword'),
        [
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password',
            'token' => $token,
            'recaptcha' => '',
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'access_token',
                'refresh_token',
                'expires_in',
                'token_type',
            ]
        );
});

test('reset password with incorrect token', function () {
    $user = factory(User::class)->create();
    $broker = Password::broker();
    $token = $broker->createToken($user);

    $response = $this->json(
        'POST',
        route('auth.resetpassword'),
        [
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password',
            'token' => 'hubabuha',
            'recaptcha' => '',
        ]
    );

    //$response->dump();

    $response
        ->assertStatus(408)
        ->assertJsonStructure(
            [
                'message',
                'code'
            ]
        );
});

test('login', function () {
    $user = factory(User::class)->create();
    $user->email_verified_at = now();
    $user->save();

    $response = $this->json(
        'POST',
        route('auth.login'),
        [
            'username' => $user->username,
            'password' => 'password',
            'token' => '',
        ]
    );

//        $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'access_token',
                'refresh_token',
                'expires_in',
                'token_type',
            ]
        );
});

test('try to login with wrong username', function () {
    $user = factory(User::class)->create();
    $user->email_verified_at = now();
    $user->save();

    $response = $this->json(
        'POST',
        route('auth.login'),
        [
            'username' => 'kupa',
            'password' => 'password',
            'token' => '',
        ]
    );

    //$response->dump();

    $response
        ->assertStatus(422)
        ->assertJsonStructure(
            [
                'message',
                'errors'
            ]
        );
});

test('try to login with wrong password', function () {
    $user = factory(User::class)->create();
    $user->email_verified_at = now();
    $user->save();

    $response = $this->json(
        'POST',
        route('auth.login'),
        [
            'username' => $user->username,
            'password' => 'wrongpassword',
            'token' => '',
        ]
    );

//        $response->dump();

    $response
        ->assertStatus(422)
        ->assertJsonStructure(
            [
                'message',
                'code'
            ]
        );
});

test('refresh token', function () {
    $user = factory(User::class)->create();

    $response = $this->json(
        'POST',
        route('auth.login'),
        [
            'username' => $user->username,
            'password' => 'password',
            'token' => '',
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'access_token',
                'refresh_token',
                'expires_in',
                'token_type',
            ]
        );

    $arrData = json_decode($response->getContent());

    $response2 = $this->withHeaders(
        [
            'Authorization' => 'Bearer ' . $arrData->access_token,
        ]
    )
        ->json(
            'POST',
            route('auth.refresh'),
            [
                'refresh_token' => $arrData->refresh_token,
            ]
        );

//         $response2->dump();

    $response2
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'access_token',
                'refresh_token',
                'expires_in',
                'token_type',
            ]
        );
});