<?php

use App\Exceptions\Chat\ChannelUserDetachException;
use App\Models\Chat\ChatMessage;
use App\Models\User\User;
use App\Traits\Chat\DeleteFromChat;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Arr;
use Tests\Factories\ChatChannelFactory;
use Tests\Factories\ChatMessageFactory;

class TestDeleteFromChat
{
    use DeleteFromChat;

    public function deleteChatChannel(int $id, string $field=null): int
    {
        return $this->_deleteChatChannel($id,$field);
    }

    public function deleteFromChat(int $id, array $userIds, string $field=null): array
    {
        return $this->_deleteFromChat($id, $userIds,$field);
    }
}

uses(RefreshDatabase::class);

beforeEach(function () {
    $this->obj = new TestDeleteFromChat();
    $this->chatChannel = ChatChannelFactory::new()->create();
});

it('checking _deleteFromChannel really removes it', function () {
    $this->obj->deleteChatChannel($this->chatChannel->id);

    $this->assertDeleted($this->chatChannel);
});

it('checking _deleteFromChannel throws Exception', function () {
    $this->chatChannel->delete();
    try {
        $this->obj->deleteChatChannel($this->chatChannel->id);
        $this->assertTrue(false);
    } catch (\Exception $exception) {
        $this->assertTrue(true);
    }
});

it ('checking _deleteFromChat detaching users from channel', function () {
    $users = $this->chatChannel->users()->pluck('user_id')->split(2)->first()->toArray();

    $this->obj->deleteFromChat($this->chatChannel->id, $users);

    $this->chatChannel->refresh();
    $actualUsers = $this->chatChannel->users()->pluck('user_id')->toArray();

    $this->assertEquals(false, Arr::has($actualUsers, $users));
});

it('checking _deleteFromChat throws Exception when something goes wrong on users detach', function () {
    $users = $this->chatChannel->users()->pluck('user_id')->split(2)->first()->toArray();

    $this->obj->deleteFromChat($this->chatChannel->id, $users);

    try {
        $this->obj->deleteFromChat($this->chatChannel->id, $users);
        $this->assertTrue(false);
    } catch (ChannelUserDetachException $exception) {
        $this->assertTrue(true);
    }
});

it ('checking  _deleteFromChat detaching users from messages', function () {
    $messages = ChatMessageFactory::new()
        ->times(10)
        ->create(['chat_channel_id' => $this->chatChannel->id]);

    $users = $this->chatChannel->users()->pluck('user_id')->toArray();

    $messages->each(function ($item) use ($users) {
         $item->users()->attach($users);
    });

    $this->obj->deleteFromChat($this->chatChannel->id, $users);

    $messages->each(function ($item) use ($users) {
       $this->assertEquals(false, Arr::has(
           $item->users()->pluck('user_id')->toArray(), $users
       ));
    });
});

it('checking _deleteFromChat throws Exception when something goes wrong on message detach', function () {
    ChatMessageFactory::new()
        ->times(10)
        ->create(['chat_channel_id' => $this->chatChannel->id]);

    $users = $this->chatChannel->users->pluck('user_id')->toArray();

    try {
        $this->obj->deleteFromChat($this->chatChannel->id, $users);
        $this->assertTrue(false);
    } catch (ChannelUserDetachException $exception) {
        $this->assertTrue(true);
    }
});

it ('checking _deleteFromChat removes users messages', function () {
    $messages = ChatMessageFactory::new()
        ->times(10)
        ->create(['chat_channel_id' => $this->chatChannel->id]);

    $users = $this->chatChannel->users()->pluck('user_id')->toArray();

    $user = User::find($users[0]);
    $userMessage = $messages->first();
    $userMessage->author()->associate($user);
    $userMessage->save();

    $messages->each(function ($item) use ($users) {
        $item->users()->attach($users);
    });

    $this->obj->deleteFromChat($this->chatChannel->id, $users);

    $this->assertDeleted($userMessage);
});

it ('checking _deleteFromChat omit message delete part when no', function () {
    $messages = ChatMessageFactory::new()
        ->times(10)
        ->create(['chat_channel_id' => $this->chatChannel->id]);

    $users = $this->chatChannel->users()->pluck('user_id')->toArray();

    $user = User::find($users[0]);
    $userMessage = $messages->first();
    $userMessage->author()->associate($user);

    $messages->each(function ($item) use ($users) {
        $item->users()->attach($users);
    });

    $this->obj->deleteFromChat($this->chatChannel->id, $users);

    $this->assertEquals(true, ChatMessage::query()->where('id', $userMessage->id)->count() > 0);
});

it ('checking _deleteFromChat returns array of statuses', function () {
    $messages = ChatMessageFactory::new()
        ->times(10)
        ->create(['chat_channel_id' => $this->chatChannel->id]);

    $users = $this->chatChannel->users()->pluck('user_id')->toArray();

    $user = User::find($users[0]);
    $userMessage = $messages->first();
    $userMessage->author()->associate($user);
    $userMessage->save();

    $messages->each(function ($item) use ($users) {
        $item->users()->attach($users);
    });

    $status = $this->obj->deleteFromChat($this->chatChannel->id, $users);

    $this->assertEquals(true, count($status) > 2);
});