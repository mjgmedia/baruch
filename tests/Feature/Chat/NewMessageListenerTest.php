<?php

use App\Events\Chat\NewMessageEvent;
use App\Jobs\Chat\PushUnreadMessageJob;
use App\Listeners\Chat\NewMessageListener;
use Illuminate\Support\Facades\Queue;
use Tests\Factories\ChatMessageFactory;
use Tests\Factories\UserFactory;

beforeEach(function () {
    $this->chatMessage = ChatMessageFactory::new()->create();
    $users = UserFactory::new()->times(10)->create();
    $this->chatMessage->users()->sync($users->pluck('id'));

    $this->event = new NewMessageEvent($this->chatMessage, 'test');
    $this->listener = new NewMessageListener();
});

it('test NewMessageListener is releasing any Jobs', function () {
    Queue::fake();
    Queue::assertNothingPushed();

    $this->listener->handle($this->event);

    $chatMessage = $this->chatMessage;
    Queue::assertPushed(function (PushUnreadMessageJob $job) use ($chatMessage) {
        return $job->message->id === $chatMessage->id;
    });
});

it('test NewMessageListener is releasing Job with proper delay', function () {
    Queue::fake();
    Queue::assertNothingPushed();

    $this->listener->handle($this->event);

    $chatMessage = $this->chatMessage;
    Queue::assertPushed(function (PushUnreadMessageJob $job) use ($chatMessage) {
        return $job->message->id === $chatMessage->id && $job->delay === config('project.chat.push_unread_delay');
    });
});

