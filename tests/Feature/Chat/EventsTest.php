<?php

use App\Events\Chat\DeletedMessageEvent;
use App\Events\Chat\NewMessageEvent;
use App\Events\Chat\UpdatedMessageEvent;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Support\Facades\Event;
use Tests\Factories\ChatMessageFactory;

it(
    'test DeletedMessageEvent',
    function () {
        Event::fake();

        $event = new DeletedMessageEvent(1, 'test');
        event($event);

        $this->assertEquals($event->deletedId, 1);
        $this->assertEquals($event->channelName, 'test');
        $this->assertEquals($event->broadcastOn(), new PrivateChannel('test'));

        Event::assertDispatched(
            function (DeletedMessageEvent $event) {
                return $event->deletedId === 1 && $event->channelName === 'test';
            }
        );
    }
);

it(
    'test NewMessageEvent',
    function () {
        Event::fake();

        $chatMessage = ChatMessageFactory::new()->create();

        $event = new NewMessageEvent($chatMessage, 'test');
        event($event);

        $this->assertEquals($event->message, $chatMessage);
        $this->assertEquals($event->channelName, 'test');
        $this->assertEquals($event->broadcastOn(), new PrivateChannel('test'));

        Event::assertDispatched(
            function (NewMessageEvent $event) use ($chatMessage) {
                return $event->message === $chatMessage && $event->channelName === 'test';
            }
        );
    }
);

it(
    'test UpdatedMessageEvent',
    function () {
        Event::fake();

        $chatMessage = ChatMessageFactory::new()->create();

        $event = new UpdatedMessageEvent($chatMessage, 'test');
        event($event);

        $this->assertEquals($event->message, $chatMessage);
        $this->assertEquals($event->channelName, 'test');
        $this->assertEquals($event->broadcastOn(), new PrivateChannel('test'));

        Event::assertDispatched(
            function (UpdatedMessageEvent $event) use ($chatMessage) {
                return $event->message === $chatMessage && $event->channelName === 'test';
            }
        );
    }
);