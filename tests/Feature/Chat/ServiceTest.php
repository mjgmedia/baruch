<?php

use App\Models\User\User;
use App\Services\ChatService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Factories\ChatChannelFactory;
use Tests\Factories\ChatMessageFactory;
use Tests\Factories\UserFactory;

uses(RefreshDatabase::class);

beforeEach(function () {
    $this->obj = new ChatService();
    $this->chatChannel = ChatChannelFactory::new()->create();
});

it('checking ChatService addToChat returns proper array', function () {
    $message = ChatMessageFactory::new()->create();
    $users = UserFactory::new()->times(10)->create();

    $returned = $this->obj->addToChat($users->pluck('id')->toArray(), $message->channel->id);

    $this->assertGreaterThan(0, count($returned));
    $this->assertEquals(true, is_array($returned));
});

it ('checking ChatService removeFromChat returns proper array', function () {
    $messages = ChatMessageFactory::new()
        ->times(10)
        ->create(['chat_channel_id' => $this->chatChannel->id]);

    $users = $this->chatChannel->users()->pluck('user_id')->toArray();

    $user = User::find($users[0]);
    $userMessage = $messages->first();
    $userMessage->author()->associate($user);
    $userMessage->save();

    $messages->each(function ($item) use ($users) {
        $item->users()->attach($users);
    });

    $status = $this->obj->removeFromChat($users, $this->chatChannel->id);

    $this->assertEquals(true, count($status) > 2);
});