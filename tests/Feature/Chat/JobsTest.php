<?php

use App\Jobs\Chat\PushUnreadMessageJob;
use Illuminate\Support\Facades\Queue;
use Tests\Factories\ChatMessageFactory;

it(
    'test PushUnreadMessageJob is pushed',
    function () {
        Queue::fake();
        Queue::assertNothingPushed();

        $chatMessage = ChatMessageFactory::new()->create();

        PushUnreadMessageJob::dispatch($chatMessage);

        Queue::assertPushed(PushUnreadMessageJob::class);
    });

