<?php

use App\Models\Chat\ChatChannel;
use App\Traits\Chat\GetChannel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use  Tests\Factories\ChatChannelFactory;

class TestGetChannel
{
    use GetChannel;

    public function getChannel(int $id, ?string $field=null)
    {
        return $this->_getChannel($id, $field);
    }
}

uses(RefreshDatabase::class);

beforeEach(function () {
    $this->obj = new TestGetChannel();
    $this->channel = ChatChannelFactory::new()->create();
});

it('testing ChannelNotFoundException will occur when no channel would be found in GetChannel', function () {
    try {
        $this->obj->getChannel($this->channel->id+1);
        $this->assertTrue(false);
    } catch (\Exception $exception) {
        $this->assertTrue(true);
    }
});

it('testing GetChannel return ChatChannel object', function () {
    $test = $this->obj->getChannel($this->channel->id);

    $this->assertEquals(ChatChannel::class, get_class($test));
});

it('testing GetChannel returns object according to specified field', function () {
    $test = $this->obj->getChannel($this->channel->author_id, 'author_id');

    $this->assertEquals(ChatChannel::class, get_class($test));
    $this->assertEquals($this->channel->author_id, $test->author_id);
});