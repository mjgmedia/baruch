<?php

use App\Traits\Chat\AddToChat;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Factories\ChatChannelFactory;
use Tests\Factories\ChatMessageFactory;
use Tests\Factories\UserFactory;

class TestAddToChat
{
    use AddToChat;

    public function addToChannel(array $usersIds, int $id, string $field=null): array
    {
        return $this->_addToChannel($usersIds, $id, $field);
    }

    public function addToMessages(array $usersIds, int $id, string $field=null): array
    {
        return $this->_addToMessages($usersIds, $id, $field);
    }
}

uses(RefreshDatabase::class);

beforeEach(function () {
    $this->obj = new TestAddToChat();
});

it ('checking _addToUser() really attaches users to ChatChannel', function () {
    $channel = ChatChannelFactory::new()->create();
    $users = UserFactory::new()->times(10)->create();

    $beforeIds = $channel->users->pluck('user_id');

    $this->obj->addToChannel($users->pluck('id')->toArray(), $channel->id);

    $channel->refresh();
    $afterIds = $channel->users()->pluck('user_id');

    $this->assertGreaterThan($beforeIds->count(), $afterIds->count());
});

it ('checking _addToUser() throw Exception when nothing changed on users attach', function () {
    $channel = ChatChannelFactory::new()->create();

    try {
        $this->obj->addToChannel($channel->users->pluck('id')->toArray(), $channel->id);
        $this->assertTrue(false);
    } catch (\Exception $exception) {
        $this->assertTrue(true);
    }
});

it('checking _addToUser() returns array with status', function () {
    $channel = ChatChannelFactory::new()->create();
    $users = UserFactory::new()->times(10)->create();

    $returned = $this->obj->addToChannel($users->pluck('id')->toArray(), $channel->id);

    $this->assertGreaterThan(0, count($returned));
    $this->assertEquals(true, is_array($returned));
});

it('checking _addToMessage really add user to all messages in ChatChannel', function () {
    $message = ChatMessageFactory::new()->create();
    $users = UserFactory::new()->times(10)->create();

    $beforeIds = $message->users->pluck('user_id');

    $this->obj->addToMessages($users->pluck('id')->toArray(), $message->channel->id);

    $message->refresh();
    $afterIds = $message->users()->pluck('user_id');

    $this->assertGreaterThan($beforeIds->count(), $afterIds->count());
});

it ('checking _addToMessage throw Exception when nothing changed', function () {
    $message = ChatMessageFactory::new()->create();

    try {
        $this->obj->addToMessages($message->users->pluck('id')->toArray(), $message->channel->id);
        $this->assertTrue(false);
    } catch (\Exception $exception) {
        $this->assertTrue(true);
    }
});

it ('checking _addToMessage sets all messages from past as read', function () {
    $message = ChatMessageFactory::new()->create();
    $users = UserFactory::new()->times(10)->create();

    $beforeCount = $message->users->pluck('read');

    $this->obj->addToMessages($users->pluck('id')->toArray(), $message->channel->id);

    $message->refresh();
    $afterCount = $message->users()->pluck('read');

    $this->assertGreaterThan($beforeCount->count(), $afterCount->count());
});

it ('checking _addToMessage returns complex array with statuses', function () {
    $message = ChatMessageFactory::new()->create();
    $users = UserFactory::new()->times(10)->create();

    $returned = $this->obj->addToMessages($users->pluck('id')->toArray(), $message->channel->id);

    $this->assertGreaterThan(0, count($returned));
    $this->assertEquals(true, is_array($returned));
});
