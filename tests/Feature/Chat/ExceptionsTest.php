<?php

use App\Exceptions\Chat\ChannelDeleteException;
use App\Exceptions\Chat\ChannelDeleteInServiceException;
use App\Exceptions\Chat\ChannelNotFoundException;
use App\Exceptions\Chat\ChannelUpdateException;
use App\Exceptions\Chat\ChannelUserAttachingException;
use App\Exceptions\Chat\ChannelUserDetachException;
use App\Exceptions\Chat\HappeningChannelTakeException;
use App\Exceptions\Chat\MessageDeleteException;
use App\Exceptions\Chat\MessagesUsersAttachingException;
use App\Exceptions\Chat\MessageUpdateException;
use App\Exceptions\Chat\TeamChannelTakenException;
use App\Exceptions\Chat\WrongChannelException;
use Tests\Factories\ChatChannelFactory;
use Tests\Factories\ChatMessageFactory;
use Tests\Factories\UserFactory;

it(
    'test ChannelDeleteException',
    function () {
        try {
            throw new ChannelDeleteException(new Exception('Test'));
        } catch (ChannelDeleteException $exception) {
            $resp = $exception->render();
            $this->assertEquals($resp->getStatusCode(), 500);
            $this->assertCount(2, get_object_vars($resp->getData()));
        }
    }
);

it(
    'test ChannelDeleteInServiceException',
    function () {
        try {
            throw new ChannelDeleteInServiceException(new Exception('Test'));
        } catch (ChannelDeleteInServiceException $exception) {
            $resp = $exception->render();
            $this->assertEquals($resp->getStatusCode(), 500);
            $this->assertCount(2, get_object_vars($resp->getData()));
        }
    }
);

it(
    'test ChannelNotFoundException',
    function () {
        try {
            throw new ChannelNotFoundException(new Exception('Test'));
        } catch (ChannelNotFoundException $exception) {
            $resp = $exception->render();
            $this->assertEquals($resp->getStatusCode(), 500);
            $this->assertCount(2, get_object_vars($resp->getData()));
        }
    }
);

it(
    'test ChannelUpdateException',
    function () {
        try {
            throw new ChannelUpdateException([], 'test');
        } catch (ChannelUpdateException $exception) {
            $resp = $exception->render();
            $this->assertEquals($resp->getStatusCode(), 500);
            $this->assertCount(2, get_object_vars($resp->getData()));
        }
    }
);

it(
    'test ChannelUserAttachingException',
    function () {
        try {
            throw new ChannelUserAttachingException([1], 2, 'field');
        } catch (ChannelUserAttachingException $exception) {
            $resp = $exception->render();
            $this->assertEquals($resp->getStatusCode(), 500);
            $this->assertCount(2, get_object_vars($resp->getData()));
        }
    }
);

it(
    'test ChannelUserDetachException',
    function () {
        $chatMessage = ChatMessageFactory::new()->create();
        try {
            throw new ChannelUserDetachException($chatMessage, 'message');
        } catch (ChannelUserDetachException $exception) {
            $resp = $exception->render();
            $this->assertEquals($resp->getStatusCode(), 500);
            $this->assertCount(2, get_object_vars($resp->getData()));
        }
    }
);

it(
    'test HappeningChannelTakeException',
    function () {
        try {
            throw new HappeningChannelTakeException();
        } catch (HappeningChannelTakeException $exception) {
            $resp = $exception->render();
            $this->assertEquals($resp->getStatusCode(), 500);
            $this->assertCount(2, get_object_vars($resp->getData()));
        }
    }
);

it(
    'test MessageDeleteException',
    function () {
        try {
            throw new MessageDeleteException(new Exception('Test'));
        } catch (MessageDeleteException $exception) {
            $resp = $exception->render();
            $this->assertEquals($resp->getStatusCode(), 500);
            $this->assertCount(2, get_object_vars($resp->getData()));
        }
    }
);

it(
    'test MessagesUsersAttachingException',
    function () {
        $message = ChatMessageFactory::new()->create();
        try {
            throw new MessagesUsersAttachingException([1], $message);
        } catch (MessagesUsersAttachingException $exception) {
            $resp = $exception->render();
            $this->assertEquals($resp->getStatusCode(), 500);
            $this->assertCount(2, get_object_vars($resp->getData()));
        }
    }
);

it(
    'test MessageUpdateException',
    function () {
        try {
            throw new MessageUpdateException([], 'hello');
        } catch (MessageUpdateException $exception) {
            $resp = $exception->render();
            $this->assertEquals($resp->getStatusCode(), 500);
            $this->assertCount(2, get_object_vars($resp->getData()));
        }
    }
);

it(
    'test TeamChannelTakenException',
    function () {
        try {
            throw new TeamChannelTakenException();
        } catch (TeamChannelTakenException $exception) {
            $resp = $exception->render();
            $this->assertEquals($resp->getStatusCode(), 200);
            $this->assertCount(2, get_object_vars($resp->getData()));
        }
    }
);

it(
    'test WrongChannelException',
    function () {
        $user = UserFactory::new()->create();
        $channel = ChatChannelFactory::new()->create();

        try {
            throw new WrongChannelException($user, $channel, []);
        } catch (WrongChannelException $exception) {
            $resp = $exception->render();
            $this->assertEquals($resp->getStatusCode(), 500);
            $this->assertCount(2, get_object_vars($resp->getData()));
        }
    }
);