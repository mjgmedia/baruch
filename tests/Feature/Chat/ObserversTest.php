<?php

use Illuminate\Support\Facades\Auth;
use Tests\Factories\ChatChannelFactory;
use Tests\Factories\ChatMessageFactory;
use Tests\Factories\UserFactory;

it('testing ChatMessageObserver adding users from channel', function () {
    $chatMessage = ChatMessageFactory::new()->create();

    $this->assertNotEmpty($chatMessage->users->pluck('id'));
});

it('testing ChatMessageObserver sets user to Auth user', function () {
    $chatMessage = ChatMessageFactory::new()->make();
    $testUser = UserFactory::new()->create();

    Auth::shouldReceive('user')->andReturn($user = $testUser);

    $chatMessage->author()->dissociate();
    $chatMessage->save();

    $this->assertEquals($chatMessage->author_id, $testUser->id);
});

it('testing ChatChannelObserver', function () {
    $chatChannel = ChatChannelFactory::new()->make();
    $testUser = UserFactory::new()->create();

    Auth::shouldReceive('user')->andReturn($user = $testUser);

    $chatChannel->author()->dissociate();
    $chatChannel->save();

    $this->assertEquals($chatChannel->author_id, $testUser->id);
});