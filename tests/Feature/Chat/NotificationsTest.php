<?php

use App\Notifications\Chat\UnreadMessageNotification;
use App\Notifications\Chat\UnreadMessagePushNotification;
use Illuminate\Support\Facades\Notification;
use Tests\Factories\ChatMessageFactory;
use Tests\Factories\UserFactory;

beforeEach(function () {
    $this->message = ChatMessageFactory::new()->create();
    $this->users = UserFactory::new()->times(10)->create();

    Notification::fake();

    // Assert that no notifications were sent...
    Notification::assertNothingSent();
});

it('checking UnreadMessageNotification pushed only to broadcast channel', function () {
    Notification::send($this->users, new UnreadMessageNotification($this->message));

    Notification::assertSentTo(
        $this->users,
        function (UnreadMessageNotification $notification, $channels) {
            return count($channels) === 1 && $channels[0] === 'broadcast';
        }
    );
});

it('checking UnreadMessagePushNotification is pushed only to PushAPI', function () {
    Notification::send($this->users, new UnreadMessagePushNotification($this->message));

    Notification::assertSentTo(
        $this->users,
        function (UnreadMessagePushNotification $notification, $channels) {
            return count($channels) === 1 && $channels[0] === 'NotificationChannels\WebPush\WebPushChannel';
        }
    );
});