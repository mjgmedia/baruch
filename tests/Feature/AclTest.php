<?php

use App\Models\User\User;
use Laravel\Passport\Passport;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

beforeEach(function () {
    $this->user = factory(User::class)->create();
    Passport::actingAs($this->user);
});

test('test check of randomly assigned 2 roles', function() {
    // Getting all roles from DB, randomly take 2 of them, pluck to have only names
    $roles = Role::all()->random(2)->pluck('name');
    // sync those 2 randomly choosen roles to our user
    $this->user->syncRoles($roles);
    // now request
    $response = $this->json(
        'POST',
        route('user.acl.has.all_roles'),
        [
            'roles' => $roles,
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'acl',
                'code',
            ]
        );
});

test('check for 3 randomly assigned permissions', function () {
    // Loading all permissions from DB, randomly selecting 3 of them
    // then selecting only names
    $perms = Permission::all()->random(3)->pluck('name');
    // add selected perms for our user
    foreach ($perms as $perm) {
        $this->user->givePermissionTo($perm);
    }
    // hit request
    $response = $this->json(
        'POST',
        route('user.acl.has.any_permission'),
        [
            'permissions' => $perms
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'acl',
                'code',
            ]
        );
});

test('check for any role', function () {
    $response = $this->json(
        'POST',
        route('user.acl.has.any_role'),
        [
            'roles' => Role::all()->pluck('name'),
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'acl',
                'code',
            ]
        );
});

test('get direct permissions', function() {
    $response = $this->json('GET', route('user.acl.get.direct_permissions'));

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'acl',
                'code',
            ]
        );
});

test('get permissions', function () {
    $response = $this->json('GET', route('user.acl.get.permissions'));

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'acl',
                'code',
            ]
        );
});

test('get permissions via roles', function () {
    $response = $this->json('GET', route('user.acl.get.permissions_via_roles'));

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'acl',
                'code'
            ]
        );
});

test ('check user has permissions', function () {
    $perm = Permission::inRandomOrder()->first();
    $response = $this->json(
        'POST',
        route('user.acl.has.permission_to'),
        [
            'permission' => $perm->name,
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'acl',
                'code'
            ]
        );
});

test('get all roles', function () {
    $response = $this->json('GET', route('user.acl.get.roles'));

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'acl',
                'code'
            ]
        );
});

test('check if user has role', function () {
    $role = Role::inRandomOrder()->first();
    $response = $this->json(
        'POST',
        route('user.acl.has.role'),
        [
            'role' => $role->name,
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'acl',
                'code'
            ]
        );
});