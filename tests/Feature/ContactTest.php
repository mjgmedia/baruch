<?php

test('contact form - correctly fill and send', function () {
    $response = $this->json(
        'POST',
        route('contact'),
        [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'subject' => $this->faker->sentence(6),
            'message' => $this->faker->paragraph(6),
        ]
    );

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'message',
                'code',
            ]
        );
});

test('contact form - incorrectly fill and send', function () {
    $response = $this->json(
        'POST',
        route('contact'),
        [
            'name' => $this->faker->name,
            'subject' => $this->faker->sentence(6),
            'message' => $this->faker->paragraph(6),
        ]
    );

//    $response->dump();

    $response
        ->assertStatus(422)
        ->assertJsonStructure(
            [
                'message',
                'errors',
            ]
        );
});