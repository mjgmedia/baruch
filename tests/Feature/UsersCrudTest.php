<?php

use App\Models\User\User;
use Laravel\Passport\Passport;

beforeEach(function () {
    $this->user = factory(User::class)->create();
    Passport::actingAs($this->user);
});

test('create', function () {
    $this->user->assignRole('admin');
    $response = $this->json(
        'POST',
        route('admin.users.crud.create'),
        [
            'name' => $this->faker->name,
            'username' => $this->faker->unique()->userName,
            'email' => $this->faker->unique()->email,
            'password' => 'password',
            'password_confirmation' => 'password',
            'roles' => 'user',
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(201)
        ->assertJsonStructure(
            [
                'data',
                'code'
            ]
        );
});

test('update', function() {
    $this->user->assignRole('admin');
    $response = $this->json(
        'POST',
        route('admin.users.crud.update', ['user' => $this->user->id]),
        [
            'name' => $this->faker->name,
            'username' => $this->faker->unique()->userName,
            'email' => $this->faker->unique()->email,
            'password' => 'password',
            'password_confirmation' => 'password',
            'roles' => 'user',
            'active' => false,
        ]
    );

//         $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'message',
                'code',
            ]
        );
});

test('index', function () {
    $this->user->assignRole('admin');
    $response = $this->json('GET', route('admin.users.crud.index'));

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'data',
                'links',
                'meta',
            ]
        );
});

test('toggle', function () {
    $this->user->assignRole('admin');
    $response = $this->json(
        'POST',
        route('admin.users.crud.toggle_status'),
        [
            'user_id' => $this->user->id
        ]
    );

//         $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'status',
                'code'
            ]
        );
});

test('delete', function () {
    $this->user->assignRole('admin');
    $response = $this->json(
        'POST',
        route('admin.users.crud.delete'),
        [
            'items' => [
                $this->user->id,
            ],
        ]
    );

//         $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'count',
                'code'
            ]
        );
});