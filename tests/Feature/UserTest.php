<?php

use App\Models\User\User;
use Laravel\Passport\Passport;

beforeEach(function () {
    $this->user = factory(User::class)->create();
    Passport::actingAs($this->user);
});

test('check user auth token', function () {
    $response = $this->json('POST', route('auth.check'));

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'authenticated',
                'code'
            ]
        );
});

test('test logout', function () {
    $response = $this->json('POST', route('auth.logout'));

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'message',
                'code',
            ]
        );
});

test('get user\'s permissions', function () {
    $response = $this->json('GET', route('auth.permissions'));

//        $response->dump();

    $response->assertStatus(200);
});

test('get user\'s roles', function () {
    $response = $this->json('GET', route('auth.roles'));

//         $response->dump();

    $response->assertStatus(200);
});