<?php

use App\Models\User\User;
use Laravel\Passport\Passport;

beforeEach(function () {
    $this->user = factory(User::class)->create();
    Passport::actingAs($this->user);
});

test('fetch all settings', function() {
    $response = $this->json('GET', route('user.settings.fetch'));

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'group',
                'rest',
                'code',
            ]
        );
});

test('get particular setting', function() {
    $response = $this->json(
        'GET',
        route(
            'user.settings.get',
            [
                'setting' => 'push_notifications'
            ]
        )
    );

//         $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'data' => [
                    'key',
                    'value',
                    'name',
                ],
            ]
        );
});

test('reset settings', function() {
    $response = $this->json(
        'GET',
        route('user.settings.reset')
    );

    //$response->dump();

    $response->assertStatus(200)
        ->assertJsonStructure(
            [
                'status',
                'message',
                'code'
            ]
        );
});

test('update some settings (correct data)', function () {
    $arrGroup = [];
    $arrRest = [];

    foreach (config('project.settings') as $key => $value) {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $arrGroup[$key]['values'][$k]['key'] = $k;
                $arrGroup[$key]['values'][$k]['value'] = $v;
            }
        } else {
            $arrRest[$key]['key'] = $key;
            $arrRest[$key]['value'] = $value;
        }
    }

    $response = $this->json(
        'POST',
        route('user.settings.update'),
        [
            'settings' => [
                'group' => $arrGroup,
                'rest' => $arrRest,
            ]
        ]
    );

//         $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'status',
                'code'
            ]
        );
});

test('update some settings (wrong data)', function () {
    $arrGroup = [];
    $arrRest = [];

    foreach (config('project.settings') as $key => $value) {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $arrGroup[$key]['values'][$k]['key'] = $k.'_fake';
                $arrGroup[$key]['values'][$k]['value'] = $v;
            }
        } else {
            $arrRest[$key]['key'] = $key.'_fakeed';
            $arrRest[$key]['value'] = $value;
        }
    }

    $response = $this->json(
        'POST',
        route('user.settings.update'),
        [
            'settings' => [
                'group' => $arrGroup,
                'rest' => $arrRest,
            ]
        ]
    );

    $response->dump();

    $response
        ->assertStatus(404)
        ->assertJsonStructure(
            [
                'message',
                'code'
            ]
        );
});