<?php

use App\Models\User\User;
use Laravel\Passport\Passport;

beforeEach(function () {
    $this->user = factory(User::class)->create();
    Passport::actingAs($this->user);
});

test('find by email', function() {
    $this->user->assignRole('admin');
    $response = $this->json('POST', route('admin.users.find.email'), ['email' => $this->user->email]);

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'data',
            ]
        );
});

test('find by email (wrong one)', function () {
    $this->user->assignRole('admin');
    $response = $this->json('POST', route('admin.users.find.email'), ['email' => $this->faker->unique()->email]);

    $response->dump();

    $response
        ->assertStatus(404)
        ->assertJsonStructure(
            [
                'message',
                'code'
            ]
        );
});

test('find by username', function () {
    $this->user->assignRole('admin');
    $response = $this->json('POST', route('admin.users.find.username'), ['username' => $this->user->username]);

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'data',
            ]
        );
});

test('find by username (wrong one)', function () {
    $this->user->assignRole('admin');
    $response = $this->json(
        'POST',
        route('admin.users.find.username'),
        [
            'username' => $this->faker->unique()
                ->userName
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(404)
        ->assertJsonStructure(
            [
                'message',
                'code'
            ]
        );
});