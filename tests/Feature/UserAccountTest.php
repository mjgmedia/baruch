<?php

use App\Models\User\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;

beforeEach(function () {
    $this->user = factory(User::class)->create();
    Passport::actingAs($this->user);
});

test('update profile without password', function() {
    $response = $this->json(
        'POST',
        route('user.account.update'),
        [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->email,
            'username' => $this->faker->unique()->userName,
            'old_password' => 'password',
        ]
    );

//         $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'data' => [
                    'id',
                    'name',
                    'username',
                    'photo_url',
                    'active',
                    'profile',
                    'roles',
                    'permissions',
                    'can',
                ],
                'message',
                'code'
            ]
        );
});

test('update profile with password change', function() {
    $newPass = $this->faker->name . $this->faker->numberBetween(1111, 9999);
    $response = $this->json(
        'POST',
        route('user.account.update'),
        [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->email,
            'username' => $this->faker->unique()->userName,
            'old_password' => 'password',
            'password' => $newPass,
            'password_confirmation' => $newPass,
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'data' => [
                    'id',
                    'name',
                    'username',
                    'photo_url',
                    'active',
                    'profile',
                    'roles',
                    'permissions',
                    'can',
                ],
                'message',
                'code'
            ]
        );
});

test('add avatar to profile (with upload)', function() {
    Storage::fake(config('project.uploads.avatar_disk'));

    $file = UploadedFile::fake()->image('avatar.jpg');

    $response = $this->json(
        'POST',
        route('user.account.upload'),
        [
            'file' => $file,
        ]
    );

//         $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'status',
                'url',
                'code',
            ]
        );
});

test('get user\'s activity stamp', function() {
    $response = $this->json(
        'GET',
        route('user.activity.get')
    );

//         $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'ts',
                'code'
            ]
        );
});

test('send user\'s activity stamp', function() {
    $response = $this->json(
        'GET',
        route('user.activity.update')
    );

//        $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'ts',
                'code'
            ]
        );
});

test('get profile data', function() {
    $response = $this->json('POST', route('user.profile.get'));

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'data' => [
                    'id',
                    'name',
                    'username',
                    'photo_url',
                    'active',
                    'profile',
                    'roles',
                    'permissions',
                    'can',
                ],
            ]
        );
});

test('update profile data (correct)', function () {
    $response = $this->json(
        'POST',
        route('user.profile.update'),
        [
            'phone' => $this->faker->phoneNumber,
            'address' => $this->faker->streetAddress,
            'city' => $this->faker->city,
            'postal_code' => $this->faker->numberBetween(11, 99) . '-' . $this->faker->numberBetween(111, 999),
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(200)
        ->assertJsonStructure(
            [
                'data' => [
                    'id',
                    'name',
                    'username',
                    'photo_url',
                    'active',
                    'profile',
                    'roles',
                    'permissions',
                    'can',
                ],
                'message',
                'code'
            ]
        );
});

test('update profile date (incorrect)', function () {
    $response = $this->json(
        'POST',
        route('user.profile.update'),
        [
            'phone' => $this->faker->name,
            'postal_code' => $this->faker->city,
        ]
    );

    // $response->dump();

    $response
        ->assertStatus(422)
        ->assertJsonStructure(
            [
                'message',
                'errors'
            ]
        );
});