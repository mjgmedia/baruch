#!/usr/bin/env bash

export DOLLAR='$'
if [ $NGINX_SSL == true ]; then
envsubst < /etc/nginx/conf.d/app_ssl.template > /etc/nginx/conf.d/app.conf 
envsubst < /etc/nginx/conf.d/api_ssl.template > /etc/nginx/conf.d/api.conf 
envsubst < /etc/nginx/conf.d/socket_ssl.template > /etc/nginx/conf.d/socket.conf 
else
envsubst < /etc/nginx/conf.d/app.template > /etc/nginx/conf.d/app.conf 
envsubst < /etc/nginx/conf.d/api.template > /etc/nginx/conf.d/api.conf 
envsubst < /etc/nginx/conf.d/socket.template > /etc/nginx/conf.d/socket.conf 
fi

if [ $NGINX_SWOOLE == true ]; then
    if [ $NGINX_SSL == true ]; then
    envsubst < /etc/nginx/conf.d/swoole_ssl.template > /etc/nginx/conf.d/api.conf 
    else
    envsubst < /etc/nginx/conf.d/swoole.template > /etc/nginx/conf.d/api.conf 
    fi
fi

nginx -g "daemon off;"