# Konfiguracja

Konfiguracja projektu jest w dwóch plikach: `docker-compose.yaml` oraz `.env`.

## docker-compose.yaml
Plik ten zawiera konfigurację każdej z usług dostępnych w projekcie (wiecęj o tym w pliku `containers.md` w katalogu z dokumentacją). 

## .env
Plik `.env` zawiera definicje zmiennych środowiskowych które są wykorzystowe przez `docker-compose` i poszczególne kontenery. Największą zaletą takie rozwiązania jest możliwość bardzo szybkiej customizacji projektu edytując wyłącznie jeden plik.

## Budowa pliku

Plik jest podzielony następująco:
* nazwy kontenerów
* ustawienia sieciowe 
* ustawienia projektu
* mysql
* postfix
* nginx
* php-fpm

### Nazwy kontenerów

Nazwy kontenerów są złożone według wzorca - **nazwa_aplikacji_nazwa_kontenera**

```
NGINX_CONTAINER=app_nginx
NUXT_CONTAINER=app_nuxt
MAIL_CONTAINER=app_mail
FPM_CONTAINER=app_fpm
DB_CONTAINER=app_db
REDIS_CONTAINER=app_redis
SUPERVISOR_CONTAINER=app_supervisor
CERTBOT_CONTAINER=app_certbot
SWOOLE_CONTAINER=app_swoole
```

### Ustawienia sieciowe

W tej sekcji możemy ustawić porty pod którymi dostępne będą: aplikacja, API i websocket. Możliwe jest także przełączenie w tryb SSL oraz wskazanie adresu IP który ma mieć dostęp do sieci Dockera.

```
DOCKER_APP_PORT=8808
DOCKER_API_PORT=8809
DOCKER_SOCKET_PORT=8043
NGINX_SSL=false
IP=0.0.0.0
```

### Ustawienia projektu

Możemy zdefiniować swoje ścieżki do katalogu który ma przetrzymywać dane kontenerów które muszą istnieć niezależnie od stanu kontenera (uruchomiony, zatrzymany, usunięty). Są to dane MariaDB, Redisa, Certbota.
Możliwe jest również ustawienie ścieżki do katalogu z logami, do katalogu z aplikacją PWA, kodu API oraz wybranie portów na którym ma działać serwer Nuxt.js, Swoole oraz PHP-FPM. A także włączenie Swoole jako domyślnego realizatora zapytań do API (zamiast php-fpm).

```
API_PATH=./www/api
APP_PATH=./www/app
DATA_DIR=./data
LOG_DIR=./logs
NUXT_PORT=3000
FPM_PORT=8999
SWOOLE_PORT=1215
NGINX_SWOOLE=false
```

### Mysql
Ustawiamy tutaj nazwę bazy danych, użytkownika, hasło oraz hasło roota.

```
MYSQL_ROOT_PASSWORD=secret
MYSQL_DATABASE=db
MYSQL_USER=maria
MYSQL_PASSWORD=secret
```

### Postfix

Postfix wymaga podana domeny z której ma wysyłać maile oraz nazwę użytkownika i hasło. Nazwę użytkownika od hasła należy oddzielić dwukropkiem.

```
maildomain=mail.example.com
smtp_user=user:pwd
```

### Nginx
W ustawieniach nginxa mamy możliwość zdefiniować adresy domenowe dla aplikacji, API i websockets. Oraz ścieżki w kontenerze dla aplikacji i API oraz ściężkę /public (Laravel).

```
NGINX_APP_DOMAIN=rywaldm.pl
NGINX_API_DOMAIN=api.rywaldm.pl
NGINX_SOCKET_DOMAIN=socket.rywaldm.pl

NGINX_APP_PATH=/var/www/app
NGINX_API_PATH=/var/www/api
NGINX_API_PUBLIC=/var/www/api/public
```

### PHP-FPM
Ustawienia PHP-FPM zawierają wyciągniete dane konfiguracyjne z php.ini.

```
PHP_OPCACHE_VALIDATE_TIMESTAMPS=0
PHP_OPCACHE_MAX_ACCELERATED_FILES=10000
PHP_OPCACHE_MEMORY_CONSUMPTION=192
PHP_OPCACHE_MAX_WASTED_PERCENTAGE=10
```

`PHP_OPCACHE_VALIDATE_TIMESTAMPS` i odpowiadający mu parametr w php.ini: `opcache.validate_timestamps` odpowiada za weryfikację trzymanych w opcache plików - czy należy cache odświeżyć lub nie. Ta opcja jest domyślnie wyłączona, ponieważ w trakcie aktualizacji projektu, nie przeładowujemy wszystkich plików tylko nie które - jak wtedy zadziała cache? Jest to niestabilne rozwiązanie. Dlatego domyślnie ta funkcjonalność opcache jest wyłączona, a po deploymencie kodu, należy zrestartować kontener fpm'a, poleceniem: `docker-compose restart php`

`PHP_OPCACHE_MAX_ACCELERATED_FILES` jest związany z parametrem: `opcache.max_accelerated_files` z php.ini. Ta zmienna odpowiada za ilość plików jaka może być przechowywana w cache. Ważne aby wartość ta **była większa** niż ilość plików w projekcie API.

`PHP_OPCACHE_MEMORY_CONSUMPTION` odpowiada wpisowi `opcache.memory_consumption` z konfiguracji zawartej w php.ini. Domyślnie w php.ini wartość ta jest ustawiona na 64MB. Projekt ten zakłada że serwowane API nie jest małe i stąd ustawienie na 192MB. Jeśli wynik pomiarów za pomocą funkcji 
`opcachegetstatus()` pokaże że wartość ta jest za mała lub za duża, wartość tej zmiennej należy odpowiednio dostosować i zrestartować kontener.

`PHP_OPCACHE_MAX_WASTED_PERCENTAGE` to wpis `opcache.max_wasted_percentage` z php.ini. Odpowiada on za



```
PHP_MAX_UPLOAD=40M

PHP_RESTART_TRESHOLD=10
PHP_RESTART_INTERVAL=1m
PHP_CONTROL_TIMEOUT=10s
```

`PHP_RESTART_TRESHOLD` to ilość procesór które musi upaść w ciągu `PHP_RESTART_INTERVAL` aby proces główny (master) FPMa uruchomił się ponownie. Natomiast `PHP_CONTROL_TIMEOUT` to ilość czasu w sekundach które proces (child) ma odczekać zanim wyłączy się po nadaniu sygnału KILL przez proces rodzica (master).

```
PHP_PM_MODE=dynamic
PHP_PM_MAX_CHILDREN = 5
PHP_PM_START_SERVERS = 3
PHP_PM_MIN_SPARE_SERVERS = 2
PHP_PM_MAX_SPARE_SERVERS = 4
PHP_PM_MAX_REQUESTS = 2000
```

Zmienna `PHP_PM_MODE` decyduje o trybie działania process managera w zdefiniowanej puli procesów FPM. 
`PHP_PM_MAX_CHILDREN` to maksymalna ilośc procesów jakie może stworzyć FPM,
`PHP_PM_START_SERVERS` to początkowa ilośc serwerów gdy FPM jest uruchamiany.
Zmienne `PHP_PM_MIN_SPARE_SERVERS` i `PHP_PM_MAX_SPARE_SERVERS` odpowiadają za minimalną i maksymalną ilość procesów które istnieją bez obciążenia.
Natomiast `PHP_PM_MAX_REQUESTS` to ilość zapytań po której proces (child) FPMa powinnien się zrestartować.
