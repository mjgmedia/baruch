# Struktura plików i katalogów

Projekt składa z 5 katalogów:
* data
* docker
* logs
* docs
* www

## /data
Katalog data służy za kontener do przetrzymywania danych które muszą pozostać na dysku nie zależnie od stanu kontenerów (włączone/wyłączone/zniszczone). Są to dane MariaDB, Redis czy Certbota. 
Nazwę tego katalogu można zmienić i wpisać do pliku `.env`.

## /docker
Ten katalog zawiera kolejną zagnieżdzoną strukturę katalogów odpowiadających nazwami, nazwom usług zdefiniowanych w pliku `docker-compose.yaml`. Są tam przechowywane pliki `Dockerfile` do modyfikacji (budowania) obrazów, pliki konfiguracyjne dla usług oraz skrypty startowe.

## /logs
Ten katalog podobnie jak katalog `docker` zawiera strukturę folderów odpowiadająym usługom. W poszczególnych folderach znajdziemy logi każdej z dostępnych usług.

## /docs
Po prostu dokumentacja.

## /www
Tu przechowywane są dwa katalogi w których należy umieścić kod API i aplikacji web która ma być serwowana. Nazwę tego katalogu również można zmienić poprzez zmienne środowiskową (w pliku `.env` również): `NGINX_APP_PATH`, `NGINX_API_PATH` oraz co ważne dla nginx: `NGINX_API_PUBLIC`.