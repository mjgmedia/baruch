# Kontenery

Projekt zawiera łącznie 8 kontenerów odpowiadających odpowiednio za:
* **nginx** - serwer www, reverse proxy
* **mysql** - relacyjna baza danych (MariaDB)
* **redis** - szybki serwer bazodanowy typu klucz/wartość jako cache
* **mail** - serwer Postfix jako serwer SMTP
* **php** - instancja PHP-FPM 
* **supervisor** - instancja PHP-CLI + supervisor, który uruchamia i nadzoruje procesy horizona oraz crona
* **certbot** - odpowiada za tworzenie certyfikatów SSL z Let's Encrypt
* **swoole** - odpowiada za uruchomienie serwera HTTP bazującego na Swoole

## Nazwy kontenerów
Nazwy wszystkich kontenerów są konfigurowalne poprzez zmienne środowiskowe z pliku `.env`.

Wartości domyślne:
```
NGINX_CONTAINER=app_nginx
NUXT_CONTAINER=app_nuxt
MAIL_CONTAINER=app_mail
FPM_CONTAINER=app_fpm
DB_CONTAINER=app_db
REDIS_CONTAINER=app_redis
SUPERVISOR_CONTAINER=app_supervisor
CERTBOT_CONTAINER=app_certbot
SWOOLE_CONTAINER=app_swoole
```

## Porty
Wystawione są tylko trzy porty. Jeden do obsługi API, drugi do obsługi aplikacji w nuxt.js, trzeci natomiast do połączeń websockets.

Porty dostępne z zewnątrz są konfigurowalne za pomocą zmiennych środowiskowych dostępnych w pliku `.env`: `DOCKER_API_PORT` to port dla API a `DOCKER_APP_PORT` to port dla aplikacji web oraz `DOCKER_SOCKET_PORT` jako port dla połączeń websockets.

Gdyby połączenia na tych portach miałybyć szyfrowane należy ustawić wartość `true` dla zmiennej `NGINX_SSL`.

Wewnątrz sieci Dockera, kontenery odpowiadające za php-fpm i nuxt.js mają konfigurowalne porty również w pliku `.env`. `NUXT_PORT` odpowiada za port Nuxt.js. `FPM_PORT` za port używany przez php-fpm. `SWOOLE_PORT` za port z którego korzysta Swoole.
