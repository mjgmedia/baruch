# Certyfikaty SSL

Uzyskanie certyfikatu SSL, należy wykonać z poziomu urządzenia gospodarza (host). Ponieważ projekt został przygotowany do działania jako usługa typu serwer, zapewnia jedynie podtrzymywanie życia certyfikatów uzyskanych za pomocą `certbot` z Let's Encrypt. Ze względu na posiadane subdomeny (`api.` i `socket.`) rozsądnie jest wygenerować certyfikat typu **wildcard**.

## Uzyskiwanie certyfikatu typu wildcard
Uruchamiamy polecenie:
```
sudo certbot certonly --manual --agree-tos --preferred-challenges=dns -d *.our.domain
```

Będziemy musieli dodać wpis typu TXT do naszej strefy DNS. Warto narzucić krótki TTL (np. 60s) aby zmiany były widoczne szybko. Przy odnowieniach certyfikatu, nie ma potrzeby aktualizacji wpisu TXT.

## Kopiowanie certyfikatu
Domyślnie certyfikat ląduje w standardowej ścieżce certbota: `/etc/letsencrypt/live/domain_name.com/`. Co więcej są tam tylko simlinki do plików które są w katalogu archive. Trzeba zatem kopiować pliki z włączonym śledzeniem symlinków:
```
sudo cp -r -L /etc/letsencrypt/live/our_domain ./data/certbot/conf/live/our_domain
```

## Włączenie komunikacji SSL
Za wczytanie konfiguracji Nginx dla połączeń szyfrowanych odpowiada zmienna `NGINX_SSL` w pliku `.env.`.